# [v2.4.7](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.6...v2.4.7) (2024-01-05)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Enhancements
- **comp**: add new component divider_s. (!40)(f39586e9)
- **comp**: add new component metadata_inserter. (!40)(f39586e9)
- **comp**: add new component message_length_adjuster. (!40)(f39586e9)
- **comp**: add new component polyphase_interpolator_xs. (!40)(f39586e9)
- **comp**: add new component polyphase_decimator_xs. (!40)(f39586e9)
- **tests**: exclude tests with too high utilization for picoevb and plutosdr. (!37)(bbb74733)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!39)(a1dbe693)

# [v2.4.6](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.3...v2.4.4) (2023-01-22)

Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

### New Features
- **comp**: add ander_s component. (!31)(bab03eb4)
- **comp**: add large_pattern_detector_b component. (!31)(bab03eb4)
- **comp**: add zero_adder_s component. (!31)(bab03eb4)
- **comp**: add sink component. (!31)(bab03eb4)
- **comp**: add symbol_mapper_b_s component. (!31)(bab03eb4)
- **comp**: add cic_decimator_xs component. (!31)(bab03eb4)
- **comp**: add poly_phase_synchronizer_c component. (!31)(bab03eb4)
- **comp**: add poly_phase_synchronizer_s component. (!31)(bab03eb4)
- **comp**: add poly_phase_synchronizer_l component. (!31)(bab03eb4)
- **comp**: add poly_phase_synchronizer_xc component. (!31)(bab03eb4)
- **comp**: add poly_phase_synchronizer_xs component. (!31)(bab03eb4)

# [v2.4.3](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Enhancements
- **comp**: add new windower components. (!13)(d9f4d6d9)
- **doc**: add component testing guidelines. (!25)(c6616da4)

### Bug Fixes
- **comp**: fix switching for multiplexers. (!24)(2b73d607)
- **doc**: fix ocpidoc errors. (!22)(61457172)
- **tools**: fix overflow error in fir_filter_scaled by switching from 32 to 64 bit fir_core. (!21)(6a59dbc6)

# [v2.4.2](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.1...v2.4.2) (2022-05-26)

No Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

# [v2.4.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.0...v2.4.1) (2022-03-16)

No Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

# [v2.4.0](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.4...v2.4.0) (2022-01-25)

Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

### Enhancements
- **comp**: add new triggered sampler components. (!12)(266ef809)

### Bug Fixes
- **comp**: change "OcpiOsDebugApi.hh" to "OcpiDebugApi.hh" in triggered_sampler sources. (!16)(f370961f)
- **tests**: Fix ocpi_testing.verifier import error for register components. (!15)(a0b9d1d9)

# [v2.3.4](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.0...v2.3.1) (2021-10-13)

Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

### Bug Fixes
- **comp**: enable `prbs_synchroniser_b_c` HDL worker to take input data when its output is unconnected. (!11)(f16b2fb9)

# [v2.3.0](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

### Enhancements
- **devops**: allow CI pipelines to be launched from within project. (!9)(bca5176c)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/ab21dcec...v2.3.0-rc.1) (2021-08-25)

Initial release of contributed "Software Defined Radio Components" project for OpenCPI
