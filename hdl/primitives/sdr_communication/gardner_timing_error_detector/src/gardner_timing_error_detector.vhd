-- HDL Implementation of a Gardner timing error detector.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Gardner timing error detector
-- This primitive implements a timing error detector with a common set of ports
-- in order to allow the swapping out of detectors
-- The error detector is designed to work at 2 samples per symbol, where the
-- samples are approaching +/-One (peak) and zero.

-- Settings and Caveats
--   resync can be used to set the TED to a known point (peak for Gardner)
--   Runs of positive or negative values are ignored from the output
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gardner_timing_error_detector is
  generic (
    data_width_g : positive := 16
    );
  port (
    clk          : in  std_logic;
    reset        : in  std_logic;
    resync       : in  std_logic;
    input_valid  : in  std_logic;
    input_data   : in  std_logic_vector(data_width_g - 1 downto 0);
    output_valid : out std_logic;
    output_data  : out std_logic_vector(data_width_g - 1 downto 0)
    );

end gardner_timing_error_detector;

architecture rtl of gardner_timing_error_detector is
  type samples_t is array(0 to 3) of signed(input_data'range);

  constant max_input_c : signed(output_data'range) := '0' & to_signed(-1, data_width_g-1);

  signal calculate_sample : std_logic;
  signal output_valid_r   : std_logic_vector(2 downto 0);
  signal input_valid_r    : std_logic_vector(3 downto 0);
  signal pre_add          : signed(data_width_g downto 0);
  signal gardner_data     : signed(2*data_width_g downto 0);
  signal samples          : samples_t;
  signal output_data_i    : signed(output_data'range);
  signal output_sample    : std_logic;

begin

  -- Input samples added to the data buffer
  shift_reg_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        samples          <= (others => (others => '0'));
        calculate_sample <= '0';
      else
        -- Slide the shift register
        if input_valid = '1' then
          samples(0)                 <= signed(input_data);
          samples(1 to samples'high) <= samples(0 to samples'high-1);
          calculate_sample           <= not calculate_sample;
        end if;
        -- Set the next sample as a zero crossing
        if resync = '1' then
          calculate_sample <= '1';
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Calculate the Gardner
  -- Also check for a zero crossing (3 way xor)
  -- output valid is only high for a single clk cycle
  ------------------------------------------------------------------------------
  gardner_p : process(clk)
    variable a, b, c : std_logic;
  begin
    if rising_edge(clk) then
      if reset = '1' then
        input_valid_r  <= (others => '0');
        output_valid_r <= (others => '0');
        output_sample  <= '0';
        output_valid   <= '0';
      else
        input_valid_r <= input_valid_r(input_valid_r'high -1 downto 0) & input_valid;
        output_valid  <= '0';

        if input_valid_r(0) = '1'then
          output_valid_r(0)                            <= calculate_sample;
          output_valid_r(output_valid_r'high downto 1) <= output_valid_r(output_valid_r'high -1 downto 0);
        end if;

        -- Actual gardner calculation
        -- Change gradients to drop onto every second null
        if input_valid_r(0) = '1' then
          pre_add <= resize(samples(0), pre_add'length) - resize(samples(2), pre_add'length);
        end if;

        if input_valid_r(1) = '1' then
          gardner_data <= pre_add * samples(1);
        end if;

        if input_valid_r(2) = '1' then
          output_data_i <= resize(shift_right(gardner_data, data_width_g), output_data_i'length);
          if (abs(samples(0)) > abs(samples(1))
              and abs(samples(2)) > abs(samples(3))) then
            output_data_i <= max_input_c - resize(shift_right(gardner_data, data_width_g), output_data_i'length);
          end if;

          -- XOR of the three sign bits (ensuring that there is a crossing
          -- point)
          a             := samples(0)(input_data'high);
          b             := samples(1)(input_data'high);
          c             := samples(2)(input_data'high);
          output_sample <= not ((a and b and c) or ((not a) and (not b) and (not c)));
        end if;

        -- Output register
        if input_valid_r(input_valid_r'high) = '1' then
          output_data  <= std_logic_vector(output_data_i);
          output_valid <= output_sample and output_valid_r(output_valid_r'high);
        end if;
      end if;
    end if;
  end process;

end rtl;
