-- HDL Implementation of a serial CRC generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Serial Cyclic Redundancy Check (CRC) Generator
-- Generates a CRC result for a serial data input stream. It has a one
-- clock cycle latency.
-- polynomial_g sets the polynomial
-- output_reflect_g enables / disables output crc reflection
-- clk_en clocks in input data on a rising clock edge when high
-- data_in is the parallel input data
-- seed sets the initial value
-- final_xor sets the final xor value
-- crc_out is the crc result
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity crc_serial is
  generic (
    polynomial_g     : std_logic_vector := X"8005";
    output_reflect_g : std_logic        := '0'
    );
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    clk_en    : in  std_logic;
    data_in   : in  std_logic;
    seed      : in  std_logic_vector(polynomial_g'length - 1 downto 0);
    final_xor : in  std_logic_vector(polynomial_g'length - 1 downto 0);
    crc_out   : out std_logic_vector(polynomial_g'length - 1 downto 0)
    );
end crc_serial;

architecture rtl of crc_serial is

  signal data        : std_logic_vector(polynomial_g'length - 1 downto 1);
  signal lfsr_q_high : std_logic_vector(polynomial_g'length - 1 downto 1);
  signal lfsr_c      : std_logic_vector(polynomial_g'length - 1 downto 0);
  signal lfsr_q      : std_logic_vector(polynomial_g'length - 1 downto 0);
  signal xor_calc    : std_logic_vector(polynomial_g'length - 1 downto 0);
  signal crc_result  : std_logic_vector(polynomial_g'length - 1 downto 0);

begin

  -- Assign data and feedback signals
  data_g : for i in 1 to polynomial_g'length - 1 generate
    data(i)        <= data_in;
    lfsr_q_high(i) <= lfsr_q(polynomial_g'high);
  end generate data_g;

  -- XOR division
  lfsr_c(0) <= data_in xor lfsr_q(polynomial_g'length - 1);
  lfsr_c(polynomial_g'length - 1 downto 1) <=
    xor_calc(polynomial_g'length - 1 downto 1);
  xor_calc <= lfsr_q(polynomial_g'length - 2 downto 0) & '0' xor
              ((data & '0' xor lfsr_q_high & '0') and polynomial_g);

  -- Generate crc
  lfsr_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        lfsr_q <= seed;
      else
        if clk_en = '1' then
          lfsr_q <= lfsr_c;
        end if;
      end if;
    end if;
  end process;

  -- Reflect output
  reflect_output_g : for i in 0 to polynomial_g'length - 1 generate
    crc_result(i) <= lfsr_q(i) when output_reflect_g = '0' else
                     lfsr_q(polynomial_g'length - 1 - i);
  end generate;

  -- Final xor
  crc_out <= crc_result xor final_xor;

end rtl;
