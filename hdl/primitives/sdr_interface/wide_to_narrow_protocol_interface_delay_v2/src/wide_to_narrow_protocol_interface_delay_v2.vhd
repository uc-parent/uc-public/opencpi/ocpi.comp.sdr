-- Wide to narrow protocol interface delay primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wide_to_narrow_protocol_interface_delay_v2 is
  generic (
    delay_g                 : natural          := 1;
    data_in_width_g         : positive         := 16;  -- Must be a multiple of
                                                       -- DATA_OUT_WIDTH_G.
    data_out_width_g        : positive         := 8;
    opcode_width_g          : positive         := 3;
    byte_enable_width_g     : positive         := 1;
    processed_data_opcode_g : std_logic_vector := "000"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- Advances the delay line.
    take_in             : in  std_logic := '1';  -- Qualifies _valid and _ready.
    input_hold_out      : out std_logic;  -- Pause data processing and input
                                          -- when high.
    processed_stream_in : in  std_logic_vector(data_out_width_g - 1 downto 0);
    -- Input interface signals
    input_som           : in  std_logic := '0';
    input_eom           : in  std_logic;
    input_eof           : in  std_logic := '0';
    input_valid         : in  std_logic;
    input_ready         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode        : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data          : in  std_logic_vector(data_in_width_g - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_eof          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode       : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data         : out std_logic_vector(data_out_width_g - 1 downto 0)
    );
end wide_to_narrow_protocol_interface_delay_v2;

architecture rtl of wide_to_narrow_protocol_interface_delay_v2 is

  constant outputs_per_input_c : integer := data_in_width_g / data_out_width_g;

  -- Determine the length of the arrays used for the delay
  -- This is just MAX(1,delay_g).
  -- When delay_g=zero, no delay is added, but we still need an array element.
  function calc_delay_array_length return positive is
  begin
    if delay_g < 1 then
      return 1;
    else
      return delay_g;
    end if;
  end function;
  constant delay_array_length_c : positive := calc_delay_array_length;

  type state_t is (passthrough_message_s, split_message_s);

  type byte_enable_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(byte_enable_width_g - 1 downto 0);
  type opcode_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(opcode_width_g - 1 downto 0);
  type data_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(data_in_width_g - 1 downto 0);

  -- Interface delay registers
  signal input_register_take        : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_som         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_eom         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_eof         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_valid       : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_byte_enable : byte_enable_array_t;
  signal input_register_opcode      : opcode_array_t;
  signal input_register_data        : data_array_t;

  -- Interface delay registers
  signal shifted_data : std_logic_vector(data_in_width_g - data_out_width_g - 1 downto 0);
  signal eom          : std_logic;
  signal opcode       : std_logic_vector(opcode_width_g - 1 downto 0);

  -- Control signals
  signal input_hold                : std_logic;
  signal stream_data_message_valid : std_logic;
  signal other_data_message_valid  : std_logic;
  signal output_word_counter       : unsigned(2 downto 0);
  signal current_state             : state_t;

begin

  -- High when valid sample data is detected
  stream_data_message_valid <= '1' when
                               input_register_valid(input_register_valid'high) = '1' and
                               input_register_opcode(input_register_opcode'high) = processed_data_opcode_g
                               else '0';
  -- High when valid non-sample data is detected
  other_data_message_valid <= '1' when
                              input_register_valid(input_register_valid'high) = '1' and
                              input_register_opcode(input_register_opcode'high) /= processed_data_opcode_g
                              else '0';

  -- Input hold is high when an input word is received that needs to be split
  -- over multiple output words, therefore backpressure must be asserted.
  input_hold     <= '1' when current_state = split_message_s else '0';
  input_hold_out <= input_hold;

  -- Add delay to align data with respective flow control signals
  delay_pipeline_0_gen : if delay_g = 0 generate
    input_register_take(0)        <= input_ready and take_in;
    input_register_valid(0)       <= input_valid and take_in;
    input_register_som(0)         <= input_som;
    input_register_eom(0)         <= input_eom;
    input_register_eof(0)         <= input_eof;
    input_register_byte_enable(0) <= input_byte_enable;
    input_register_opcode(0)      <= input_opcode;
    input_register_data(0)        <= input_data;
  end generate;

  delay_pipeline_1_gen : if delay_g = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take  <= (others => '0');
          input_register_valid <= (others => '0');
        elsif(enable = '1' and input_hold = '0') then
          input_register_take(0)  <= input_ready and take_in;
          input_register_valid(0) <= input_valid and take_in;
        end if;
        -- Other registers don't need to be reset as gated by
        -- input_register_take.
        if(enable = '1' and input_hold = '0') then
          input_register_som(0)         <= input_som;
          input_register_eom(0)         <= input_eom;
          input_register_eof(0)         <= input_eof;
          input_register_byte_enable(0) <= input_byte_enable;
          input_register_opcode(0)      <= input_opcode;
          input_register_data(0)        <= input_data;
        end if;
      end if;
    end process;
  end generate;

  delay_pipeline_2_plus_gen : if delay_g > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take  <= (others => '0');
          input_register_valid <= (others => '0');
        elsif(enable = '1' and input_hold = '0') then
          input_register_take  <= input_register_take(delay_g - 2 downto 0) & (input_ready and take_in);
          input_register_valid <= input_register_valid(delay_g - 2 downto 0) & (input_valid and take_in);
        end if;
        -- Other registers don't need to be reset as gated by
        -- input_register_take.
        if(enable = '1' and input_hold = '0') then
          input_register_som         <= input_register_som(delay_g - 2 downto 0) & input_som;
          input_register_eom         <= input_register_eom(delay_g - 2 downto 0) & input_eom;
          input_register_eof         <= input_register_eof(delay_g - 2 downto 0) & input_eof;
          input_register_byte_enable <= input_register_byte_enable(delay_g - 2 downto 0) & input_byte_enable;
          input_register_opcode      <= input_register_opcode(delay_g - 2 downto 0) & input_opcode;
          input_register_data        <= input_register_data(delay_g - 2 downto 0) & input_data;
        end if;
      end if;
    end process;
  end generate;

  interface_state_machine_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state <= passthrough_message_s;
      elsif (enable = '1') then
        case current_state is
          when passthrough_message_s =>
            if other_data_message_valid = '1' then
              current_state       <= split_message_s;
              shifted_data        <= input_register_data(input_register_data'high)(data_in_width_g - 1 downto data_out_width_g);
              eom                 <= input_register_eom(input_register_eom'high);
              opcode              <= input_register_opcode(input_register_opcode'high);
              output_word_counter <= to_unsigned(outputs_per_input_c - 2, output_word_counter'length);
            end if;
          when split_message_s =>
            -- Shift input data right by the length of the output data interface
            shifted_data        <= std_logic_vector(unsigned(shifted_data) srl data_out_width_g);
            -- Update number of remaining shifts
            output_word_counter <= output_word_counter - 1;
            -- When all output words are sent go back to passthrough state
            if output_word_counter = 0 then
              current_state <= passthrough_message_s;
            end if;
        end case;
      end if;
    end if;
  end process;

  output_mux_p : process(current_state, input_register_valid,
                         input_register_som, input_register_eom, input_register_eof,
                         input_register_byte_enable, input_register_opcode,
                         input_register_data, input_register_take, enable,
                         stream_data_message_valid, processed_stream_in,
                         output_word_counter, opcode, eom, shifted_data,
                         other_data_message_valid)
  begin
    if (current_state = passthrough_message_s) then
      -- In passthrough condition, pass all signals through directly
      output_give  <= input_register_take(input_register_take'high);
      output_valid <= input_register_valid(input_register_valid'high);
      output_som   <= input_register_som(input_register_som'high);
      if other_data_message_valid = '0' then
        output_eom <= input_register_eom(input_register_eom'high);
      else
        -- Suppress EOM when valid non-sample data as it will be sent over
        -- several messages with the EOM sent at the end.
        output_eom <= '0';
      end if;
      output_eof         <= input_register_eof(input_register_eof'high);
      output_byte_enable <= input_register_byte_enable(input_register_byte_enable'high);
      output_opcode      <= input_register_opcode(input_register_opcode'high);
      if stream_data_message_valid = '1' then
        -- When the delayed interface opcode is the sample opcode and the data
        -- is valid, output the processed sample data rather than the delayed
        -- data.
        output_data <= std_logic_vector(processed_stream_in);
      else
        -- Output the DATA_OUT_WIDTH_G LSBs of the delayed input data
        output_data <= input_register_data(input_register_data'high)(data_out_width_g - 1 downto 0);
      end if;
    else
      -- Otherwise output remaining part of input message
      output_give        <= '1';
      -- Message is always valid as only valid messages will cause this state
      -- to be entered
      output_valid       <= '1';
      output_byte_enable <= (others => '1');
      -- Continue outputting the stored opcode
      output_opcode      <= opcode;
      -- SOM always 0 as SOM was already sent with 1st word of data interface
      output_som         <= '0';
      -- If EOM flag was set on the input interface then output it on the last
      -- word of the output interface.
      if output_word_counter = 0 then
        output_eom <= eom;
      else
        output_eom <= '0';
      end if;
      -- Only forward eof once we have finished
      output_eof  <= '0';
      -- Output the DATA_OUT_WIDTH_G LSBs of the shifted input data
      output_data <= shifted_data(data_out_width_g - 1 downto 0);
    end if;
  end process;

end rtl;
