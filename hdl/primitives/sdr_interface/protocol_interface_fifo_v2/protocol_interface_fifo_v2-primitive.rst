.. protocol_interface_fifo_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _protocol_interface_fifo_v2-primitive:


Protocol Interface FIFO for V2 HDL Interface (``protocol_interface_fifo_v2``)
=============================================================================
Delays OpenCPI interface signals to align them with a processed data signal, making use of a FIFO structure for messages.

Design
------
When data processing is performed on a stream of input data it typically results in a valid output one or more clock cycles after the input. This primitive delays OpenCPI interface signals in order to align them with a processed data stream. When the input opcode is equal to ``processed_data_opcode_g`` the processed data stream is output instead of the delayed input data.

Implementation
--------------
A FIFO of depth ``fifo_depth_g`` is constructed, this stores the opcode of each incoming message. If the opcode matches ``processed_data_opcode_g``, then the data is ignored, otherwise the data is also stored in a FIFO.

``input_som`` and ``input_ready`` being asserted together is used to mark the start of a message, while ``input_eom`` is used to mark the end of a message. When the current opcode is equal to ``processed_data_opcode_g``, the messages are passed to the output and the end of message signal is controlled by ``processed_end_in``. If the ``message_length_counter`` reaches the maximum value controlled by the ``max_message_length_g`` then end of message signal will also be forced high.

If the output interface is not ready to accept data then the FIFO is not advanced, and ``take_out`` and ``processed_ready_out`` are driven low.

Interface
---------

Generics
~~~~~~~~

 * ``enforce_msg_boundary_g`` (``boolean``): Sets whether EOM markers on ``processed_data_opcode_g`` is adhered to, or if ``max_message_length_g`` should only be used.

 * ``data_in_width_g`` (``positive``): Sets the width of the input interface data signal.

 * ``data_out_width_g`` (``positive``): Sets the width of the output interface data signal.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

 * ``fifo_depth_g`` (``positive``): Sets the depth of the internal FIFO for opcodes other than ``processed_data_opcode_g``.

 * ``processed_data_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for which the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal.

 * ``metadata_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for metadata messages.

 * ``max_message_length_g`` (``integer``): Sets the maximum message length allowed upon the output port.

 * ``force_eof_timeout_g`` (``natural``): forces EOF to be passed through after the defined number of clock cycles where ``enable`` is high. Set to 0 to disable time-out.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. When high, the delay pipeline is advanced. Therefore, data is absorbed on ``input` and ``output`` changes when ``enable`` is high.

 * ``drop_last_message`` (``std_logic``), in: Remove the previous message from the FIFO. Qualified by ``enable``.

 * ``take_in`` (``std_logic``), in: Qualifies when any down stream IP is accepting data. Optional - can be left unconnected or driven high when not used. This only needs to be used when ``enable`` can be high while the input is not changing.

 * ``take_out`` (``std_logic``), out: Qualifies ``input_valid`` and ``input_ready``. High when primitive can take incoming data.

 * ``drop_message`` (``std_logic``), in: Removes the next message to be transmitted to be dropped (Head of FIFO). Qualified by ``enable``

 * ``fifo_full_error`` (``std_logic``), out: Asserted if either the Opcode or Data FIFO become full.

 * ``processed_stream_in`` (``std_logic_vector``, ``data_width_g`` bits), in: The processed data stream. I.e. the data this primitive is aligning the interface signals with. Qualified by ``processed_valid_in`` and ``processed_ready_out``.

 * ``processed_valid_in`` (``std_logic``), in: Valid signal from the down-stream IP. Qualifies ``processed_stream_in``.

 * ``processed_end_in`` (``std_logic``), in: End marker signal from the down-steam IP. Optional - can be left unconnected if ``output_eom`` is not required for opcode ``processed_data_opcode_g``, or if ``enforce_msg_boundary_g`` is ``false``.

 * ``processed_ready_out`` (``std_logic``), out: Ready signal to the down-stream IP. Qualifies ``processed_stream_in``.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Ready signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``data_width_g`` bits), in: Data signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``data_width_g`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``protocol_interface_fifo_v2`` are:

 * None.
