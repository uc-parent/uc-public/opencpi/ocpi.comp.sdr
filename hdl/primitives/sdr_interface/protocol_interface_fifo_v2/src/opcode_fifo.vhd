-- Opcode FIFO primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity opcode_fifo is
  generic (
    enforce_msg_boundary_g  : boolean          := true;
    depth_g                 : positive         := 8;
    opcode_width_g          : positive         := 3;
    processed_data_opcode_g : std_logic_vector := "000"
    );
  port (
    clk       : in  std_logic;
    reset     : in  std_logic;
    enable    : in  std_logic := '1';   -- Advances the delay line.
    drop_last : in  std_logic;
    write     : in  std_logic;
    take      : in  std_logic;
    empty     : out std_logic;
    full      : out std_logic;

    input_byte_enable : in std_logic;
    input_opcode      : in std_logic_vector(opcode_width_g - 1 downto 0);
    input_eof         : in std_logic;

    output_byte_enable : out std_logic;
    output_opcode      : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_eof         : out std_logic;

    last_opcode      : out std_logic_vector(opcode_width_g - 1 downto 0);
    last_byte_enable : out std_logic
    );
end opcode_fifo;

architecture rtl of opcode_fifo is

  type opcode_array_t is array (depth_g-1 downto 0) of std_logic_vector(opcode_width_g downto 0);
  signal opcode_fifo : opcode_array_t := (others => (others => '1'));

  constant counter_width_c    : integer := integer(ceil(log2(real(depth_g))));
  signal opcode_depth         : unsigned(counter_width_c-1 downto 0);
  signal empty_i              : std_logic;
  signal full_i               : std_logic;
  signal allowed_input        : std_logic;
  signal output_byte_enable_i : std_logic;
  signal output_opcode_i      : std_logic_vector(opcode_width_g - 1 downto 0);
begin

  enforce_msg_boundary_gen : if enforce_msg_boundary_g = true generate
    allowed_input <= '1';
  end generate;

  not_enforce_msg_boundary_gen : if enforce_msg_boundary_g = false generate
    allowed_input <= '0' when (opcode_fifo(0)(input_opcode'range) = processed_data_opcode_g and
                               input_opcode = processed_data_opcode_g)
                     else '1';
  end generate;

  sync_fifo_p : process(clk)
    variable opcode_depth_v : unsigned(opcode_depth'range);
  begin
    if rising_edge(clk) then
      if reset = '1' then
        opcode_depth <= (others => '0');
        empty_i      <= '1';
        full_i       <= '0';
      elsif enable = '1' then
        opcode_depth_v := opcode_depth;

        -- This might be able remove the drop_last once block-based ordering is
        -- agreed on.
        if take = '1' and empty_i = '0' then
          opcode_depth_v := opcode_depth_v - 1;
        end if;

        if drop_last = '1' and empty_i = '0' then
          opcode_depth_v := opcode_depth_v - 1;
          if write = '0' then
            for i in 1 to depth_g-1 loop
              opcode_fifo(i-1) <= opcode_fifo(i);
            end loop;
          end if;
        end if;

        -- If there is an EOF, then just fill the FIFO, as there is no new
        -- messages coming until the worker is reset.
        if full_i = '0' and (write = '1' or input_eof = '1') and allowed_input = '1' then
          if drop_last = '0' and (write = '1' or input_eof = '1') then
            for i in 1 to depth_g-1 loop
              opcode_fifo(i) <= opcode_fifo(i-1);
            end loop;
          end if;
          if input_eof = '1' then
            -- processed_data_opcode_g always has a byte enable set,
            -- so this is used to mean EOF
            opcode_fifo(0) <= '0' & processed_data_opcode_g;
          else
            opcode_fifo(0) <= input_byte_enable & input_opcode;
          end if;
          opcode_depth_v := opcode_depth_v + 1;
        end if;

        if opcode_depth_v = 0 then
          empty_i <= '1';
        else
          empty_i <= '0';
        end if;

        if opcode_depth_v = depth_g-1 then
          full_i <= '1';
        else
          full_i <= '0';
        end if;

        opcode_depth <= opcode_depth_v;
      end if;
    end if;
  end process;

  -- The top bit holds the byte_enable, while the rest of the word is the
  -- opcode.
  output_opcode_i <= opcode_fifo(to_integer(opcode_depth-1))(output_opcode'range) when empty_i = '0'
                     else processed_data_opcode_g;
  output_byte_enable_i <= opcode_fifo(to_integer(opcode_depth-1))(opcode_width_g) when empty_i = '0'
                          else '1';

  output_eof         <= '1' when output_opcode_i = processed_data_opcode_g and output_byte_enable_i = '0' else '0';
  output_opcode      <= output_opcode_i;
  output_byte_enable <= output_byte_enable_i;

  last_opcode      <= opcode_fifo(0)(last_opcode'range);
  last_byte_enable <= opcode_fifo(0)(opcode_width_g);

  empty <= empty_i;
  full  <= full_i;

end rtl;
