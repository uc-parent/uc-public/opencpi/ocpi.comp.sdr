-- Protocol interface FIFO primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity protocol_interface_fifo_v2 is
  generic (
    enforce_msg_boundary_g  : boolean          := true;
    data_in_width_g         : positive         := 8;
    data_out_width_g        : positive         := 8;
    opcode_width_g          : positive         := 3;
    byte_enable_width_g     : positive         := 1;
    fifo_depth_g            : positive         := 8;
    processed_data_opcode_g : std_logic_vector := "000";
    metadata_opcode_g       : std_logic_vector := "101";
    max_message_length_g    : integer          := 4096;
    force_eof_timeout_g     : natural          := 0
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- Acts as a clk enable
    drop_last_message   : in  std_logic := '0';  -- Remove the previous msg started
    take_in             : in  std_logic := '1';  -- Take signal coming from IP
    take_out            : out std_logic;  -- Take signal going to input port
    drop_message        : in  std_logic := '0';  -- Remove the next value within the FIFO
    fifo_full_error     : out std_logic;
    -- IP Output data stream
    processed_stream_in : in  std_logic_vector(data_out_width_g - 1 downto 0);
    processed_valid_in  : in  std_logic;
    processed_end_in    : in  std_logic;
    -- Extra flag telling IP to pause processing (as output is busy with
    -- non-sample opcodes)
    processed_ready_out : out std_logic;
    -- Input interface signals
    input_som           : in  std_logic := '0';
    input_eom           : in  std_logic;
    input_eof           : in  std_logic := '0';
    input_valid         : in  std_logic;
    input_ready         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode        : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data          : in  std_logic_vector(data_in_width_g - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_eof          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode       : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data         : out std_logic_vector(data_out_width_g - 1 downto 0)
    );
end protocol_interface_fifo_v2;

architecture rtl of protocol_interface_fifo_v2 is

  function valid_reg_length(input_data_width, output_data_width : positive) return natural is
  begin
    if input_data_width < output_data_width then
      return output_data_width / input_data_width;
    else
      return input_data_width / output_data_width;
    end if;
  end function;

  type data_array_t is array ((3*fifo_depth_g)-1 downto 0) of std_logic_vector(data_out_width_g - 1 downto 0);

  type state_t is (sample, other);
  signal state : state_t;

  signal take            : std_logic;
  signal msg_in_progress : std_logic;

  signal input_valid_i : std_logic;
  signal input_data_i  : std_logic_vector(data_out_width_g - 1 downto 0);

  signal data_fifo_write : std_logic;
  signal data_fifo_take  : std_logic;
  signal data_fifo_empty : std_logic;
  signal data_fifo_full  : std_logic;
  signal data_fifo       : data_array_t;
  signal data_depth      : integer range 0 to data_array_t'length;
  signal data_fifo_out   : std_logic_vector(data_out_width_g - 1 downto 0);

  signal next_opcode       : std_logic_vector(opcode_width_g-1 downto 0);
  signal next_byte_en      : std_logic;
  signal opcode_fifo_take  : std_logic;
  signal opcode_fifo_write : std_logic;
  signal opcode_fifo_full  : std_logic;

  constant message_counter_size_c         : integer := integer(ceil(log2(real(max_message_length_g))));
  constant time_opcode_sample_count_c     : integer := integer(96/data_out_width_g);
  constant metadata_opcode_sample_count_c : integer := integer(128/data_out_width_g);
  signal message_length_counter           : unsigned(message_counter_size_c - 1 downto 0);

  signal data_eom  : std_logic;
  signal other_eom : std_logic;

  signal delay_byte_enable : std_logic_vector(byte_enable_width_g - 1 downto 0);
  signal output_som_i      : std_logic;
  signal output_eom_i      : std_logic;
  signal output_eof_sent   : std_logic;
  signal output_eof_i      : std_logic;
  signal output_give_i     : std_logic;
  signal output_valid_i    : std_logic;

  signal last_opcode      : std_logic_vector(opcode_width_g - 1 downto 0);
  signal last_byte_enable : std_logic;

begin

  take <= take_in;

  assert (take = '1' and data_fifo_full = '0') or take = '0'
    report "Data FIFO overflowed, data will be lost.";

  take_out            <= take;
  processed_ready_out <= '0' when state = other else '1';

  --------
  -- Opcode handling
  --------
  opcode_fifo_i : entity work.opcode_fifo
    generic map(
      enforce_msg_boundary_g  => enforce_msg_boundary_g,
      depth_g                 => fifo_depth_g,
      opcode_width_g          => opcode_width_g,
      processed_data_opcode_g => processed_data_opcode_g
      )
    port map (
      clk       => clk,
      reset     => reset,
      enable    => enable,
      drop_last => drop_last_message,
      write     => opcode_fifo_write,
      take      => opcode_fifo_take,
      full      => opcode_fifo_full,

      input_byte_enable => input_byte_enable(0),
      input_opcode      => input_opcode,
      input_eof         => input_eof,

      output_byte_enable => next_byte_en,
      output_opcode      => next_opcode,
      output_eof         => output_eof_sent,

      last_opcode      => last_opcode,
      last_byte_enable => last_byte_enable
      );

  opcode_fifo_write <= input_ready and input_som and take;
  opcode_fifo_take  <= drop_message or (processed_end_in and (output_give_i or output_valid_i)) when state = sample else
                       drop_message or (output_eom_i and (output_give_i or output_valid_i));
  delay_byte_enable <= (others => next_byte_en);

  enforce_eof_gen : if force_eof_timeout_g > 0 generate
    signal eof_timeout : integer range 0 to (force_eof_timeout_g-1);
  begin
    partial_input_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          output_eof_i <= '0';
        elsif enable = '1' then
          output_eof_i <= '0';
          if input_eof = '1' then
            eof_timeout <= eof_timeout + 1;
          else
            eof_timeout <= 0;
          end if;

          if output_eof_sent = '1' or eof_timeout = force_eof_timeout_g - 1 then
            eof_timeout  <= force_eof_timeout_g - 1;
            output_eof_i <= '1';
          end if;
        end if;
      end if;
    end process;
  end generate;
  no_enforce_eof_gen : if force_eof_timeout_g = 0 generate
    output_eof_i <= output_eof_sent;
  end generate;

  --------
  -- Data (non-sample) handling
  --------
  same_width_gen : if data_in_width_g = data_out_width_g generate
    input_data_i  <= input_data;
    input_valid_i <= input_valid;
  end generate;
  smaller_input_width_gen : if data_in_width_g < data_out_width_g generate
    signal input_valid_r : std_logic_vector(valid_reg_length(data_in_width_g, data_out_width_g) - 2 downto 0);
    signal input_data_r  : std_logic_vector((data_out_width_g - data_in_width_g)-1 downto 0);
  begin
    partial_input_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_valid_r <= (others => '0');
        elsif enable = '1' then
          input_data_r <= input_data & input_data_r(input_data_r'high downto data_in_width_g);
          if input_valid_r(input_valid_r'high) = '1' or input_eom = '1' then
            input_valid_r <= (others => '0');
          else
            input_valid_r <= input_valid_r(input_valid_r'high-1 downto 0) & input_valid;
          end if;
        end if;
      end if;
    end process;
    input_data_i  <= input_data & input_data_r;
    input_valid_i <= input_valid_r(input_valid_r'high) and input_valid;
  end generate;

  data_fifo_write <= input_valid_i and input_ready and take when input_opcode /= processed_data_opcode_g and data_fifo_full = '0'                        else '0';
  data_fifo_take  <= '1'                                    when data_fifo_empty = '0' and next_byte_en = '1' and next_opcode /= processed_data_opcode_g else '0';

  data_fifo_p : process(clk)
    variable data_depth_v : integer range 0 to data_array_t'length;
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data_depth      <= 0;
        data_fifo_empty <= '1';
        data_fifo_full  <= '0';
      elsif enable = '1' then
        data_depth_v := data_depth;

        -- If taking, then just remove an entry
        if data_fifo_take = '1' and data_fifo_empty = '0'
          and next_opcode /= processed_data_opcode_g and next_byte_en = '1' then
          data_depth_v := data_depth_v - 1;
        end if;
        -- If writing then shift FIFO entries up by an entry
        if data_fifo_full = '0' and data_fifo_write = '1' then
          for i in 1 to data_fifo'length-1 loop
            data_fifo(i) <= data_fifo(i-1);
          end loop;
        end if;
        -- If dropping a whole message, then remove 3 entries, and shift the
        -- FIFO back by 3 as well.
        if drop_last_message = '1' and data_depth >= 3
          and last_opcode /= processed_data_opcode_g and last_byte_enable = '1' then
          data_depth_v := data_depth_v - 3;
          if last_opcode = metadata_opcode_g then
            data_depth_v := data_depth_v - 1;
          end if;
          for i in 3 to data_fifo'length-1 loop
            data_fifo(i-3) <= data_fifo(i);
          end loop;
        end if;
        -- If writing then insert the new entry into the bottom of the FIFO.
        if data_fifo_full = '0' and data_fifo_write = '1' then
          data_depth_v := data_depth_v + 1;
          data_fifo(0) <= input_data_i;
        end if;

        if data_depth_v = 0 then
          data_fifo_empty <= '1';
        else
          data_fifo_empty <= '0';
        end if;

        if data_depth_v >= data_fifo'length then
          data_fifo_full <= '1';
        else
          data_fifo_full <= '0';
        end if;

        data_depth <= data_depth_v;
      end if;
    end if;
  end process;

  data_fifo_out <= data_fifo(data_depth-1) when data_fifo_empty = '0'
                   else input_data_i;

  fifo_full_error <= data_fifo_full or opcode_fifo_full;

  --------
  -- Output and mode control
  --------
  state <= other when next_opcode /= processed_data_opcode_g or output_eof_i = '1' else sample;

  -- Controls the output messages
  interface_delay_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        msg_in_progress <= '0';

      elsif enable = '1' then
        -- Force SOM to be false when ever a message is in progress.
        output_som_i <= '1';
        if output_som_i = '1' and (output_give_i = '1' or output_valid_i = '1') then
          msg_in_progress <= '1';
          output_som_i    <= '0';
        end if;
        if msg_in_progress = '1' then
          output_som_i <= '0';
        end if;
        if output_eom_i = '1' and (output_give_i = '1' or output_valid_i = '1') then
          msg_in_progress <= '0';
          output_som_i    <= '1';
        end if;
      end if;
    end if;
  end process;

  -- Count to make sure within max_message_size incase data opcode didn't
  -- include EOM
  message_counter_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        message_length_counter <= (others => '0');
      else
        if enable = '1' and output_valid_i = '1' then
          if output_eom_i = '1' or drop_message = '1' then
            message_length_counter <= (others => '0');
          else
            message_length_counter <= message_length_counter + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  data_eom <= '1' when message_length_counter = (max_message_length_g-1)
              else processed_end_in;

  other_eom <= '1' when (next_byte_en = '0') else
               '1' when (next_byte_en = '1' and next_opcode /= metadata_opcode_g and message_length_counter = time_opcode_sample_count_c-1) else
               '1' when (next_byte_en = '1' and next_opcode = metadata_opcode_g and message_length_counter = metadata_opcode_sample_count_c-1) else
               '0';

  output_eom_i <= data_eom when state = sample else
                  other_eom;

  output_data <= processed_stream_in when state = sample else
                 data_fifo_out;

  output_valid_i <= processed_valid_in when state = sample and output_eof_i = '0' else
                    '1' when next_byte_en = '1' and data_fifo_empty = '0' else
                    '0';

  output_give_i <= processed_valid_in when state = sample else
                   '1' when output_eof_i = '0' and next_byte_en = '0' else
                   '0';

  output_byte_enable <= (others => '1') when state = sample else
                        delay_byte_enable;

  output_opcode <= processed_data_opcode_g when state = sample else
                   next_opcode;

  output_eof   <= output_eof_i;
  output_eom   <= output_eom_i;
  output_som   <= output_som_i;
  output_give  <= output_give_i;
  output_valid <= output_valid_i;

end rtl;
