-- Narrow to wide protocol interface delay primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity narrow_to_wide_protocol_interface_delay_v2 is
  generic (
    delay_g                 : natural          := 1;
    data_in_width_g         : positive         := 8;
    data_out_width_g        : positive         := 16;  -- Must be a multiple of
                                                       -- DATA_in_WIDTH_G.
    opcode_width_g          : positive         := 3;
    byte_enable_width_g     : positive         := 1;
    processed_data_opcode_g : std_logic_vector := "000"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;         -- Advances the delay line.
    take_in             : in  std_logic := '1';  -- Qualifies _valid and _ready.
    processed_stream_in : in  std_logic_vector(data_out_width_g - 1 downto 0);
    -- Input interface signals
    input_som           : in  std_logic := '0';
    input_eom           : in  std_logic;
    input_eof           : in  std_logic := '0';
    input_valid         : in  std_logic;
    input_ready         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode        : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data          : in  std_logic_vector(data_in_width_g - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_eof          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode       : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data         : out std_logic_vector(data_out_width_g - 1 downto 0)
    );
end narrow_to_wide_protocol_interface_delay_v2;

architecture rtl of narrow_to_wide_protocol_interface_delay_v2 is

  constant inputs_per_output_c : integer := data_out_width_g / data_in_width_g;

  -- Size of the counter used to keep track of the input words
  -- It counts from inputs_per_output_c-1 downto 0
  constant counter_size_c : integer := integer(ceil(log2(real(inputs_per_output_c))));

  -- Determine the length of the arrays used for the delay
  -- This is just MAX(1,delay_g).
  -- When delay_g=zero, no delay is added, but we still need an array element.
  function calc_delay_array_length return positive is
  begin
    if delay_g < 1 then
      return 1;
    else
      return delay_g;
    end if;
  end function;
  constant delay_array_length_c : positive := calc_delay_array_length;

  type byte_enable_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(byte_enable_width_g - 1 downto 0);
  type opcode_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(opcode_width_g - 1 downto 0);
  type data_array_t is array (delay_array_length_c - 1 downto 0) of std_logic_vector(data_in_width_g - 1 downto 0);

  -- Interface delay registers
  signal input_register_take        : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_som         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_eom         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_eof         : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_valid       : std_logic_vector(delay_array_length_c - 1 downto 0);
  signal input_register_byte_enable : byte_enable_array_t;
  signal input_register_opcode      : opcode_array_t;
  signal input_register_data        : data_array_t;

  -- Data input shift register
  signal shifted_data : std_logic_vector(data_out_width_g - data_in_width_g - 1 downto 0);

  -- Control signals
  signal other_data_message_valid : std_logic;
  signal input_word_counter       : unsigned(counter_size_c-1 downto 0);

begin

  -- Add delay to align data with respective flow control signals
  delay_pipeline_0_gen : if delay_g = 0 generate
    input_register_take(0)        <= input_ready and take_in;
    input_register_valid(0)       <= input_valid and take_in;
    input_register_som(0)         <= input_som;
    input_register_eom(0)         <= input_eom;
    input_register_eof(0)         <= input_eof;
    input_register_byte_enable(0) <= input_byte_enable;
    input_register_opcode(0)      <= input_opcode;
    input_register_data(0)        <= input_data;
  end generate;

  delay_pipeline_1_gen : if delay_g = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take  <= (others => '0');
          input_register_valid <= (others => '0');
        elsif(enable = '1') then
          input_register_take(0)  <= input_ready and take_in;
          input_register_valid(0) <= input_valid and take_in;
        end if;
        -- Other registers don't need to be reset as qualified by take and valid
        if(enable = '1') then
          input_register_som(0)         <= input_som;
          input_register_eom(0)         <= input_eom;
          input_register_eof(0)         <= input_eof;
          input_register_byte_enable(0) <= input_byte_enable;
          input_register_opcode(0)      <= input_opcode;
          input_register_data(0)        <= input_data;
        end if;
      end if;
    end process;
  end generate;

  delay_pipeline_2_plus_gen : if delay_g > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take  <= (others => '0');
          input_register_valid <= (others => '0');
        elsif(enable = '1') then
          input_register_take  <= input_register_take(delay_g - 2 downto 0) & (input_ready and take_in);
          input_register_valid <= input_register_valid(delay_g - 2 downto 0) & (input_valid and take_in);
        end if;
        -- Other registers don't need to be reset as qualified by take and valid
        if(enable = '1') then
          input_register_som         <= input_register_som(delay_g - 2 downto 0) & input_som;
          input_register_eom         <= input_register_eom(delay_g - 2 downto 0) & input_eom;
          input_register_eof         <= input_register_eof(delay_g - 2 downto 0) & input_eof;
          input_register_byte_enable <= input_register_byte_enable(delay_g - 2 downto 0) & input_byte_enable;
          input_register_opcode      <= input_register_opcode(delay_g - 2 downto 0) & input_opcode;
          input_register_data        <= input_register_data(delay_g - 2 downto 0) & input_data;
        end if;
      end if;
    end process;
  end generate;

  interface_state_machine_p : process(clk)
  begin
    if rising_edge(clk) then
      -- This shift-register assembles multiple narrow input words
      -- into one wide output word.
      -- It runs for all input message types, but the output is ignored
      -- for samples messages.
      if reset = '1' then
        input_word_counter <= (others => '0');
      elsif enable = '1' and input_register_valid(input_register_valid'high) = '1' then
        -- Got a new input word.

        -- Keep a count of the number of words shifted.
        if input_register_eom(input_register_eom'high) = '1' then
          input_word_counter <= (others => '0');
        elsif input_word_counter = 0 then
          input_word_counter <= to_unsigned(inputs_per_output_c - 1, input_word_counter'length);
        else
          input_word_counter <= input_word_counter - 1;
        end if;
      end if;
      -- There is no need to reset shifted_data
      if enable = '1' and input_register_valid(input_register_valid'high) = '1' then
        -- Data shift register
        shifted_data <= input_register_data(input_register_data'high) & shifted_data(shifted_data'high downto data_in_width_g);
      end if;
    end if;
  end process;

  output_mux_p : process(input_register_valid, input_register_som,
                         input_register_eom, input_register_eof, input_register_byte_enable,
                         input_register_opcode, input_register_take,
                         input_register_data, processed_stream_in,
                         input_word_counter, shifted_data)
  begin

    -- Select external data for samples messages
    -- Forward the shifted value for non-samples messages
    if input_register_opcode(input_register_opcode'high) = processed_data_opcode_g then
      output_data <= processed_stream_in;
    else
      output_data <= input_register_data(input_register_data'high) & shifted_data;
    end if;

    -- Forward the input valid signal for samples messages
    -- or when we have finished shifting all words of non-sample messages
    if input_register_opcode(input_register_opcode'high) = processed_data_opcode_g or input_word_counter = 1 then
      output_valid <= input_register_valid(input_register_valid'high);
    else
      output_valid <= '0';
    end if;

    -- Since V2 interface, give is only required for zero-length messages
    -- There would be no harm in asserting with the valid output, but
    -- the logic would be more complex.
    if input_register_valid(input_register_valid'high) = '0' then
      output_give <= input_register_take(input_register_take'high);
    else
      output_give <= '0';
    end if;

    -- Just forward all other signals directly.
    output_opcode      <= input_register_opcode(input_register_opcode'high);
    output_byte_enable <= input_register_byte_enable(input_register_byte_enable'high);
    output_eom         <= input_register_eom(input_register_eom'high);
    output_som         <= input_register_som(input_register_som'high);
    output_eof         <= input_register_eof(input_register_eof'high);
  end process;

end rtl;
