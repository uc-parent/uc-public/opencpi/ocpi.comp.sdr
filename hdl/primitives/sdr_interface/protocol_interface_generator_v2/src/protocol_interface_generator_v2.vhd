-- Protocol interface delay version 2 primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity protocol_interface_generator_v2 is
  generic (
    delay_g                 : positive         := 1;  -- must not be 0
    data_width_g            : positive         := 8;
    max_message_size_g      : positive         := 16384;
    opcode_width_g          : positive         := 3;
    byte_enable_width_g     : positive         := 1;
    processed_data_opcode_g : std_logic_vector := "000";
    discontinuity_opcode_g  : std_logic_vector := "100"
    );
  port (
    clk                   : in  std_logic;  -- System clk
    reset                 : in  std_logic;  -- System reset
    output_ready          : in  std_logic;  -- High when output is ready
    -- High when discontinuity event occurs
    discontinuity_trigger : in  std_logic;
    generator_enable      : in  std_logic;  -- High when generator is enabled
    generator_reset       : in  std_logic;  -- High when generator is reset
    -- Connect output from data processing module
    processed_stream_in   : in  std_logic_vector(data_width_g - 1 downto 0);
    -- Specifies the length that the output message should be
    message_length        : in  unsigned(15 downto 0);
    -- High when processing module should be disabled.
    output_hold           : out std_logic;
    -- Output interface signals
    output_som            : out std_logic;
    output_eom            : out std_logic;
    output_valid          : out std_logic;
    output_give           : out std_logic;
    output_byte_enable    : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode         : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data           : out std_logic_vector(data_width_g - 1 downto 0)
    );
end protocol_interface_generator_v2;

architecture rtl of protocol_interface_generator_v2 is
  constant ocpi_message_limit : natural := max_message_size_g / (data_width_g / 8);

  -- interface delay registers
  signal valid_store           : std_logic_vector(delay_g - 1 downto 0);
  signal discontinuity_store   : std_logic_vector(delay_g - 1 downto 0);
  signal discontinuity_store_r : std_logic_vector(delay_g - 1 downto 0);
  signal discontinuity_event   : std_logic;
  signal discontinuity_capture : std_logic;
  signal generator_reset_r     : std_logic;
  signal generator_enable_r    : std_logic;
  signal discontinuity_r       : std_logic;
  -- Message signal
  signal message_limit         : unsigned(message_length'range);
  signal message_counter       : unsigned(message_length'range);
  -- State machine signals
  type state_t is (idle_s, passthrough_message_s, send_eom_s, send_discontinuity_s);
  signal current_state         : state_t;

begin

  -- Holds discontinuity until next valid output cycle, as discontinuity
  -- signal may not be aligned with output_ready
  discontinuity_capture_p : process(clk)
  begin
    if rising_edge(clk) then
      -- Only extend discontinuity pulse if it occurs at a time where it
      -- would not be captured.
      if reset = '1' or generator_reset = '1' or
        (output_ready = '1' and generator_enable = '1' and
         (current_state = idle_s or current_state = passthrough_message_s)) then
        discontinuity_capture <= '0';
      elsif discontinuity_trigger = '1' then
        discontinuity_capture <= '1';
      end if;
    end if;
  end process;

  discontinuity_event <= discontinuity_capture or discontinuity_trigger;

  -- valid_store stops invalid messages from leaving the component
  -- until the specified number of delay cycles have elapsed after a reset.
  -- discontinuity_store delays the discontinuity signal by the specified
  -- number of delay cycles so that it is aligned with the output data.
  gen_delay_1_gen : if DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' or generator_reset = '1' then
          valid_store <= (others => '0');
        elsif (output_ready = '1' and generator_enable = '1' and
               (current_state = idle_s or
                current_state = passthrough_message_s)) then
          valid_store(0) <= '1';
        end if;
      end if;
    end process;
    discontinuity_store(0) <= discontinuity_event;
  end generate;

  gen_delay_2_plus_gen : if DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' or generator_reset = '1' then
          valid_store           <= (others => '0');
          discontinuity_store_r <= (others => '0');
        elsif (output_ready = '1' and generator_enable = '1' and
               (current_state = idle_s or
                current_state = passthrough_message_s)) then
          valid_store           <= valid_store(valid_store'length - 2 downto 0) & '1';
          discontinuity_store_r <= discontinuity_store;
        end if;
      end if;
    end process;
    discontinuity_store <= discontinuity_store_r(discontinuity_store'length - 2 downto 0) & discontinuity_event;
  end generate;

  message_limit <= message_length when message_length < ocpi_message_limit
                   else to_unsigned(ocpi_message_limit, message_limit'length);

  -- Counts the length of messages so the SOM and EOM can be correctly inserted.
  message_length_counter_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' or generator_reset = '1' or generator_enable = '0' then
        -- Reset message counter whenever a system or generator reset occurs,
        -- or when the generator is disabled, as the message will be finished
        -- early.
        message_counter <= (others => '0');
      else
        -- Counter advances whenever valid data is output from the system.
        if output_ready = '1' and
          (current_state = idle_s or current_state = passthrough_message_s) and
          valid_store(valid_store'high) = '1' then
          -- Counter wraps when a full message is sent or a discontinuity
          -- occurs, which forces the message to end early.
          if message_counter = (message_limit - 1) or discontinuity_store(discontinuity_store'high) = '1' then
            message_counter <= (others => '0');
          else
            message_counter <= message_counter + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  -- The generator component should be disabled while in the send_eom or
  -- send_discontinuity states as the output of the generator component
  -- is not passed through in these states.
  output_hold <= '1' when current_state = send_eom_s or current_state = send_discontinuity_s else '0';

  interface_state_machine_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state <= idle_s;
      else
        case current_state is
          -- Wait in idle state until the generator component is ready to
          -- send valid data.
          when idle_s =>
            if generator_enable = '1' and generator_reset = '0' and valid_store(valid_store'high) = '1' then
              current_state <= passthrough_message_s;
            end if;
          -- In passthrough state the generator component will output
          -- whenever output_ready is high.
          when passthrough_message_s =>
            -- Store reset / enable / discontinuity so that later states know
            -- what path to take through state machine.
            generator_reset_r <= generator_reset;
            discontinuity_r   <= discontinuity_store(discontinuity_store'high);
            -- Check for reset, disable, or discontinuity conditions
            if generator_reset = '1' then
              -- If already on last word of a message, or between end of
              -- message and start of new one.
              if message_counter = 0 then
                -- No EOM required
                current_state <= send_discontinuity_s;
              else
                -- Send EOM followed by a discontinuity
                current_state <= send_eom_s;
              end if;
            elsif generator_enable = '0' then
              -- If already on last word of a message, or between end of message
              -- and start of new one.
              if message_counter = 0 then
                -- No EOM required, straight to idle state
                current_state <= idle_s;
              else
                -- Send EOM, then go to idle
                current_state <= send_eom_s;
              end if;
            elsif discontinuity_store(discontinuity_store'high) = '1' then
              -- If already on last word of a message
              if message_counter = message_length - 1 then
                -- No EOM required, send discontinuity
                current_state <= send_discontinuity_s;
              else
                -- End current message, then send discontinuity
                current_state <= send_eom_s;
              end if;
            -- Else, keep passing through data
            end if;

          -- End a message with a late EOM signal
          when send_eom_s =>
            if output_ready = '1' then
              if generator_reset_r = '1' or discontinuity_r = '1' then
                -- If reset or discontinuity then send discontinuity ZLM
                current_state <= send_discontinuity_s;
              else
                -- If enable is low, then just go back to idle state and
                -- don't send a discontinuity.
                current_state <= idle_s;
              end if;
            end if;
          -- Send discontinuity ZLM
          when send_discontinuity_s =>
            if output_ready = '1' then
              if generator_reset_r = '1' then
                -- Go to idle and wait until out of reset
                current_state <= idle_s;
              else
                -- Carry on sending data
                current_state <= passthrough_message_s;
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

  output_mux_p : process(current_state,
                         processed_stream_in, message_counter, message_length,
                         generator_enable, generator_reset)
  begin
    -- Output gated by valid and byte_enable signal, so no need for a mux

    if current_state = send_discontinuity_s then
      output_opcode <= discontinuity_opcode_g;
    else
      output_opcode <= processed_data_opcode_g;
    end if;

    if message_length = 1 or current_state = send_discontinuity_s then
      output_som <= '1';
      output_eom <= '1';
    elsif message_counter = message_length - 1 or current_state = send_eom_s then
      output_som <= '0';
      output_eom <= '1';
    elsif message_counter = 0 then
      output_som <= '1';
      output_eom <= '0';
    else
      output_som <= '0';
      output_eom <= '0';
    end if;
  end process;

  output_data        <= processed_stream_in;
  output_byte_enable <= (others => '1');

  output_valid <= valid_store(valid_store'high) when generator_enable = '1' and generator_reset = '0'
                  and not (current_state = send_discontinuity_s or current_state = send_eom_s)
                  else '0';
  output_give <= '1' when generator_enable = '1' and generator_reset = '0'
                 and (current_state = send_discontinuity_s or current_state = send_eom_s)
                 else '0';

end rtl;
