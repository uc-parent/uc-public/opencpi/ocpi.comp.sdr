.. metadata_inserter_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _metadata_inserter_v2-primitive:


Metadata Inserter for V2 HDL Interface (``metadata_inserter_v2``)
=================================================================
Inserts a metadata message into the stream of data when prompted to do so via a trigger signal.

Design
------
Inserts a metadata message, with configurable id and value, when the ``trigger`` signal is asserted.

Implementation
--------------
When ``trigger`` is asserted, back pressure is applied to the input interface. A metadata message with id/value set to ``metadata_id`` and ``metadata_value`` is output. Input back pressure is then removed and the normal data flow resumes.

The primitive adds no delay to normal interface messages.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g`` (``positive``): Sets the width of the interface data signal.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

 * ``metadata_opcode_g`` (``natural``): Sets the value of the metadata opcode.

 * ``data_opcode_g`` (``natural``): Sets the value of the data opcode.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``metadata_id`` (``std_logic_vector``, 32 bits), in: ID of the metadata message that should be inserted.

 * ``metadata_value`` (``std_logic_vector``, 64 bits), in: Value of the metadata message that should be inserted.

 * ``trigger`` (``std_logic``), in: High when a metadata message should be outputted.

 * ``message_complete`` (``std_logic``), out: Set for 1 clock when metadata message has been sent

 * ``take_in`` (``std_logic``), in: High when data is taken from the input interface.

 * ``take_out`` (``std_logic``), out: High when primitive can take incoming data.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``data_width_g`` bits), in: Data signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Ready signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: Output of SOM signal from input interface with metadata inserted.

 * ``output_eom`` (``std_logic``), out: Output of EOM signal from input interface with metadata inserted.

 * ``output_eof`` (``std_logic``), out: Output of EOF signal from input interface with metadata inserted.

 * ``output_valid`` (``std_logic``), out: Output of valid signal from input interface with metadata inserted.

 * ``output_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), out: Output of byte enable signal from input interface with metadata inserted.

 * ``output_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), out: Output of opcode signal from input interface with metadata inserted.

 * ``output_data`` (``std_logic_vector``, ``data_width_g`` bits), out: Output of data signal from input interface with metadata inserted.

 * ``output_give`` (``std_logic``), out: Output of ready signal from input interface with metadata inserted.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``metadata_inserter_v2`` are:

 * Triggering multiple metadata messages when the output is applying back pressure will result in not all messages being sent.
