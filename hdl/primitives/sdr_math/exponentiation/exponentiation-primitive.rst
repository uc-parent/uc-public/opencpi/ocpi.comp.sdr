.. exponentiation documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: exp


.. _exponentiation-primitive:


Exponentiation (``exponentiation``)
===================================
Pipelined exponentiation calculator.

Design
------
Exponentiation is the mathematical operation of raising one quantity to the power of another.

The mathematical representation of the implementation is given in :eq:`exponentiation-equation`.

.. math::
   :label: exponentiation-equation

   y[n] = b^{x[n]}

In :eq:`exponentiation-equation`:

 * :math:`y[n]` is the output values.

 * :math:`x[n]` is the input values, known as the exponent or power.

 * :math:`b` is the base.

The relationship between an exponentiation and a logarithm is given in :eq:`exponentiation_logarithm-equation`. Plots of three commonly used bases for exponentiations are shown in :numref:`exponentiation_plots-diagram`.

.. math::
   :label: exponentiation_logarithm-equation

   b^y = x

   log_b(x) = y

.. _exponentiation_plots-diagram:

.. figure:: exponentiation_curves.svg
   :alt: Common exponentiation plots.
   :align: center

   Exponentiation plots for bases :math:`2`, :math:`e` and :math:`10`.

Implementation
--------------
This pipelined implementation avoids the use of multiplication and division operations. Instead it employs shifting and addition / subtraction to approximate an exponentiation. It is derived from the C implementation provided at https://quinapalus.com/efunc.html.

The base of the exponentiation is set using the generic ``base_g``. A log table of power of two logarithm values is generated from ``base_g`` at build time and used throughout the implementation to avoid complex mathematical operations. Base values less than one are not supported.

The input and output bit widths are set using the generics ``input_size_g`` and ``output_size_g``. The value assigned to ``output_size_g`` determines the size of the build time log table, which is equal to :math:`\log_2{( \texttt{output_size_g} )} + 7`.

``data_in`` and ``data_out`` are both *unsigned* and 16-bit fixed point values.

The code below shows the equivalent C implementation of the algorithm for a base value of :math:`e`. The literal values assigned to ``y`` in the sample C implementation below come from the log table generated at build time.

In the C implementation ``y`` is initialised in line 3 to :math:`2^{16}` (``0x10000``). This is the lowest value ``y`` will ever be since all subsequent operations increase its value closer to the resultant exponentiation value. As ``y`` iteratively converges on the resultant value, ``x`` is reduced as close to zero as possible without being less than zero. Each time ``x`` is reduced in value, ``y`` is correspondingly increased, such that the closer in value ``x`` is to zero the closer ``y`` is to the exponentiation result.

For each subtraction of a log table value from ``x``, ``y`` is appropriately increased in value. The relationship is such that if :math:`k` is subtracted from ``x``, ``y`` is multiplied by :math:`e^k`. The log table values are calculated so every multiplication to ``y`` is no more computationally intensive than a shift or addition operation. For example, multiplying by :math:`2^n` is achieved by a shift, and multiplication by :math:`\pm 2^n \pm 1` is achieved by a shift and addition. Lines 4 to 8 below show multiplication by :math:`2^n` values and lines 9 to 24 below show multiplication by :math:`2^{-n} + 1`.

.. code-block:: C
   :linenos:

   int64_t fxexp(int64_t x) {
     int64_t t,y;
     y = 0x10000;                                  // 2^16
     t = x - 0xB1721; if(x >= 0) x = t, y <<= 16;  // log_e(2^16) * 2^16
     t = x - 0x58B91; if(x >= 0) x = t, y <<= 8;   // log_e(2^8) * 2^16
     t = x - 0x2C5C8; if(x >= 0) x = t, y <<= 4;   // log_e(2^4) * 2^16
     t = x - 0x162E4; if(x >= 0) x = t, y <<= 2;   // log_e(2^2) * 2^16
     t = x - 0x0B172; if(x >= 0) x = t, y <<= 1;   // log_e(2^1) * 2^16
     t = x - 0x067CD; if(x >= 0) x = t, y += y>>1; // log_e(1+2^(-1)) * 2^16
     t = x - 0x03920; if(x >= 0) x = t, y += y>>2; // log_e(1+2^(-2)) * 2^16
     t = x - 0x01E27; if(x >= 0) x = t, y += y>>3; // log_e(1+2^(-3)) * 2^16
     t = x - 0x00F85; if(x >= 0) x = t, y += y>>4; // log_e(1+2^(-4)) * 2^16
     t = x - 0x007E1; if(x >= 0) x = t, y += y>>5; // log_e(1+2^(-5)) * 2^16
     t = x - 0x003F8; if(x >= 0) x = t, y += y>>6; // log_e(1+2^(-6)) * 2^16
     t = x - 0x001FE; if(x >= 0) x = t, y += y>>7; // log_e(1+2^(-7)) * 2^16
     if(x & 0x100) y += y>>8;
     if(x & 0x080) y += y>>9;
     if(x & 0x040) y += y>>10;
     if(x & 0x020) y += y>>11;
     if(x & 0x010) y += y>>12;
     if(x & 0x008) y += y>>13;
     if(x & 0x004) y += y>>14;
     if(x & 0x002) y += y>>15;
     if(x & 0x001) y += y>>16;
     return y;
   }

The amount of error introduced by this iterative approximation depends on the residual value in ``x``. Since :math:`e^x` is approximately :math:`1+x` it is possible to correct the final answer by multiplying it by :math:`1+x`. However, this operation requires a general multiplication and the additional accuracy is most likely not worthwhile for hardware implementations.

The clock cycle delay through this pipelined implementation is equal to :math:`23 + 2 \lceil \log_2{( \texttt{output_size_g} )} \rceil` clock cycles. The ``data_valid_out`` output indicates when a result is available.

Interface
---------

Generics
~~~~~~~~

 * ``input_size_g`` (``integer``): Input bit width.

 * ``output_size_g`` (``integer``): Output bit width.

 * ``base_g`` (``real``): Base value of exponentiation.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Enable. If ``clk_en`` is low the module will not operate.

 * ``data_valid_in`` (``std_logic``), in: Valid In. High when data on ``data_in`` port is available.

 * ``data_in`` (``unsigned(input_size_g - 1 downto 0)``), in: 16-bit fixed point input data.

 * ``data_valid_out`` (``std_logic``), out: High when data on ``data_out`` port is available.

 * ``data_out`` (``unsigned(output_size_g - 1 downto 0)``), out: 16-bit fixed point output data.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations in ``exponentiation`` are:

 * Negative input and output values are not supported.

 * Base values less than or equal to one are not supported.
