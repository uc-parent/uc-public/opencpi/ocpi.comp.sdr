-- Non-pipelined version of non-restoring divider.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity non_restoring_divider is
  generic (
    data_width_g : integer := 32;
    method_g     : string  := "truncate"
    );
  port (
    clk            : in  std_logic;
    reset          : in  std_logic;
    clk_en         : in  std_logic;
    data_in_valid  : in  std_logic;
    data_in_ready  : out std_logic;
    numerator      : in  signed(data_width_g - 1 downto 0);
    denominator    : in  signed(data_width_g - 1 downto 0);
    data_valid_out : out std_logic;
    quotient       : out signed(data_width_g - 1 downto 0);
    remainder      : out signed(data_width_g - 1 downto 0)
    );
end non_restoring_divider;

architecture rtl of non_restoring_divider is
  signal running        : std_logic;
  signal d_is_negative  : std_logic;
  signal valid_pipeline : std_logic_vector(data_width_g + 1 downto 0);
  signal remainder_r    : std_logic_vector((2*data_width_g) - 1 downto 0);
  signal denominator_r  : std_logic_vector(data_width_g - 1 downto 0);

  signal r_negative : std_logic;
  signal q_negative : std_logic;

  signal remainder_top : signed(remainder'range);
  signal remainder_low : signed(remainder'range);

begin

  -- Non Restoring divider:
  -- This follows the same principle as a basic long divider.
  -- 1. Load the numerator into the remainder
  -- 2. If the remainder is positive, subtract the denominator
  --    Add 1 to the quotient in this bit position
  -- 3. If the remainder is negative, add the denominator
  --    Add -1 to the quotient in this bit position (mark as 0)
  -- 4. Shift to the next digit position, and repeat the process for the
  --    data_width_g iterations.
  -- 5. At the end, do a 1's compliment subtraction of the quotient to turn
  --    the 0's into -1's in the quotient.
  --
  -- This version uses the remainder_r to hold the remainder and quotient at
  -- the same time (as both are shifted together).

  register_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        valid_pipeline <= std_logic_vector(to_unsigned(1, valid_pipeline'length));
      elsif clk_en = '1' then
        -- Load data
        if valid_pipeline(0) = '1' and data_in_valid = '1' then
          -- Handle negative denominator through swapping sign of the numerator
          -- Record that the sign was swapped, (as this is needed later to
          -- handle output signs)
          if denominator(denominator'high) = '1' then
            d_is_negative <= '1';
            remainder_r   <= std_logic_vector(-(resize(numerator, remainder_r'length)));
            -- sign extend up to the top bit
            denominator_r <= std_logic_vector(-denominator);
          else
            d_is_negative                        <= '0';
            remainder_r(data_width_g-1 downto 0) <= std_logic_vector(numerator);
            for i in remainder_r'high downto data_width_g loop
              remainder_r(i) <= (numerator(numerator'high));
            end loop;
            denominator_r <= std_logic_vector(denominator);
          end if;

          valid_pipeline <= valid_pipeline(valid_pipeline'high-1 downto 0) & valid_pipeline(valid_pipeline'high);
        end if;

        -- running a divider, 1 bit per clock cycle.
        -- The remainder is shifted up towards the top of the word, with the
        -- new quotient bit being inserted into the bottom of the remainder.
        if valid_pipeline(0) = '0' then
          if remainder_r(remainder_r'high) = '0' then
            remainder_r(0)                                    <= '1';
            remainder_r(data_width_g-1 downto 1)              <= remainder_r(data_width_g-2 downto 0);
            remainder_r(remainder_r'high downto data_width_g) <= std_logic_vector(signed(remainder_r(remainder_r'high-1 downto data_width_g-1)) - signed(denominator_r));
          else
            remainder_r(0)                                    <= '0';
            remainder_r(data_width_g-1 downto 1)              <= remainder_r(data_width_g-2 downto 0);
            remainder_r(remainder_r'high downto data_width_g) <= std_logic_vector(signed(remainder_r(remainder_r'high-1 downto data_width_g-1)) + signed(denominator_r));
          end if;

          valid_pipeline <= valid_pipeline(valid_pipeline'high-1 downto 0) & valid_pipeline(valid_pipeline'high);
        end if;
      end if;
    end if;
  end process;

  remainder_top <= signed(remainder_r(remainder_r'high downto data_width_g));
  remainder_low <= signed(remainder_r(data_width_g-1 downto 0));

  r_negative <= remainder_top(remainder_top'high);
  q_negative <= remainder_low(remainder_low'high);

  truncate_gen : if method_g = "truncate" generate
    signal denominator_i : signed(denominator'range);
    signal rounding      : signed(quotient'range);
    signal r_equal_denom : std_logic;
  begin

    r_equal_denom <= '1' when signed(remainder_top) = -signed(denominator_r) else '0';

    round_to_zero_p : process (remainder_top, r_negative, q_negative, r_equal_denom)
    begin
      rounding <= to_signed(0, quotient'length);
      if remainder_top /= 0 then
        if (r_equal_denom = '1' and r_negative = '1') or
          (r_negative = '1' and q_negative = '1') then
          rounding <= to_signed(1, quotient'length);
        elsif (r_equal_denom = '1' and r_negative = '0') or
          (r_negative = '0' and q_negative = '0') then
          rounding <= to_signed(-1, quotient'length);
        end if;
      end if;
    end process;

    denominator_p : process (remainder_top, r_negative, q_negative, denominator_r, r_equal_denom)
    begin
      denominator_i <= signed(denominator_r);
      if remainder_top /= 0 and r_equal_denom = '0' then
        if r_negative = '0' and q_negative = '0' then
          denominator_i <= -signed(denominator_r);
        elsif (r_negative = '1' and q_negative = '0') or (r_negative = '0' and q_negative = '1') then
          denominator_i <= (others => '0');
        end if;
      end if;
    end process;

    output_p : process(clk)
    begin
      if rising_edge(clk) then
        if clk_en = '1' then
          -- Output formatting.
          -- The output is formatted differently depending upon whether the
          -- remainder is negative, and if the input was negative.
          -- (Quotient is only dependant upon the remainder sign)
          running <= valid_pipeline(valid_pipeline'high);

          remainder <= remainder_top;

          -- Truncate leads to a special case where rounding up from fractions
          -- between -1 and 0.
          if signed(remainder_top) /= 0 then
            if d_is_negative = '1' then
              remainder <= -(remainder_top + denominator_i);
            else
              remainder <= remainder_top + denominator_i;
            end if;
          end if;

          quotient <= remainder_low - (signed(not remainder_low) + rounding);

        end if;
      end if;
    end process;
  end generate;

  floor_gen : if method_g = "floor" generate
    process(clk)
    begin
      if rising_edge(clk) then
        if clk_en = '1' then
          -- Output formatting.
          -- The output is formatted differently depending upon whether the
          -- remainder is negative, and if the input was negative.
          -- (Quotient is only dependant upon the remainder sign)
          running <= valid_pipeline(valid_pipeline'high);

          if valid_pipeline(valid_pipeline'high) = '1' then
            if remainder_r(remainder_r'high) = '1' then
              quotient <= remainder_low - signed(not remainder_low) - 1;
              if d_is_negative = '1' then
                remainder <= -(remainder_top + signed(denominator_r));
              else
                remainder <= remainder_top + signed(denominator_r);
              end if;
            else
              quotient <= remainder_low - signed(not remainder_low);
              if d_is_negative = '1' then
                remainder <= -remainder_top;
              else
                remainder <= remainder_top;
              end if;
            end if;
          end if;
        end if;
      end if;
    end process;
  end generate;

  data_in_ready  <= valid_pipeline(0);
  data_valid_out <= running;

end rtl;
