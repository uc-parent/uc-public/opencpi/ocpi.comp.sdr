# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# This Makefile is for the primitive library: sdr_dsp

# Set this variable to any other primitive libraries that this library depends on.
# If they are remote from this project, use slashes in the name (relative or absolute)
# If they are in this project, they must be compiled first, and this requires that the
# PrimitiveLibraries variable be set in the hdl/primitives/Makefile such that the
# libraries are in dependency order.
Libraries=sdr_util

# Set this variable to the list of source files in dependency order
# If it is not set, all .vhd and .v files will be compiled in wildcard/random order,
# except that any *_pkg.vhd files will be compiled first
#SourceFiles=

# Remember two rules for OpenCPI primitive libraries, in order to be usable with all tools:
# 1. Any entity (VHDL) or module (verilog) must have a VHDL component declaration in sdr_dsp_pkg.vhd
# 2. Entities or modules to be used from outside the library must have the file name
#    be the same as the entity/module name, and one entity/module per file.

SourceFiles+=types/src/fir_tap_ram_select_pkg.vhd
SourceFiles+=sdr_dsp_pkg.vhd
SourceFiles+=rounding_halfup/src/rounding_halfup.vhd
SourceFiles+=rounding_halfeven/src/rounding_halfeven.vhd
SourceFiles+=rounding_truncate/src/rounding_truncate.vhd
SourceFiles+=cordic_rec_to_pol/src/cordic_rec_to_pol.vhd
SourceFiles+=cordic_sin_cos/src/cordic_sin_cos.vhd
SourceFiles+=cordic_dds/src/cordic_dds.vhd
SourceFiles+=multiply_accumulate/src/multiply_accumulate.vhd
SourceFiles+=fir_filter/src/fir_filter.vhd
include fir_filter_symmetric/Makefile
SourceFiles+=cic_dec/src/cic_dec.vhd
SourceFiles+=cic_int/src/cic_int.vhd
SourceFiles+=polyphase_interpolator/src/polyphase_interpolator.vhd
SourceFiles+=polyphase_decimator/src/polyphase_decimator.vhd
SourceFiles+=windower/src/windower.vhd
include fft/fft_pre.mk

include $(OCPI_CDK_DIR)/include/hdl/hdl-library.mk
