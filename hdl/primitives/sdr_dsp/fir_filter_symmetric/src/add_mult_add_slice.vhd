-- HDL Implementation of submodule add_mult_add_slice(Symmetric FIR Filter)
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_mult_add_slice is
  generic (
    number_adder_terms_g : natural range 0 to 3 := 3;
    data_width_g         : natural              := 16;
    accumulate_width_g   : natural              := 32
    );
  port(
    clk          : in  std_logic;
    reset        : in  std_logic;
    enable       : in  std_logic;
    -- Coefficient interface
    coeff_valid  : in  std_logic;
    coeff_in     : in  signed(data_width_g - 1 downto 0);
    coeff_out    : out signed(data_width_g - 1 downto 0);
    -- Input Data
    take_data    : in  std_logic;
    preadd_a_in  : in  signed(data_width_g - 1 downto 0);
    preadd_b_in  : in  signed(data_width_g - 1 downto 0);
    accumulate_b : in  signed(accumulate_width_g - 1 downto 0) := (others => '0');
    accumulate_c : in  signed(accumulate_width_g - 1 downto 0) := (others => '0');
    -- Output Data
    preadd_a_out : out signed(data_width_g - 1 downto 0);
    preadd_b_out : out signed(data_width_g - 1 downto 0);
    output       : out signed(accumulate_width_g - 1 downto 0);
    output_valid : out std_logic
    );
end add_mult_add_slice;

architecture rtl of add_mult_add_slice is

  -- Using a function to allow a generic to affect the value of a constant
  function calc_processing_delay(number_adder_terms : natural range 0 to 3) return positive is
  begin
    -- We need an additional pipe line delay for 3 adder terms
    case number_adder_terms is
      when 0 =>
        return 3;  -- 3 cycle delay for the register, preadder and mult
      when others =>
        return 4;                       -- Additional delay for the post adder
    end case;
  end function;

  constant proc_delay : positive := calc_processing_delay(number_adder_terms_g);

  -- signals for additional delay to the datain
  signal preadd_a_r : signed(data_width_g - 1 downto 0) := (others => '0');
  signal preadd_b   : signed(data_width_g - 1 downto 0) := (others => '0');

  -- signals for MAC
  signal coeff      : signed(data_width_g - 1 downto 0)       := (others => '0');
  signal accumulate : signed(accumulate_width_g - 1 downto 0) := (others => '0');

  signal take_pipe : std_logic_vector(proc_delay - 1 downto 0) := (others => '0');

  signal preadd  : signed(data_width_g downto 0)           := (others => '0');
  signal product : signed(accumulate_width_g - 1 downto 0) := (others => '0');
begin

  -- DSP Internal valid sample cascade
  valid_pipe_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        take_pipe(take_pipe'high downto 1) <= take_pipe(take_pipe'high-1 downto 0);
        if reset = '1' then
          take_pipe(0) <= '0';
        else
          take_pipe(0) <= take_data;
        end if;
      end if;
    end if;
  end process;

  coeff_reg_p : process(clk)
  begin
    if rising_edge(clk) then
      if coeff_valid = '1' then
        -- We do not reset the coefficients
        coeff <= coeff_in;
      end if;
    end if;
  end process;
  coeff_out <= coeff;

  ---------------------------------------------------------------------------
  -- FIR filter engine,
  -- This is making use of a preadder, Multiply, and then add together.
  ---------------------------------------------------------------------------
  --        Preadder and multiply             |    Post Adder options
  --                                          |
  --               ___      ___      ___      |    Number terms = 0
  -- preadd_a_in -| R |----|   |    |   |     |    product -------------->
  --              |___|    | + |----| * |---> |
  --               ___     |   |    |   |     |    Number terms = 1
  -- preadd_b_in -| R |----|___|    |___|     |                ___
  --              |___|               |       |    product ---| R |------>
  --               ___                |       |               |___|
  --    coeff_in -| R |----------------       |
  --              |___|                       |    Number terms = 2
  --                                          |                ___
  --            Processing delay = 3 Clocks   |    product ---| + |------>
  --                                          | accumulate_b -|___|
  --                                          |
  --                                          |    Number terms = 3
  --                                          |                ___
  --                                          |    product ---|   |----->
  --                                          | accumulate_b -| + |
  --                                          | accumulate_C -|___|
  --
  --                                          Processing delay = 0 or 1 clock
  --

  -- Stage 0, re-register the inputted preadd_a_r (z-2),
  --          and register the other inputs (z-1)
  input_reg_a_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        preadd_a_r <= (others => '0');
      elsif enable = '1' and take_data = '1' then
        preadd_a_r <= preadd_a_in;
      end if;
    end if;
  end process;
  -- Generate statement to prevent cascade path P&R error
  -- Prevent every 3rd DSP slice from absorbing preadd_b_r register
  -- This is an issue found with P&R in Vivado. If similar issues
  -- are seen in other tools, add the equivalent attribute
  attribute_gen : if number_adder_terms_g = 3 generate
    signal preadd_b_r                  : signed(data_width_g - 1 downto 0) := (others => '0');
    attribute dont_touch               : string;
    attribute dont_touch of preadd_b_r : signal is "true";
  begin
    input_reg_b_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          preadd_b_r <= (others => '0');
        elsif enable = '1' and take_data = '1' then
          preadd_b_r <= preadd_b_in;
        end if;
      end if;
    end process;
    preadd_b <= preadd_b_r;
  end generate;
  no_attribute_gen : if number_adder_terms_g /= 3 generate
    signal preadd_b_r : signed(data_width_g - 1 downto 0) := (others => '0');
  begin
    input_reg_b_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          preadd_b_r <= (others => '0');
        elsif enable = '1' and take_data = '1' then
          preadd_b_r <= preadd_b_in;
        end if;
      end if;
    end process;
    preadd_b <= preadd_b_r;
  end generate;

  preadd_b_out <= preadd_b;


  -- Stage 1, Add new input and delayed input (these are the symmetric pairs)
  pre_add_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        preadd <= resize(preadd_a_r, data_width_g + 1) + resize(preadd_b, data_width_g + 1);
      end if;
    end if;
  end process;

  -- Stage 2, Multiply
  mult_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        product <= resize((preadd * coeff), accumulate_width_g);
      end if;
    end if;
  end process;

  -- Stage 3, Add the product and the cascaded accumulation
  --          (note that the dsp slices are enabled on a cascade, therefore
  --           the internal processing pipeline delay remains constant).
  adder_0t_gen : if number_adder_terms_g = 0 generate
    -- If there are 0 terms, pass through to the output without registering
    accumulate <= product;
  end generate;

  adder_1t_gen : if number_adder_terms_g = 1 generate
    -- If there is 1 terms, pass through to the output with registering
    accumulate_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          accumulate <= product;
        end if;
      end if;
    end process;
  end generate;

  adder_2t_gen : if number_adder_terms_g = 2 generate
    -- If there are 2 terms, So add product to accumulate_b and register
    accumulate_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          accumulate <= product + accumulate_b;
        end if;
      end if;
    end process;
  end generate;

  adder_3t_gen : if number_adder_terms_g = 3 generate
    -- If there are 3 terms, so add product to accumulate_b/_c
    -- note for this to be possible, one of the accumulate_b/_c must be
    -- capable of using the cascade path
    accumulate_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          accumulate <= product + accumulate_b + accumulate_c;
        end if;
      end if;
    end process;
  end generate;

  output_valid <= take_pipe(take_pipe'high);

  -- Type conversion for outputs
  preadd_a_out <= preadd_a_r;
  output       <= accumulate;

end rtl;
