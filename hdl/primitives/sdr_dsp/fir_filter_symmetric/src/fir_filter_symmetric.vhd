-- HDL Implementation of Symmentric FIR Filter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.std_logic_misc.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.slice_accumulator_number_terms;

entity fir_filter_symmetric is
  generic (
    num_taps_g          : natural := 11;
    input_data_width_g  : natural := 16;
    output_data_width_g : natural := 16;
    -- Valid modes are:
    -- symmetric_fir, half_band_fir, half_band_decimate, half_band_interpolate
    mode_g              : string  := "symmetric_fir";
    rounding_type_g     : string  := "truncate";
    binary_point_g      : natural := 15;
    saturation_en_g     : boolean := false
    );
  port (
    clk              : in  std_logic;
    reset            : in  std_logic;
    data_reset       : in  std_logic := '0';
    enable           : in  std_logic := '1';
    -- Tap data input
    tap_data_in      : in  std_logic_vector(input_data_width_g - 1 downto 0);
    tap_valid_in     : in  std_logic;
    tap_ready_out    : out std_logic;
    -- Input sample data input
    input_data_in    : in  std_logic_vector(input_data_width_g - 1 downto 0);
    input_valid_in   : in  std_logic;
    input_last_in    : in  std_logic := '0';
    input_ready_out  : out std_logic;
    -- Output sample data
    output_data_out  : out std_logic_vector(output_data_width_g-1 downto 0);
    output_valid_out : out std_logic;
    output_last_out  : out std_logic;
    output_ready_in  : in  std_logic
    );
end fir_filter_symmetric;

architecture rtl of fir_filter_symmetric is
  -- Function slices_from_num_taps returns the number of Multiply-Accumulate
  -- calculations which are needed, this function always returns the same value
  -- but is kept in case of optimisations in the future
  function slices_from_num_taps(number_of_taps : integer; mode : string) return integer is
  begin
    if mode = "half_band_interpolate" or mode = "half_band_decimate" then
      return (number_of_taps+1)/4;
    else
      return (number_of_taps+1)/2;
    end if;
  end function;

  --------
  -- Multiply-Accumulate constants and signals
  --------
  -- Total number of Multiply accumulate slices (i.e. dsp slices required)
  constant num_slices_c : natural := slices_from_num_taps(num_taps_g, mode_g);

  -- Output width of the multiply accumulate stage (i.e. dsp slice output)
  constant product_width_c : natural := (2 * input_data_width_g) + integer(ceil(log2(real(num_taps_g)))) + 1;

  function is_first_tap_valid (num_taps : integer; mode : string) return std_logic is
  begin
    if (mode = "half_band_interpolate" or mode = "half_mode_decimate") and (num_taps_g rem 4) = 1 then
      return '0';
    else
      return '1';
    end if;
  end function;
  constant first_tap_valid_c : std_logic := is_first_tap_valid(num_taps_g, mode_g);

  constant central_tap_idx_c : integer := num_taps_g/2;

  function last_msg_pipe_length (num_taps : integer; num_slices : integer; adder_stages : integer) return natural is
    variable return_length : integer;
  begin
    if mode_g = "half_band_decimate" then
      -- accumulator delay (3 or 4)
      -- rounding (1)
      -- accumlator to adder tree register (1)
      if (num_taps_g rem 4) = 1 then
        return_length := 5 + adder_stages;
      else
        return_length := 6 + adder_stages;
      end if;
    elsif mode_g = "half_band_interpolate" then
      -- central_output_valid_r pipeline (2 or 3)
      -- central accumulator delay (3)
      -- rounding (1)
      if (num_taps_g rem 4) = 1 then
        return_length := 6 + adder_stages;
      else
        return_length := 7 + adder_stages;
      end if;
    else
      -- accumulator delay (3 or 4)
      -- rounding (1)
      -- accumlator to adder tree register (1)
      case slice_accumulator_number_terms(num_slices - 1, num_slices) is
        when 0 =>
          return_length := 5 + adder_stages;
        when others =>
          return_length := 6 + adder_stages;
      end case;
    end if;
    return return_length;
  end function;

  -- How many outputs are expected at the start of the adder tree (i.e. the
  -- number of output values from the multiply accumulate slices).
  constant num_addertree_values_c : positive := integer(ceil(real(num_slices_c)/3.0));
  -- How many 2-term adder stages are required to get down to a single value
  constant num_addertree_stages_c : natural  := integer(ceil(log2(real(num_addertree_values_c))));

  -- Valid shift register for the _last flags
  signal last_pipe                  : std_logic_vector(last_msg_pipe_length(num_taps_g, num_slices_c, num_addertree_stages_c)-1 downto 0);
  -- Reset shift register
  constant reset_slice_c            : natural := 2;
  constant reset_adder_tree_valid_c : natural := 3;
  signal reset_pipe                 : std_logic_vector(last_msg_pipe_length(num_taps_g, num_slices_c, num_addertree_stages_c) downto 0);
  signal filter_resetting           : std_logic;

  -- Input data array and valid flags
  type input_data_array_t is array (natural range <>) of signed(input_data_width_g - 1 downto 0);
  signal input_data          : std_logic_vector(input_data_width_g - 1 downto 0);
  signal input_valid         : std_logic;
  signal input_ready         : std_logic;
  signal interpolated_sample : std_logic;

  -- Tap related signals
  signal tap_array         : input_data_array_t(0 to num_slices_c) := (others => (others => '0'));
  signal tap_valid         : std_logic;
  signal tap_valid_i       : std_logic;
  signal central_tap_valid : std_logic_vector(num_taps_g - 1 downto 0);

  -- The sample array is set to hold twice the number of slices to accommodate
  -- the forward and backward path. For the half band decimator, the number of
  -- slices is half, but the registers are still required, so 4x is used.
  -- Two are added for the central tap slice and one is to simplify expressions.
  signal sample_array : input_data_array_t(0 to (4*(num_slices_c-1))+3) := (others => (others => '0'));

  type accumulator_array_t is array (natural range <>) of signed(product_width_c - 1 downto 0);
  signal accumulator_array : accumulator_array_t(0 to num_slices_c-1) := (others => (others => '0'));
  signal accumulator_valid : std_logic_vector(num_slices_c-1 downto 0);

  --------
  -- Adder tree constants and signals.
  --------
  -- Output width from the adder tree (allowing for 1bit growth at each level
  -- of the adder tree)
  constant addertree_values_width_c : positive     := product_width_c + num_addertree_stages_c;
  type adder_tree_array_t is array (natural range <>) of signed(addertree_values_width_c - 1 downto 0);
  type adder_tree_t is array (0 to num_addertree_stages_c) of adder_tree_array_t(0 to num_addertree_values_c-1);
  signal adder_tree                 : adder_tree_t := (others => (others => (others => '0')));
  signal adder_tree_valid           : std_logic_vector(num_addertree_stages_c downto 0);
  signal adder_tree_out             : signed(addertree_values_width_c downto 0);
  signal adder_tree_out_valid       : std_logic;

  -- Output data signals
  signal output_data_i  : signed(output_data_out'range);
  signal output_last    : std_logic;
  signal output_valid_i : std_logic;
  signal output_valid   : std_logic;

begin

  -- Make sure that when the filter is configured to be a half band
  -- the number of coefficients is set correctly, else cause the build to fail
  generate_error_gen : if ((num_taps_g + 1) mod 4 /= 0 and mode_g /= "symmetric_fir") generate
    signal generate_error : integer;
  begin
    -- Half band filter requires a number of coefficients
    -- that equates to 4n-1 where n is any positive number
    generate_error <= 1/0;
  end generate;

  -- Pass through the ready signal
  tap_ready_out <= output_ready_in and enable;

  update_reset_pipe_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        reset_pipe <= (others => '1');
      elsif enable = '1' then
        reset_pipe <= reset_pipe(reset_pipe'high - 1 downto 0) & data_reset;
      end if;
    end if;
  end process;
  filter_resetting <= reset or or_reduce(reset_pipe);

  last_pipe_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        last_pipe <= last_pipe(last_pipe'high-1 downto 0) & (input_last_in and input_valid_in and input_ready and not(reset));
      end if;
    end if;
  end process;

  ---------------------------------------------------------------------------
  -- When the filter is interpolating then we need to gate the input stream
  -- to allow for the additional samples to be inserted, this is done be
  -- applying back pressure while validating the interpolated sample into the
  -- filter
  ---------------------------------------------------------------------------
  gen_in_flow_interpolate : if mode_g = "half_band_interpolate" generate
  begin
    gen_interpolated_valid_p : process(clk)
    begin
      if rising_edge(clk) then
        if data_reset = '1' or reset = '1' then
          interpolated_sample <= '0';
        elsif output_ready_in = '1' and enable = '1' then
          if interpolated_sample = '1' or input_valid_in = '1' then
            interpolated_sample <= not interpolated_sample;
          end if;
        end if;
      end if;
    end process;

    -- Hold off the input data stream for 1 sample every sample to allow the
    -- zero padded data to be inserted, due to an interpolation factor of 2.
    input_ready <= output_ready_in and enable and not filter_resetting when interpolated_sample = '0' else '0';
  end generate;

  gen_ready_decimate : if mode_g = "half_band_decimate" generate
  begin
    input_ready <= output_ready_in and enable and not filter_resetting;
  end generate;

  gen_taps_half : if mode_g = "half_band_interpolate" or mode_g = "half_band_decimate" generate
  begin

    input_tap_reg_p : process(clk)
    begin
      if rising_edge(clk) then
        tap_array(0) <= signed(tap_data_in);

        if reset = '1' then
          tap_valid_i <= first_tap_valid_c;
        else
          if tap_valid_in = '1' then
            tap_valid_i <= not tap_valid_i;
          end if;
        end if;
      end if;
    end process;
    tap_valid <= tap_valid_i and tap_valid_in;

    central_tap_reg_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          central_tap_valid <= (0 => '1', others => '0');
        elsif tap_valid_in = '1' then
          -- rotate through taps
          central_tap_valid <= central_tap_valid(central_tap_valid'high -1 downto 0) & central_tap_valid(central_tap_valid'high);
        end if;
      end if;
    end process;
  end generate;

  ---------------------------------------------------------------------------
  -- When the filter is not interpolating or decimating
  ---------------------------------------------------------------------------
  gen_out_flow : if mode_g /= "half_band_interpolate" and mode_g /= "half_band_decimate" generate
  begin
    input_ready <= output_ready_in and enable and not filter_resetting;

    input_tap_reg_p : process(clk)
    begin
      if rising_edge(clk) then
        tap_valid <= tap_valid_in;
        if tap_valid_in = '1' then
          tap_array(0) <= signed(tap_data_in);
        end if;
      end if;
    end process;
  end generate;


  input_data      <= input_data_in;
  input_valid     <= input_valid_in and input_ready;
  input_ready_out <= input_ready;

  ---------------------------------------------------------------------------
  -- Input: i=0
  -- This takes data on it's initial input, once inserted within the array,
  -- the position is shifted by the generate loop
  --
  -- If bit 1 is set then the taps look like 1-111-1, if it is 0 then it is
  -- -1-111-1-, so skip the first element
  ---------------------------------------------------------------------------


  -- sample_array is registered within the slices, so no initial registering
  -- here
  sample_array(0) <= signed(input_data);

  fir_mac_slice_gen : for i in 0 to num_slices_c - 1 generate
    ---------------------------------------------------------------------------
    -- This takes data based on it's relative position within the shift
    -- registers
    ---------------------------------------------------------------------------
    signal preadd_b_in  : signed(input_data_width_g-1 downto 0);
    signal preadd_b_out : signed(input_data_width_g-1 downto 0);
    signal accumulate_b : signed(product_width_c - 1 downto 0);
    signal accumulate_c : signed(product_width_c - 1 downto 0);
    signal output       : signed(product_width_c - 1 downto 0);
  begin

    interpolate_b_gen : if mode_g = "half_band_interpolate" generate
      preadd_b_in                        <= sample_array((num_slices_c*2) - (i+1));
      sample_array((num_slices_c*2) - i) <= preadd_b_out;
    end generate;

    decimate_b_gen : if mode_g = "half_band_decimate" generate
      preadd_b_in                             <= sample_array(sample_array'high - (i*2)-1);
      sample_array(sample_array'high - (i*2)) <= preadd_b_out;
      sample_shift_p : process(clk)
      begin
        if rising_edge(clk) then
          if reset_pipe(reset_slice_c) = '1' then
            sample_array(sample_array'high - (i*2)-1) <= (others => '0');
            sample_array((i*2)+2)                     <= (others => '0');
          elsif enable = '1' and input_valid_in = '1' then
            -- shift data along the sample array in place of the slice optimised
            -- away by the half band implementation
            sample_array(sample_array'high - (i*2)-1) <= sample_array(sample_array'high - (i*2)-2);  -- feedback (preadd_b)
            sample_array((i*2)+2)                     <= sample_array((i*2)+1);  -- feedforward (preadd_a)
          end if;
        end if;
      end process;
    end generate;

    other_b_gen : if mode_g /= "half_band_interpolate" and mode_g /= "half_band_decimate" generate
      -- If this is not the last slice, then there is always an A and B value in
      -- a symmetric filter
      even_coeff_in_gen : if not ((num_taps_g mod 2) = 1 and i = (num_slices_c - 1)) generate
        preadd_b_in <= sample_array(num_taps_g-1 - i);
      end generate;
      -- This is the last slice in the filter, and the number of coefficients is
      -- odd, that means the center tap is only used once
      odd_coeff_in_gen : if ((num_taps_g mod 2) = 1 and i = (num_slices_c - 1)) generate
        preadd_b_in <= (others => '0');
      end generate;
      -- else, the central tap is used twice
      even_coeff_out_gen : if not ((num_taps_g mod 2) = 1 and i = (num_slices_c - 1)) generate
        sample_array(num_taps_g - i) <= preadd_b_out;
      end generate;
    end generate;

    -- The accumulation has only 1 term present or is in bypass
    -- then the accumulate_b input can be nulled
    acc_b_null_gen : if slice_accumulator_number_terms(i, num_slices_c) <= 1 generate
      accumulate_b <= (others => '0');
    end generate;
    -- The accumulation phase has at least 2 terms present,
    -- then the accumulate_b is extracted from array
    acc_b_gen : if slice_accumulator_number_terms(i, num_slices_c) > 1 generate
      accumulate_b <= accumulator_array(i-1);
    end generate;

    -- The accumulation phase does not have 3 terms present
    -- so null accumulate_c
    acc_c_null_gen : if slice_accumulator_number_terms(i, num_slices_c) /= 3 generate
      accumulate_c <= (others => '0');
    end generate;
    -- The accumulation phase has 3 terms present
    acc_c_gen : if slice_accumulator_number_terms(i, num_slices_c) = 3 generate
      accumulate_c <= accumulator_array(i-2);
    end generate;

    decimate_mac_gen : if mode_g = "half_band_decimate" generate
      mac_i : entity work.add_mult_add_slice
        generic map(
          number_adder_terms_g => slice_accumulator_number_terms(i, num_slices_c),
          data_width_g         => input_data_width_g,
          accumulate_width_g   => product_width_c
          )
        port map(
          clk    => clk,
          reset  => reset_pipe(reset_slice_c),
          enable => enable,

          coeff_valid => tap_valid,
          coeff_in    => tap_array(i),
          coeff_out   => tap_array(i+1),

          take_data    => input_valid,
          preadd_a_in  => sample_array(i*2),
          preadd_a_out => sample_array((i*2)+1),
          preadd_b_in  => preadd_b_in,
          preadd_b_out => preadd_b_out,

          accumulate_b => accumulate_b,
          accumulate_c => accumulate_c,
          output       => accumulator_array(i),

          output_valid => accumulator_valid(i)
          );
    end generate;

    other_mac_gen : if mode_g /= "half_band_decimate" generate
      mac_i : entity work.add_mult_add_slice
        generic map(
          number_adder_terms_g => slice_accumulator_number_terms(i, num_slices_c),
          data_width_g         => input_data_width_g,
          accumulate_width_g   => product_width_c
          )
        port map(
          clk    => clk,
          reset  => reset_pipe(reset_slice_c),
          enable => enable,

          coeff_valid => tap_valid,
          coeff_in    => tap_array(i),
          coeff_out   => tap_array(i+1),

          take_data    => input_valid,
          preadd_a_in  => sample_array(i),
          preadd_a_out => sample_array(i+1),
          preadd_b_in  => preadd_b_in,
          preadd_b_out => preadd_b_out,

          accumulate_b => accumulate_b,
          accumulate_c => accumulate_c,
          output       => accumulator_array(i),

          output_valid => accumulator_valid(i)
          );
    end generate;

    -- Adder tree value insertion (for everything but the last value this is
    -- taking every 3rd slice)
    put_in_tree_intermediate_gen : if i /= (num_slices_c - 1) and slice_accumulator_number_terms(i, num_slices_c) = 3 generate
      adder_tree_every_third_p : process(clk)
      begin
        if rising_edge(clk) then
          if enable = '1' then
            adder_tree(0)((i-2)/3) <= resize(accumulator_array(i), addertree_values_width_c);
          end if;
        end if;
      end process;
    end generate;
    -- For the last slice this will be the last element of the adder tree.
    put_in_tree_end_gen : if i = (num_slices_c - 1) generate
      adder_tree_last_p : process(clk)
      begin
        if rising_edge(clk) then
          if enable = '1' then
            adder_tree(0)(num_addertree_values_c-1) <= resize(accumulator_array(i), addertree_values_width_c);
          end if;
        end if;
      end process;
    end generate;
  end generate fir_mac_slice_gen;

  --------
  -- Adder Tree
  --------
  add_tree_valid_insert_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        adder_tree_valid(adder_tree_valid'high downto 1) <= adder_tree_valid(adder_tree_valid'high-1 downto 0);

        if reset_pipe(reset_adder_tree_valid_c) = '1' then
          adder_tree_valid(0) <= '0';
        else
          adder_tree_valid(0) <= accumulator_valid(accumulator_valid'high);
        end if;
      end if;
    end if;
  end process;

  adder_tree_gen : for stage in 0 to num_addertree_stages_c-1 generate
    function stage_length (stage : integer; adder_width : integer) return integer is
    begin
      if stage = 0 then
        return adder_width;
      else
        return integer(ceil(real(adder_width) / (2.0**real(stage))));
      end if;
    end function;
    constant stage_len_c : natural := stage_length(stage, num_addertree_values_c);
  begin
    adder_tree_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          for i in 0 to (stage_len_c/2)-1 loop
            adder_tree(stage+1)(i) <=
              resize(adder_tree(stage)(2*i)(product_width_c + stage downto 0)
                     + adder_tree(stage)((2*i)+1)(product_width_c + stage downto 0), addertree_values_width_c);
          end loop;
          if (stage_len_c mod 2) = 0 then
            adder_tree(stage+1)(stage_len_c/2) <=
              resize(adder_tree(stage)(stage_len_c-2)(product_width_c + stage downto 0)
                     + adder_tree(stage)(stage_len_c-1)(product_width_c + stage downto 0), addertree_values_width_c);
          else
            adder_tree(stage+1)(stage_len_c/2) <= adder_tree(stage)(stage_len_c-1);
          end if;
        end if;
      end if;
    end process;
  end generate;

  half_band_dvalid_gen : if mode_g = "half_band_interpolate" or mode_g = "half_band_decimate" generate
    function central_valid_delay (num_taps : natural; constant num_addertree_stages_c : natural) return natural is
    begin
      if (num_taps_g rem 4) = 1 then
        return num_addertree_stages_c;
      else
        return num_addertree_stages_c + 2;
      end if;
    end function;
    constant preadd_zero_c     : signed(input_data_width_g-1 downto 0) := (others => '0');
    constant accumulate_zero_c : signed(product_width_c - 1 downto 0)  := (others => '0');

    signal preadd_a       : signed(input_data_width_g - 1 downto 0);
    signal central_output : signed(product_width_c - 1 downto 0);

    signal central_data_r         : accumulator_array_t(central_valid_delay(num_taps_g, num_addertree_stages_c) downto 0) := (others => (others => '0'));
    signal central_output_valid_r : std_logic_vector(central_valid_delay(num_taps_g, num_addertree_stages_c) downto 0);
    signal central_out_padded     : signed(addertree_values_width_c downto 0);
    signal central_output_valid   : std_logic;
  begin
    -- As the starting data might be 0, or might be actual input, depending
    -- upon the tap length, the first output might need suppressing.

    decimator_preadd_gen : if mode_g = "half_band_decimate" generate
      preadd_a <= sample_array(((num_slices_c-1)*2)+1-1);
    end generate;

    interpolate_preadd_gen : if mode_g = "half_band_interpolate" generate
      input_stages_1_gen : if (num_taps_g rem 4) = 1 generate
        preadd_a <= sample_array(num_slices_c);
      end generate;

      input_stages_multi_gen : if (num_taps_g rem 4) /= 1 generate
        preadd_a <= sample_array(num_slices_c-1);
      end generate;
    end generate;

    mac_i : entity work.add_mult_add_slice
      generic map(
        number_adder_terms_g => 0,
        data_width_g         => input_data_width_g,
        accumulate_width_g   => product_width_c
        )
      port map(
        clk    => clk,
        reset  => reset_pipe(reset_slice_c),
        enable => enable,

        coeff_valid => central_tap_valid(central_tap_idx_c),
        coeff_in    => tap_array(0),
        coeff_out   => open,

        take_data    => input_valid,
        preadd_a_in  => preadd_a,
        preadd_a_out => open,
        preadd_b_in  => preadd_zero_c,
        preadd_b_out => open,

        accumulate_b => accumulate_zero_c,
        accumulate_c => accumulate_zero_c,
        output       => central_output,
        output_valid => central_output_valid
        );

    stages_1_gen : if central_valid_delay(num_taps_g, num_addertree_stages_c) < 1 generate
      adder_tree_p : process(clk)
      begin
        if rising_edge(clk) then
          if reset = '1' or reset_pipe(reset_pipe'high - 1) = '1' then
            central_output_valid_r(0) <= '0';
          elsif enable = '1' then
            central_data_r(0)         <= central_output;
            central_output_valid_r(0) <= central_output_valid;
          end if;
        end if;
      end process;
      central_out_padded(central_output'range)                                 <= central_output;
      central_out_padded(central_out_padded'high downto central_output'length) <= (others => central_output(central_output'high));
    end generate;

    stages_multi_gen : if central_valid_delay(num_taps_g, num_addertree_stages_c) > 0 generate
      adder_tree_p : process(clk)
      begin
        if rising_edge(clk) then
          if reset = '1' or reset_pipe(reset_pipe'high - 1) = '1' then
            central_output_valid_r <= (others => '0');
          elsif enable = '1' then
            central_data_r         <= central_data_r(central_data_r'high-1 downto 0) & central_output;
            central_output_valid_r <= central_output_valid_r(central_output_valid_r'high-1 downto 0) & central_output_valid;
          end if;
        end if;
      end process;
      central_out_padded(central_output'range)                                 <= central_data_r(central_data_r'high);
      central_out_padded(central_out_padded'high downto central_output'length) <= (others => central_data_r(central_data_r'high)(central_output'high));
    end generate;

    decimate_adder_tree_gen : if mode_g = "half_band_decimate" generate
      adder_tree_out_valid <= adder_tree_valid(adder_tree_valid'high);
      adder_tree_out       <= adder_tree(adder_tree'high)(0) + central_out_padded;
    end generate;
    interpolate_adder_tree_gen : if mode_g = "half_band_interpolate" generate
      adder_tree_out_valid <= adder_tree_valid(adder_tree_valid'high) or central_output_valid_r(central_output_valid_r'high);
      adder_tree_out       <= resize(adder_tree(adder_tree'high)(0), adder_tree_out'length) when central_output_valid_r(central_output_valid_r'high) = '0' else central_out_padded;
    end generate;
  end generate;

  -- Output of the adder tree
  other_dvalid_gen : if mode_g /= "half_band_interpolate" and mode_g /= "half_band_decimate" generate
    adder_tree_out       <= resize(adder_tree(adder_tree'high)(0), adder_tree_out'length);
    adder_tree_out_valid <= adder_tree_valid(adder_tree_valid'high);
  end generate;

  -- Resetting the rounder with the last register in the reset pipe
  output_round_real_i : entity work.output_rounding
    generic map (
      rounding_type_g => rounding_type_g,
      input_width_g   => adder_tree_out'length,
      output_width_g  => output_data_i'length,
      binary_point_g  => binary_point_g,
      saturation_en_g => saturation_en_g
      )
    port map (
      clk            => clk,
      reset          => reset_pipe(reset_pipe'high),
      enable         => enable,
      data_in        => adder_tree_out,
      data_valid_in  => adder_tree_out_valid,
      data_out       => output_data_i,
      data_valid_out => output_valid_i
      );

  -- Generate the decimated data by inhibiting the data valid flag every other
  -- input sample
  decimate_out_valid_gen : if mode_g = "half_band_decimate" generate
    -- Set to true to keep the first (and discard the second) sample
    constant keep_first_sample_c : std_logic := '0';
    signal keep_sample           : std_logic;
    signal output_seen           : std_logic;
  begin
    dec_dvalid_p : process(clk)
    begin
      if rising_edge(clk) then
        -- Resetting the decimator with the last regsiter in the reset pipe
        if reset_pipe(reset_pipe'high) = '1' then
          keep_sample <= keep_first_sample_c;
          output_seen <= '0';
        else
          -- Update the decimator every output sample
          if enable = '1' then
            if output_last = '1' then
              output_seen <= '0';
            end if;
            if output_valid_i = '1' and output_ready_in = '1' and enable = '1' then
              keep_sample <= not keep_sample;
              if keep_sample = '0' then
                output_seen <= '1';
              end if;
            end if;
          end if;
        end if;
      end if;
    end process;

    output_data_out <= std_logic_vector(output_data_i);
    output_valid    <= keep_sample and output_valid_i;
    output_last     <= output_seen and output_valid_i and last_pipe(last_pipe'high);
  end generate;

  -- Pass the data valid flag directly through when the data is not decimated
  other_out_valid_gen : if mode_g /= "half_band_decimate" generate
    output_valid    <= output_valid_i;
    output_last     <= last_pipe(last_pipe'high);
    output_data_out <= std_logic_vector(output_data_i);
  end generate;

  output_valid_out <= output_valid;
  output_last_out  <= output_last;

end rtl;
