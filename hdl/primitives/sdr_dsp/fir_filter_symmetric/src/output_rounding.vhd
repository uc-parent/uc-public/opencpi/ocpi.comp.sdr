-- HDL Implementation of submodule Output data rounding (Symmetric FIR Filter)
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;
use sdr_dsp.sdr_dsp.rounding_halfeven;
use sdr_dsp.sdr_dsp.rounding_truncate;

entity output_rounding is
  generic (
    rounding_type_g : string;
    input_width_g   : natural;
    output_width_g  : natural;
    binary_point_g  : natural;
    saturation_en_g : boolean
    );
  port (
    clk            : in  std_logic;
    reset          : in  std_logic;
    enable         : in  std_logic;
    data_in        : in  signed(input_width_g - 1 downto 0);
    data_valid_in  : in  std_logic;
    data_out       : out signed(output_width_g - 1 downto 0);
    data_valid_out : out std_logic
    );
end output_rounding;

architecture rtl of output_rounding is

begin
  ------------------------------------------------------------------------------
  -- Selectable Rounding
  ------------------------------------------------------------------------------
  truncate_gen : if rounding_type_g = "truncate" generate
    round_truncate_i : rounding_truncate
      generic map (
        input_width_g   => data_in'length,
        output_width_g  => data_out'length,
        saturation_en_g => false
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => data_in,
        data_valid_in  => data_valid_in,
        binary_point   => binary_point_g,
        data_out       => data_out,
        data_valid_out => data_valid_out
        );
  end generate;

  truncation_with_saturation_gen : if rounding_type_g = "truncate_with_saturation" generate
    round_truncate_with_saturation_i : rounding_truncate
      generic map (
        input_width_g   => data_in'length,
        output_width_g  => data_out'length,
        saturation_en_g => true
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => data_in,
        data_valid_in  => data_valid_in,
        binary_point   => binary_point_g,
        data_out       => data_out,
        data_valid_out => data_valid_out
        );
  end generate;

  half_up_gen : if rounding_type_g = "half_up" generate
    round_half_up_i : rounding_halfup
      generic map (
        input_width_g   => data_in'length,
        output_width_g  => data_out'length,
        saturation_en_g => saturation_en_g
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => data_in,
        data_valid_in  => data_valid_in,
        binary_point   => binary_point_g,
        data_out       => data_out,
        data_valid_out => data_valid_out
        );
  end generate;

  half_even_gen : if rounding_type_g = "half_even" generate
    round_half_even_i : rounding_halfeven
      generic map (
        input_width_g   => data_in'length,
        output_width_g  => data_out'length,
        saturation_en_g => saturation_en_g
        )
      port map (
        clk            => clk,
        reset          => reset,
        clk_en         => enable,
        data_in        => data_in,
        data_valid_in  => data_valid_in,
        binary_point   => binary_point_g,
        data_out       => data_out,
        data_valid_out => data_valid_out
        );
  end generate;

end rtl;
