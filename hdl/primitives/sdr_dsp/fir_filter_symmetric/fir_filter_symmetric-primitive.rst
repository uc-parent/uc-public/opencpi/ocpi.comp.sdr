.. fir_filter_symmetric documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter_symmetric-primitive:


Symmetric Finite Impulse Response (FIR) Filter (``fir_filter_symmetric``)
=========================================================================
Symmetric FIR filter primitive with fixed resource usage.

Design
------
Implements an FIR filter with a compile time definable number of taps.

The mathematical representation of the implementation if N taps are symmetric and ``num_taps_g`` is odd is defined in :eq:`fir_filter_symmetric-odd-equation`.

.. math::
   :label: fir_filter_symmetric-odd-equation_label

   y[n] = \left( \sum_{k=0}^{\frac{N}{2}-1} h[k] \times (x[n-k] + x[n-((N - 1)-k)]) \right) + h[\frac{N-1}{2}] \times x[n-\frac{N-1}{2}]

In :eq:`fir_filter_symmetric-odd-equation`:

 * :math:`y[n]` is the output values.

 * :math:`x[n]` is the input values.

 * :math:`N` is the total number of taps.

 * :math:`h[k]` is the taps values, from :math:`0` to :math:`N - 1`.

The :eq:`fir_filter_symmetric-odd-equation`, results in the folding of :math:`h[k]` taps around :math:`N/2`. A block diagram representation of the :eq:`fir_filter_symmetric-odd-equation` is given in the :numref:`symmetric_fir_odd_taps-diagram` below.

.. _fir_filter_symmetric_odd_taps-diagram:

.. figure:: symmetric_fir.svg
   :alt: Block diagram of a symmetric finite impulse response (FIR) filter implementation
   :align: center

   Symmetric FIR filter odd taps implementation.

Depending on the definable number of taps ``num_taps_g`` the exact reflection around the central tap(s) is different. If the N taps are symmetric and the ``num_taps_g`` is even then the equation for the output :math:`y`, is expressed as:

.. math::
   :label: fir_filter_symmetric-even-primitive-equation

   y[n] = \sum_{k=0}^{\frac{N}{2}-1} h[k] \times (x[n-k] + x[n-((N - 1)-k)])

A block diagram representation of even the implementation is given in :numref:`fir_filter_symmetric_even_taps-diagram` below.

.. _fir_filter_symmetric_even_taps-diagram:

.. figure:: symmetric_fir_even_taps.svg
   :alt: Block diagram of a symmetric finite impulse response (FIR) filter implementation
   :align: center

   Symmetric FIR filter even tap implementation.

Note: The only difference between :numref:`fir_filter_symmetric_odd_taps-diagram` and :numref:`fir_filter_symmetric_even_taps-diagram` is if ``num_taps_g`` is the even and the central tap(s) is reached (:math:`h((N-2)/2)`) there is no singular fold before generating the output :math:`y[n]`.

Implementation
--------------
The primitive is designed to work with taps with the same precision as the input data. The input data width is customisable at compile / build time. Output data width is rounded or truncated to the same width as the input data width with `binary_point_g` set to the LSB.

The number of multiply accumulate (MAC) units used to implement the filter is fixed to half of the taps (rounded up to the nearest integer), therefore allowing data to be processed on every clock cycle.

Each MAC unit is used to perform part the FIR filter operation. The results of each of those MACs then need to be added together to get the final filter output. This is undertaken through an adder tree. The first stage of accumulation adds up to 3 MAC units, then the output of this stage passed through :math:`log_2 \lceil \frac{N}{3} \rceil` set of 2 term adders.

The ``half_band_decimate`` mode keeps the first sample in the output data stream after a reset.

The ``half_band_interpolate`` mode does not implement the odd taps apart from the central tap, discarding the zero valued coefficients, further reducing the required MACs. It also inserts an additional sample into the input data stream after the first valid sample after a reset.

Interface
---------

Generics
~~~~~~~~

 * ``num_taps_g`` (``natural``): Sets the number of taps the FIR filter has.

 * ``input_data_width_g`` (``natural``): Bit width of input signal.

 * ``output_data_width_g`` (``natural``): Bit width of output signal.

 * ``mode_g`` (``string``): Sets the running mode to use with the data stream, Should be set to one of (``symmetric_fir``, ``half_band_fir``, ``half_band_decimate``, ``half_band_interpolate``)

 * ``rounding_type_g`` (``string``): Sets the rounding mode to use, this should be one of (``truncate``, ``truncate_with_saturation``, ``half_up``, ``half_even``)

 * ``binary_point_g`` (``natural``): Sets the least significant bit of the output data slice from the accumulator

 * ``saturation_en_g`` (``boolean``): Enables saturation on data when rounding and/or truncation to a reduced output resolution has caused an overflow.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``data_reset`` (``std_logic``), in: Data Reset. Active high, synchronous with rising edge of clock, resets the data path only.

 * ``enable`` (``std_logic``), in: Clock enable for module. Module is enabled when ``enable`` is high.

 * ``tap_data_in`` (``std_logic_vector``, ``input_data_width_g`` bits), in: Tap data to the FIR filter. Shifted in on each cycle ``coeff_ready_out`` and ``coeff_valid_in`` are high.

 * ``tap_valid_in`` (``std_logic``), in: ``tap_data_in`` is valid when ``tap_valid_in`` is high.

 * ``tap_ready_out`` (``std_logic``), out: ``tap_data_in`` is capable of accepting data when ``tap_ready_out`` is high.

 * ``input_data_in`` (``std_logic_vector``, ``input_data_width_g`` bits), in: Input data to the FIR filter.

 * ``input_valid_in`` (``std_logic``), in: ``input_data_in`` is valid when ``input_data_in`` is high.

 * ``input_last_in`` (``std_logic``), in: ``input_last_in`` is valid when ``input_data_in`` is high, marks the sample to align with output data.

 * ``input_ready_out`` (``std_logic``), out: ``input_data_in`` is capable of accepting data when ``input_ready_out`` is high.

 * ``output_data_out`` (``std_logic_vector``, ``output_data_width_g`` bits), out: Output data from the FIR filter.

 * ``output_valid_out`` (``std_logic``), out: ``output_data_out`` is valid when ``output_valid_out`` is high.

 * ``output_last_out`` (``std_logic``), out: ``output_last_out`` is valid when ``output_valid_out`` is high, marks the sample to align with output data.

 * ``output_ready_in`` (``std_logic``), in: ``output_data_out`` is outputted when ``output_ready_in`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Half Up Rounder Primitive <rounding_halfup-primitive>`.

 * :ref:`Half Even Rounder Primitive <rounding_halfeven-primitive>`.

 * :ref:`Truncate Rounder Primitive <rounding_truncate-primitive>`.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

 * ``ieee.std_logic_misc``

.. _fir_filter_symmetric-primitive_Limitations:

Limitations
-----------
Limitations of ``fir_filter_symmetric`` are:

 * The primitive is capable of accepting new samples on every clock cycle. This does not take advantage of potential resource savings through reusing MAC units with different taps if a lower throughput is required. (see :ref:`FIR filter primitive <fir_filter-primitive>` for a multiplier optimised version).

 * The primitive uses a pipelined adder tree. If ``num_taps_g`` :math:`> 128` then other FIR filter implementations are more suitable as the adder tree and MAC units will use significant FPGA resources.

 * The half band implementation uses the second half of the coefficient and no checks are made for symmetry.
