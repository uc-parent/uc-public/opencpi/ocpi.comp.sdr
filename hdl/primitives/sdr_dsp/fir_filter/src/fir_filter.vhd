-- HDL Implementation of a FIR filter
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.std_logic_misc.all;

use work.fir_tap_ram_select_pkg.all;

entity fir_filter is
  generic (
    data_in_width_g   : integer := 16;
    num_multipliers_g : integer := 16;
    tap_width_g       : integer := 16;
    num_taps_g        : integer := 2
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    data_rst       : in  std_logic;
    enable         : in  std_logic;
    data_in_valid  : in  std_logic;
    take           : out std_logic;
    data_in        : in  signed(data_in_width_g - 1 downto 0);
    tap_data_in    : in  signed(tap_width_g - 1 downto 0);
    tap_valid_in   : in  std_logic;
    tap_address_in : in  unsigned(integer(ceil(log2(real(num_taps_g)))) downto 0);  -- Additional bit added to address width to deal with initialisation of non-power-of-2 taps/multipliers
    data_out       : out signed(data_in_width_g + tap_width_g + integer(ceil(log2(real(num_taps_g)))) - 1 downto 0);
    data_valid_out : out std_logic
    );
end fir_filter;

architecture behavioural of fir_filter is

  -- Calculates the final output size of the FIR filter.
  -- When adding N numbers inputWidthA + inputWidthB + log2(N) gives the number
  -- of extra bits required to store the result.
  constant adder_tree_size_c : integer := data_in_width_g + tap_width_g + integer(ceil(log2(real(num_taps_g))));

  ------------------------------------------------------------------------------
  -- Operation 1 - Multiply Accumulate Signals
  ------------------------------------------------------------------------------
  -- Given the number of multiplier units available, calculate the number of
  -- clock cycles required to perform multiply accumulate (MAC) operation on all
  -- taps and inputs.
  constant stages_c : integer := integer(ceil(real(num_taps_g) / real(num_multipliers_g)));

  -- Calculate the size needed for each of the MAC units. When adding N numbers
  -- inputWidthA + inputWidthB +  log2(N) gives the number of extra bits
  -- required to store the result.
  constant accumulator_size_c : integer := data_in_width_g + tap_width_g + integer(ceil(log2(real(stages_c))));

  -- Calculate the size of the buffer that is the smallest multiple of
  -- num_multipliers_g that is larger than or equal to num_taps_g.
  constant buffer_size_c : integer := stages_c * num_multipliers_g;

  -- Array to store delayed input data.
  type delay_array_t is array(natural range <>) of signed(data_in_width_g - 1 downto 0);
  signal data_pipe : delay_array_t(0 to buffer_size_c - 1) := (others => (others => '0'));

  -- Array to store output of multiplier units
  type multiplier_result_array_t is array(natural range <>) of signed(data_in_width_g + tap_width_g - 1 downto 0);
  signal multiplier_result : multiplier_result_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Array to store accumulator results
  type accumulator_array_t is array(natural range <>) of signed(accumulator_size_c - 1 downto 0);
  signal accumulator : accumulator_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Points to the current delayed input value for each stage of the FIR
  -- operation.
  signal current_data : delay_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Points to the current tap value for each stage of the FIR operation.
  type taps_array_t is array(natural range <>) of signed((tap_width_g - 1) downto 0);
  signal current_tap : taps_array_t(0 to num_multipliers_g - 1);

  -- Indicates a stage 0 input into the MAC units, so that the accumulators
  -- can be reest at this point.
  signal clear_accumulator : std_logic;

  -- Indicates that the MAC operation is done. Used to trigger the serial
  -- adder process.
  signal mac_ready : std_logic;

  -- Keeps track of the current stage of the serial adder process
  signal stage_counter : integer range 0 to stages_c - 1 := (stages_c - 1);

  ------------------------------------------------------------------------------
  -- Operation 2 - Serialised Adder Signals
  ------------------------------------------------------------------------------
  -- Work out how many adders are required to add up the accumulator for each
  -- multiplier within stages_c clock cycles.
  constant num_serial_adders_c : integer := integer(ceil(real(num_multipliers_g) / real(stages_c)));

  -- Given the number of adders, work out the actual number of clock cycles
  -- required.
  constant serial_adder_stages_c : integer := integer(ceil(real(num_multipliers_g) / real(num_serial_adders_c)));

  -- Calculate length of buffer to hold the expanded accumulator results.
  constant serial_adder_buffer_size_c : integer := serial_adder_stages_c * num_serial_adders_c;

  -- Buffer to hold input and results of serial adders.
  type serial_adder_array_t is array(natural range <>) of signed(adder_tree_size_c - 1 downto 0);
  signal serial_adder_buffer : serial_adder_array_t(0 to serial_adder_buffer_size_c - 1) := (others => (others => '0'));

  -- Keeps track of current stage within the serial adder
  signal adder_stage_counter : integer range 0 to serial_adder_stages_c - 1 := 0;

  -- Points to the current input value for each stage of the serialised
  -- addition operation.
  signal current_adder_input : serial_adder_array_t(0 to num_serial_adders_c - 1) := (others => (others => '0'));

  ------------------------------------------------------------------------------
  -- Operation 3 - Parallel Adder Tree Signals
  ------------------------------------------------------------------------------
  -- To add N numbers together using two input adders takes log2(N) stages.
  constant adder_tree_stages_c : integer := integer(ceil(log2(real(num_serial_adders_c))));

  -- Buffer size is the closest power-of-2 that is equal or above the
  -- num_serial_adders_c.
  constant adder_tree_buffer_size_c : integer := 2**adder_tree_stages_c;

  -- Create array wide enough to hold the widest part of the adder tree.
  type adder_tree_array_t is array(0 to adder_tree_buffer_size_c - 1) of signed(adder_tree_size_c - 1 downto 0);
  -- Create array of arrays to form the adder tree. Each stage of the adder
  -- tree will be of fixed length, but only half the number of registers that
  -- were present in the previous stage will be used.
  -- We rely on synthesis to identify and remove these unused registers.
  -- E.g. To add 8 numbers the following register usage pattern would occur
  -- where X indicates an unused register.
  -- Stage 0: 01234567
  -- Stage 1: 0123XXXX
  -- Stage 2: 01XXXXXX
  -- Stage 3: 0XXXXXXX
  type adder_tree_array_array_t is array(0 to adder_tree_stages_c) of adder_tree_array_t;
  signal adder_tree_buffer : adder_tree_array_array_t := (others => (others => (others => '0')));

  ------------------------------------------------------------------------------
  -- Control Signals
  ------------------------------------------------------------------------------
  -- Total input to output delay is:
  -- stages_c
  -- + 3 (for the MAC delay)
  -- + serial_adder_stages_c (for the serial adder)
  -- + adder_tree_stages_c   (for the parallel adder)
  constant mac_delay_c   : natural := 3;
  signal data_valid_pipe : std_logic_vector(stages_c + mac_delay_c + serial_adder_stages_c + adder_tree_stages_c - 1 downto 0);

  signal reset_pipe       : std_logic_vector(stages_c + mac_delay_c + serial_adder_stages_c + adder_tree_stages_c - 1 downto 0);
  signal data_rst_r       : std_logic;
  signal joint_rst        : std_logic;
  signal filter_resetting : std_logic;

  -- High when both fir_ready and data_in_valid and high
  signal data_taken : std_logic;

  -- High when the FIR implementation is able to take data
  signal fir_ready : std_logic;

  ------------------------------------------------------------------------------
  -- Tap address to RAM select/address mapping
  ------------------------------------------------------------------------------
  constant tap_ram_select_c  : integer_array_t := tap_ram_select_f(2**tap_address_in'length, stages_c);  -- Map entire address space.
  constant tap_ram_address_c : integer_array_t := tap_ram_address_f(2**tap_address_in'length, stages_c);

  type tap_array_2d_t is array (0 to (stages_c - 1)) of signed((tap_width_g - 1) downto 0);
  type tap_array_t is array (0 to (num_multipliers_g - 1)) of tap_array_2d_t;
  signal tap_ram : tap_array_t := (others => (others => (others => '0')));

begin

  -- Make sure that when the multipliers are set to be
  -- more than the number of taps the build fails.
  generate_error_gen : if (num_multipliers_g > num_taps_g) generate
    signal generate_error : integer;
  begin
    -- num_multipligers_g must be less than or equal to num_taps_g
    generate_error <= 1/0;
  end generate;

  joint_rst        <= reset_pipe(reset_pipe'high);
  filter_resetting <= or_reduce(reset_pipe);

  ------------------------------------------------------------------------------
  -- Tap RAM Load
  --
  -- Infers RAM: block or distributed depending on size (automatic).
  ------------------------------------------------------------------------------
  tap_rm_load_p : process(clk)
    variable tap_ram_select_in  : integer range 0 to (num_multipliers_g - 1);
    variable tap_ram_address_in : integer range 0 to (stages_c - 1);
  begin
    if rising_edge(clk) then

      -- Split incoming address into RAM select/address
      tap_ram_select_in  := tap_ram_select_c(to_integer(tap_address_in));
      tap_ram_address_in := tap_ram_address_c(to_integer(tap_address_in));

      -- Write to RAM
      for i in 0 to (num_multipliers_g - 1) loop
        if (tap_valid_in = '1') and (i = tap_ram_select_in) then
          tap_ram(i)(tap_ram_address_in) <= tap_data_in;
        end if;
      end loop;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Data input
  ------------------------------------------------------------------------------
  -- Data taken when FIR is ready for more data, and the input data is valid.
  data_taken <= '1' when fir_ready = '1' and data_in_valid = '1' else '0';

  -- Delay the input valid signal in order to generate an output valid signal.
  valid_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        data_valid_pipe <= (others => '0');
      elsif enable = '1' then
        data_valid_pipe <= data_valid_pipe(data_valid_pipe'high - 1 downto 0) & data_taken;
      end if;
    end if;
  end process;

  -- Reset pipeline, activated on a rising edge of data_rst
  reset_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        reset_pipe(reset_pipe'high)            <= '1';
        reset_pipe(reset_pipe'high-1 downto 0) <= (others => '0');
      elsif enable = '1' then
        data_rst_r <= data_rst;         -- only care about rising edge
        reset_pipe <= reset_pipe(reset_pipe'high - 1 downto 0) & (not(data_rst_r) and data_rst);
      end if;
    end if;
  end process;

  -- Input data delay shift register
  data_pipe_p : process(clk)
  begin
    if rising_edge(clk) then
      if joint_rst = '1' then
        data_pipe <= (others => (others => '0'));
      elsif (enable = '1') and (data_taken = '1') then
        data_pipe(0) <= data_in;

        for i in 1 to (num_taps_g - 1) loop
          data_pipe(i) <= data_pipe(i - 1);
        end loop;
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Control Signals
  ------------------------------------------------------------------------------
  clear_accumulator <= data_valid_pipe(mac_delay_c - 1);
  mac_ready         <= data_valid_pipe(stages_c + mac_delay_c - 1);
  fir_ready         <= '1' when (stage_counter = (stages_c - 1)) and filter_resetting = '0' else '0';
  take              <= fir_ready;


  ------------------------------------------------------------------------------
  -- Operation 1 - Multiply Accumulate Signals
  ------------------------------------------------------------------------------
  -- Multiply each tap with a delayed version of the input signal.

  -- Step through each stage once per clock cycle
  -- Used to control multiplexers for MAC inputs
  stage_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        stage_counter <= stages_c - 1;
      elsif enable = '1' then
        if stage_counter = (stages_c - 1) then
          if data_in_valid = '1' then
            stage_counter <= 0;
          end if;
        else
          stage_counter <= stage_counter + 1;
        end if;
      end if;
    end if;
  end process;

  -- Select data and tap for multiplication
  -- A mux will be created for accessing the data pipe.
  -- Taps will be selected from the appropriate RAM
  current_stage_mux_p : process(clk)
  begin
    if rising_edge(clk) then
      if (enable = '1') then
        for i in 0 to (num_multipliers_g - 1) loop
          current_tap(i)  <= tap_ram(i)(stage_counter);
          current_data(i) <= data_pipe(stage_counter + (stages_c * i));
        end loop;
      end if;
    end if;
  end process;

  -- Performs the MAC operation
  multiply_accumulate_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        for i in 0 to (num_multipliers_g - 1) loop
          multiplier_result(i) <= current_tap(i) * current_data(i);
          if (clear_accumulator = '1') then
            accumulator(i) <= resize(multiplier_result(i), accumulator_size_c);
          else
            accumulator(i) <= accumulator(i) + resize(multiplier_result(i), accumulator_size_c);
          end if;
        end loop;
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Operation 2 - Serialised adder
  ------------------------------------------------------------------------------
  -- The result of each MAC unit needs to be added up.
  -- When there are multiple clock cycles between the output of the MAC units
  -- the addition can be done serially.

  -- If the addition can be done over multiple clock cycles, then generate the
  -- serialised adder unit.
  serial_adder_gen : if serial_adder_stages_c > 1 generate
    serial_adder_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          if mac_ready = '1' then
            -- Register all the accumulator output data
            for i in 0 to (num_multipliers_g - 1) loop
              serial_adder_buffer(i) <= resize(accumulator(i), adder_tree_size_c);
            end loop;
          else
            -- Create num_serial_adders_c serial adders
            for i in 0 to (num_serial_adders_c - 1) loop
              serial_adder_buffer(i*serial_adder_stages_c) <= serial_adder_buffer(i*serial_adder_stages_c) + current_adder_input(i);
            end loop;
          end if;
        end if;
      end if;
    end process;

    -- Serial adder stage counter
    -- Used to select which inputs are fed into the serial adder.
    serial_adder_stage_counter_p : process(clk)
    begin
      if rising_edge(clk) then
        if joint_rst = '1' then
          adder_stage_counter <= 0;
        elsif enable = '1' then
          if adder_stage_counter = (serial_adder_stages_c - 1) then
            adder_stage_counter <= 0;
          else
            -- Only start counter once mac is ready
            if (mac_ready = '1' and adder_stage_counter = 0) or
              adder_stage_counter /= 0 then
              adder_stage_counter <= adder_stage_counter + 1;
            end if;
          end if;
        end if;
      end if;
    end process;

    -- Select the correct input for each serial adder based on the current
    -- adder stage.
    current_adder_stage_mux_p : process(adder_stage_counter, serial_adder_buffer)
    begin
      for i in 0 to (num_serial_adders_c - 1) loop
        current_adder_input(i) <= serial_adder_buffer(adder_stage_counter + (serial_adder_stages_c * i));
      end loop;
    end process;
  end generate;

  -- If there is only a single clock cycle between MAC outputs, then the
  -- the addition of the MAC outputs must be done in a parallel pipelined
  -- manner. In this process the accumulator outputs are just registered.
  no_serial_adder_gen : if serial_adder_stages_c = 1 generate
    -- Store the accumulator data
    no_serial_adder_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          if mac_ready = '1' then
            for i in 0 to (num_multipliers_g - 1) loop
              serial_adder_buffer(i) <= resize(accumulator(i), adder_tree_size_c);
            end loop;
          end if;
        end if;
      end if;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- Operation 3 - Parallel Adder Tree Signals
  ------------------------------------------------------------------------------
  -- Creates a fully pipelined adder to add an array of numbers.

  -- Connect the first stage of the adder tree to outputs of the serialised
  -- adders.
  adder_tree_input_p : process(serial_adder_buffer)
  begin
    for i in 0 to (num_serial_adders_c - 1) loop
      adder_tree_buffer(0)(i) <= serial_adder_buffer(i*serial_adder_stages_c);
    end loop;
  end process;

  -- If there is more than one serial adder unit, generate the pipelined adder
  -- tree.
  adder_tree_gen : if adder_tree_stages_c > 0 generate
    adder_tree_stage_gen : for stage_gen in 1 to (adder_tree_stages_c) generate
      adder_tree_p : process(clk)
      begin
        if rising_edge(clk) then
          if enable = '1' then
            for i in 0 to ((2**((adder_tree_stages_c - (stage_gen-1)) - 1)) - 1) loop
              adder_tree_buffer(stage_gen)(i) <= adder_tree_buffer(stage_gen-1)(2*i) + adder_tree_buffer(stage_gen-1)((2*i) + 1);
            end loop;
          end if;
        end if;
      end process;
    end generate;
  end generate;

  -- Output signals
  data_valid_out <= data_valid_pipe(data_valid_pipe'high);
  data_out       <= adder_tree_buffer(adder_tree_stages_c)(0);

end behavioural;
