#!/usr/bin/env python3

# Python coefficient conversion script
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Script to convert hex files into verilog headers."""

import argparse
from ast import arg
import logging
from os.path import basename, splitext
from pathlib import Path


def parse_file_header(file):
    """Parse hex file header.

    Args:
        file (Pathlike): file to parse.

    Returns:
        dict: Dictionary containing information from the file header.
    """
    fields = ["Stage", "Bits", "NWide", "Offset", "Inv"]
    field_type = {"Stage": int, "Bits": int,
                  "NWide": int, "Offset": int, "Inv": bool}

    file_dict = {}
    with file as f:
        file_dict["Name"] = basename(file.name)
        for line in f.readlines():
            for field in fields:
                if field in line:
                    logging.debug(f"Found field \"{field}\" in line: {line}")
                    file_dict[field] = field_type[field](
                        line.partition(":")[-1].split()[0])
    return file_dict


def parse_hex_file_body(file):
    """Parse hex file coefficients (file body).

    Args:
        file (Pathlike): file to parse.

    Returns:
        list: Array of data from file.
    """
    data = []
    if file.closed:
        file = open(file.name, "r")
    with file as f:
        for line in f:
            if line.startswith("//"):
                continue
            data.append(line.strip())
    return data


def parse_files(input_files, output_file):
    """Parse files from hex coefficients to verilog headers.

    Args:
        input_files (list): List of files.
        output_file (pathlike): file to save all coefficients into.
    """
    if isinstance(output_file, Path):
        output_file = output_file.open(mode="w")
    with output_file as o:
        for f in input_files:
            logging.info(f"Parsing file: {f.name}")
            if isinstance(f, Path):
                f = f.open()
            fft_info = parse_file_header(f)
            fft_info["data"] = parse_hex_file_body(f)

            o.write("localparam [0:{0}] {1} = {{\n".format(
                (2 * fft_info["Bits"] * len(fft_info["data"])) - 1,
                splitext(fft_info["Name"])[0]
            ))
            for d in fft_info["data"][:-1]:
                o.write("  {}'h{}, \n".format(2*fft_info["Bits"], d))
            o.write("  {}'h{}\n".format(
                2*fft_info["Bits"], fft_info["data"][-1]))
            o.write("};\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Converts multiple hex files into a verilog header file")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Print additional logging")
    parser.add_argument("-o", "--output_file", type=argparse.FileType("w", encoding="UTF-8"),
                        default="coeff.vh",
                        help="File path for the verilog header file (.vh extension)")

    parser.add_argument("input_files",
                        type=argparse.FileType("r", encoding="UTF-8"),
                        nargs="+",
                        help="File path of coefficient files (.hex extension)")
    arguments = parser.parse_args()

    if arguments.verbose:
        logging.basicConfig(level="DEBUG")
    else:
        logging.basicConfig(level="WARNING")

    logging.debug(f"Saving output data to: {arguments.output_file}")
    parse_files(arguments.input_files, arguments.output_file)
