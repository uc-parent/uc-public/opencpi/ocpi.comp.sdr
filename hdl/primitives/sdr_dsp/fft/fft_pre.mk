# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# It looks like the autogeneration from OCPI only works for xsim, as
# synthesised targets are handled through variable manipulation as opposed to
# rules, therefore call out to python for generation of files.

# Makefile to generate the fft core files required for the HDL FFT worker.
#
# Uses the fftgen tool from ZipCPU's dblclockfft core generator. Will
# check the dblclockfft prerequisite is present (attempting to download
# it if not present), generate the fft cores required by the build
# configurations of the fast_fourier_transform_xs.hdl worker, and then
# add those generated files to the "SourceFiles" list.
#
# If the dblclockfft prerequisite is not present, a warning is emitted
# that the prerequisite could not be obtained, the SourceFiles list
# will *not* contain the generated files, and the build will complete
# but the FFT primitive will not be usable by components.

ifneq ($(MAKECMDGOALS),clean)
# Run generate.py to create the files in gen/fft required, and set
# the FFT_LENGTH_CORE_FILES to the list of files that should have been generated
OUT := $(shell python3 fft/generate.py)
FFT_LENGTH_CORE_FILES := $(shell python3 fft/generate.py --get-files)
endif

SourceFiles += $(FFT_LENGTH_CORE_FILES)

