-- HDL Implementation of a cdc_pulse_handshake
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sdr_util;
use sdr_util.sdr_util.cdc_single_bit;

entity cdc_pulse_handshake is
  generic (
    dst_registers_g : natural range 2 to 10 := 2);
  port (
    src_clk   : in  std_logic;
    src_reset : in  std_logic;
    src_pulse : in  std_logic;
    src_ready : out std_logic;
    dst_clk   : in  std_logic;
    dst_reset : in  std_logic;
    dst_pulse : out std_logic);
end entity cdc_pulse_handshake;

architecture rtl of cdc_pulse_handshake is

  signal src_acknowledge : std_logic;
  signal src_toggle      : std_logic;
  signal dst_toggle      : std_logic;
  signal dst_toggle_r    : std_logic;

begin

  -----------------------------------------------------------------------------
  -- Input Clock Domain (src_clk)
  -----------------------------------------------------------------------------

  -- Indicates when circuit is 'ready' to accept new input
  -- Following reception of a pulse on the Input clock domain it is held not
  -- ready until a toggle signal is transferred to the Output clock domain and
  -- acknowledged back to the Input clock domain
  src_ready <= src_acknowledge xnor src_toggle;

  -- Toggle when an input pulse has been received
  -- Hold steady whilst transferring to the Output clock domain to ensure
  -- capture irrespective of relative clock frequencies.
  input_enabled_toggle_p : process(src_clk)
  begin
    if (rising_edge(src_clk)) then
      if (src_reset = '1') then
        src_toggle <= '0';
      elsif (src_pulse = '1' and src_acknowledge = src_toggle) then
        src_toggle <= not src_toggle;
      end if;
    end if;
  end process;

  -- Synchronise dst_toggle to the input clock domain
  -- Opposite reset state to src_toggle ensures src_ready indicates
  -- not ready during reset.
  acknowledge_synchroniser_i : cdc_single_bit
    generic map (
      dst_registers_g        => dst_registers_g,
      src_register_g         => '0',
      register_reset_level_g => '1')
    port map (
      src_clk    => '0',
      src_reset  => '0',
      src_enable => '0',
      src_in     => dst_toggle,
      dst_clk    => src_clk,
      dst_reset  => src_reset,
      dst_out    => src_acknowledge);

  -----------------------------------------------------------------------------
  -- Output Clock Domain (dst_clk)
  -----------------------------------------------------------------------------

  -- Synchronise src_toggle to the output clock domain
  toggle_synchroniser_i : cdc_single_bit
    generic map (
      dst_registers_g        => dst_registers_g,
      src_register_g         => '0',
      register_reset_level_g => '0')
    port map (
      src_clk    => '0',
      src_reset  => '0',
      src_enable => '0',
      src_in     => src_toggle,
      dst_clk    => dst_clk,
      dst_reset  => dst_reset,
      dst_out    => dst_toggle);

  -- Delay to detect change of state of toggle
  level_detection_prep_reg_p : process(dst_clk)
  begin
    if (rising_edge(dst_clk)) then
      if (dst_reset = '1') then
        dst_toggle_r <= '0';
      else
        dst_toggle_r <= dst_toggle;
      end if;
    end if;
  end process;

  -- Pulse when toggle changes state
  -- Indicating on the Output clock domain that a pulse has been received on the
  -- Input clock domain
  dst_pulse <= dst_toggle xor dst_toggle_r;

  -----------------------------------------------------------------------------

end architecture rtl;
