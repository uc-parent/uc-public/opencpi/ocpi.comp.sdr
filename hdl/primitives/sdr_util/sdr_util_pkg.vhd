-- Utilities Primitive Package (and Body)
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules in
-- this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package sdr_util is

  -- For passing opcodes to/from protocol-less workers/primitives
  type opcode_t is (
    sample_op_e,
    time_op_e,
    sample_interval_op_e,
    flush_op_e,
    discontinuity_op_e,
    metadata_op_e
    );

  function slv_to_opcode(inslv : std_logic_vector) return opcode_t;

  function opcode_to_slv(inop : opcode_t; opcode_width : integer) return std_logic_vector;

  component cdc_single_bit is
    generic (
      dst_registers_g        : natural range 2 to 10 := 2;  -- Range 2 - 10 synchroniser registers for increasing MTBF
      src_register_g         : std_logic             := '0';  -- 0=no, 1=yes input register for improved MTBF
      register_reset_level_g : std_logic             := '0'   -- 0=low, 1=high
      );
    port (
      src_clk    : in  std_logic;  -- optional; required when src_register_g='1'
      src_reset  : in  std_logic;  -- optional; required when src_register_g='1'
      src_enable : in  std_logic;  -- optional; required when src_register_g='1'
      src_in     : in  std_logic;
      dst_clk    : in  std_logic;
      dst_reset  : in  std_logic;       -- optional; if not required, tie '0'
      dst_out    : out std_logic
      );
  end component;

  component cdc_fast_pulse_to_slow_sticky is
    port(
      -- fast clock domain
      fast_clk    : in  std_logic;
      fast_reset  : in  std_logic;
      fast_pulse  : in  std_logic;      -- pulse to be detected
      -- slow clock domain
      slow_clk    : in  std_logic;
      slow_reset  : in  std_logic;
      slow_clear  : in  std_logic;      -- clears sticky bit
      slow_sticky : out std_logic   -- sticky bit set when fast_pulse is high
      );
  end component;

  component cdc_pulse_handshake is
    generic (
      dst_registers_g : natural range 2 to 10 := 2
      );
    port (
      src_clk   : in  std_logic;
      src_reset : in  std_logic;
      src_pulse : in  std_logic;
      src_ready : out std_logic;
      dst_clk   : in  std_logic;
      dst_reset : in  std_logic;
      dst_pulse : out std_logic
      );
  end component;

  component cdc_bits_feedback is
    generic (
      width_g : positive := 1
      );
    port (
      src_clk    : in  std_logic;
      src_reset  : in  std_logic;
      src_enable : in  std_logic;
      src_ready  : out std_logic;
      src_in     : in  std_logic_vector(width_g-1 downto 0);
      dst_clk    : in  std_logic;
      dst_reset  : in  std_logic;
      dst_out    : out std_logic_vector(width_g-1 downto 0)
      );
  end component;

  component cdc_fifo is
    generic (
      width_g : natural                          := 1;
      depth_g : natural range 16 to natural'high := 16
      );
    port (
      src_clk     : in  std_logic;
      src_reset   : in  std_logic;
      src_enqueue : in  std_logic;
      src_in      : in  std_logic_vector(width_g-1 downto 0);
      src_full_n  : out std_logic;
      dst_clk     : in  std_logic;
      dst_reset   : in  std_logic;
      dst_dequeue : in  std_logic;
      dst_out     : out std_logic_vector(width_g-1 downto 0);
      dst_empty_n : out std_logic
      );
  end component;

  component counter_updown
    generic (
      counter_size_g : integer := 8
      );
    port (
      clk        : in  std_logic;
      reset      : in  std_logic;
      enable     : in  std_logic;
      direction  : in  std_logic;
      load_trig  : in  std_logic;
      load_value : in  unsigned(counter_size_g - 1 downto 0);
      length     : in  unsigned(counter_size_g - 1 downto 0);
      count_out  : out unsigned(counter_size_g - 1 downto 0)
      );
  end component;

  component triggered_sampler
    generic (
      -- This primitive assumes data is one sample per word for the purposes of
      -- maintaining sample time. Therefore data_width_g must be equal to
      -- the sample width where accurate time output messages are important.
      data_width_g           : positive;  -- Width of input and output data.
      capture_length_width_g : positive;  -- Width of capture_length
      little_endian_g        : boolean  -- Defines input/output message
                                        -- word order.
      );
    port (
      -- Control signals
      clk                     : in  std_logic;  -- Single clock.
      rst                     : in  std_logic;  -- Reset.
                                                -- Active high.
                                                -- Synch to rising clk.
      -- Properties
      bypass                  : in  std_logic;
      capture_length          : in  unsigned(capture_length_width_g - 1 downto 0);
      send_flush              : in  std_logic;
      capture_on_meta         : in  std_logic;
      capture_single_in       : in  std_logic;
      capture_single_written  : in  std_logic;
      capture_single_out      : out std_logic;
      capture_continuous      : in  std_logic;
      capture_in_progress_out : out std_logic;
      -- Trigger input stream.
      -- Uses the standard OpenCPI streaming data interface.
      trigger_som             : in  std_logic;
      trigger_ready           : in  std_logic;
      trigger_take            : out std_logic;
      -- Sample data input stream.
      -- Uses the standard OpenCPI streaming data interface.
      input_data              : in  std_logic_vector(data_width_g - 1 downto 0);
      input_opcode            : in  opcode_t;
      input_som               : in  std_logic;
      input_eom               : in  std_logic;
      input_eof               : in  std_logic;
      input_valid             : in  std_logic;
      input_ready             : in  std_logic;
      input_take              : out std_logic;
      -- Sample data output stream.
      -- Uses the standard OpenCPI streaming data interface.
      output_data             : out std_logic_vector(data_width_g - 1 downto 0);
      output_opcode           : out opcode_t;
      output_eom              : out std_logic;
      output_eof              : out std_logic;
      output_valid            : out std_logic;
      output_give             : out std_logic;
      output_ready            : in  std_logic
      );
  end component;

  component circular_buffer is
    generic (
      data_width_g         : integer := 8;
      buffer_depth_g       : integer := 16;
      relative_read_addr_g : boolean := true;
      rd_pipeline_g        : integer := 2
      );
    port (
      clk               : in  std_logic;
      reset             : in  std_logic;
      enable            : in  std_logic;
      -- Definable wrapping depth, if less than buffer_depth_g is desired.
      buffer_wrap_depth : in  unsigned(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0) := (others => '1');
      -- On a write enable the RAM is shifted
      wr_enable         : in  std_logic;
      wr_data           : in  std_logic_vector(data_width_g-1 downto 0);
      rd_addr           : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      rd_data           : out std_logic_vector(data_width_g-1 downto 0);
      tail_addr         : out std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      tail_data         : out std_logic_vector(data_width_g-1 downto 0)
      );
  end component;

  component ram is
    generic (
      data_width_g     : integer := 8;
      buffer_depth_g   : integer := 16;
      dual_port_g      : boolean := false;
      rd_pipeline_g    : integer := 2;
      write_first_g    : boolean := false;
      initialization_g : std_logic_vector
      );
    port (
      clk       : in  std_logic;
      enable    : in  std_logic;
      -- On a write enable the ram is shifted
      wr_enable : in  std_logic;
      wr_addr   : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      wr_data   : in  std_logic_vector(data_width_g-1 downto 0);
      -- rd_addr is only used when dual_port_g = true
      rd_addr   : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      rd_data   : out std_logic_vector(data_width_g-1 downto 0)
      );
  end component;

  component ram_simple_dual_port is
    generic (
      data_width_g    : integer                         := 8;
      buffer_depth_g  : integer                         := 16;
      read_pipeline_g : integer range 1 to integer'high := 3
      );
    port (
      clk             : in  std_logic;
      ram_enable      : in  std_logic;
      register_enable : in  std_logic;
      -- On a write enable the ram is shifted
      write_enable    : in  std_logic;
      write_address   : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      write_data      : in  std_logic_vector(data_width_g-1 downto 0);
      read_address    : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
      read_data       : out std_logic_vector(data_width_g-1 downto 0)
      );
  end component;

  component fifo_instant_read is
    generic (
      data_width_g    : integer                         := 8;
      buffer_depth_g  : integer                         := 16;
      read_pipeline_g : integer range 3 to integer'high := 4
      );
    port (
      clk               : in  std_logic;
      reset             : in  std_logic;
      -- Read/Write
      write_enable      : in  std_logic;
      write_data        : in  std_logic_vector(data_width_g-1 downto 0);
      read_enable       : in  std_logic;
      read_valid        : out std_logic;
      read_data         : out std_logic_vector(data_width_g-1 downto 0);
      -- Status
      fill_level        : out natural;
      full_count_clear  : in  std_logic;
      full_count        : out natural;
      full              : out std_logic;
      empty_count_clear : in  std_logic;
      empty_count       : out natural;
      empty             : out std_logic
      );
  end component;

end package sdr_util;

package body sdr_util is

  -- For passing opcodes to/from protocol-less workers/primitives
  function slv_to_opcode(inslv : std_logic_vector) return opcode_t is
  begin
    return opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  function opcode_to_slv(inop : opcode_t; opcode_width : integer) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(opcode_t'pos(inop), opcode_width));
  end function;

end package body sdr_util;
