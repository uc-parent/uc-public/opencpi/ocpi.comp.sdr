-- HDL Triggered Sampler Primitive
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Description : Sample-type independent triggered sampler
-- -----------------------------------------------------------------------------
-- Usage :
-- When triggered, this allows a prescribed number of samples to flow from
--   input to output.
-- It can be triggered in the following ways:
-- * When capture_continuous is high
-- * When capture_single_written is pulsed high with capture_single high
-- * Any metadata message is received on "input" port
-- * Any message is received on "trigger" port
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_util;
use sdr_util.sdr_util.all;

entity triggered_sampler is
  generic (
    -- This primitive assumes data is one sample per word for the purposes of
    -- maintaining sample time. Therefore data_width_g must be equal to
    -- the sample width where accurate time output messages are important.
    data_width_g           : positive;  -- Width of input and output data.
    capture_length_width_g : positive;  -- Width of capture_length
    little_endian_g        : boolean    -- Defines input/output message
                                        -- word order.
    );
  port (
    -- Control signals
    clk                     : in  std_logic;  -- Single clock.
    rst                     : in  std_logic;  -- Reset.
                                              -- Active high.
                                              -- Synch to rising clk.
    -- Properties
    bypass                  : in  std_logic;
    capture_length          : in  unsigned(capture_length_width_g - 1 downto 0);
    send_flush              : in  std_logic;
    capture_on_meta         : in  std_logic;
    capture_single_in       : in  std_logic;
    capture_single_written  : in  std_logic;
    capture_single_out      : out std_logic;
    capture_continuous      : in  std_logic;
    capture_in_progress_out : out std_logic;
    -- Trigger input stream.
    -- Uses the standard OpenCPI streaming data interface.
    trigger_som             : in  std_logic;
    trigger_ready           : in  std_logic;
    trigger_take            : out std_logic;
    -- Sample data input stream.
    -- Uses the standard OpenCPI streaming data interface.
    input_data              : in  std_logic_vector(data_width_g - 1 downto 0);
    input_opcode            : in  opcode_t;
    input_som               : in  std_logic;
    input_eom               : in  std_logic;
    input_eof               : in  std_logic;
    input_valid             : in  std_logic;
    input_ready             : in  std_logic;
    input_take              : out std_logic;
    -- Sample data output stream.
    -- Uses the standard OpenCPI streaming data interface.
    output_data             : out std_logic_vector(data_width_g - 1 downto 0);
    output_opcode           : out opcode_t;
    output_eom              : out std_logic;
    output_eof              : out std_logic;
    output_valid            : out std_logic;
    output_give             : out std_logic;
    output_ready            : in  std_logic
    );
end entity triggered_sampler;

architecture rtl of triggered_sampler is

  ------------------------------------------------------------------------------
  -- Types, Constants, Functions and Procedures
  ------------------------------------------------------------------------------

  -- Container for forward direction streaming interface.
  type stream_t is record
    data   : std_logic_vector(data_width_g - 1 downto 0);
    opcode : opcode_t;
    qsom   : std_logic;  -- already (anded with) ready and/or valid
    qeom   : std_logic;  -- already (anded with) ready and/or valid
    eof    : std_logic;
    valid  : std_logic;
    ready  : std_logic;
  end record;

  -- Size of the time and interval values (32.64)
  -- This is fixed in timed_sample_metadata-prot.xml
  constant time_seconds_bits_c  : positive := 32;
  constant time_fraction_bits_c : positive := 64;
  constant time_bits_c          : positive := time_seconds_bits_c + time_fraction_bits_c;

  -- Time and sample interval are received/transmitted over a number of words
  -- Calculate the number of words.
  constant time_words_c : positive := (time_bits_c + data_width_g - 1) / data_width_g;

  -- Implements word-based shift register for clocking wide data values
  -- to/from a narrow bus over a number of cycles.
  -- Shifts by newword'length bits
  -- Shifts newword in at lsb or msb end - according to endianness
  impure function shift_advance(
    constant shiftreg : std_logic_vector;
    constant newword  : std_logic_vector
    ) return std_logic_vector is
    variable shifted : std_logic_vector(shiftreg'range);
    variable word    : std_logic_vector(shiftreg'length - 1 downto 0);
  begin
    word := shiftreg;                   -- Normalise to "downto 0" range
    if shiftreg'length = newword'length then
      -- No shift required
      word := newword;
    elsif little_endian_g then
      -- Shift lsword first
      word := newword & word(word'high downto newword'length);
    else
      -- Shift msword first
      word := word(word'high-newword'length downto 0) & newword;
    end if;
    shifted := word;                    -- Revert result to original range
    return shifted;
  end function shift_advance;

  -- As above but shifts in the specified number of '-'.
  impure function shift_advance(
    constant shiftreg  : std_logic_vector;
    constant shiftsize : positive       -- Number of bits to shift by
    ) return std_logic_vector is
  begin
    return shift_advance(shiftreg, (shiftsize - 1 downto 0 => '-'));
  end function shift_advance;

  -- Extracts the output from a word-based shift register
  function shift_out(
    constant shiftreg  : std_logic_vector;
    constant shiftsize : positive       -- Number of bits to shift by
    ) return std_logic_vector is
    constant word    : std_logic_vector(shiftreg'length - 1 downto 0) := shiftreg;
    variable outword : std_logic_vector(shiftsize - 1 downto 0);
  begin
    if little_endian_g then
      -- Shift lsword first
      outword := word(outword'length - 1 downto 0);
    else
      -- Shift msword first
      outword := word(word'high downto word'high-outword'length + 1);
    end if;
    return outword;
  end function shift_out;

  -- FSM states
  type fsm_state_t is (
    STATE_IDLE,
    STATE_BYPASS,
    STATE_PAUSE,
    STATE_TIME,
    STATE_SAMPLE_INTERVAL,
    STATE_SAMPLES,
    STATE_FLUSH,
    STATE_DISCONTINUITY
    );

  ------------------------------------------------------------------------------
  -- Signals
  ------------------------------------------------------------------------------

  signal input_qsom           : std_logic;
  signal input_qeom           : std_logic;
  signal trigger_qsom         : std_logic;
  -- Trigger detection
  signal start_capture        : std_logic;
  signal capture_single       : std_logic;
  signal capture_queued       : std_logic;
  signal capture_pending      : std_logic;
  signal capture_busy         : std_logic;
  signal capture_done         : std_logic;
  signal capture_in_progress  : std_logic;
  -- Delay
  signal input_d              : stream_t;
  signal bypass_d             : std_logic;
  -- Sample time / sample interval
  signal sample_time          : unsigned(time_bits_c - 1 downto 0);
  signal sample_interval      : unsigned(time_bits_c - 1 downto 0);
  signal ishift               : std_logic_vector(time_bits_c - 1 downto 0);
  signal ishifted             : std_logic_vector(time_bits_c - 1 downto 0);
  signal got_time             : std_logic;
  signal got_sample_interval  : std_logic;
  signal in_enable            : std_logic;
  -- FSM
  signal fsm_state            : fsm_state_t;
  signal fsm_state_next       : fsm_state_t;
  signal oshift               : std_logic_vector(time_bits_c - 1 downto 0);
  signal oshift_load_time     : std_logic;
  signal oshift_load_interval : std_logic;
  signal ocount               : unsigned(capture_length'range);  -- Counts samples (or time words)
  signal ocount_load_time     : std_logic;
  signal ocount_load_length   : std_logic;
  signal ocount_decrement     : std_logic;
  -- Output mux
  signal muxout               : stream_t;
  signal out_enable           : std_logic;

begin

  -- Extra qualification on som and eom inputs
  input_qsom   <= input_som and input_ready;
  input_qeom   <= input_eom and input_ready;
  trigger_qsom <= trigger_som and trigger_ready;

  ------------------------------------------------------------------------------
  -- Trigger Detect
  ------------------------------------------------------------------------------
  detect_b : block is
  begin

    -- This block generates a pulse to start a capture process.
    -- The pulse is only generated when a capture is not already in progress.
    -- Triggers that occur during a capture are queued.

    ----------------------------------------------------------------------------
    -- Capture trigger detection
    ----------------------------------------------------------------------------
    detect_p : process(clk) is
    begin
      if rising_edge(clk) then

        -- Sync reset
        if rst = '1' then

          bypass_d        <= '0';
          capture_single  <= '0';
          capture_pending <= '0';
          capture_queued  <= '0';

        else

          -- Implement capture_single register
          -- Set here - may be cleared below
          if capture_single_written = '1' then
            capture_single <= capture_single_in;
          end if;

          -- Here we detect trigger events on any of the sources
          --  trigger port
          --  input port (meta)
          --  capture_single register
          --  capture_continuous register

          -- Terminology:
          --  pending - capture accepted - waiting for first sample.
          --      if caused by single that reg has now been cleared.
          --  busy - capture accepted and first sample seen
          --  in-progress - capture pending or busy
          --  queued - next capture queued by trigger while "in-progress"

          -- Trigger detection
          if out_enable = '1' then

            -- Detect start of any message on trigger port
            if trigger_qsom = '1' then
              if capture_in_progress = '1' then
                capture_queued <= '1';
              else
                capture_pending <= '1';
              end if;
            end if;

            -- Detect start of metadata message on input port
            --   but only if required by capture_on_meta
            if input_qsom = '1' and input_opcode = metadata_op_e and capture_on_meta = '1' then
              if capture_in_progress = '1' then
                capture_queued <= '1';
              else
                capture_pending <= '1';
              end if;
            end if;

            -- Detect a capture_single trigger.
            -- Note that the single_capture signal is a delayed version of the
            --   property, so we need to check for register writes on the
            --   current cycle.
            -- Trigger when register is high AND has not just been set low.
            if (capture_single = '1' and (capture_single_written = '0' or capture_single_in = '1')) or
              -- OR the register has just been set high.
              (capture_single_written = '1' and capture_single_in = '1')
            then
              if capture_in_progress = '0' then
                capture_pending <= '1';
                capture_single  <= '0';
              end if;
            end if;

            -- Detect capture_continuous.
            if capture_continuous = '1' then
              if capture_in_progress = '0' then
                capture_pending <= '1';
              end if;
            end if;

            -- Pending is cleared on start
            if start_capture = '1' then
              capture_pending <= '0';
            end if;

            -- Once capture is complete, start any queued capture
            if capture_queued = '1' and capture_in_progress = '0' then
              capture_pending <= '1';
              capture_queued  <= '0';
            end if;

            -- Delay bypass with data and triggers
            bypass_d <= bypass;

          end if;

          -- Clear all current, pending and queued captures on bypass
          if bypass = '1' then
            capture_single  <= '0';
            capture_pending <= '0';
            capture_queued  <= '0';
          end if;
        end if;

      end if;
    end process detect_p;

    ----------------------------------------------------------------------------
    -- Generate FSM start pulse
    ----------------------------------------------------------------------------

    start_capture <=
      '1' when (
        bypass_d = '0' and                -- Never start in bypass mode
        out_enable = '1' and              -- Only start when fsm is ready
        capture_busy = '0' and            -- Only start when not already busy
        input_d.qsom = '1' and            -- Align with start of ...
        input_d.opcode = sample_op_e and  -- ... sample message
        capture_pending = '1'
        ) else
      '0';

  end block detect_b;

  ------------------------------------------------------------------------------
  -- Sample Delay
  ------------------------------------------------------------------------------
  delay_ctrl_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- Sync reset
      if rst = '1' then
        -- Invalidate all meta and data signals
        input_d.qsom  <= '0';
        input_d.qeom  <= '0';
        input_d.eof   <= '0';
        input_d.valid <= '0';
        input_d.ready <= '0';
      elsif in_enable = '1' then
        input_d.qsom  <= input_qsom;
        input_d.qeom  <= input_qeom;
        input_d.eof   <= input_eof;
        input_d.valid <= input_valid;
        input_d.ready <= input_ready;
      end if;
    end if;
  end process delay_ctrl_p;

  delay_data_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- There is no need to reset data and opcode since they are
      -- qualified by the meta signals
      if in_enable = '1' then
        input_d.data   <= input_data;
        input_d.opcode <= input_opcode;
      end if;
    end if;
  end process delay_data_p;

  ------------------------------------------------------------------------------
  -- Data input shift register
  ------------------------------------------------------------------------------

  -- Shift register for deserialising time and interval values which
  -- may be received over several cycles.

  ishift_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- Input data shift register
      -- This runs all the time and we just grab the output when needed
      if in_enable = '1' and input_d.valid = '1' then
        ishift <= ishifted;
      end if;
    end if;
  end process ishift_p;

  ishifted <= shift_advance(ishift, input_d.data);

  ------------------------------------------------------------------------------
  -- Sample Time counter & Sample Interval register
  ------------------------------------------------------------------------------

  -- Maintain time counter
  time_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- There is no need to reset sample_time since it is
      -- qualified by got_sample_interval
      if in_enable = '1' then
        -- Maintain sample time counter
        -- It always contains the time of the next sample
        -- It is aligned with the output of the delay line

        -- On end of time message, save the shifted value
        if input_d.qeom = '1' and input_d.opcode = time_op_e then
          sample_time <= unsigned(ishifted);

        -- On sample in, advance the counter
        elsif input_d.valid = '1' and input_d.opcode = sample_op_e then
          -- Advance time counter
          -- Note, this will need to change if data_width_g /= sample width
          --   is to be supported
          sample_time <= sample_time + sample_interval;
        end if;
      end if;
    end if;
  end process time_p;

  -- Store latest sample interval
  sample_interval_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- There is no need to reset sample_interval since it is
      --   qualified by got_sample_interval
      if in_enable = '1' then
        -- Maintain latest sample interval
        -- It is aligned with the output of the delay line

        -- On end of interval message, save the shifted value
        if input_d.qeom = '1' and input_d.opcode = sample_interval_op_e then
          sample_interval <= unsigned(ishifted);
        end if;
      end if;
    end if;
  end process sample_interval_p;

  -- Maintain status of the time and sample interval signals
  time_status_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- Sync reset
      if rst = '1' then
        -- Invalidate time and sample interval
        got_time            <= '0';
        got_sample_interval <= '0';
      elsif in_enable = '1' then
        -- Maintain sample time and interval status
        if input_d.qeom = '1' and input_d.opcode = time_op_e then
          -- Have now stored a valid time
          got_time <= '1';
        elsif input_d.qeom = '1' and input_d.opcode = sample_interval_op_e then
          -- Have now stored a valid interval
          got_sample_interval <= '1';
        elsif input_d.qeom = '1' and input_d.opcode = discontinuity_op_e then
          -- Discontinuity messages invalidate time since we no longer know
          --  what the next sample time will be
          got_time <= '0';
        elsif input_d.valid = '1' and input_d.opcode = sample_op_e then
          -- Time becomes invalid on samples if sample interval is unknown
          if got_sample_interval = '0' then
            got_time <= '0';
          end if;
        end if;
      end if;
    end if;
  end process time_status_p;

  ------------------------------------------------------------------------------
  -- FSM
  ------------------------------------------------------------------------------

  -- Generate up-stream enable (take) signal
  -- Take when not generating our own messages
  in_enable <= '1' when
               out_enable = '1' and     -- downstream enable
               (fsm_state = STATE_IDLE or fsm_state = STATE_SAMPLES or fsm_state = STATE_BYPASS or bypass_d = '1')
               and start_capture = '0'  -- not about to start a capture
               else '0';

  -- Generate next state and output mux
  fsm_next_p : process (fsm_state, input_d, bypass_d, start_capture, got_time,
                        got_sample_interval, ocount, oshift, send_flush) is
  begin

    -- Default path is from input
    muxout     <= input_d;
    muxout.eof <= '0';                  -- eof handled explicitly

    -- Default to remain in the current state
    fsm_state_next <= fsm_state;

    -- Default shift and count control signals
    oshift_load_time     <= '0';
    oshift_load_interval <= '0';
    ocount_load_time     <= '0';
    ocount_load_length   <= '0';
    ocount_decrement     <= '0';

    -- Determine next state and mux output
    case fsm_state is
      when STATE_IDLE =>

        -- Waiting for a capture to start

        if input_d.eof = '1' then
          -- Forward the message and stay in this state
          muxout.eof   <= '1';
          muxout.ready <= '0';
          muxout.valid <= '0';

        elsif bypass_d = '1' and input_d.qsom = '1' then
          -- Forward any message
          -- Move to bypass state to finish sending of more than 1 word.
          if input_d.qeom = '0' then
            fsm_state_next <= STATE_BYPASS;
          end if;

        elsif start_capture = '1' then
          -- This will always be aligned with SOM of sample message.

          -- Don't output sample message yet
          muxout.ready <= '0';
          muxout.valid <= '0';

          fsm_state_next <= STATE_PAUSE;

        elsif input_d.opcode = flush_op_e or
          input_d.opcode = metadata_op_e then

          -- Forward meta and flush during idle
          null;

        else
          -- Don't forward time, sample interval or discontinuity
          muxout.ready <= '0';
          muxout.valid <= '0';
        end if;

      when STATE_BYPASS =>
        -- Forward all messages
        -- Return on eom
        if input_d.qeom = '1' then
          fsm_state_next <= STATE_IDLE;
        end if;

      when STATE_PAUSE =>

        -- Single cycle pause to allow sample time calc to complete.
        -- Necessary when previous message was a sample message.
        muxout.ready <= '0';
        muxout.valid <= '0';

        -- Straight to next state
        if got_time = '1' then
          fsm_state_next   <= STATE_TIME;
          oshift_load_time <= '1';
          ocount_load_time <= '1';
        elsif got_sample_interval = '1' then
          fsm_state_next       <= STATE_SAMPLE_INTERVAL;
          oshift_load_interval <= '1';
          ocount_load_time     <= '1';
        else
          fsm_state_next     <= STATE_SAMPLES;
          ocount_load_length <= '1';
        end if;

      when STATE_TIME =>

        -- Generate time message.
        muxout.ready  <= '1';
        muxout.valid  <= '1';
        muxout.qeom   <= '0';           -- May be overriden below
        muxout.opcode <= time_op_e;
        muxout.data   <= shift_out(oshift, muxout.data'length);

        -- Count words and advance on last
        if ocount = 1 then
          muxout.qeom <= '1';

          if got_sample_interval = '1' then
            fsm_state_next       <= STATE_SAMPLE_INTERVAL;
            oshift_load_interval <= '1';
            ocount_load_time     <= '1';
          else
            fsm_state_next     <= STATE_SAMPLES;
            ocount_load_length <= '1';
          end if;

        else
          ocount_decrement <= '1';
        end if;

        -- Ignore EOF on input since we are shift_out

      when STATE_SAMPLE_INTERVAL =>

        -- Generate interval message
        muxout.ready  <= '1';
        muxout.valid  <= '1';
        muxout.qeom   <= '0';           -- May be overriden below
        muxout.opcode <= sample_interval_op_e;
        muxout.data   <= shift_out(oshift, muxout.data'length);

        -- Count words and advance on last
        if ocount = 1 then
          muxout.qeom        <= '1';
          fsm_state_next     <= STATE_SAMPLES;
          ocount_load_length <= '1';
        else
          ocount_decrement <= '1';
        end if;

        -- Ignore EOF on input since we are now capturing.
        -- The EOF will be generated later.

      when STATE_SAMPLES =>

        -- Forward all messages and count samples

        if input_d.eof = '1' then
          -- There will be no more samples
          -- Abort capture and return to idle
          -- No need to generate flush or discontinuity
          muxout.eof     <= '1';
          muxout.ready   <= '0';
          muxout.valid   <= '0';
          fsm_state_next <= STATE_IDLE;

        elsif bypass_d = '1' then
          -- Forward any message - even samples which exceed the cap length
          --   on eom, return to idle - don't send flush/discontinuity
          if input_d.qeom = '1' then
            fsm_state_next <= STATE_IDLE;
          end if;

        elsif input_d.valid = '1' and input_d.opcode = sample_op_e then

          -- Got a sample.
          -- Decrement counter and advance on last
          ocount_decrement <= '1';
          if ocount = 1 then
            muxout.qeom <= '1';
            if send_flush = '1' then
              fsm_state_next <= STATE_FLUSH;
            else
              fsm_state_next <= STATE_DISCONTINUITY;
            end if;
          end if;

        end if;

      when STATE_FLUSH =>

        -- Generate single cycle flush
        muxout.ready   <= '1';
        muxout.valid   <= '0';
        muxout.qeom    <= '1';
        muxout.opcode  <= flush_op_e;
        fsm_state_next <= STATE_DISCONTINUITY;

        -- Ignore EOF on input.
        -- The EOF will be generated later.

      when STATE_DISCONTINUITY =>

        muxout.ready   <= '1';
        muxout.valid   <= '0';
        muxout.qeom    <= '1';
        muxout.opcode  <= discontinuity_op_e;
        fsm_state_next <= STATE_IDLE;

        -- Ignore EOF on input.
        -- The EOF will be generated later.

    end case;

  end process fsm_next_p;

  -- FSM and output registers
  fsm_ctrl_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- Sync reset
      if rst = '1' then
        fsm_state    <= STATE_IDLE;
        output_give  <= '0';
        output_valid <= '0';

      elsif out_enable = '1' then

        -- FSM
        fsm_state <= fsm_state_next;

        -- Register mux output
        output_give  <= muxout.ready;
        output_valid <= muxout.valid;
        output_eof   <= muxout.eof;

      end if;
    end if;
  end process fsm_ctrl_p;

  fsm_data_p : process(clk) is
  begin
    if rising_edge(clk) then
      -- There is no need to reset these signals since they will be initialised
      --   before use or are qualified by one of the previously reset signals
      if out_enable = '1' then

        -- Output data shift register
        -- Used for shifting time and interval messages
        if oshift_load_time = '1' then
          oshift <= std_logic_vector(sample_time);
        elsif oshift_load_interval = '1' then
          oshift <= std_logic_vector(sample_interval);
        else
          oshift <= shift_advance(oshift, output_data'length);
        end if;

        -- Output counter
        if ocount_load_length = '1' then
          ocount <= capture_length;
        elsif ocount_load_time = '1' then
          ocount <= to_unsigned(time_words_c, ocount'length);
        elsif ocount_decrement = '1' then
          ocount <= ocount - 1;
        end if;

        -- Register mux output
        output_eom    <= muxout.qeom;
        output_opcode <= muxout.opcode;
        output_data   <= muxout.data;

      end if;
    end if;
  end process fsm_data_p;

  -- Generate busy signal
  capture_busy <= '1' when (fsm_state /= STATE_IDLE and fsm_state /= STATE_BYPASS) else '0';

  -- This pulses high on completion of capture
  capture_done <= '1' when (fsm_state_next = STATE_IDLE and fsm_state = STATE_DISCONTINUITY and out_enable = '1') else '0';

  -- In progress == "capture accepted and waiting for or transferring samples"
  -- Include "not capture_done" so in_progress falls as FSM goes back to idle.
  -- This ensures capture_continuous will capture back-to-back packets.
  capture_in_progress <= (capture_busy or capture_pending) and not capture_done;

  ------------------------------------------------------------------------------
  -- Port mappings
  ------------------------------------------------------------------------------

  -- Properties
  capture_single_out      <= capture_single;
  capture_in_progress_out <= capture_in_progress;

  -- Take inputs when pipe advances.
  input_take   <= in_enable;
  trigger_take <= out_enable;

  out_enable <= output_ready;

end architecture rtl;
