.. triggered_sampler documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _triggered_sampler-primitive:


Triggered Sampler (``triggered_sampler``)
=========================================
Implements the core functionality of the ``triggered_sampler_*`` HDL workers.

Design
------
This primitive implements the core functionality of the ``triggered_sampler_*`` HDL workers, with the workers providing only a very thin data type conversion layer. Because there is no sample data processing, this single primitive can be used for all protocol variants.

To minimise duplication of logic in the individual workers, this primitive is designed to handle the full OpenCPI streaming data interfaces, and contains logic for handling all opcodes. There is no need for any logic to be implemented in the triggered_sampler workers that use this primitive; they are required only to map the ports and remap the protocol specific opcodes.

Implementation
--------------
The structure of the primitive is shown in the following figure, with descriptions for the main blocks below.

.. _triggered_sampler_primitive_design-diagram:

.. figure:: triggered_sampler_primitive_design.svg
   :alt: Structure of triggered sampler primitive.
   :align: center

   Triggered Sampler Block Diagram

Flow Control
~~~~~~~~~~~~
Note that flow of data in all paths is governed by ``output_ready`` signal, which is also forwarded to the input and trigger take signals, thus passing on any backpressure to the upstream logic. While the primitive is generating additional time, sample_interval, flush and discontinuity messages associated with each capture, backpressure is applied to the input interface through the take signal.

Trigger Detect
~~~~~~~~~~~~~~
This block is responsible for detecting trigger events on the data and property input and instructing the control FSMs to start a capture operation.

The specific trigger conditions are:

 * ``capture_single`` = ``true``

 * ``capture_continuous`` = ``true``

 * SOM on trigger interface.

 * SOM of metadata message on input interface.

If one or more triggers are detected during a capture, this block will ensure another capture is started once the current one is complete.

The logic ensures that when a trigger SOM aligns with an input SOM, the capture begins with that sample.

An EOF on the trigger interface immediately prevents further triggers by that port.

An EOF on the input interface cancels any pending capture and immediately prevents further captures.

Delay
~~~~~
This delays all input messages by one cycle to account for trigger detection logic latency.

Sample Interval
~~~~~~~~~~~~~~~
This primitive detects and decodes sample_interval messages on the input, storing the received value for new samples and also forwarding to the output.

Until a sample_interval message is received, the interval is marked as unknown, with got_interval='0', to prevent the invalid value being output by the FSM.

Sample Time
~~~~~~~~~~~
This primitive maintains the sample time of the next output sample.

It detects and decodes time messages on the input, initialising the counter to the received value.

Until a time message is received, the sample_time is marked as unknown, with got_time='0', to prevent invalid values being output by the FSM.

The sample_time is also marked as unknown on receipt of a discontinuity on the input.

For each input sample, the counter is incremented by "sample_interval", but if the sample_interval is unknown at that point, the sample_time counter is marked as unknown.

Control FSM
~~~~~~~~~~~
This controls the capture process, generation of new capture framing messages and forwarding of messages from input to output.

It monitors the capture_start signal and input_d (delayed) input.

It has the following state flow:

.. _triggered_sampler_primitive_fsm_states-diagram:

.. figure:: triggered_sampler_primitive_fsm_states.svg
   :alt: State diagram for triggered sampler primitive control FSM
   :align: center

   Triggered Sampler FSM State Flow

The actions performed in each state are:

 * IDLE

     * On EOF in,

         * Forwards EOF to output.

     * In bypass,

         * Forwards all messages to output.

         * Go to BYPASS state for non-zero length messages.

     * In non-bypass,

         * Forwards metadata and flush messages.

         * On start_capture, go to PAUSE.

 * BYPASS

     * Finish forwarding the current message.

     * Return to IDLE on EOF.

     * This state ensures whole messages are forwarded in the case that bypass is de-asserted part way through a message.

 * PAUSE

     * Single cycle pause to allow sample_time to update to complete.

     * Nothing is written to output.

     * Advance to TIME, SAMPLE INTERVAL or SAMPLES (depending on whether time and interval are known).

 * TIME

     * Shift out the current sample_time value over one or more cycles (depending on bus width) to generate a time message.

     * When complete, advance to SAMPLE INTERVAL or SAMPLES (depending on whether interval is known).

 * SAMPLE INTERVAL

     * Shift out the current sample_time value over one or more cycles (depending on bus width) to generate a sample_interval message.

     * When complete, go to SAMPLES.

 * SAMPLES

     * On EOF in,

         * Forwards EOF to output.

         * Abort capture (since no more samples are coming) and Go to IDLE.

     * On bypass,

         * Forward any message to output.

         * On EOM, return to IDLE.

     * Otherwise,

         * Forward all messages to output.

         * Count the number of samples forwarded.

         * Once the required capture_length has been reached, go to FLUSH or DISCONTINUITY, depending on the state of the send_flush property.

 * FLUSH

     * Write single work zero-length flush message to output.

     * Go to DISCONTINUITY.

 * DISCONTINUITY

     * Write single work zero-length discontinuity message to output.

     * Return to IDLE.

The in_enable signal is forced low while the FSM is in PAUSE, TIME, SAMPLE INTERVAL, FLUSH or DISCONTINUITY state, and is therefore unable to process (pass-on or discard) input messages.

Interface
---------

Types
~~~~~
This type is defined in the ``sdr_util_pkg`` package:

 * ``opcode_t`` (``enum``): For passing opcodes to/from the primitive.

     * ``sample_op_e``

     * ``time_op_e``

     * ``sample_interval_op_e``

     * ``flush_op_e``

     * ``discontinuity_op_e``

     * ``metadata_op_e``


Workers will need to translate between their specific opcode type and this generic primitive type.

Generics
~~~~~~~~

 * ``data_width_g`` (positive): Width of the data input and output signals.

 * ``capture_length_width_g`` (positive): Width of the capture_length property.

 * ``little_endian_g`` (boolean): Defines input/output message word order.

Ports
~~~~~

 * Control signals

     * ``clk`` (``std_logic``), in: Single clock.

     * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of ``clk``.

 * Properties

     * ``bypass`` (``std_logic``), in:

     * ``capture_length`` (unsigned, capture_length_width_g bits), in:

     * ``send_flush`` (``std_logic``), in:

     * ``capture_on_meta`` (``std_logic``), in:

     * ``capture_single_in`` (``std_logic``), in:

     * ``capture_single_written`` (``std_logic``), in:

     * ``capture_single_out`` (``std_logic``), out:

     * ``capture_continuous`` (``std_logic``), in:

     * ``capture_in_progress_out`` (``std_logic``), out:

 * Trigger input stream. Uses the standard OpenCPI streaming data interface.

     * ``trigger_som`` (``std_logic``), in:

     * ``trigger_ready`` (``std_logic``), in:

     * ``trigger_take`` (``std_logic``), out:

 * Sample data input stream. Uses the standard OpenCPI streaming data interface.

     * ``input_data`` (``std_logic_vector``, data_width_g bits), in:

     * ``input_opcode`` (opcode_t), in:

     * ``input_som`` (``std_logic``), in:

     * ``input_eom`` (``std_logic``), in:

     * ``input_eof`` (``std_logic``), in:

     * ``input_valid`` (``std_logic``), in:

     * ``input_ready`` (``std_logic``), in:

     * ``input_take`` (``std_logic``), out:

 * Sample data output stream. Uses the standard OpenCPI streaming data interface.

     * ``output_data`` (``std_logic_vector``, data_width_g bits), out:

     * ``output_opcode`` (opcode_t), out:

     * ``output_som`` (``std_logic``), out:

     * ``output_eom`` (``std_logic``), out:

     * ``output_eof`` (``std_logic``), out:

     * ``output_valid`` (``std_logic``), out:

     * ``output_give`` (``std_logic``), out:

     * ``output_ready`` (``std_logic``), in:

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``triggered_sampler`` are:

 * Supports version 2 interfaces only.

 * Only tested in little endian configuration.
