.. circular_buffer documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _circular_buffer-primitive:


Circular Buffer (``circular_buffer``)
=====================================
A circular buffer implementation, which allows independent reading addressing

Design
------
This is a circular buffer that will allow the data to be written, while simultaneously allowing a separate address to be read.

Implementation
--------------
Inputs are read into the primitive on the rising edge of ``clk``. Outputs are set on rising edge of ``clk``. ``reset`` provides a synchronous, active high, reset signal to the address control (but does not guarantee a memory reset).

The write address is set to zero on a reset.

The primitive is designed as read first, therefore, the read data line will be updated before the new value is written into RAM.

The primitive is capable of relative addressing, where the address given is the offset of the current write pointer; or absolute addressing, where the read address given is used on the RAM directly.

The primitive provides the tail information. This relates to the current next write pointer, and is provided in case multiple buffers need to be connected together.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g``         (``integer``): The bit width of the RAM and data ports.

 * ``buffer_depth_g``       (``integer``): The RAM-depth of the component (in words).

 * ``relative_read_addr_g`` (``boolean``): True when read address should be relative, False for absolute read addressing.

 * ``rd_pipeline_g``        (``integer``): Read data pipeline length.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Primitive begins to count each rising clock edge when high.

 * ``buffer_wrap_depth`` (``unsigned``), in : The buffer depth to use before wrapping (if less than the ``buffer_depth_g``).

 * ``wr_enable``  (``std_logic``), in : Data on ``wr_data`` is written to the RAM when asserted.

 * ``wr_data``    (``std_logic_vector``), in : Data to write into RAM.

 * ``rd_addr``    (``std_logic_vector``), in : Address to read from within the RAM, The exact interpretation of this data depends upon the ``relative_read_addr_g`` generic.

 * ``rd_data``    (``std_logic_vector``), out: Data read from the RAM (located at ``rd_addr``).

 * ``tail_addr``  (``std_logic_vector``), out: Address of the write pointer within the RAM block

 * ``tail_data``  (``std_logic_vector``), out: Data present at the write pointer.


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``circular_buffer`` are:

 * None
