-- HDL Implementation of a circular buffer
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity circular_buffer is
  generic (
    data_width_g         : integer := 8;
    buffer_depth_g       : integer := 16;
    relative_read_addr_g : boolean := true;
    rd_pipeline_g        : integer := 2
    );
  port (
    clk    : in std_logic;
    reset  : in std_logic;
    enable : in std_logic;

    -- Definable wrapping depth, if less than buffer_depth_g is desired.
    buffer_wrap_depth : in unsigned(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0) := (others => '1');

    -- On a write enable the RAM is shifted
    wr_enable : in  std_logic;
    wr_data   : in  std_logic_vector(data_width_g-1 downto 0);
    -- The read data can have its address changed, this is the distance from
    -- the last written data word, (so 0=last written word, 1=second to last
    -- word, buffer_depth-1=current_write_pointer (same as tail))
    rd_addr   : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    rd_data   : out std_logic_vector(data_width_g-1 downto 0);
    -- Tail data always points to the word which has just been over-written by
    -- write point move. (can be used for connecting multiple buffers)
    tail_addr : out std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    tail_data : out std_logic_vector(data_width_g-1 downto 0)
    );
end circular_buffer;

architecture rtl of circular_buffer is

  function min(left, right : integer) return integer is
  begin
    if left < right then
      return left;
    else
      return right;
    end if;
  end function;

  type ram_t is array(0 to buffer_depth_g-1) of std_logic_vector(data_width_g-1 downto 0);
  signal ram : ram_t := (others => (others => '0'));

  signal ram_boundary : integer range 0 to buffer_depth_g-1;
  signal wr_addr      : integer range 0 to buffer_depth_g-1;
  signal rd_addr_i    : integer range 0 to buffer_depth_g-1;
  signal rd_addr_r    : integer range 0 to buffer_depth_g-1;
  signal tail_data_i  : std_logic_vector(data_width_g-1 downto 0);


begin
  ram_boundary <= min(to_integer(buffer_wrap_depth), (buffer_depth_g-1));

  rel_rd_addr_gen : if relative_read_addr_g generate
    rd_offset_p : process(rd_addr, wr_addr, ram_boundary)
      variable rd_addr_v : integer range -(buffer_depth_g-2) to buffer_depth_g-1;
    begin
      rd_addr_v := (wr_addr-1) - to_integer(unsigned(rd_addr));
      if rd_addr_v < 0 then
        rd_addr_i <= ram_boundary + 1 + rd_addr_v;
      else
        rd_addr_i <= rd_addr_v;
      end if;
    end process;
  end generate;
  abs_rd_addr_gen : if not relative_read_addr_g generate
    rd_addr_i <= to_integer(unsigned(rd_addr));
  end generate;

  address_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' then
        rd_addr_r <= rd_addr_i;
      end if;

      if reset = '1' then
        wr_addr <= 0;
      elsif enable = '1' and wr_enable = '1' then
        if wr_addr >= ram_boundary then
          wr_addr <= 0;
        else
          wr_addr <= wr_addr + 1;
        end if;
      end if;
    end if;
  end process;

  tail_addr <= std_logic_vector(to_unsigned(wr_addr, tail_addr'length));

  ram_p : process(clk)
  begin
    if rising_edge(clk) then
      if enable = '1' and wr_enable = '1' then
        tail_data_i  <= ram(wr_addr);
        ram(wr_addr) <= wr_data;
      end if;
    end if;
  end process;

  pipe_0_g : if rd_pipeline_g = 0 generate
    rd_data   <= ram(rd_addr_r);
    tail_data <= tail_data_i;
  end generate;

  pipe_1_g : if rd_pipeline_g = 1 generate
    signal reg_rd : std_logic_vector(rd_data'range);
    signal reg_tl : std_logic_vector(rd_data'range);
  begin
    ram_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          reg_rd <= ram(rd_addr_r);
          reg_tl <= tail_data_i;
        end if;
      end if;
    end process;
    rd_data   <= reg_rd;
    tail_data <= reg_tl;
  end generate;

  pipe_2_g : if rd_pipeline_g > 1 generate
    type reg_t is array(0 to rd_pipeline_g-1) of std_logic_vector(rd_data'range);
    signal reg_rd : reg_t;
    signal reg_tl : reg_t;
  begin
    ram_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          for n in 1 to rd_pipeline_g-1 loop
            reg_rd(n) <= reg_rd(n-1);
            reg_tl(n) <= reg_tl(n-1);
          end loop;
          reg_rd(0) <= ram(rd_addr_r);
          reg_tl(0) <= tail_data_i;
        end if;
      end if;
    end process;
    rd_data   <= reg_rd(reg_rd'high);
    tail_data <= reg_tl(reg_tl'high);
  end generate;

end rtl;
