-- HDL Implementation of a cdc_bits_feedback
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sdr_util;
use sdr_util.sdr_util.cdc_pulse_handshake;

entity cdc_bits_feedback is
  generic (
    width_g : positive := 1);
  port (
    src_clk    : in  std_logic;
    src_reset  : in  std_logic;
    src_enable : in  std_logic;
    src_ready  : out std_logic;
    src_in     : in  std_logic_vector(width_g-1 downto 0);
    dst_clk    : in  std_logic;
    dst_reset  : in  std_logic;
    dst_out    : out std_logic_vector(width_g-1 downto 0)
    );
end entity cdc_bits_feedback;

architecture rtl of cdc_bits_feedback is

  signal src_acknowledge : std_logic;
  signal src_data        : std_logic_vector(width_g-1 downto 0);
  signal dst_cdc_data    : std_logic_vector(width_g-1 downto 0);
  signal dst_load        : std_logic;

begin

  -----------------------------------------------------------------------------
  -- Input Clock Domain (src_clk)
  -----------------------------------------------------------------------------

  -- Capture input data bus
  -- Hold steady whilst transferring to the Output clock domain to prevent
  -- metastability
  src_input_reg_p : process(src_clk)
  begin
    if (rising_edge(src_clk)) then
      if (src_reset = '1') then
        src_data <= (others => '0');
      elsif (src_enable = '1' and src_acknowledge = '1') then
        src_data <= src_in;
      end if;
    end if;
  end process;

  -- Ready to receive further input changes
  src_ready <= src_acknowledge;

  -----------------------------------------------------------------------------
  -- Output Clock Domain (dst_clk)
  -----------------------------------------------------------------------------

  -- Synchronise enable pulse to the output clock domain
  -- Feedback ready to receive on the input clock domain
  enable_synchroniser_i : cdc_pulse_handshake
    generic map(
      dst_registers_g => 2)
    port map(
      src_clk   => src_clk,
      src_reset => src_reset,
      src_pulse => src_enable,
      src_ready => src_acknowledge,
      dst_clk   => dst_clk,
      dst_reset => dst_reset,
      dst_pulse => dst_load
      );

  -- Capture the data on the output clock domain when dst_load is asserted.
  -- Setup and hold time are assured since at least 2 dest_clk occured since
  -- src_data have been written.
  dst_data_reg_p : process(dst_clk)
  begin
    if (rising_edge(dst_clk)) then
      if (dst_reset = '1') then
        dst_cdc_data <= (others => '0');
      elsif (dst_load = '1') then
        dst_cdc_data <= src_data;
      end if;
    end if;
  end process;

  dst_out <= dst_cdc_data;

  -----------------------------------------------------------------------------

end architecture rtl;
