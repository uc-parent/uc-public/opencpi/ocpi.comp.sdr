-- HDL Implementation of a Simple Dual Port RAM buffer
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity ram_simple_dual_port is
  generic (
    data_width_g    : integer                         := 8;
    buffer_depth_g  : integer                         := 16;
    read_pipeline_g : integer range 1 to integer'high := 3
    );
  port (
    clk             : in  std_logic;
    ram_enable      : in  std_logic;
    register_enable : in  std_logic;
    -- On a write enable the ram is shifted
    write_enable    : in  std_logic;
    write_address   : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    write_data      : in  std_logic_vector(data_width_g-1 downto 0);
    read_address    : in  std_logic_vector(integer(ceil(log2(real(buffer_depth_g))))-1 downto 0);
    read_data       : out std_logic_vector(data_width_g-1 downto 0)
    );
end ram_simple_dual_port;

architecture rtl of ram_simple_dual_port is

  type ram_t is array(0 to buffer_depth_g-1) of std_logic_vector(data_width_g-1 downto 0);
  signal ram : ram_t;

begin

  -----------------------------------------------------------------------------
  -- Write to RAM
  -----------------------------------------------------------------------------

  ram_write_p : process(clk)
  begin
    if rising_edge(clk) then
      if ram_enable = '1' and write_enable = '1' then
        ram(to_integer(unsigned(write_address))) <= write_data;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Read from RAM with a single stage pipeline delay
  -----------------------------------------------------------------------------

  pipe_1_gen : if read_pipeline_g = 1 generate
    ram_read_1_p : process(clk)
    begin
      if rising_edge(clk) then
        if ram_enable = '1' then
          read_data <= ram(to_integer(unsigned(read_address)));
        end if;
      end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Read from RAM with a multi-stage pipeline delay
  -----------------------------------------------------------------------------

  pipe_multi_gen : if read_pipeline_g > 1 generate
    type reg_t is array(0 to read_pipeline_g-1) of std_logic_vector(read_data'range);
    signal reg_rd                 : reg_t;
    -- Prevent pipeline from being absorbed into an SRL
    attribute srl_style           : string;
    attribute srl_style of reg_rd : signal is "register";
  begin
    ram_read_multi_p : process(clk)
    begin
      if rising_edge(clk) then
        if register_enable = '1' then
          reg_rd <= ram(to_integer(unsigned(read_address))) & reg_rd(0 to reg_rd'high-1);
        end if;
      end if;
    end process;
    read_data <= reg_rd(reg_rd'high);
  end generate;

  -----------------------------------------------------------------------------

end rtl;
