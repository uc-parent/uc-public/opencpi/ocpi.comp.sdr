.. cdc_single_bit documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _cdc_single_bit-primitive:


Clock Domain Crossing - Single Bit (``cdc_single_bit``)
=======================================================
Single bit clock domain crossing (CDC) synchroniser.

Design
------
Performs synchronisation of a single bit to the destination clock domain.

Implementation
--------------

The single bit clock domain crossing synchroniser is essentially a shift register (``dst_cdc_sr``) used to transfer the input signal (``src_in``) from the source (``src_clk``) to the destination (``dst_clk``) clock domain, producing an output (``dst_out``) with a much reduced chance of metastability occurring. Since the input can change state asynchronously to the destination clock, there is a chance that the setup and hold times of the first of the synchroniser registers will be violated, potentially leading to its output being driven to a metastable state. This metastable state would have a whole ``dst_clk`` clock cycle to settle before being sampled by the next register, thus making it less likely that its output would be metastable. Although 2 registers are generally considered sufficient for most circuits, the primitive allows for register lengths between 2 and 10 by setting the ``dst_registers_g`` generic. Increasing the length of the synchroniser reduces the chance of a metastable state from propagating through all of the registers and appearing on the output, increasing the Mean Time Between Failure (MTBF) of the circuit.

The primitive can optionally register the input signal (``src_in``) in the source clock domain by setting ``src_register_g`` high. Although this is not necessary, it is recommended that the input to the synchroniser registers is not driven by combinatorial logic, which can glitch and take time to settle when its inputs change state. Glitches could be sampled by the synchroniser registers, causing undesirable state changes at the output. Glitches and the increased settling time make it more likely that the synchroniser registers sample the input at a point where the setup and hold times are violated, increasing the potential for metastability and reducing the MTBF of the circuit.

If all transitions of the input signal must be observed at the output, consideration should be given to the clock frequencies and rate of change of the input signal. As a rule of thumb, synchronising a slower input signal into a faster destination clock domain is generally not a problem using this primitive if the destination clock domain frequency is 1.5X (or more) the rate of change of the input signal. This is because the faster clock signal will sample the slower input signal at least once before it can change state, whilst allowing for setup and hold times. Note that the maximum rate of change of the input signal will be at the source clock frequency but certain inputs, such as resets, may change at a much slower rate. If all transitions of the input signal must be observed at the output but the conditions above can not be met then alternative synchronisation schemes should be considered.

A block diagram representation of the implementation is given in :numref:`cdc_single_bit-diagram`.

.. _cdc_single_bit-diagram:

.. figure:: cdc_single_bit.svg
   :alt: Block diagram of a single bit clock domain crossing (CDC) synchroniser implementation
   :align: center

   Block diagram of a single bit clock domain crossing (CDC) synchroniser implementation.

Further information on metastability and synchronisation can be found at:

 * http://www.sunburst-design.com/papers/CummingsSNUG2008Boston_CDC.pdf

 * https://www.edn.com/crossing-the-abyss-asynchronous-signals-in-a-synchronous-world/

The primitive passes signals between different clock domains and has been designed for that purpose. The primitive may contain attributes and should be paired with constraints to control their placement, optimising the MTBF through the synchronisers, and preventing the timing analyser from needlessly attempting to time the crossing signals. The Xilinx Vivado tool has the capability to report on the quality of the clock domain crossings in the design and can accept waivers for warnings generated for CDC architectures that it can not automatically verify. Platform/application developers should therefore consider adding the constraints, or similar if not using Vivado, to designs that instantiate the primitive. Refer to constraints listed below together with the documentation for all clock domain clocking primitives identified in the dependencies section, and all of their dependencies.

.. code-block::

   ### CDC constraints for cdc_single_bit.vhd instantiated throughout the design ###
   # Applies false path to D input of the first register of the CDC shift registers.
   # This is valid because a metastable state should have time to resolve before
   # reaching the following registers. The constraint:
   #   Constrains the signal feeding the D input of the *cdc_sr_reg[0] register
   #   Does not apply to similarly named signals, only to those in the cdc_single_bit primitive
   set_false_path -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hier -filter {NAME =~ *cdc_sr_reg[0] && FILE_NAME =~ */cdc_single_bit.vhd}]]

Interface
---------

Generics
~~~~~~~~

 * ``dst_registers_g``  (``natural``): Number (2 to 10) of synchronisation registers (defaults to 2). Increase for improved MTBF.

 * ``src_register_g``   (``std_logic``): Register the input in the source clock domain prior to resynchronisation for improved MTBF (defaults to '0', no register).

 * ``register_reset_level_g``  (``std_logic``): Initialisation and reset state of the source and destination registers (defaults to '0').

Ports
~~~~~

 * ``src_clk`` (``std_logic``), in: Source clock. Input registered on rising edge. Tie low when ``src_register_g`` is low.

 * ``src_reset`` (``std_logic``), in: Source reset. Active high, synchronous with rising edge of source clock. Tie low when ``src_register_g`` is low.

 * ``src_enable`` (``std_logic``), in: Source clock enable. Module is enabled when ``src_enable`` is high. Tie low when ``src_register_g`` is low.

 * ``src_in`` (``std_logic``), in: Input to be resynchronised.

 * ``dst_clk`` (``std_logic``), in: Destination clock. Synchronisation registers and output registered on rising edge.

 * ``dst_reset`` (``std_logic``), in: Optional destination reset. Active high, synchronous with rising edge of destination clock. Tie low if not required.

 * ``dst_out`` (``std_logic``), out: Output synchronised to the destination clock.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cdc_single_bit`` are:

 * The destination clock domain (``dst_clk``) frequency must be 1.5X (or more) the rate of change of the input signal (``src_in``).
