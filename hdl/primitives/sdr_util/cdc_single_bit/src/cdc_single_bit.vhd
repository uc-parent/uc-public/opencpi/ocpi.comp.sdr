-- HDL Implementation of a cdc_single_bit
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cdc_single_bit is
  generic (
    dst_registers_g        : natural range 2 to 10 := 2;  -- Range 2 - 10 synchroniser registers for increasing MTBF
    src_register_g         : std_logic             := '0';  -- 0=no, 1=yes input register for improved MTBF
    register_reset_level_g : std_logic             := '0');  -- 0=low, 1=high
  port (
    src_clk    : in  std_logic;  -- optional; required when src_register_g='1'
    src_reset  : in  std_logic;  -- optional; required when src_register_g='1'
    src_enable : in  std_logic;  -- optional; required when src_register_g='1'
    src_in     : in  std_logic;
    dst_clk    : in  std_logic;
    dst_reset  : in  std_logic;         -- optional; if not required, tie '0'
    dst_out    : out std_logic);
end entity cdc_single_bit;

architecture rtl of cdc_single_bit is

  -- Initialise registers to the reset level in case the reset is not used.
  signal src_r      : std_logic                                := register_reset_level_g;
  signal dst_cdc_sr : std_logic_vector(0 to dst_registers_g-1) := (0 to dst_registers_g-1 => register_reset_level_g);

  -- Xilinx attribute to identify synchroniser registers, instructing the tool
  -- to place them close together and apply a DONT_TOUCH to prevent
  -- optimisations. Altera performs this automatically when it detects
  -- synchronisers (as stated in Altera white paper WP01082).
  attribute async_reg               : string;
  attribute async_reg of dst_cdc_sr : signal is "TRUE";

begin

  -----------------------------------------------------------------------------
  -- Input Clock Domain (src_clk)
  -----------------------------------------------------------------------------

  -- Not registering source input in the source clock domain
  -- A chance that combinatorial glitches will be sampled by the destination
  -- synchroniser, reducing MTBF
  no_input_reg_gen : if src_register_g = '0' generate
    src_r <= src_in;
  end generate;

  -- Registering source input in the source clock domain
  -- Improved MTBF over non-registered source input
  input_reg_gen : if src_register_g = '1' generate
    input_reg_p : process(src_clk)
    begin
      if (rising_edge(src_clk)) then
        if (src_reset = '1') then
          src_r <= register_reset_level_g;
        elsif (src_enable = '1') then
          src_r <= src_in;
        end if;
      end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Output Clock Domain (dst_clk)
  -----------------------------------------------------------------------------

  synchroniser_p : process(dst_clk)
  begin
    if (rising_edge(dst_clk)) then
      if (dst_reset = '1') then
        dst_cdc_sr <= (others => register_reset_level_g);
      else
        dst_cdc_sr(0)                      <= src_r;  -- metastability CDC register
        dst_cdc_sr(1 to dst_registers_g-1) <= dst_cdc_sr(0 to dst_registers_g-2);
      end if;
    end if;
  end process;

  dst_out <= dst_cdc_sr(dst_registers_g-1);

  -----------------------------------------------------------------------------

end architecture rtl;
