-- HDL Implementation of a fifo buffer instant read
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_util;
use sdr_util.sdr_util.ram_simple_dual_port;

entity fifo_instant_read is
  generic (
    data_width_g    : integer                         := 8;
    buffer_depth_g  : integer                         := 16;
    read_pipeline_g : integer range 3 to integer'high := 4
    );
  port (
    clk               : in  std_logic;
    reset             : in  std_logic;
    -- Read/Write
    write_enable      : in  std_logic;
    write_data        : in  std_logic_vector(data_width_g-1 downto 0);
    read_enable       : in  std_logic;
    read_valid        : out std_logic;
    read_data         : out std_logic_vector(data_width_g-1 downto 0);
    -- Status
    fill_level        : out natural;
    full_count_clear  : in  std_logic;
    full_count        : out natural;
    full              : out std_logic;
    empty_count_clear : in  std_logic;
    empty_count       : out natural;
    empty             : out std_logic
    );
end fifo_instant_read;

architecture rtl of fifo_instant_read is

  constant ushort_max_c        : natural                                  := 65535;
  constant ram_depth_c         : natural                                  := buffer_depth_g;
  constant ram_address_width_c : natural                                  := integer(ceil(log2(real(ram_depth_c))));
  constant ram_max_address_c   : unsigned(ram_address_width_c-1 downto 0) := to_unsigned(ram_depth_c-1, ram_address_width_c);
  constant buffer_length_c     : natural                                  := read_pipeline_g;
  constant ram_read_pipeline_c : natural                                  := read_pipeline_g - 1;

  -- Simple Dual Port RAM
  signal ram_write_address : unsigned(ram_address_width_c-1 downto 0);
  signal ram_read_address  : unsigned(ram_address_width_c-1 downto 0);
  signal ram_write         : std_logic;
  signal ram_read          : std_logic;
  signal ram_enable        : std_logic;
  signal ram_read_data     : std_logic_vector(read_data'range);
  signal ram_fill_level    : natural;
  signal ram_empty         : std_logic;

  -- Skid Buffer
  signal buffer_ram_available_pipe : std_logic_vector(buffer_length_c-2 downto 0);
  signal buffer_count              : natural range 0 to buffer_length_c;
  type buffer_t is array(0 to buffer_length_c-1) of std_logic_vector(read_data'range);
  signal buffer_data               : buffer_t;
  signal buffer_inflight           : natural range 0 to buffer_length_c;
  signal buffer_empty              : std_logic;
  signal buffer_full               : std_logic;

  -- FIFO (RAM + Skid)
  signal fifo_fill_level  : natural;
  signal fifo_full        : std_logic;
  signal fifo_empty_count : natural;
  signal fifo_full_count  : natural;

begin
  -----------------------------------------------------------------------------
  -- RAM write & read addresses
  -- Increments for each write/read, rolls over after reaching the buffer depth
  -----------------------------------------------------------------------------
  ram_write_address_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        ram_write_address <= (others => '0');
        ram_read_address  <= (others => '0');
      else
        -- Write
        if (ram_write = '1') then
          if (ram_write_address = ram_max_address_c) then
            ram_write_address <= (others => '0');
          else
            ram_write_address <= ram_write_address + 1;
          end if;
        end if;
        -- Read
        if (ram_read = '1') then
          if (ram_read_address = ram_max_address_c) then
            ram_read_address <= (others => '0');
          else
            ram_read_address <= ram_read_address + 1;
          end if;
        end if;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Instantiate Simple Dual Port RAM
  -----------------------------------------------------------------------------
  ram_write  <= write_enable and not(fifo_full);
  -- Transfer from RAM (if data available) to buffer (if space available, or
  -- will be due to current read)
  ram_read   <= '1' when ((ram_empty = '0') and ((buffer_full = '0') or ((read_enable = '1') and (buffer_empty = '0')))) else '0';
  ram_enable <= write_enable or read_enable;

  sdpram_i : ram_simple_dual_port
    generic map (
      data_width_g    => data_width_g,
      buffer_depth_g  => ram_depth_c,
      read_pipeline_g => ram_read_pipeline_c
      )
    port map (
      clk             => clk,
      ram_enable      => ram_enable,
      register_enable => '1',
      write_enable    => ram_write,
      write_address   => std_logic_vector(ram_write_address),
      write_data      => write_data,
      read_address    => std_logic_vector(ram_read_address),
      read_data       => ram_read_data
      );
  -----------------------------------------------------------------------------
  -- RAM Status
  -- Monitors the amount of data stored in the RAM
  -----------------------------------------------------------------------------
  ram_status_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        ram_empty      <= '1';
        ram_fill_level <= 0;
      else
        ------------------
        -- Increasing Fill
        ------------------
        if ((ram_write = '1') and (ram_read = '0')) then
          ram_empty <= '0';
          if (ram_fill_level < ram_depth_c) then
            -- Not full
            ram_fill_level <= ram_fill_level + 1;
          end if;
        ------------------
        -- Decreasing Fill
        ------------------
        elsif ((ram_write = '0') and (ram_read = '1')) then
          if (ram_fill_level < 1) then
            -- Empty
            ram_empty <= '1';
          elsif (ram_fill_level = 1) then
            -- Enter empty
            ram_empty      <= '1';
            ram_fill_level <= ram_fill_level - 1;
          else
            -- Not empty
            ram_empty      <= '0';
            ram_fill_level <= ram_fill_level - 1;
          end if;
        end if;
      ------------------
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Skid buffer
  -- Stores some of the RAM data so that it is available to be read
  -- instantaneously rather than having to wait for the pipeline delay.
  -----------------------------------------------------------------------------
  skid_buffer_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        buffer_count              <= 0;
        buffer_data               <= (others => (others => '0'));
        buffer_inflight           <= 0;
        buffer_ram_available_pipe <= (others => '0');
        buffer_empty              <= '1';
        buffer_full               <= '0';
      else
        -- Pipeline to indicate data available from the RAM
        buffer_ram_available_pipe <= buffer_ram_available_pipe(buffer_ram_available_pipe'high-1 downto 0) & ram_read;

        -- Store RAM output in a skid buffer
        if ((buffer_ram_available_pipe(buffer_ram_available_pipe'high) = '0') and ((read_enable = '1') and (buffer_empty = '0'))) then
          -- Reading only from buffer
          buffer_count                         <= buffer_count - 1;
          buffer_data(0 to buffer_data'high-1) <= buffer_data(1 to buffer_data'high);
          if (buffer_count = 1) then
            -- Enter empty
            buffer_empty <= '1';
          end if;
        elsif ((buffer_ram_available_pipe(buffer_ram_available_pipe'high) = '1') and ((read_enable = '0') or (buffer_empty = '1'))) then
          -- Writing only to buffer
          buffer_count              <= buffer_count + 1;
          buffer_data(buffer_count) <= ram_read_data;
          buffer_empty              <= '0';
        elsif ((buffer_ram_available_pipe(buffer_ram_available_pipe'high) = '1') and ((read_enable = '1') and (buffer_empty = '0'))) then
          -- Reading and writing from/to buffer
          buffer_data(0 to buffer_data'high-1) <= buffer_data(1 to buffer_data'high);
          buffer_data(buffer_count-1)          <= ram_read_data;
        end if;

        -- Monitor skid buffer stored data and data in flight from the RAM to
        -- the buffer
        if ((ram_read = '0') and ((read_enable = '1') and (buffer_empty = '0'))) then
          -- Reading only from buffer
          buffer_inflight <= buffer_inflight - 1;
          buffer_full     <= '0';
        elsif ((ram_read = '1') and ((read_enable = '0') or (buffer_empty = '1'))) then
          -- Writing only to buffer (write in flight)
          buffer_inflight <= buffer_inflight + 1;
          if (buffer_inflight = buffer_length_c - 1) then
            -- Enter full
            buffer_full <= '1';
          end if;
        end if;

      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- FIFO Status
  -- Monitors the amount of data stored in the FIFO (RAM + Buffer)
  -----------------------------------------------------------------------------
  full_empty_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        fifo_full       <= '0';
        fifo_fill_level <= 0;
      else
        ------------------
        -- Increasing Fill
        ------------------
        if ((ram_write = '1') and ((read_enable = '0') or (buffer_empty = '1'))) then
          if (fifo_fill_level > ram_depth_c-1) then
            -- Full
            fifo_full <= '1';
          elsif (fifo_fill_level = ram_depth_c-1) then
            -- Enter full
            fifo_full       <= '1';
            fifo_fill_level <= fifo_fill_level + 1;
          else
            -- Not full
            fifo_full       <= '0';
            fifo_fill_level <= fifo_fill_level + 1;
          end if;
        ------------------
        -- Decreasing Fill
        ------------------
        elsif ((ram_write = '0') and ((read_enable = '1') and (buffer_empty = '0'))) then
          fifo_full <= '0';
          if (fifo_fill_level > 0) then
            -- Not empty
            fifo_fill_level <= fifo_fill_level - 1;
          end if;
        end if;
      ------------------
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Count times FIFO has been Full and Empty
  -----------------------------------------------------------------------------
  full_empty_counts_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        fifo_full_count  <= 0;
        fifo_empty_count <= 0;
      else
        --------------
        -- Full count
        --------------
        if (full_count_clear = '1') then
          fifo_full_count <= 0;
        elsif ((ram_write = '1') and ((read_enable = '0') or (buffer_empty = '1')) and (fifo_fill_level = ram_depth_c-1)) then
          -- Enter full
          if (fifo_full_count < ushort_max_c) then
            fifo_full_count <= fifo_full_count + 1;
          end if;
        end if;
        --------------
        -- Empty count
        --------------
        if (empty_count_clear = '1') then
          fifo_empty_count <= 0;
        elsif ((buffer_ram_available_pipe(buffer_ram_available_pipe'high) = '0') and ((read_enable = '1') and (buffer_empty = '0')) and (buffer_count = 1)) then
          -- Enter empty
          if (fifo_empty_count < ushort_max_c) then
            fifo_empty_count <= fifo_empty_count + 1;
          end if;
        end if;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- FIFO Output
  -----------------------------------------------------------------------------
  read_valid  <= '1' when ((read_enable = '1') and (buffer_empty = '0')) else '0';
  read_data   <= buffer_data(0);
  fill_level  <= fifo_fill_level;
  full        <= fifo_full;
  empty       <= buffer_empty;
  full_count  <= fifo_full_count;
  empty_count <= fifo_empty_count;
-----------------------------------------------------------------------------
end rtl;
