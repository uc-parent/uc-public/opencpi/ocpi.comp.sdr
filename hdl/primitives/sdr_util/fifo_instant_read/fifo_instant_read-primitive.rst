.. fifo_instant_read documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fifo_instant_read-primitive:

FIFO Instant Read (``fifo_instant_read``)
=========================================
A First In / First Out implementation using :ref:`Simple Dual Port RAM <ram_simple_dual_port-primitive>`, supported by a skid buffer on the read output to provide data instantly when read.

Design
------
This is a First In / First Out buffer implementation using block RAM as the primary memory store. Multiple registers have been implemented around the block RAM in order to optimise it for high speed operation. To reduce the latency introduced by the additional registers, a skid buffer has been added to the read output. The skid buffer reads the data out of the RAM as soon as it becomes available and, once the data egresses the RAM pipeline, stores it in the skid buffer registers making it available for instant access. The skid buffer is sized to take into account the pipeline delay through the RAM and, providing enough data has been written to the RAM, the skid buffer should allow contiguous data to be read from the FIFO without adding any additional inter-packet gap.

Implementation
--------------
Providing ``full`` is de-asserted, ``write_data`` may be written to the FIFO on the rising edge of ``clk`` once ``write_enable`` is asserted. Providing ``empty`` is de-asserted, ``read_data`` will be available from the FIFO and validated by ``read_valid`` and may be captured on the rising edge of ``clk`` once ``read_enable`` is asserted.

The ``full`` port indicates that the FIFO RAM is full and no more data should be written. The ``empty`` port indicates that the FIFO skid buffer is empty and no data should be read. The ``fill_level`` port indicates the total number of words in the FIFO, whether they be in the RAM, the skid buffer or transitioning between them along the RAM pipeline. Therefore, the ``fill_level`` port could indicate that the FIFO is full / not full or empty / not empty before the ``full`` or ``empty`` ports are asserted/de-asserted.

The ``full_count`` and ``empty_count`` ports indicate the number of times the FIFO has been full or empty and may be reset by asserting the ``full_count_clear`` and ``empty_count_clear`` ports respectively.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g``         (``integer``): The bit width of the FIFO and data ports, (defaults to 8).

 * ``buffer_depth_g``       (``integer``): The FIFO depth of the primitive (in words of ``data_width_g``), (defaults to 16).

 * ``read_pipeline_g``        (``integer``): Read data pipeline length (defaults to 4).

Ports
~~~~~

 * ``clk``                 (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset``                 (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``write_enable``           (``std_logic``), in : Data on ``write_data`` is written to the FIFO when asserted.

 * ``write_data``             (``std_logic_vector``), in : Data to write into the FIFO.

 * ``read_enable``           (``std_logic``), in : Data read from the FIFO onto ``read_data`` when asserted.

 * ``read_valid``            (``std_logic``), out : Data from the FIFO on ``read_data`` is valid.

 * ``read_data``             (``std_logic_vector``), out: Data read from the FIFO.

 * ``fill_level``          (``natural``), out: Number of words held in the FIFO (RAM and skid buffer).

 * ``full_count_clear``    (``std_logic``), in: Clear the ``full_count`` when asserted.

 * ``full_count``          (``natural``), out: Number of times the FIFO was full.

 * ``full``                (``std_logic``), out: The FIFO RAM is full and no more data should be written.

 * ``empty_count_clear``   (``std_logic``), in: Clear the ``empty_count`` when asserted.

 * ``empty_count``         (``natural``), out: Number of times the FIFO was empty.

 * ``empty``               (``std_logic``), out: The FIFO skid buffer is empty and no more data should be read.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

   * :ref:`RAM Simple Dual Port Primitive <ram_simple_dual_port-primitive>`.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``fifo_instant_read`` are:

 * None
