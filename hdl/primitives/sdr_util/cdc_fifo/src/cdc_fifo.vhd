-- HDL Implementation of a cdc_fifo
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.std_logic_misc.all;

entity cdc_fifo is
  generic (
    width_g : natural                          := 1;
    depth_g : natural range 16 to natural'high := 16);
  port (
    src_clk     : in  std_logic;
    src_reset   : in  std_logic;
    src_enqueue : in  std_logic;
    src_in      : in  std_logic_vector(width_g-1 downto 0);
    src_full_n  : out std_logic;
    dst_clk     : in  std_logic;
    dst_reset   : in  std_logic;
    dst_dequeue : in  std_logic;
    dst_out     : out std_logic_vector(width_g-1 downto 0);
    dst_empty_n : out std_logic);
end entity cdc_fifo;

architecture rtl of cdc_fifo is

  constant index_width_c : natural := natural(ceil(log2(real(depth_g))));

  -- Create a vector with the 2 MSb's set
  function set_2_most_significant_bits_f return std_logic_vector is
    variable bus_v : std_logic_vector(index_width_c downto 0) := (others => '0');
  begin
    bus_v(index_width_c downto index_width_c-1) := "11";
    return bus_v;
  end set_2_most_significant_bits_f;

  constant two_most_significant_bits_set_c : std_logic_vector(index_width_c downto 0) := set_2_most_significant_bits_f;

  -- FIFO Memory
  type fifo_memory_t is array (0 to depth_g-1) of std_logic_vector(width_g-1 downto 0);
  signal fifo_memory  : fifo_memory_t;
  signal dst_cdc_data : std_logic_vector(width_g-1 downto 0);

  -- Enqueue Pointer support
  signal src_lucal_code_next        : std_logic_vector(index_width_c+1 downto 0);
  signal src_lucal_code_future      : std_logic_vector(index_width_c+1 downto 0);
  signal src_enqueue_pointer_next   : std_logic_vector(index_width_c downto 0);
  signal src_enqueue_pointer_future : std_logic_vector(index_width_c downto 0);
  signal src_enqueue_pointer_index  : std_logic_vector(index_width_c-1 downto 0);
  signal src_not_full_r             : std_logic;
  signal src_not_full_next          : std_logic;
  signal src_not_full_future        : std_logic;

  -- Dequeue Pointer support
  signal dst_lucal_code_next       : std_logic_vector(index_width_c+1 downto 0);
  signal dst_lucal_code_future     : std_logic_vector(index_width_c+1 downto 0);
  signal dst_dequeue_pointer_next  : std_logic_vector(index_width_c downto 0);
  signal dst_dequeue_pointer_index : std_logic_vector(index_width_c-1 downto 0);
  signal dst_not_empty_r           : std_logic;
  signal dst_not_empty_next        : std_logic;

  -- Synchronise enqueue and dequeue point across domains
  signal dst_pointer_cdc_data     : std_logic_vector(index_width_c downto 0);
  signal dst_enqueue_pointer_next : std_logic_vector(index_width_c downto 0);
  signal src_pointer_cdc_data     : std_logic_vector(index_width_c downto 0);
  signal src_dequeue_pointer_next : std_logic_vector(index_width_c downto 0);

  -- Increment Lucal code
  -- Lucal codes append an even parity bit to the LSb of a gray code,
  -- allowing arithmetic to be implemented directly on the code
  function increment_lucal_code(lucal_in : std_logic_vector(index_width_c+1 downto 0))
    return std_logic_vector is
    variable gray_code_v         : std_logic_vector(index_width_c downto 0);
    variable even_parity_v       : std_logic;
    variable gray_shift_v        : std_logic_vector(index_width_c downto 0);
    variable invert_bits_v       : std_logic_vector(index_width_c downto 0);
    variable incremented_gray_v  : std_logic_vector(index_width_c downto 0);
    variable incremented_lucal_v : std_logic_vector(index_width_c+1 downto 0);
  begin
    -- Split lucal code into gray code and even parity bit
    gray_code_v      := lucal_in(index_width_c+1 downto 1);
    even_parity_v    := lucal_in(0);
    -- Calculate bit inversions
    invert_bits_v(0) := not even_parity_v;
    for i in 1 to index_width_c loop
      gray_shift_v := std_logic_vector(shift_left(signed(gray_code_v), 2 + index_width_c - i));
      if i < index_width_c then
        invert_bits_v(i) := even_parity_v and gray_code_v(i-1) and (not or_reduce(gray_shift_v));
      else
        invert_bits_v(i) := even_parity_v and (not or_reduce(gray_shift_v));
      end if;
    end loop;
    -- Invert bits to increment the gray code
    incremented_gray_v  := invert_bits_v xor gray_code_v;
    -- Concatenate the updated parity bit to form the lucal code
    incremented_lucal_v := incremented_gray_v & not even_parity_v;
    return incremented_lucal_v;
  end function;

  -- Xilinx attribute to identify synchroniser registers, instructing the tool
  -- to place them close together and apply a DONT_TOUCH to prevent
  -- optimisations. ALtera performs this automatically when it detects
  -- synchronisers (as stated in Atera white paper WP01082).
  attribute async_reg                             : string;
  attribute async_reg of dst_pointer_cdc_data     : signal is "TRUE";
  attribute async_reg of dst_enqueue_pointer_next : signal is "TRUE";
  attribute async_reg of src_pointer_cdc_data     : signal is "TRUE";
  attribute async_reg of src_dequeue_pointer_next : signal is "TRUE";

begin

  generate_error_gen : if (depth_g /= 2**index_width_c) generate
    signal generate_error : integer;
  begin
    -- depth_g must be a power of 2
    generate_error <= 1/0;
  end generate;

  -- Outputs
  dst_out     <= dst_cdc_data;
  dst_empty_n <= dst_not_empty_r;
  src_full_n  <= src_not_full_r;

  -----------------------------------------------------------------------------
  -- Input Clock Domain (src_clk)
  -----------------------------------------------------------------------------

  -- Enqueue pointer and full flag control
  -- The enqueue pointer is Gray coded so that only a single bit change occurs
  -- when crossing between clock domains. The pointer is one bit wider than the
  -- width required to address the FIFO memory to aid FIFO full/empty detection.
  -- Incremented of the Gray code is performed using a lucal code.
  enqueue_pointer_p : process (src_clk)
  begin
    if rising_edge(src_clk) then
      if (src_reset = '1') then
        src_lucal_code_next   <= (others                                  => '0');  -- gray 0 & parity
        src_lucal_code_future <= (src_lucal_code_future'length-1 downto 2 => '0') & "11";  -- gray 1 & parity
        src_not_full_r        <= '0';  -- Full during reset to avoid spurious loads
      elsif (src_enqueue = '1') then
        -- Increment src_enqueue_pointer_next & future and update the not full
        -- flag. The future signal makes sure the full flag responds on the
        -- correct clock cycle when enqueue occurs.
        src_lucal_code_next   <= src_lucal_code_future;
        src_lucal_code_future <= increment_lucal_code(src_lucal_code_future);
        src_not_full_r        <= src_not_full_future;
      else
        src_not_full_r <= src_not_full_next;
      end if;
    end if;
  end process;

  -- Remove parity bit to generate a gray code pointer to cross clock domains
  src_enqueue_pointer_next   <= src_lucal_code_next(index_width_c+1 downto 1);
  src_enqueue_pointer_future <= src_lucal_code_future(index_width_c+1 downto 1);

  -- Remove 2 gray code MSbs to generate a FIFO address
  src_enqueue_pointer_index <= src_lucal_code_next(index_width_c-1 downto 0);

  -- Indicate not full when the enqueue pointer, with it's 2 MSBs inverted, is
  -- not equal to the resynchronised dequeue pointer
  src_not_full_next   <= '1' when ((src_enqueue_pointer_next xor two_most_significant_bits_set_c) /= src_dequeue_pointer_next)   else '0';
  src_not_full_future <= '1' when ((src_enqueue_pointer_future xor two_most_significant_bits_set_c) /= src_dequeue_pointer_next) else '0';


  -----------------------------------------------------------------------------
  -- Clock Domain Crossing
  -----------------------------------------------------------------------------

  -- Synchronise the enqueue pointer to the Output Clock Domain (dst_clk)
  -- Gray codes only change 1 bit at a time so if the change occurs near a
  -- clock edge, the worst that can happen is that the synchronised output
  -- receives the non-updated value rather than a corruption to almost any
  -- value that could occur if it were a binary pointer. The synchronised
  -- output would eventually receive a correctly updated value and allow
  -- operation to continue with an additional delay, but without error.
  enqueue_pointer_sync_p : process (dst_clk)
  begin
    if rising_edge(dst_clk) then
      if (dst_reset = '1') then
        dst_pointer_cdc_data     <= (others => '0');
        dst_enqueue_pointer_next <= (others => '0');
      else
        dst_pointer_cdc_data     <= src_enqueue_pointer_next;  -- Clock domain crossing
        dst_enqueue_pointer_next <= dst_pointer_cdc_data;
      end if;
    end if;
  end process;

  -- Fifo memory write
  fifo_write_p : process (src_clk)
  begin
    if rising_edge(src_clk) then
      if (src_enqueue = '1') then
        fifo_memory(to_integer(unsigned(src_enqueue_pointer_index))) <= src_in;
      end if;
    end if;
  end process;

  -- Fifo memory read
  -- Read from a location that is guaranteed to be stable, since the FIFO would
  -- indicate it were empty if the updated enqueue pointer hasn't had time to
  -- propogate through it's synchroniser.
  fifo_read_p : process (dst_clk)
  begin
    if rising_edge(dst_clk) then
      -- When the FIFO contains data the first sample is read to be presented
      -- on dst_out. This is replaced by the next sample(s) each time
      -- dst_dequeue is asserted
      if ((dst_not_empty_r = '0' or dst_dequeue = '1') and dst_not_empty_next = '1') then
        dst_cdc_data <= fifo_memory(to_integer(unsigned(dst_dequeue_pointer_index)));  -- clock domain crossing
      end if;
    end if;
  end process;

  -- Synchronise the dequeue pointer to the Input Clock Domain (src_clk)
  -- Gray codes only change 1 bit at a time so if the change occurs near a
  -- clock edge, the worst that can happen is that the synchronised output
  -- receives the non-updated value rather than a corruption to almost any
  -- value that could occcur if it were a binary pointer. The synchronised
  -- output would eventually receive a correctly updated value and allow
  -- operation to continue with an additional delay, but without error.
  dequeue_pointer_sync_p : process (src_clk)
  begin
    if rising_edge(src_clk) then
      if (src_reset = '1') then
        src_pointer_cdc_data     <= (others => '0');
        src_dequeue_pointer_next <= (others => '0');
      else
        src_pointer_cdc_data     <= dst_dequeue_pointer_next;  -- clock domain crossing
        src_dequeue_pointer_next <= src_pointer_cdc_data;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Output Clock Domain (dst_clk)
  -----------------------------------------------------------------------------

  -- Lucal code and empty flag control
  -- The Lucal code consists of a Gray code and a parity bit. Used to generate
  -- a Gray code pointer to cross clock domains and generate full/empty flags,
  -- and unique FIFO addresses.
  dst_pointer_p : process (dst_clk)
  begin
    if rising_edge(dst_clk) then
      if (dst_reset = '1') then
        dst_lucal_code_next   <= (others                                  => '0');  -- gray 0 & parity
        dst_lucal_code_future <= (dst_lucal_code_future'length-1 downto 2 => '0') & "11";  -- gray 1 & parity
        dst_not_empty_r       <= '0';   -- Empty during reset
      -- When the FIFO contains data the first sample is read to be presented
      -- on dst_out. This is replaced by the next sample(s) each time
      -- dst_dequeue is asserted
      elsif ((dst_not_empty_r = '0' or dst_dequeue = '1') and dst_not_empty_next = '1') then
        -- Increment dst_dequeue_pointer_next & future and update the not empty
        -- flag. The future signal makes sure the empty flag responds on the
        -- correct clock cycle when dequeue occurs.
        dst_lucal_code_next   <= dst_lucal_code_future;
        dst_lucal_code_future <= increment_lucal_code(dst_lucal_code_future);
        dst_not_empty_r       <= '1';
      elsif (dst_dequeue = '1' and dst_not_empty_next = '0') then
        -- Continue reporting empty when reading from empty FIFO
        dst_not_empty_r <= '0';
      end if;
    end if;
  end process;

  -- Remove parity bit to generate a gray code pointer to cross clock domains
  dst_dequeue_pointer_next <= dst_lucal_code_next(index_width_c+1 downto 1);

  -- Remove 2 gray code MSbs to generate a FIFO address
  dst_dequeue_pointer_index <= dst_lucal_code_next(index_width_c-1 downto 0);

  -- Indicate not empty when the dequeue pointer is not equal to the
  -- resynchronised enqueue pointer
  dst_not_empty_next <= '1' when (dst_dequeue_pointer_next /= dst_enqueue_pointer_next) else '0';

  -----------------------------------------------------------------------------

end architecture rtl;
