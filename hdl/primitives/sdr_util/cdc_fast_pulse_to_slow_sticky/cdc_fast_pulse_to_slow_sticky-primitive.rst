.. cdc_fast_pulse_to_slow_sticky documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _cdc_fast_pulse_to_slow_sticky-primitive:


Clock Domain Crossing - Fast Pulse to Slow Sticky (``cdc_fast_pulse_to_slow_sticky``)
=====================================================================================
Fast pulse to slow sticky clock domain crossing (CDC) synchroniser.

Design
------
Capture an active high pulse on a fast clock domain, indicating on a slow clock domain that it has occurred until cleared from the slow clock domain, using clock domain crossing (CDC) synchronisers.

Implementation
--------------
The input signal (``fast_pulse``) is monitored for an active high level on the fast clock domain (``fast_clk``) and latched until it is cleared. The latched signal is transferred through a :ref:`single bit synchroniser <cdc_single_bit-primitive>` to the slow clock domain (``slow_clk``) and used to drive the output (``slow_sticky``) until it is cleared using the (``slow_clear``) input. The (``slow_clear``) input is latched and transferred through a :ref:`single bit synchroniser <cdc_single_bit-primitive>` to the fast clock domain (``fast_clk``) and used to clear the latched input. The cleared latched input signal then propagates back to clear the latched clear. Indication and subsequent clearance of the incoming pulse takes multiple clock delays due to signals propagating through synchronisers.

Both synchronisers meet the requirements of the :ref:`single bit synchroniser <cdc_single_bit-primitive>`. Fast to slow meets the requirements because the output is latched on the slow side. Slow to fast meets the requirements provided the fast clock domain is 1.5X the frequency (or more) of the rate of change of the (``slow_clear``) input.

A block diagram representation of the implementation is given in :numref:`cdc_fast_pulse_to_slow_sticky-diagram`.

.. _cdc_fast_pulse_to_slow_sticky-diagram:

.. figure:: cdc_fast_pulse_to_slow_sticky.svg
   :alt: Block diagram of a fast pulse to slow sticky clock domain crossing (CDC) synchroniser implementation
   :align: center

   Block diagram of a fast pulse to slow sticky clock domain crossing (CDC) synchroniser implementation.

Further information on metastability and synchronisation can be found at:

 * http://www.sunburst-design.com/papers/CummingsSNUG2008Boston_CDC.pdf

 * https://www.edn.com/crossing-the-abyss-asynchronous-signals-in-a-synchronous-world/

The primitive passes signals between different clock domains and has been designed for that purpose. The primitive may contain attributes and should be paired with constraints to control their placement, optimising the MTBF through the synchronisers, and preventing the timing analyser from needlessly attempting to time the crossing signals. The Xilinx Vivado tool has the capability to report on the quality of the clock domain crossings in the design and can accept waivers for warnings generated for CDC architectures that it can not automatically verify. Platform/application developers should therefore consider adding the constraints, or similar if not using Vivado, to designs that instantiate the primitive. Refer to the documentation for all clock domain clocking primitives identified in the dependencies section, and all of their dependencies.

Interface
---------

Generics
~~~~~~~~

 * None.

Ports
~~~~~

 * ``fast_clk`` (``std_logic``), in: Fast clock. Input registered on rising edge.

 * ``fast_reset`` (``std_logic``), in: Fast reset. Active high, synchronous with rising edge of source clock.

 * ``fast_pulse`` (``std_logic``), in: Input active high pulse to be detected and resynchronised.

 * ``slow_clk`` (``std_logic``), in: Slow clock. Synchronisation registers and output registered on rising edge.

 * ``slow_reset`` (``std_logic``), in: Slow reset. Active high, synchronous with rising edge of clock. Tie low if not required.

 * ``slow_clear`` (``std_logic``), in: Slow clear. Clear the ``slow_sticky`` output.

 * ``slow_sticky`` (``std_logic``), out: Output synchronised and set active high when a fast pulse has been received until cleared by ``slow_clear``.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Clock Domain Crossing - Single bit primitive <cdc_single_bit-primitive>`.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cdc_fast_pulse_to_slow_sticky`` are:

 * None.
