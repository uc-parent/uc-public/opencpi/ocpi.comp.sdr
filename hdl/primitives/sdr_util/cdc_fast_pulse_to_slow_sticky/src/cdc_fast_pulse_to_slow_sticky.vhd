-- HDL Implementation of a cdc_fast_pulse_to_slow_sticky
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sdr_util;
use sdr_util.sdr_util.cdc_single_bit;

entity cdc_fast_pulse_to_slow_sticky is
  port(
    -- fast clock domain
    fast_clk    : in  std_logic;
    fast_reset  : in  std_logic;
    fast_pulse  : in  std_logic;        -- pulse to be detected
    -- slow clock domain
    slow_clk    : in  std_logic;
    slow_reset  : in  std_logic;
    slow_clear  : in  std_logic;        -- clears sticky bit
    slow_sticky : out std_logic);  -- sticky bit set when fast_pulse is high
end entity cdc_fast_pulse_to_slow_sticky;

architecture rtl of cdc_fast_pulse_to_slow_sticky is

  signal slow_clear_latched  : std_logic := '0';
  signal fast_pulse_sticky   : std_logic;
  signal fast_clear          : std_logic;
  signal slow_pulse_sticky   : std_logic;
  signal slow_sticky_out     : std_logic;
  signal slow_clear_transfer : std_logic;

begin

  -----------------------------------------------------------------------------
  -- Fast Clock Domain (fast_clk)
  -----------------------------------------------------------------------------

  -- Latch an active state from fast_pulse input until seen on the slow clock
  -- domain and cleared. Clearance takes multiple clock delays due to signals
  -- propagating through synchronisers.
  fast_pulse_sticky_p : process(fast_clk)
  begin
    if (rising_edge(fast_clk)) then
      if (fast_reset = '1') then
        fast_pulse_sticky <= '0';
      else
        fast_pulse_sticky <= (fast_pulse or fast_pulse_sticky) and
                             (not fast_clear);
      end if;
    end if;
  end process;

  -- Synchronise the slow_clear signal for use in the fast clock domain
  clear_synchroniser_i : cdc_single_bit
    generic map(
      dst_registers_g        => 2,
      src_register_g         => '1',
      register_reset_level_g => '0')
    port map(
      src_clk    => slow_clk,
      src_reset  => slow_reset,
      src_enable => '1',
      src_in     => slow_clear_transfer,
      dst_clk    => fast_clk,
      dst_reset  => fast_reset,
      dst_out    => fast_clear);

  -----------------------------------------------------------------------------
  -- Slow Clock Domain (slow_clk)
  -----------------------------------------------------------------------------

  -- Synchronise the fast_pulse_sticky for use in the slow clock domain
  -- No input register required since fast_pulse_sticky comes straight from a
  -- register (no combinatorial logic to introduce glitches)
  sticky_synchroniser_i : cdc_single_bit
    generic map(
      dst_registers_g        => 2,
      src_register_g         => '0',
      register_reset_level_g => '0')
    port map(
      src_clk    => '0',                -- No input register
      src_reset  => '0',                -- No input register
      src_enable => '0',                -- No input register
      src_in     => fast_pulse_sticky,
      dst_clk    => slow_clk,
      dst_reset  => slow_reset,
      dst_out    => slow_pulse_sticky);

  -- Latch the clear input until it has cleared slow_pulse_sticky
  slow_sticky_latch_p : process(slow_clk)
  begin
    if(rising_edge(slow_clk)) then
      if(slow_reset = '1') then
        slow_clear_latched <= '0';
      else
        if (slow_pulse_sticky = '0') then
          slow_clear_latched <= '0';
        elsif (slow_clear = '1') then
          slow_clear_latched <= '1';
        end if;
      end if;
    end if;
  end process;

  slow_clear_transfer <= slow_clear_latched or slow_clear;

  -- Assert or clear the sticky output in the slow clock domain
  slow_sticky_p : process(slow_clk)
  begin
    if(rising_edge(slow_clk)) then
      if(slow_reset = '1') then
        slow_sticky_out <= '0';
      else
        slow_sticky_out <= slow_pulse_sticky and (not slow_clear_transfer);
      end if;
    end if;
  end process;

  slow_sticky <= slow_sticky_out;

  -----------------------------------------------------------------------------

end rtl;
