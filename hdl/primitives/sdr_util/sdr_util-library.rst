.. Utilities primitive library index page

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _sdr_utilities_library:

Utilities Primitive Library
===========================
Primitive library for utilities primitives.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Primitives

   */*-primitive

.. _opcode_t-type:

Opcode enumeration for type-less components.

.. literalinclude:: sdr_util_pkg.vhd
   :language: vhdl
   :lines: 31-39

.. _opcode_to_slv-function:

Conversion of opcode type (`opcode_t-type`_) to SLV function.

.. _slv_to_opcode-function:

Conversion of SLV to opcode type (`opcode_t-type`_) function.
