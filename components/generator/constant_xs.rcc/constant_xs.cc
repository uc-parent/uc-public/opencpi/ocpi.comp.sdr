// RCC implementation of constant_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>

#include "constant_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Constant_xsWorkerTypes;

class Constant_xsWorker : public Constant_xsWorkerBase {
  bool enable = false;
  int16_t real_value = 0;
  int16_t imaginary_value = 0;
  bool discontinuity_on_value_change = false;
  uint16_t message_length = 0;

  bool value_changed = false;

  // This flag is used to suppress discontinuity opcodes during the
  // parameter initialisation stage. This must be set within the writesync
  // function as well as the run function, as the writesync function may be
  // called multiple times before the program enters its operating state.
  bool value_startup_complete = false;

  // Notification that enable property has been written
  RCCResult enable_written() {
    this->enable = properties().enable;
    return RCC_OK;
  }

  // Notification that real value property has been written
  RCCResult real_value_written() {
    this->real_value = properties().real_value;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state
    if (this->value_startup_complete) {
      this->value_changed = true;
    }
    return RCC_OK;
  }

  // Notification that imaginary value property has been written
  RCCResult imaginary_value_written() {
    this->imaginary_value = properties().imaginary_value;

    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state
    if (this->value_startup_complete) {
      this->value_changed = true;
    }
    return RCC_OK;
  }

  // Notification that discontinuity_on_value_change property has been
  // written
  RCCResult discontinuity_on_value_change_written() {
    this->discontinuity_on_value_change =
        properties().discontinuity_on_value_change;
    return RCC_OK;
  }

  // Notification that message_length property has been written
  RCCResult message_length_written() {
    this->message_length = properties().message_length;

    if (this->message_length == 0) {
      setError("message length must be greater than 0");
      return RCC_FATAL;
    } else if (this->message_length >
               (CONSTANT_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(int32_t))) {
      setError("message length greater than maximum allowed value");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    this->value_startup_complete = true;

    if (this->value_changed && this->discontinuity_on_value_change) {
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      this->value_changed = false;
      return RCC_ADVANCE;
    }

    if (this->enable) {
      Complex_short_timed_sampleSampleData *outData =
          output.sample().data().data();
      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      output.sample().data().resize(this->message_length);
      Complex_short_timed_sampleSampleData value = {
          this->real_value, this->imaginary_value};
      std::fill(&outData[0], &outData[0] + this->message_length, value);
      return RCC_ADVANCE;
    } else {
      return RCC_OK;
    }
  }
};

CONSTANT_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CONSTANT_XS_END_INFO
