.. ramp_generator_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _ramp_generator_l:


Ramp Generator (``ramp_generator_l``)
=====================================
Generates a sawtooth waveform.

Design
------
Generates a ramp output. The shape of the sawtooth wave is to be set using the properties ``step_size``, ``start_amplitude``, and ``stop_amplitude``.

The output of the ramp generator is controlled by a counter that increments by ``step_size`` every output sample. Typically the ramp generator is configured such that the counter counts toward ``stop_amplitude``. When the counter value equals or passes the ``stop_amplitude`` it resets its value to ``start_amplitude``.

.. _ramp_generator_l-diagram:

.. figure:: ramp_generator_l.svg
   :alt: Graph of sawtooth wave and how available properties relate to the output wave.
   :align: center

   Ramp generator output.

The mathematical representation of the implementation is given in :eq:`ramp_generator_l_gradient-equation` and :eq:`ramp_generator_l-equation`.

.. math::
   :label: ramp_generator_l_gradient-equation

   A_{step} = \frac{A_{stop} - A_{start}}{n_{stop} - n_{start}}

.. math::
   :label: ramp_generator_l-equation

   x[n] = \begin{cases}
            A_{start} + n A_{step} & n_{start} < n <= n_{stop} \\
            A_{start} & \text{otherwise}
          \end{cases}

In :eq:`ramp_generator_l_gradient-equation` and :eq:`ramp_generator_l-equation`:

 * :math:`x[n]` is the output values.

 * :math:`A_{start}`, :math:`A_{stop}` are ``start_amplitude`` and ``stop_amplitude`` respectively.

 * :math:`A_{step}` is the ``step_size``.

 * :math:`n_{start}`, :math:`n_{stop}` are start sample and stop sample respectively.

 * :math:`n` is samples counting from :math:`A_{start}`.

Interface
---------
.. literalinclude:: ../specs/ramp_generator_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
If ``discontinuity_on_amplitude_change`` or ``discontinuity_on_step_size_change`` are true, a discontinuity opcode is output whenever amplitude or step size related properties are changed during run time, respectively.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ramp_generator_l.hdl ../ramp_generator_l.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface generator version 2 primitive <protocol_interface_generator_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``ramp_generator_l`` are:

 * Setting ``message_length`` to 0 or greater than the ``ocpi_max_bytes_output``/``sample_size_bytes`` will result in undefined behaviour.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
