#!/usr/bin/env python3

# Python implementation of carrier generator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of carrier generator for testing."""

import math
import numpy

import opencpi.ocpi_testing as ocpi_testing


class CarrierGenerator(ocpi_testing.Implementation):
    """Python implementation of carrier generator for testing."""

    def __init__(self, enable, step_size, gain,
                 discontinuity_on_step_size_change,
                 discontinuity_on_gain_change, message_length):
        """Initialise the carrier generator class.

        Args:
            enable (bool): When true an output carrier wave is generated.
                           When false the component output is disabled.
            step_size (int): Sets the step size of the carrier generator which
                             determines the output frequency.
            gain (numeric): Sets the amplitude of the output carrier signal
            discontinuity_on_step_size_change (bool): Sends a discontinuity
                            message at the point the step size changes at the output.
            discontinuity_on_gain_change (bool): Sends a discontinuity message
                            at the point the carrier amplitude changes at the output.
            message_length (int): Sets the output message length.
        """
        amplitude_scaling = 1.646760258
        amplitude = gain * amplitude_scaling

        super().__init__(
            enable=enable, step_size=step_size, gain=amplitude,
            discontinuity_on_step_size_change=discontinuity_on_step_size_change,
            discontinuity_on_gain_change=discontinuity_on_gain_change,
            message_length=message_length)

        self.input_ports = []

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, *args):
        """Handle incoming Sample opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def time(self, *args):
        """Handle incoming Time opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def sample_interval(self, *args):
        """Handle incoming Sample Interval opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def flush(self, *args):
        """Handle incoming Flush opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def discontinuity(self, *args):
        """Handle incoming Discontinuity opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def metadata(self, *args):
        """Handle incoming Metadata opcode.

        Raises:
            TypeError: Carrier generator does not have any input ports.
        """
        raise TypeError("Carrier generator does not have any input ports")

    def generate(self, total_output_length):
        """Generate output samples.

        Args:
            total_output_length (int): Number of output samples to generate.

        Returns:
            Formatted messages
        """
        if self.enable:
            index_array = numpy.array(range(1, total_output_length + 1))
            step_index = (2*math.pi*self.step_size*index_array)/(2**32)

            real = self.gain * numpy.cos(step_index)
            imaginary = self.gain * numpy.sin(step_index)

            number_of_messages = math.ceil(
                total_output_length / self.message_length)
            messages = []
            sample_index = 0
            for message_index in range(number_of_messages):
                this_message_length = min(total_output_length - sample_index,
                                          self.message_length)
                complex_data = [
                    complex(numpy.int16(real_),
                            numpy.int16(imaginary_)) for real_, imaginary_ in
                    zip(real[sample_index:sample_index + this_message_length],
                        imaginary[sample_index:
                                  sample_index + this_message_length])]
                messages.append({"opcode": "sample", "data": complex_data})
                sample_index = sample_index + this_message_length

            return self.output_formatter(messages)

        else:
            return self.output_formatter([])
