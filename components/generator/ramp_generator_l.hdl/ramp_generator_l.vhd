-- HDL Implementation of a ramp generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  -- interface constants
  constant delay_c        : integer := 1;
  constant opcode_width_c : integer :=
    integer(ceil(log2(real(long_timed_sample_opcode_t'pos(long_timed_sample_opcode_t'high)+1))));

  function opcode_to_slv(inop : in long_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(long_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return long_timed_sample_opcode_t is
  begin
    return long_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  -- Interface signals
  signal enable      : std_logic;
  signal output_hold : std_logic;

  signal amplitude_change_discontinuity : std_logic;
  signal step_size_change_discontinuity : std_logic;
  signal discontinuity                  : std_logic;

  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data   : std_logic_vector(output_out.data'length - 1 downto 0);

  signal count      : signed(props_in.step_size'range);
  signal count_r    : signed(props_in.step_size'range);
  signal next_count : signed(props_in.step_size'range);

begin

  -- Enable the generator when enable property is true and output is ready,
  -- but not when the interface module is outputting other message signals.
  enable <= output_in.ready and props_in.enable and not output_hold;

  amplitude_change_discontinuity <= '1' when ((props_in.start_amplitude_written = '1' or
                                               props_in.stop_amplitude_written = '1') and
                                              props_in.discontinuity_on_amplitude_change = '1')
                                    else '0';

  step_size_change_discontinuity <= '1' when (props_in.step_size_written = '1' and
                                              props_in.discontinuity_on_step_size_change = '1')
                                    else '0';

  discontinuity <= amplitude_change_discontinuity or step_size_change_discontinuity;

  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      processed_data_opcode_g => opcode_to_slv(long_timed_sample_sample_op_e),
      discontinuity_opcode_g  => opcode_to_slv(long_timed_sample_discontinuity_op_e)
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      discontinuity_trigger => discontinuity,
      generator_enable      => props_in.enable,
      generator_reset       => ctl_in.reset,
      processed_stream_in   => std_logic_vector(count_r),
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );

  output_out.opcode <= slv_to_opcode(output_opcode);
  output_out.data   <= output_data;

  ramp_generator_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        count <= (others => '0');
      else
        if props_in.start_amplitude_written = '1' or props_in.stop_amplitude_written = '1' then
          count <= props_in.start_amplitude;

        elsif enable = '1' then
          count_r <= count;
          if props_in.step_size >= 0 then
            if count >= props_in.stop_amplitude then
              count <= props_in.start_amplitude;
            else
              count <= next_count;
            end if;
          else
            if count <= props_in.stop_amplitude then
              count <= props_in.start_amplitude;
            else
              count <= next_count;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  next_count <= count + props_in.step_size;

end rtl;
