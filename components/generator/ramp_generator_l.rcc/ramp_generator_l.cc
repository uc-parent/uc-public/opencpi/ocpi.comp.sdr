// Ramp generator implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "ramp_generator_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Ramp_generator_lWorkerTypes;

class Ramp_generator_lWorker : public Ramp_generator_lWorkerBase {
  bool enable = false;
  bool discontinuity_on_amplitude_change = false;
  bool discontinuity_on_step_size_change = false;

  uint16_t message_length = 0;
  int32_t step_size = 0;
  int32_t start_amplitude = 0;
  int32_t stop_amplitude = 0;

  bool step_size_changed = false;
  bool amplitude_changed = false;

  // These flags are used to surpress discontinuity opcodes during the
  // parameter initialisation stage. These must be set within the writesync
  // functions as well as the run function, as the writesync function may be
  // called multiple times before the program enters its operating state.
  bool startup_complete = false;

  int32_t count = start_amplitude;
  int32_t next_count = 0;

  RCCResult enable_written() {
    enable = properties().enable;
    return RCC_OK;
  }

  RCCResult message_length_written() {
    message_length = properties().message_length;
    if (message_length == 0) {
      setError("message_length must be greater than 0");
      return RCC_FATAL;
    } else if (message_length >
               RAMP_GENERATOR_L_OCPI_MAX_BYTES_OUTPUT / sizeof(int32_t)) {
      setError("message_length greater than maximum allowed value");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult step_size_written() {
    step_size = properties().step_size;
    // The ramp should be reset to its initial state by setting sample_counter
    // to start_amplitude.
    this->count = start_amplitude;
    ;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    if (startup_complete) {
      step_size_changed = true;
    }
    return RCC_OK;
  }

  RCCResult start_amplitude_written() {
    start_amplitude = properties().start_amplitude;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    count = start_amplitude;
    if (startup_complete) {
      amplitude_changed = true;
    }
    return RCC_OK;
  }

  RCCResult stop_amplitude_written() {
    stop_amplitude = properties().stop_amplitude;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    count = start_amplitude;
    if (startup_complete) {
      amplitude_changed = true;
    }
    return RCC_OK;
  }

  RCCResult discontinuity_on_amplitude_change_written() {
    discontinuity_on_amplitude_change =
        properties().discontinuity_on_amplitude_change;
    return RCC_OK;
  }

  RCCResult discontinuity_on_step_size_change_written() {
    discontinuity_on_step_size_change =
        properties().discontinuity_on_step_size_change;
    return RCC_OK;
  }

  RCCResult run(bool) {
    startup_complete = true;

    if (step_size_changed && discontinuity_on_step_size_change) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      step_size_changed = false;
      return RCC_OK;
    }

    if (amplitude_changed && discontinuity_on_amplitude_change) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      amplitude_changed = false;
      return RCC_OK;
    }

    if (enable) {
      int32_t *outData = output.sample().data().data();
      output.setOpCode(Long_timed_sampleSample_OPERATION);
      output.sample().data().resize(message_length);
      for (size_t i = 0; i < message_length; i++) {
        *outData++ = count;
        next_count = count + step_size;
        if (step_size >= 0) {
          if (count >= stop_amplitude) {
            count = start_amplitude;
          } else
            count = next_count;
        } else if (count <= stop_amplitude) {
          count = start_amplitude;
        } else
          count = next_count;
      }

      output.advance();
      return RCC_OK;
    } else {
      count = start_amplitude;
      return RCC_OK;
    }
  }
};

RAMP_GENERATOR_L_START_INFO

RAMP_GENERATOR_L_END_INFO
