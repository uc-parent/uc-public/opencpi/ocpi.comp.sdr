#!/usr/bin/env python3

# Runs checks for Max Calculator XS testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Runs checks for Max Calculator XS testing."""

import os
import sys
import opencpi.ocpi_testing as ocpi_testing
from max_calculator_xs import MaxCalculatorXS

input_file_path = str(sys.argv[2])
output_file_path = str(sys.argv[1])

max_i_is_valid = (os.environ["OCPI_TEST_max_i_is_valid"].upper() == "TRUE")
max_q_is_valid = (os.environ["OCPI_TEST_max_q_is_valid"].upper() == "TRUE")
max_i = int(os.environ["OCPI_TEST_max_i"])
max_q = int(os.environ["OCPI_TEST_max_q"])

max_calculator_implementation = MaxCalculatorXS()

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(max_calculator_implementation)
verifier.set_port_types(["complex_short_timed_sample"],
                        ["complex_short_timed_sample"],
                        ["equal"])
if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)

properties_match = True
message = "ERROR: {0} expected {1} but Unit Under Test returned {2}"

if max_i_is_valid != max_calculator_implementation.max_i_is_valid:
    properties_match = False
    print(message.format("max_i_is_valid",
                         max_calculator_implementation.max_i_is_valid,
                         max_i_is_valid))

if max_i != max_calculator_implementation.max_i and max_calculator_implementation.max_i_is_valid:
    properties_match = False
    print(message.format("max_i",
                         max_calculator_implementation.max_i,
                         max_i))

if max_q_is_valid != max_calculator_implementation.max_q_is_valid:
    properties_match = False
    print(message.format("max_q_is_valid",
                         max_calculator_implementation.max_q_is_valid,
                         max_q_is_valid))

if max_q != max_calculator_implementation.max_q and max_calculator_implementation.max_q_is_valid:
    properties_match = False
    print(message.format("max_q",
                         max_calculator_implementation.max_q,
                         max_q))

if not properties_match:
    sys.exit(1)
