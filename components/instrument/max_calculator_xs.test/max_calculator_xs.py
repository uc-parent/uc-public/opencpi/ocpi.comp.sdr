#!/usr/bin/env python3

# Python implementation of max_calculator_xs block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of max_calculator_xs block for verification."""

import opencpi.ocpi_testing as ocpi_testing


class MaxCalculatorXS(ocpi_testing.Implementation):
    """The Max Calculator class."""

    def __init__(self):
        """Initialise component."""
        super().__init__()
        self.reset()

    def reset(self):
        """Initialise."""
        self.max_i_is_valid = False
        self.max_q_is_valid = False
        self.max_i = -2**15
        self.max_q = -2**15

    def sample(self, values):
        """Functionality that a sample opcode message triggers.

        Args:
            values (list): The values on the input port.

        Returns:
            List of formatted output opcodes.
        """
        for value in values:
            if value.real > self.max_i:
                self.max_i = value.real
            if value.imag > self.max_q:
                self.max_q = value.imag
        # Maximum values are valid if any samples have been received.
        self.max_i_is_valid = True
        self.max_q_is_valid = True

        return self.output_formatter(
            [{"opcode": "sample", "data": values}])
