-- HDL implementation of max_calculator_xs.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  constant i_word_index_c : integer := input_in.data'length/2;
  constant q_word_index_c : integer := input_in.data'length;
  -- 16 bit signed minimum value
  constant max_negative_c : integer := -(2**15);
  signal input_r          : worker_input_in_t;
  signal max_i_r          : std_logic_vector(input_in.data'length/2-1 downto 0);
  signal max_q_r          : std_logic_vector(input_in.data'length/2-1 downto 0);
  signal i_data           : std_logic_vector(input_in.data'length/2-1 downto 0);
  signal q_data           : std_logic_vector(input_in.data'length/2-1 downto 0);

begin

  -- register data
  register_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if output_in.ready = '1' then
        input_r                <= input_in;
        output_out.data        <= input_r.data;
        output_out.byte_enable <= input_r.byte_enable;
        output_out.opcode      <= input_r.opcode;
        output_out.eom         <= input_r.eom;
        output_out.som         <= input_r.som;
        output_out.eof         <= input_r.eof;
      end if;
      -- only clear control signals on reset
      if ctl_in.reset = '1' then
        input_r.valid    <= '0';
        input_r.ready    <= '0';
        output_out.valid <= '0';
        output_out.give  <= '0';
      elsif output_in.ready = '1' then
        output_out.valid <= input_r.valid;
        output_out.give  <= input_r.ready;
      end if;
    end if;
  end process;

  i_data         <= input_r.data(i_word_index_c-1 downto 0);
  q_data         <= input_r.data(q_word_index_c-1 downto i_word_index_c);
  input_out.take <= output_in.ready;

  property_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        max_i_r                  <= std_logic_vector(to_signed(max_negative_c, max_i_r'length));
        max_q_r                  <= std_logic_vector(to_signed(max_negative_c, max_q_r'length));
        props_out.max_i_is_valid <= '0';
        props_out.max_q_is_valid <= '0';
      else
        -- reset properties on read
        if props_in.max_i_read = '1' then
          max_i_r <= std_logic_vector(to_signed(max_negative_c, max_i_r'length));
        end if;
        if props_in.max_q_read = '1' then
          max_q_r <= std_logic_vector(to_signed(max_negative_c, max_i_r'length));
        end if;
        if props_in.max_i_is_valid_read = '1' then
          props_out.max_i_is_valid <= '0';
        end if;
        if props_in.max_q_is_valid_read = '1' then
          props_out.max_q_is_valid <= '0';
        end if;

        if output_in.ready = '1' then
          if input_r.valid = '1' and input_r.opcode = complex_short_timed_sample_sample_op_e then
            -- new sample always triggers valid max
            props_out.max_i_is_valid <= '1';
            props_out.max_q_is_valid <= '1';
            -- set properties on new maximum
            if signed(i_data) > signed(max_i_r) then
              max_i_r <= i_data;
            end if;
            if signed(q_data) > signed(max_q_r) then
              max_q_r <= q_data;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  props_out.max_i <= signed(max_i_r);
  props_out.max_q <= signed(max_q_r);

end rtl;
