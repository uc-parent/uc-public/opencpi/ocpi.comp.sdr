.. max_calculator_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _max_calculator_xs:

Maximum Calculator (``max_calculator_xs``)
==========================================
Streams incoming I/Q data from the input port to the output port and makes the maximum I and maximum Q values available for reading via properties.

Design
------
I/Q data is continuously read from the input port and written to the output port.

All I/Q values in the input data are compared against maximum I/Q values stored in properties within the worker.
If the I/Q values in the input exceed the current I/Q maximum values then the maximum values are updated with the new input values.
The flags indicating the maximum value is valid are set true if a sample has been received since the last reset.

When the maximum I/Q value is read from the component, it sets the property to the minimum value that can be represented by a signed 16 bit integer, -32768, and the associated valid property is set to false to indicate no maximum value exists.

Interface
---------
.. literalinclude:: ../specs/max_calculator_xs-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
All opcodes are passed through this component without modification.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Reading the ``max_i`` or ``max_q`` property will reset the value to the initial value of -32768 and clear the corresponding ``max_i_is_valid`` or ``max_q_is_valid`` property.

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../max_calculator_xs.rcc ../max_calculator_xs.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 0,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``max_calculator_xs`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
