#!/usr/bin/env python3

# Python implementation of FIR filter for verification.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of fir filter scaled."""

import numpy
import decimal

import opencpi.ocpi_testing as ocpi_testing
from sdr_interface.sample_time import SampleTime


class FirFilter(ocpi_testing.Implementation):
    """Fir Filter verification."""

    def __init__(self,
                 taps,
                 group_delay_seconds=0,
                 group_delay_fractional=0):
        """Initialise FIR filter.

        Args:
            taps (list): Taps for FIR filter.
            group_delay_seconds (int): Offset to be subtracted from received time.
            group_delay_fractional (int): Offset to be subtracted from received time.
        """
        taps = [numpy.float32(tap) for tap in taps]
        self._number_of_taps = len(taps)
        super().__init__(taps=taps)

        self._filter_buffer_real = [numpy.float32(0.0)] * len(self.taps)
        self._filter_buffer_imaginary = [numpy.float32(0.0)] * len(self.taps)
        self._data_received = False
        self._sample_time = SampleTime(
            group_delay_seconds=group_delay_seconds,
            group_delay_fractional=group_delay_fractional)

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._filter_buffer_real = [numpy.float32(0.0)] * len(self.taps)
        self._filter_buffer_imaginary = [numpy.float32(0.0)] * len(self.taps)
        self._data_received = False

    def sample(self, input_):
        """Handle sample opcode applying FIR filter to samples.

        Calls _filter function.

        Args
            input_ (list of complex float): Sample opcode data for the input port.

        Returns:
            Formatted messages produced on receipt of the sample opcode.
        """
        data = self._filter(input_)
        self._data_received = True

        return self.output_formatter([{"opcode": "sample", "data": data}])

    def flush(self, input_):
        """Process zero sample values to flush out any in-progress values.

        If there is any in-progress data, flush it out by processing
        tap x zeros, forwarding on any sample opcode messages produced.
        Then forward the flush opcode.

        Args:
            input_: Ignored.

        Returns:
            List of messages produced on receipt of the flush opcode.
        """
        messages = []
        if self._data_received:
            flush_data = self._filter(
                [numpy.float32(0.0)] * self._number_of_taps)
            messages = [{"opcode": "sample", "data": flush_data}]
        messages.append({"opcode": "flush", "data": None})
        self.reset()
        return self.output_formatter(messages)

    def _filter(self, input_):
        """Performs the FIR filter algorithm.

        Args:
            input_ (list of complex float): Opcode data for the input port.

        Returns:
            FIR output
        """
        data = [complex(0.0, 0.0)] * len(input_)

        for index, value in enumerate(input_):
            self._filter_buffer_real = numpy.insert(
                self._filter_buffer_real, 0, numpy.float32(value.real))
            self._filter_buffer_imaginary = numpy.insert(
                self._filter_buffer_imaginary, 0, numpy.float32(value.imag))
            self._filter_buffer_real = self._filter_buffer_real[0:-1]
            self._filter_buffer_imaginary = self._filter_buffer_imaginary[0:-1]

            output_real = numpy.float32(0.0)
            output_imaginary = numpy.float32(0.0)
            for tap, sample_real, sample_imaginary in zip(
                    self.taps, self._filter_buffer_real,
                    self._filter_buffer_imaginary):
                output_real = output_real + tap * sample_real
                output_imaginary = output_imaginary + tap * sample_imaginary

            data[index] = complex(numpy.float32(output_real),
                                  numpy.float32(output_imaginary))

        return data

    def discontinuity(self, input_):
        """Reset FIR filter and forward discontinuity opcode.

        Args:
            input_: Ignored.

        Returns:
            Formatted messages.
        """
        self.reset()
        return self.output_formatter([{"opcode": "discontinuity", "data": None}])

    def time(self, value):
        """Process an incoming time opcode, applying group delay.

        Args:
            value (decimal): Timestamp value.

        Returns:
            Formatted timestamp message.
        """
        self._sample_time.time = decimal.Decimal(value)

        ret = [[{"opcode": "time", "data": self._sample_time.opcode_time()}]]

        return self.output_formatter(*ret)
