#!/usr/bin/env python3

# This file is used to generate taps for the unit tests.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Tap generator for the unit tests."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from sdr_dsp.tap_generator import generate_taps

# Parse arguments
output_path = sys.argv[-1]

number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
taps_shape = os.environ["OCPI_TEST_taps_shape"]


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

logging.debug("Generating {} taps with {} window"
              .format(number_of_taps, taps_shape))

seed = ocpi_testing.get_test_seed(number_of_taps, taps_shape)

taps = generate_taps(number_of_taps, number_of_branches=1,
                     taps_shape=taps_shape,
                     seed=seed)

with open(output_path, "w") as f:
    for tap in taps:
        tap = tap
        f.write(f"{tap}\n")
