#!/usr/bin/env python3

# Python implementation of threshold block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of threshold."""

import opencpi.ocpi_testing as ocpi_testing


class Threshold(ocpi_testing.Implementation):
    """Python implementation of threshold."""

    def __init__(self, threshold, invert_output):
        """Initialise the class.

        Args:
            threshold (short): Threshold at which to quantise the data.
            invert_output (bool): When true, the output is inverted.
        """
        super().__init__(threshold=threshold, invert_output=invert_output)

        self._below_threshold = self.invert_output
        self._above_threshold = not self.invert_output

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        pass

    def sample(self, values):
        """Handle an incoming sample opcode.

        Args:
            values (list of unsigned): The sample values on the input port.

        Returns:
            List of boolean: Formatted messages.
        """
        output_values = [self._below_threshold] * len(values)
        for index, value in enumerate(values):
            if value >= self.threshold:
                output_values[index] = self._above_threshold

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
