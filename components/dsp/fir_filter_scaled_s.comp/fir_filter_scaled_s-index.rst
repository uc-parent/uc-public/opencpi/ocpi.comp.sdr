.. fir_filter_scaled_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter_scaled_s:


Finite Impulse Response (FIR) Filter (scaled) (``fir_filter_scaled_s``)
=======================================================================
Applies finite impulse response (FIR) filter to a stream of values, with an output scale factor to prevent overflows.
The output of the FIR filter is divided by :math:`2^{\texttt{scale_factor}}` before passing out of component.

Design
------
.. ocpi_documentation_include:: ../doc/fir_filter_scaled/design.inc

   |component_name|: fir_filter_scaled_s
   |include_dir|: ../doc/fir_filter_scaled/

Interface
---------
.. literalinclude:: ../specs/fir_filter_scaled_s-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
.. ocpi_documentation_include:: ../doc/fir_filter_scaled/opcodes.inc

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../fir_filter_scaled_s.hdl ../fir_filter_scaled_s.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
.. ocpi_documentation_include:: ../doc/fir_filter_scaled/dependencies.inc

   |optional_128_bit_dependency|:

Limitations
-----------
.. ocpi_documentation_include:: ../doc/fir_filter_scaled/limitations.inc

   |tap_width_minus_1|: 15
   |component_name|: fir_filter_scaled_s


Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
