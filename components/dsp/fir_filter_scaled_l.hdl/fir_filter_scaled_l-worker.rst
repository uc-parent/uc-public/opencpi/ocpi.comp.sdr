.. fir_filter_scaled_l HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _fir_filter_scaled_l-HDL-worker:


``fir_filter_scaled_l`` HDL Worker
==================================
HDL implementation with adjustable number of multipliers.

Detail
------
.. ocpi_documentation_include:: ../doc/fir_filter_scaled/hdl_worker_detail.inc

   |component_name|: fir_filter_scaled_l

It should be noted that most FPGAs cannot perform a 32 bit x 32 bit MAC operation within a single DSP block. Where possible the `fir_filter_scaled_s` should be used instead.

.. ocpi_documentation_worker::

   number_of_multipliers: Sets the number of multiplier units used in the FIR filter.

Utilisation
-----------
.. ocpi_documentation_utilization::
