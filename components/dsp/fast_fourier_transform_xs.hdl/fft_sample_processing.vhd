-- Fast Fourier Transform sample data processing
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library work;
use work.fast_fourier_transform_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.metadata_inserter_v2;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;

library sdr_dsp;
use sdr_dsp.sdr_dsp.fast_fourier_transform_map;

entity fft_sample_processing is
  generic (
    data_width_g  : positive;
    inverse_fft_g : boolean;
    fft_length_g  : integer
    );
  port (
    clk           : in std_logic;
    reset         : in std_logic;
    enable        : in std_logic;
    discontinuity : in std_logic;

    gain_factor : in unsigned(15 downto 0);

    input_in : in  worker_input_in_t;
    fft_0bin : out std_logic;

    output_data          : out std_logic_vector(31 downto 0);
    processed_data_valid : out std_logic;
    processed_end_in     : out std_logic;
    processed_ready_out  : in  std_logic

    );
end fft_sample_processing;

architecture rtl of fft_sample_processing is
  constant input_width_c              : integer := input_in.data'length/2;
  constant fft_length_width_c         : integer := integer(ceil(log2(real(fft_length_g))));
  constant gain_value_high_c          : integer := 32;
  constant gain_factor_binary_point_c : integer := 8;
  constant fft_input_growth_c         : integer := 1;
  constant gain_data_width_c          : integer := (input_width_c + gain_factor_binary_point_c) + (gain_factor'length +1);
  -- The output width seems to accumulate for every 2 recursive folds of the DFT
  constant output_width_c             : integer := (fft_input_growth_c+input_width_c) + (fft_length_width_c/2) + 1;

  -- Signals related to the input of the FFT
  signal in_valid_r             : std_logic;
  signal fft_valid              : std_logic;
  signal fft_enable             : std_logic;
  signal input_data_i           : std_logic_vector(input_width_c-1 downto 0);
  signal input_data_q           : std_logic_vector(input_width_c-1 downto 0);
  signal gain_data_i            : signed(gain_data_width_c-1 downto 0);
  signal gain_data_q            : signed(gain_data_width_c-1 downto 0);
  signal gain_data_i_resized    : std_logic_vector(input_width_c-1 downto 0);
  signal gain_data_q_resized    : std_logic_vector(input_width_c-1 downto 0);
  signal fft_input_data         : std_logic_vector((input_in.data'length)+(2*fft_input_growth_c)-1 downto 0);  -- 2 bits larger than input data
  signal processed_data_valid_i : std_logic;

  -- Signals for output of FFT
  signal fft_0bin_i         : std_logic;
  signal fft_has_output     : std_logic;
  signal fft_output_counter : unsigned(fft_length_width_c - 1 downto 0);
  signal fft_output_data    : std_logic_vector(output_width_c*2-1 downto 0);
  signal output_data_i      : std_logic_vector(output_width_c-1 downto 0);
  signal output_data_q      : std_logic_vector(output_width_c-1 downto 0);


begin

  fft_enable <= fft_valid and enable;

  -- Within OCPI data is stored QI
  input_data_i <= input_in.data(input_data_i'length-1 downto 0);
  input_data_q <= input_in.data(input_in.data'high downto input_data_i'length);

  ------
  -- Gain adjustment
  -- Course gain adjustment to allow scaling of the FFT input data.
  -- The input is saturated, as opposed to wrapping in the case of an overflow.
  gain_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        in_valid_r <= '0';
        fft_valid  <= '0';
      else
        if enable = '1' then
          -- Push valid when EOF is high as well, this is to allow for any
          -- partial frames to complete. (flush with 0's so as to avoid DC
          -- additions).
          in_valid_r <= input_in.valid or input_in.eof;
          fft_valid  <= in_valid_r;

          if input_in.eof = '0' then
            gain_data_i <= (signed(input_data_i) & to_signed(0, 8)) * signed('0' & gain_factor);
            gain_data_q <= (signed(input_data_q) & to_signed(0, 8)) * signed('0' & gain_factor);
          else
            gain_data_i <= (others => '0');
            gain_data_q <= (others => '0');
          end if;

          -- Truncate with saturation
          if gain_data_i(gain_data_i'high downto gain_value_high_c) > 0 then
            -- max value
            gain_data_i_resized(gain_data_i_resized'high)            <= '0';
            gain_data_i_resized(gain_data_i_resized'high-1 downto 0) <= (others => '1');

          elsif gain_data_i(gain_data_i'high downto gain_value_high_c) < -1 then
            -- min value
            gain_data_i_resized(gain_data_i_resized'high)            <= '1';
            gain_data_i_resized(gain_data_i_resized'high-1 downto 0) <= (others => '0');
          else
            gain_data_i_resized <= std_logic_vector(gain_data_i(gain_value_high_c-1 downto 16));
          end if;

          if gain_data_q(gain_data_q'high downto gain_value_high_c) > 0 then
            -- max value
            gain_data_q_resized(gain_data_q_resized'high)            <= '0';
            gain_data_q_resized(gain_data_q_resized'high-1 downto 0) <= (others => '1');

          elsif gain_data_q(gain_data_q'high downto gain_value_high_c) < -1 then
            -- min value
            gain_data_q_resized(gain_data_q_resized'high)            <= '1';
            gain_data_q_resized(gain_data_q_resized'high-1 downto 0) <= (others => '0');
          else
            gain_data_q_resized <= std_logic_vector(gain_data_q(gain_value_high_c-1 downto 16));
          end if;
        end if;
      end if;
    end if;
  end process;

  -- Sign extend, and concatenate data in IQ ordering.
  fft_input_data <= gain_data_i_resized(gain_data_i_resized'high) & gain_data_i_resized
                    & gain_data_q_resized(gain_data_q_resized'high) & gain_data_q_resized;

  fft_gen_i : entity sdr_dsp.fast_fourier_transform_map
    generic map (
      fft_length_g  => fft_length_g,
      inverse_fft_g => inverse_fft_g,
      input_width_g => input_data_i'length + fft_input_growth_c
      )
    port map (
      clk         => clk,
      reset       => reset,
      enable      => fft_enable,
      input_data  => fft_input_data,
      output_data => fft_output_data,
      output_0bin => fft_0bin_i
      );

  -- Mark the FFT valid, SOM and EOM flags
  fft_output_counter_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        fft_has_output <= '0';
      end if;

      if fft_enable = '1' then
        if fft_0bin_i = '1' then
          fft_output_counter <= (others => '0');
          fft_has_output     <= '1';
        else
          fft_output_counter <= fft_output_counter + 1;
        end if;
      end if;
    end if;
  end process;

  processed_data_valid_i <= (fft_enable and (fft_has_output or fft_0bin_i)) and not discontinuity;
  -- Mark the End 2 early, as the counter actually marks complete on
  -- fft_length-1.
  processed_end_in       <= processed_data_valid_i when fft_has_output = '1' and fft_output_counter = (fft_length_g-2)
                            else '0';

  processed_data_valid <= processed_data_valid_i;
  fft_0bin             <= fft_0bin_i;

  output_data_i <= fft_output_data(fft_output_data'high downto output_data_i'length);
  output_data_q <= fft_output_data(output_data_i'high downto 0);
  -- Swap data back into QI ordering.
  output_data   <= output_data_q(output_data_q'high-fft_input_growth_c downto output_data_q'high - (15+fft_input_growth_c)) &
                   output_data_i(output_data_i'high-fft_input_growth_c downto output_data_i'high - (15+fft_input_growth_c));

end rtl;
