-- Fast Fourier Transform metadata inserter
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library work;
use work.fast_fourier_transform_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.metadata_inserter_v2;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;


entity fft_metadata_inserter is
  port (
    clk      : in  std_logic;
    reset    : in  std_logic;
    enable   : in  std_logic;
    take_in  : in  std_logic;
    take_out : out std_logic;

    increment_fft_block_value : in std_logic;
    metadata_id_written       : in std_logic;
    metadata_value_written    : in std_logic;
    metadata_id               : in std_logic_vector(31 downto 0);
    metadata_value            : in std_logic_vector(63 downto 0);

    fft_0bin : in std_logic;
    input_in : in worker_output_out_t;

    output_in  : in  worker_output_in_t;
    output_out : out worker_output_out_t
    );
end fft_metadata_inserter;

architecture rtl of fft_metadata_inserter is

  constant opcode_width_c : integer := integer(ceil(log2(real(complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));

  signal take : std_logic;

  signal message_complete : std_logic;
  signal trigger          : std_logic;
  signal metadata_value_i : unsigned(metadata_value'range) := (others => '0');

  signal input_opcode  : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data   : std_logic_vector(output_out.data'range);


  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

begin

  take_out <= take;

  trigger <= '1' when (input_in.som = '1' and input_in.valid = '1' and
                       input_in.opcode = complex_short_timed_sample_sample_op_e and
                       input_in.eof = '0' and take_in = '1')
             else '0';

  -----------------------------------------------------------------------------
  -- Reads in the metadata id and value from the property data
  -----------------------------------------------------------------------------
  props_data_proc_p : process (clk)
  begin
    if rising_edge(clk) then
      -- Register the metadata value when it is updated
      if metadata_value_written = '1' then
        metadata_value_i <= unsigned(metadata_value);
      elsif increment_fft_block_value = '1' and message_complete = '1' then
        metadata_value_i <= metadata_value_i + 1;
      end if;
    end if;
  end process props_data_proc_p;

  input_opcode <= opcode_to_slv(input_in.opcode);

  metadata_inserter_i : metadata_inserter_v2
    generic map (
      data_width_g        => input_in.data'length,
      opcode_width_g      => opcode_width_c,
      byte_enable_width_g => input_in.byte_enable'length,
      metadata_opcode_g   => opcode_to_slv(complex_short_timed_sample_metadata_op_e),
      data_opcode_g       => opcode_to_slv(complex_short_timed_sample_sample_op_e)
      )
    port map (
      clk                => clk,
      reset              => reset,
      metadata_id        => metadata_id,
      metadata_value     => std_logic_vector(metadata_value_i),
      trigger            => trigger,
      message_complete   => message_complete,
      take_in            => enable,
      take_out           => take,
      input_data         => input_in.data,
      input_byte_enable  => input_in.byte_enable,
      input_opcode       => input_opcode,
      input_som          => input_in.som,
      input_eom          => input_in.eom,
      input_eof          => input_in.eof,
      input_valid        => input_in.valid,
      input_ready        => input_in.give,
      output_data        => output_data,
      output_byte_enable => output_out.byte_enable,
      output_opcode      => output_opcode,
      output_eom         => output_out.eom,
      output_som         => output_out.som,
      output_eof         => output_out.eof,
      output_valid       => output_out.valid,
      output_give        => output_out.give
      );
  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
