-- Flush injector for complex short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- On receipt of a flush opcode, applies backpressure to the input port and
-- generates an input message of `flush_length` stream samples of value zero,
-- before releasing backpressure and sending on the flush opcode. All other
-- message types passthrough undelayed.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.cic_decimator_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.flush_inserter_v2;

entity complex_short_flush_injector is
  generic (
    data_in_width_g         : integer          := 32;
    max_message_length_g    : integer          := 4096;
    processed_data_opcode_g : std_logic_vector := "000";
    flush_opcode_g          : std_logic_vector := "011"
    );
  port (
    clk               : in  std_logic;
    reset             : in  std_logic;
    enable            : in  std_logic;
    data_valid_in     : in  std_logic;
    take_in           : in  std_logic;
    input_in          : in  worker_input_in_t;
    flush_length      : in  unsigned;
    flush_change      : in  std_logic;
    decimation_factor : in  unsigned;
    flush_ready       : out std_logic;
    input_out         : out worker_input_out_t;
    input_interface   : out worker_input_in_t
    );
end complex_short_flush_injector;

architecture rtl of complex_short_flush_injector is

  constant opcode_width_c      : integer := integer(ceil(log2(real(complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := input_in.byte_enable'length;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal input_opcode           : std_logic_vector(opcode_width_c - 1 downto 0);
  signal input_interface_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal decimator_count        : unsigned(decimation_factor'range);
  signal max_count              : unsigned(flush_length'length downto 0) := (others => '0');
  signal flush_length_mod_max   : unsigned(max_count'range);
  signal flush_length_mod_min   : unsigned(max_count'range);
  signal flush_length_diff      : unsigned(max_count'range);
  signal flush_length_calc      : unsigned(max_count'length downto 0);
  signal flush_length_total     : unsigned(flush_length_calc'range);
  signal count_valid            : std_logic;
  signal count_valid_r          : std_logic;
  signal data_seen              : std_logic;
  signal flush                  : std_logic;

  type state_t is (passthrough_message_s, flush_s);
  signal take_flush_opcode : std_logic;
  signal flush_hold        : std_logic;
  signal flush_start       : std_logic;
  signal flush_counter     : unsigned(flush_length_total'range);
  signal message_counter   : unsigned(integer(ceil(log2(real(max_message_length_g)))) - 1 downto 0);
  signal current_state     : state_t;

begin

  flush <= '1' when input_in.ready = '1' and input_in.opcode = complex_short_timed_sample_flush_op_e else '0';

  input_opcode <= opcode_to_slv(input_in.opcode);

  -- Calculate samples to decimation boundary based on
  -- decimation_factor - ((flush_length + decimation_counter)
  --    MOD decimation_factor)
  flush_length_calc_p : process(clk)
  begin
    if rising_edge(clk) then
      flush_ready <= '1';
      -- calculate the modulation values
      if flush_change = '1' and max_count < flush_length then
        max_count   <= resize(max_count + decimation_factor, max_count'length);
        flush_ready <= '0';
      elsif flush_change = '0' then
        max_count   <= (others => '0');
        flush_ready <= '0';
      end if;
      flush_length_mod_max <= max_count;
      flush_length_mod_min <= max_count - decimation_factor;
      flush_length_diff    <= flush_length_mod_max - flush_length;
    end if;
  end process;

  -- track decimation progress and calculate the correct
  -- modulation based on the counter
  decimator_track_p : process(clk)
    variable flush_length_mod_v : unsigned(flush_length_mod_max'range);
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data_seen       <= '0';
        decimator_count <= to_unsigned(0, decimation_factor'length);
        count_valid     <= '0';
        count_valid_r   <= '0';
      else
        if data_valid_in = '1' then
          data_seen <= '1';
          -- track progress into a decimated output sample
          if decimator_count >= decimation_factor-1 then
            decimator_count <= (others => '0');
          else
            decimator_count <= decimator_count + 1;
          end if;
        end if;

        -- decide which modulus value to use
        if decimator_count > flush_length_diff then
          flush_length_calc <= resize(decimation_factor + flush_length_mod_max, flush_length_calc'length);
        else
          flush_length_calc <= resize(decimation_factor + flush_length_mod_min, flush_length_calc'length);
        end if;

        -- flush no zeroes when there has been no data
        if data_seen = '0' then
          flush_length_total <= (others => '0');
        -- use base flush length to avoid unnecessary extra zero
        elsif decimator_count = flush_length_mod_max or decimator_count = flush_length_mod_min then
          flush_length_total <= resize(flush_length, flush_length_total'length);
        -- use flush length to next decimation boundary
        else
          flush_length_total <= resize(flush_length_calc - decimator_count, flush_length_total'length);
        end if;
        -- delay registering of flush till count is valid
        if data_valid_in = '0' then
          count_valid   <= '1';
          count_valid_r <= count_valid;
        else
          count_valid   <= '0';
          count_valid_r <= '0';
        end if;
        if current_state = flush_s then
          data_seen <= '0';
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Flush Logic
  ------------------------------------------------------------------------------
  flush_state_machine_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state     <= passthrough_message_s;
        take_flush_opcode <= '0';
      else
        case current_state is
          when passthrough_message_s =>
            if input_in.ready = '1' then
              -- If the input opcode is a flush and we have not yet flushed
              -- the data samples.
              if input_opcode = flush_opcode_g and take_flush_opcode = '0' and count_valid_r = '1' then
                if flush_length_total = 0 then
                  take_flush_opcode <= '1';
                else
                  current_state   <= flush_s;
                  flush_counter   <= (flush_length_total - 1);
                  flush_start     <= '1';
                  message_counter <= to_unsigned((max_message_length_g - 1), message_counter'length);
                end if;
              elsif (input_in.eom = '1' and take_in = '1') then
                -- Clear after the flush is sent. Check if EOM as flush could
                -- be a split message.
                take_flush_opcode <= '0';
              end if;
            end if;
          when others =>
            if take_in = '1' then
              if message_counter = 0 then
                message_counter <= to_unsigned((max_message_length_g - 1), message_counter'length);
              else
                message_counter <= message_counter - 1;
              end if;
              flush_counter <= flush_counter - 1;
              flush_start   <= '0';
              if flush_counter = 0 then
                take_flush_opcode <= '1';
                current_state     <= passthrough_message_s;
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- Disables input until data has been flushed
  flush_hold <= '0' when input_opcode /= flush_opcode_g or take_flush_opcode = '1' else '1';
  -- Flush interface signals
  output_mux_p : process(current_state, input_in.ready, input_in.data, input_in.valid,
                         input_in.som, input_in.eom, input_in.eof, input_in.byte_enable, input_opcode,
                         flush_counter, flush_start, flush_hold, message_counter)
  begin
    if (current_state = passthrough_message_s) then
      input_interface.ready       <= input_in.ready and not flush_hold;
      input_interface.data        <= input_in.data;
      input_interface.valid       <= input_in.valid;
      input_interface.som         <= input_in.som;
      input_interface.eom         <= input_in.eom;
      input_interface.eof         <= input_in.eof;
      input_interface.byte_enable <= input_in.byte_enable;
      input_interface_opcode      <= input_opcode;
    else
      input_interface.ready <= '1';
      input_interface.data  <= (others => '0');
      input_interface.valid <= '1';
      if flush_start = '1' or message_counter = (max_message_length_g - 1) then
        input_interface.som <= '1';
      else
        input_interface.som <= '0';
      end if;
      if flush_counter = 0 or message_counter = 0 then
        input_interface.eom <= '1';
      else
        input_interface.eom <= '0';
      end if;
      input_interface.eof         <= '0';
      input_interface.byte_enable <= (others => '1');
      input_interface_opcode      <= processed_data_opcode_g;
    end if;
  end process;

  input_interface.opcode <= slv_to_opcode(input_interface_opcode);

  -- Passthrough take except when we are flushing
  input_out.take <= take_in and not flush_hold;


end rtl;
