.. fir_filter_symmetric_dependencies

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The dependencies to other elements in OpenCPI are:

 * HDL OpenCPI dependencies:

   * :ref:`Symmetric FIR Primitive <fir_filter_symmetric-primitive>`.

   * :ref:`|primitive_name| <|primitive_link|>`.

   * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`.

   * ``ocpi.types.all``

 * RCC OpenCPI dependencies:

   * ``components/dsp/common/fir/symmetric_fir_core.hh``

   * ``components/math/common/time_utils.hh``

There are also dependencies on:

 * HDL specific dependencies:

   * ``ieee.std_logic_1164``

   * ``ieee.numeric_std``

   * ``ieee.math_real``

 * RCC specific dependencies:

   * None.
