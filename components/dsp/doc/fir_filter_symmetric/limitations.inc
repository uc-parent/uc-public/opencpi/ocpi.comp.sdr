.. fir_filter_symmetric_limitations

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Limitations of ``|component_name|`` are:

 * HDL Limitations:

   * :ref:`Symmetric FIR Primitive Limitations <fir_filter_symmetric-primitive_Limitations>`.

   * :ref:`Protocol Interface Delay Primitive v2 Limitations <protocol_interface_delay_v2-primitive_Limitations>`.

   * :ref:`Flush Inserter Primitive v2 Limitations <flush_inserter_v2-primitive_Limitations>`.

 * RCC Limitations:

   * None.
