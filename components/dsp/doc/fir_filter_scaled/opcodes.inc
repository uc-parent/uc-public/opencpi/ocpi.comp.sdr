.. fir_filter_scaled_opcodes

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The FIR filter is applied to values in sample opcode messages.

When a flush opcode message is received, the filter buffer is flushed by injecting ``number_of_taps`` zero value samples. After the flush has been completed and the resulting sample opcode message sent, the flush message is then forwarded on.

When a time opcode message is received, the group-delay value is subtracted from it and the new value forwarded to the output port.

When a discontinuity opcode message is received, the filter buffer is reset, and the discontinuity message is then forwarded on.

All other opcodes are passed through this component.
