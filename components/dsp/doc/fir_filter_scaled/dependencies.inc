.. fir_filter_scaled_dependencies

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The dependencies to other elements in OpenCPI are:

 * HDL OpenCPI dependencies:

   * :ref:`FIR filter primitive <fir_filter-primitive>`.

   * :ref:`Rounding (half-up) primitive <rounding_halfup-primitive>`.

   * :ref:`Rounding (half-even) primitive <rounding_halfeven-primitive>`.

   * :ref:`Rounding (truncate) primitive <rounding_truncate-primitive>`.

   * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`.

   * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`.

   * ``ocpi.types.all``

   * ``ocpi.wci.all``

 * RCC OpenCPI dependencies:

   * ``components/dsp/common/fir/fir_core.hh``

   * ``components/dsp/common/time_utils.hh``

   |optional_128_bit_dependency|

There is also a dependency on:

 * HDL specific dependencies:

   * ``ieee.std_logic_1164``

   * ``ieee.numeric_std``

   * ``ieee.math_real``

 * RCC specific dependencies:

   * None.
