.. fir_filter_scaled_limitations

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Limitations of ``|component_name|`` are:

 * The implementation does not take advantage of the resource savings possible when the FIR taps are symmetrical.

 * The HDL implementation uses a pipelined adder tree when possible, however if the number of serial adders becomes large other FIR filter implementations are more suitable as the adder tree will use significant FPGA resources.

 * ``scale_factor`` should be set to within the range 0 and (:math:`|tap_width_minus_1| + \lceil log_{2}\left(N \right) \rceil`) (:math:`|tap_width_minus_1|` is the tap width minus 1, also see :eq:`|component_name|-fir-filter-scaled-equation` for notation). Setting it to :math:`\left \lceil log_{2}\left ( \sum_{k=0}^{N-1} h[k] \right ) \right \rceil` will achieve a DC gain through the FIR filter of between 0.5 and 1. There is no protection within the RCC or HDL so if the property is not set within the specified bounds then the output will contain all zeros.

 * The minimum value for the number of taps property is 2. A value less than this will cause the fir_filter to produce incorrect sample values.
