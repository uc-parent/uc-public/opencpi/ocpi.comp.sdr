.. cic_interpolator_design

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

CIC interpolators are an efficient method of increasing the sample rate of an input signal by an integer factor. CIC interpolators do not require any multiplication operations making them particularly suitable for compact FPGA and ASIC designs.

In a CIC interpolator the comb stage is applied first, then the up sampling is applied before the integrator stage is applied last. CIC filters have an inherent gain that typically needs to be removed; in this component this is done via a right shift operation allowing for power-of-2 gain compensation. A half-up rounding unit is used to minimise the amount of DC bias caused by the right shift operation.

A block diagram representation of the implementation is given in :numref:`|component_name|-cic_interpolator_xs-diagram`.

.. _|component_name|-cic_interpolator_xs-diagram:

.. figure:: |include_dir|cic_interpolator_xs.svg
   :alt: Block diagram of CIC interpolator implementation
   :align: center

   Block diagram of CIC interpolator implementation.

In :numref:`|component_name|-cic_interpolator_xs-diagram`

 * :math:`N` is the order of the CIC filter (``cic_order``).

 * :math:`R` is the up sampling factor (``up_sample_factor``)

 * :math:`M` is the delay factor (``cic_delay``, this is typically only ever set to 1 or 2).

 * :math:`S` is the power of 2 gain compensation.

Frequency Response
~~~~~~~~~~~~~~~~~~
The frequency response of a CIC interpolator is given by in :eq:`|component_name|-cic_int_freq_resp-equation`.

.. math::
   :label: |component_name|-cic_int_freq_resp-equation

   H\left (\omega  \right ) = \left | \frac{sin\left ( \frac{\omega M}{2} \right )}{sin\left ( \frac{\omega }{2R} \right )} \right | ^{N}

In :eq:`|component_name|-cic_int_freq_resp-equation`:

 * :math:`H[\omega]` is the narrowband frequency response.

When up sampling a signal it is typically desirable to minimise the amplitude of any images that occur at multiples of the Nyquist frequency of the lower sample rate. The order and delay factor of the CIC sets the wideband frequency response of a CIC filter, with higher values giving more suppression of images.

The wideband frequency response (i.e. the response after the up sampling operation is performed) is shown in :numref:`|component_name|-cic_interpolator_wide_freq_m1-diagram` and :numref:`|component_name|-cic_interpolator_wide_freq_m2-diagram`.

.. _|component_name|-cic_interpolator_wide_freq_m1-diagram:

.. figure:: |include_dir|cic_interpolator_wide_freq_m1.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 1`. :math:`R = 8`.

.. _|component_name|-cic_interpolator_wide_freq_m2-diagram:

.. figure:: |include_dir|cic_interpolator_wide_freq_m2.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 2`. :math:`R = 8`.

Gain
~~~~
Increasing the order and differential delay will result in an increase in the gain of the CIC. A larger gain means the width of internal registers used by the CIC interpolator (set by the parameter ``cic_register_size``) must be larger in order to store the calculated values. This in turn means the filter takes up more space on an FPGA.

The gain of a CIC interpolator is given by :math:`G` in :eq:`|component_name|-cic_int_gain-equation`

.. math::
   :label: |component_name|-cic_int_gain-equation

   G = \frac{\left ( RM \right )^{N}}{R}

The minimum ``cic_register_size`` in bits needed to correctly perform the CIC interpolation is given in :eq:`|component_name|-cic_int_gain_width-equation`. In this component ``inWidth`` is 16 bits.

.. math::
   :label: |component_name|-cic_int_gain_width-equation

   \texttt{cic_register_size} = \text{inWidth} + \left\lceil \log_{2} \left( \frac{(RM)^{N}}{R} \right) \right\rceil

Passband Attenuation
~~~~~~~~~~~~~~~~~~~~
CIC interpolators have attenuation in the original passband. The larger the CIC order and delay factor the more attenuation in the passband. The narrowband frequency response of a CIC interpolator is shown in :numref:`|component_name|-cic_interpolator_narrow_freq-diagram`.

.. _|component_name|-cic_interpolator_narrow_freq-diagram:

.. figure:: |include_dir|cic_interpolator_narrow_freq.svg
   :alt: Graph of narrowband CIC frequency response
   :align: center

   Narrowband (before interpolation) CIC frequency response. :math:`R = 8`.

The narrowband frequency response of a CIC filter is same regardless of the ``up_sample_factor`` (:math:`R`). This is shown in :numref:`|component_name|-cic_interpolator_narrow_freq_r-diagram`.

.. _|component_name|-cic_interpolator_narrow_freq_r-diagram:

.. figure:: |include_dir|cic_interpolator_narrow_freq_r.svg
   :alt: Graph comparing narrowband CIC frequency response for different values of R
   :align: center

   Narrowband (before interpolation) CIC frequency response. :math:`N = 3`. :math:`M = 2`.

Mathematical Representation
~~~~~~~~~~~~~~~~~~~~~~~~~~~
:eq:`|component_name|comb_and_interpolate-equation` and :eq:`|component_name|-cic_interpolate-equation` give a mathematical representation of the completed CIC filter implementation.

.. math::
   :label: |component_name|comb_and_interpolate-equation

   g[n] = \begin{cases}
            \sum_{i=0}^{N} (-1)^{i} \binom{N}{i} x[\frac{n}{R}-iM]  &  \hfil \frac{n}{R} \in \mathbb{Z} \\
            \hfil 0                                                 &  \hfil \text{otherwise}
          \end{cases}

.. math::
   :label: |component_name|-cic_interpolate-equation

   h[n] = \sum_{m=0}^{n}\left( \frac{\prod_{l=1}^{N-1} \left( n+N-m-l \right) }{(N-1)!} g[m] \right)

In :eq:`|component_name|comb_and_interpolate-equation` and :eq:`|component_name|-cic_interpolate-equation`, :math:`h[n]` is the output values and :math:`x[n]` is the input data stream.

In :eq:`|component_name|comb_and_interpolate-equation` and :eq:`|component_name|-cic_interpolate-equation` the notation :math:`\binom{n}{x}` is the binomial expansion, or more informally described as ":math:`n` choose :math:`x`".

After the CIC filter implementation using the above an attenuation stage is applied to allow the option of cancelling out the inherent CIC filter gain, this attenuation is implemented as a right shift, which is equivalent to the expression in :eq:`|component_name|-cic_attenuation_stage-equation`.

.. math::
   :label: |component_name|-cic_attenuation_stage-equation

   y[n] = \frac{h[n]}{2^S}
