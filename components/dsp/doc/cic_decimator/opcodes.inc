.. cic_decimator_opcodes

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Sample opcode messages are down sampled by the CIC decimator which means that only 1 in ``down_sample_factor`` samples result in an output sample. Input sample opcode messages which contain insufficient samples to complete a decimation will be decimated away resulting in no output sample message for that input message.

As this component is designed to down sample the input data stream, when a timestamp opcode message is received there is a chance the sample that the timestamp relates to may not be output from the component. For this reason the timestamp opcode messages are only passed through directly if not processing a decimation block. If a decimation is only partly completed, the timestamp is not directly passed through, but instead stored and incremented (by the sample interval) for each sample processed including zero samples injected by a flush. The corrected timestamp is then inserted into the output stream directly after the next sample. This may result in a single sample sample opcode message before the timestamp message. The result of this is that timestamp opcode messages are passed through the component as close as possible to in position to the relative input stream position, and the timestamp is corrected so that it always contains the time of the next valid message with a sample opcode. This adjustment of the timestamp by the sample interval is in addition to adjustment by the group delay.

Sample interval opcode messages are adjusted for the decimation by multiplying the interval value by the down sample factor. The sample interval opcode message with the new value is sent to the output. The sample interval is stored in the component so that it can be used to increment the timestamp. Note, it is advised to send a timestamp after a sample interval change. See :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>` for more details.

Flush opcode messages trigger the component to insert a number of zero samples decided by the following equation :math:`{\texttt{samples_left}}+{\texttt{cic_order}}*(({\texttt{down_sample_factor}}*{\texttt{cic_differential_delay}})-1)`.
Samples left refers to the number of input samples required to trigger a decimation output, leaving the decimation buffer empty.

These are inserted into the input of the CIC in order to flush out any historical data.

If a timestamp is held pending a decimation sample output, it will be inserted into the output stream immediately after the next output sample. On receipt of a flush opcode message, backpressure is applied to the input port while the samples are flushed. This will typically result in the output of a sample opcode message that is roughly :math:`\frac{\texttt{flush_length}}{\texttt{down_sample_factor}}` samples long. The flush opcode message is then passed from the input to the output. If the CIC filter is in reset state (i.e. after start, discontinuity, or flush) then no zero samples are required and the flush opcode is immediately passed to the output.

Discontinuity opcode messages reset the CIC filter buffers to zero. The discontinuity opcode message is then passed from the input to the output. If a timestamp is held pending a decimation sample output, it will be inserted into the output stream before the discontinuity message.

Metadata opcode messages are forwarded to the output.
