#!/usr/bin/env python3

# Python implementation of cic_interpolator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Cascaded Integrator-Comb Interpolator complex short implementation."""

from numpy import int32
from sdr_dsp.cic_interpolator import CicInterpolatorSingleChannel


import opencpi.ocpi_testing as ocpi_testing
import numpy as np
import decimal
from sdr_interface.sample_time import SampleTime


class CicInterpolator(ocpi_testing.Implementation):
    """Cascaded Integrator-Comb Interpolator reference implementation."""

    def __init__(self, cic_order, delay_factor, up_sample_factor,
                 output_scale_factor, cic_register_size):
        """CIC Interpolator initialisation.

        Args:
            cic_order (int): CIC filter order parameter.
            delay_factor (int): CIC filter delay parameter.
            up_sample_factor (int): Output sample count for each input sample.
            output_scale_factor (int): Number of bits to scale output sample value.
            cic_register_size (int): Size of CIC internal registers.
        """
        super().__init__(cic_order=cic_order,
                         delay_factor=delay_factor,
                         up_sample_factor=up_sample_factor,
                         output_scale_factor=output_scale_factor,
                         cic_register_size=cic_register_size)
        self.flush_length = (cic_order*delay_factor)-1
        self._max_message_samples = 4096

        self._cic_real = CicInterpolatorSingleChannel(
            self.cic_order, self.delay_factor, self.up_sample_factor,
            self.output_scale_factor, self.cic_register_size)
        self._cic_imag = CicInterpolatorSingleChannel(
            self.cic_order, self.delay_factor, self.up_sample_factor,
            self.output_scale_factor, self.cic_register_size)

        self._sample_time = SampleTime(
            interval_up_sampling_factor=self.up_sample_factor)

    def reset(self):
        """Reset CIC filters."""
        self._cic_real.reset()
        self._cic_imag.reset()

    def sample(self, values):
        """Handle sample opcode message.

        Separates the real and imaginary values, performs the
        cic interpolator filter on each, and reassembles back
        into complex samples.

        If the output count is larger than the maximum message size
        then it is split into multiple sample opcode messages.

        Args:
            values (list of complex int): Input sample values.

        Returns:
            Formatted sample messages.
        """
        input_data = np.array(values)
        output_real = self._cic_real.sample(np.real(input_data))
        output_imag = self._cic_imag.sample(np.imag(input_data))
        output = list(np.array(output_real) + (np.array(output_imag) *
                                               complex(0, 1)))

        # Sample messages cannot have more than 4096 samples in each message.
        # As this component increases the data size, break long messages up
        # into messages with no more than 4096 samples.
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]
        return self.output_formatter(messages)

    def flush(self, *inputs):
        """Handle flush opcode.

        Zero values are fed through the cic interpolator filter and then
        the flush opcode forwarded.
        """
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length

            # As sample() will return a named tuple, with the names the output
            # port names - and cic_interpolator only has one output port
            # called "output" get the messages from that port. Then add the
            # flush message as this will be passed through after the flushed
            # data.
            messages = self.sample(flush_data).output
            messages.append({"opcode": "flush", "data": None})
            return self.output_formatter(messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])

    def sample_interval(self, value):
        """sample_interval opcode handler.

        Scales the sample_interval by the interpolation factor.

        Args:
            value (decimal): Sample interval.

        Returns:
            Formatted sample_interval message.
        """
        # Store the received sample interval and apply interpolation factor to output
        self._sample_time.interval = value

        messages = [{"opcode": "sample_interval",
                     "data": self._sample_time.opcode_interval()}]
        return self.output_formatter(messages)

    def discontinuity(self, *inputs):
        """Handle discontinuity opcode.

        Args:
            inputs : Not used.

        Returns:
            Empty array of message dictionaries.
        """
        self.reset()
        return self.output_formatter([{"opcode": "discontinuity", "data": None}])
