<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <!-- Case 00 : Typical Values-->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 01 : Up-sample Factor-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" values="2,4,10,31,48,78,511"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 02 : Scale Output-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" values="0,1,2,8,15,20"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Flush length property requires flush opcode - tested in Case 11 -->
  <!-- Case 03 : CIC Order-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" values="3,4,5"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 04 : CIC Differential Delay-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" values="1,2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 05: CIC Register Size-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="48"/>
  </case>
  <!-- Case 06 : Extreme Sample Values-->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero,real_zero,imaginary_zero"/>
  </case>
  <!-- Case 07 : Input Stressing-->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 08 : Message Size-->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 09 : Time Opcode-->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 10 : Sample Interval Opcode
                 (odd up_sample_factor to test precision)-->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" values="2,5,10,19,511"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 11 : Flush Opcode-->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" values="3,4,5"/>
    <property name="cic_differential_delay" values="1,2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 12 : Discontinuity Opcode-->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 13 : Metadata Opcode-->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" value="5"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 14 : Soak Test-->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="up_sample_factor" values="4,16"/>
    <property name="scale_output" values="5,20"/>
    <property name="cic_order" values="3,4"/>
    <property name="cic_differential_delay" values="1,2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
