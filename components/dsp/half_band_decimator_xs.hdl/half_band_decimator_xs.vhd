-- HDL Implementation of Half Band Decimator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.fir_filter_symmetric;
use sdr_dsp.sdr_dsp.slice_accumulator_number_terms;

architecture rtl of worker is
  function to_string(rounding_type : in work.half_band_decimator_xs_constants.rounding_type_t) return string is
  begin
    case rounding_type is
      when truncate_with_saturation_e => return "truncate_with_saturation";
      when truncate_e                 => return "truncate";
      when half_up_e                  => return "half_up";
      when half_even_e                => return "half_even";
      when others                     => return "truncate";
    end case;
  end function;

  function calculate_delay_length (num_taps : integer; adder_stages : integer) return natural is
    variable return_length : integer;
  begin
    if num_taps rem 4 = 1 then
      -- accumulator delay (3)
      -- rounding (1)
      -- accumlator to adder tree register (1)
      return_length := 5 + adder_stages;
    else
      -- accumulator delay (4)
      return_length := 6 + adder_stages;
    end if;
    return return_length;
  end function;

  constant data_width_c      : integer                              := input_in.data'length/2;
  constant num_taps_c        : integer                              := to_integer(number_of_taps);
  constant mode_c            : string                               := "half_band_decimate";
  constant num_slices_c      : integer                              := (num_taps_c+1)/4;
  constant adder_delay_c     : natural                              := integer(ceil(log2(ceil(real(real(num_slices_c)/3.0)))));
  constant delay_c           : positive                             := calculate_delay_length(num_taps_c, adder_delay_c);
  constant delay_width_c     : positive                             := integer(ceil(log2(real(delay_c))));
  constant delay_unsigned_c  : unsigned(delay_width_c - 1 downto 0) := to_unsigned(delay_c, delay_width_c);
  constant decimation_factor : unsigned(delay_width_c - 1 downto 0) := to_unsigned(2, delay_width_c);

  signal take                 : std_logic;
  signal input_interface      : worker_input_in_t;
  signal no_data              : std_logic;
  signal keep_sample          : std_logic;
  signal flush                : std_logic;
  signal discontinuity        : std_logic;
  signal discontinuity_hold_i : std_logic;
  signal discontinuity_hold   : std_logic;
  signal flush_length         : unsigned(number_of_taps'length downto 0);

  signal tap_data  : signed(15 downto 0);
  signal tap_valid : std_logic;

  signal data_valid_in    : std_logic;
  signal data_eom_in      : std_logic;
  signal input_ready_real : std_logic;

  signal fir_real_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_imag_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_real_output_valid : std_logic;

  signal processed_data   : std_logic_vector((2*data_width_c)-1 downto 0);
  signal processed_end_in : std_logic;

  signal input_hold  : std_logic;
  signal data_enable : std_logic;

begin

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= '1';
  props_out.raw.error <= '0';

  -- interface handling
  take        <= input_ready_real and not (input_hold or discontinuity_hold);
  data_enable <= output_in.ready and not input_hold;

  data_valid_in <= '1' when input_interface.opcode = complex_short_timed_sample_sample_op_e
                   and input_interface.valid = '1' and discontinuity_hold = '0' else '0';

  data_eom_in <= data_valid_in and input_interface.eom;

  discontinuity <= '1' when input_interface.ready = '1' and take = '1'
                   and input_interface.opcode = complex_short_timed_sample_discontinuity_op_e
                   else '0';

  flush <= '1' when input_interface.ready = '1' and take = '1'
           and input_interface.opcode = complex_short_timed_sample_flush_op_e
           else '0';

  -- Coefficient data is only 16 bits, therefore there are 2 coefficient
  -- messages possible on the raw interface
  tap_data <= signed(props_in.raw.data(15 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(31 downto 16));
  -- only allow coefficient valid high for half of coefficients because other
  -- half are same(symmetric)
  tap_valid <= '1' when its(props_in.raw.is_write) else '0';

  -----------------------------------------------------------------------------
  -- Process to check for flush or discontinuity, when no_data (i.e. no data
  -- samples have been passed to the FIR filters) then the flush length is
  -- overridden to 0.
  -----------------------------------------------------------------------------
  no_data_flag_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' or discontinuity = '1' or flush = '1' then
        no_data     <= '1';
        keep_sample <= '1';
      else
        if take = '1' and data_valid_in = '1' then
          no_data     <= '0';
          -- Keep track of whether the current sample is kept or decimated away
          -- (every second sample).
          keep_sample <= not keep_sample;
        end if;
      end if;
    end if;
  end process;

  -- An additional zero sample is injected in the flush if the final sample
  -- would be decimated away.
  flush_length <= to_unsigned(num_taps_c, flush_length'length) when no_data = '0' and keep_sample = '0'
                  else to_unsigned(num_taps_c+1, flush_length'length) when no_data = '0' and keep_sample = '1'
                  else (others => '0');

  discontinuity_hold_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if input_ready_real = '1' then
        discontinuity_hold_i <= '0';
      end if;
      if discontinuity = '1' then
        discontinuity_hold_i <= '1';
      end if;
    end if;
  end process;
  discontinuity_hold <= discontinuity_hold_i and not input_ready_real;
  -----------------------------------------------------------------------------
  -- Data flushing
  -----------------------------------------------------------------------------
  complex_short_flush_injector_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output/4)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  -----------------------------------------------------------------------------
  -- Real Half band filter constructed from the symetric FIR filter
  -----------------------------------------------------------------------------
  fir_filter_symmetric_real_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => discontinuity,
      enable           => data_enable,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data(data_width_c-1 downto 0),
      input_valid_in   => data_valid_in,
      input_last_in    => data_eom_in,
      input_ready_out  => input_ready_real,
      output_data_out  => fir_real_output_data,
      output_valid_out => fir_real_output_valid,
      output_last_out  => processed_end_in,
      output_ready_in  => '1'
      );

  -----------------------------------------------------------------------------
  -- Imaginary Half band filter constructed from the symetric FIR filter
  -----------------------------------------------------------------------------
  fir_filter_symmetric_imag_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1,
      saturation_en_g     => false
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => discontinuity,
      enable           => data_enable,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data((2*data_width_c)-1 downto data_width_c),
      input_valid_in   => data_valid_in,
      input_ready_out  => open,
      output_data_out  => fir_imag_output_data,
      output_valid_out => open,
      output_ready_in  => '1'
      );

  -- Create the data stream by concatenating the real and imaginary(Imag:Real)
  processed_data <= fir_imag_output_data & fir_real_output_data;

  -----------------------------------------------------------------------------
  -- Interface delay module - This is a customised version of the downsample
  -- delay, needed due to the way time stamps and group delays are currently
  -- being applied to the opcodes.
  -----------------------------------------------------------------------------
  interface_delay_i : entity work.complex_short_down_sampled_protocol_delay
    generic map (
      delay_g               => delay_c,
      max_decimation_g      => 2,
      data_in_width_g       => input_in.data'length,
      ocpi_max_bytes_output => to_integer(ocpi_max_bytes_output)
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      enable                 => output_in.ready,
      delay_length           => delay_unsigned_c,
      take_in                => input_ready_real,
      take_out               => open,
      decimation_factor      => decimation_factor,
      group_delay_seconds    => unsigned(props_in.group_delay_seconds),
      group_delay_fractional => unsigned(props_in.group_delay_fractional),
      input_in               => input_interface,
      processed_stream_in    => processed_data,
      processed_valid_in     => fir_real_output_valid,
      processed_end_in       => processed_end_in,
      output_out             => output_out,
      input_hold_out         => input_hold
      );

end rtl;
