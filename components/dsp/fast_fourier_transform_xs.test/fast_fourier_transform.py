#!/usr/bin/env python3

# Python implementation of Fast Fourier Transform for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of Fast Fourier Transform for verification."""

import numpy as np
import math
import logging

from sdr_interface.sample_time import SampleTime

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class FastFourierTransform_ComplexShort(ocpi_testing.Implementation):
    """Complex short version of the FFT ocpi implementation."""

    def __init__(self, inverse_fft=False, indicate_start_of_fft_block=False,
                 start_of_fft_block_id=0, start_of_fft_block_value=0,
                 increment_fft_block_value=False,
                 fft_length=64, gain_factor=1 << 8,
                 metadata_fifo_size=4,
                 max_message_samples=ocpi_protocols.PROTOCOLS["complex_short_timed_sample"].max_sample_length,
                 test_name="",
                 logging_level: str = "DEBUG"):
        """Fast Fourier Transform.

        Args:
            inverse_fft (bool): Perform inverse FFT.
            indicate_start_of_fft_block (bool): Whether to add metadata before each fft frame output.
            If True, the following 3 must be configured:
            start_of_fft_block_id (uint): Id to use for metadata.
            start_of_fft_block_value (uint64): Value to use for metadata.
            increment_fft_block_value (bool):  Increment value after each output.
            fft_length (uint): Number of samples to use for each FFT call.
            gain_factor (ushort): Scaled int (8.8) to apply before FFT.
            metadata_fifo_size (int): Maximum number of opcodes held in metadata_fifo.
            max_message_samples (uint): Maximum number of samples to place in a single sample opcode.
            test_name (string): For logging() calls.
            logging_level (string): For logging() calls.
        """
        super().__init__()

        self._inverse_fft = inverse_fft
        self._indicate_start_of_fft_block = indicate_start_of_fft_block
        self._increment_fft_block_value = increment_fft_block_value
        self._start_of_fft_block_id = int(start_of_fft_block_id)
        self._start_of_fft_block_value = int(start_of_fft_block_value)
        self._gain_factor = float(gain_factor) / (2**8)
        self._metadata_fifo_size = metadata_fifo_size
        self._max_message_samples = max_message_samples
        self._processed_buffer_count = 0

        logging.basicConfig(
            level=logging_level, filename=f"fast_fourier_transform_python-{test_name}.log")

        if math.log2(fft_length).is_integer() is not True:
            logging.error("fft_length MUST be a power of 2")
            raise ValueError("FFT Length")

        self._fft_length = int(fft_length)

        self._sample_time = SampleTime()

        self.reset()

    def reset(self):
        """Initialises internal state."""
        self._sample_buffer = []
        self._sample_time.time = None
        self._metadata_fifo = []

    def _flush_held_opcodes(self):
        """Gets held opcodes (time and metadata) for output and clears.

        Returns:
            List of output messages.
        """
        messages = []
        if self._sample_time.time is not None:
            messages.append(
                {"opcode": "time", "data": self._sample_time.opcode_time()})
            self._sample_time.time = None

        messages += self._metadata_fifo
        self._metadata_fifo = []
        return messages

    def sample(self, samples):
        """Processes the sample opcode.

        Performs fft frame processing and generates output data.

        Args:
            samples (list of complex int): Sample input values.

        Returns:
            Formatted messages. This may contain:
                Zero or more sample opcodes containing FFT frames.
                Zero or more timestamp opcodes.
                Zero or more metadata opcodes.
        """
        self._any_samples_seen = True
        logging.debug(f"Sample buffer received with {len(samples)} values")

        if self._sample_time.time is not None:
            self._sample_time.advance_time_by(
                min(len(samples), self._fft_length-len(self._sample_buffer)))

        messages = self._process_samples(samples)
        return self.output_formatter(messages)

    def _process_samples(self, samples, flush=False):
        """Handles internal processing of sample values.

        Responsible for creating a fft frame from sample data,
        parsing fft frame into the fft core and turning the output
        of the fft core into an array of complex values.

        Args:
            samples (List of Complex Short): Sample input values.
            flush (boolean): Indicates if a flushing operation is in progress.

        Returns:
            Processed FFT Frame in the form of an array of complex values.
        """
        messages = []

        logging.debug("input: {} samples".format(len(samples)))
        self._sample_buffer.extend(samples)

        while len(self._sample_buffer) >= self._fft_length:

            if self._indicate_start_of_fft_block:
                messages.append(self._metadata_inserter())

            fft_frame = [o * self._gain_factor
                         for o in self._sample_buffer[:self._fft_length]]

            self._sample_buffer = self._sample_buffer[self._fft_length:]
            fft_frame_clipped = [complex(
                (np.clip(np.real(o), -(2**15), (2**15)-1)),
                (np.clip(np.imag(o), -(2**15), (2**15)-1))
            ) for o in fft_frame]
            if any([a != b for a, b in zip(fft_frame, fft_frame_clipped)]):
                logging.info(
                    f"clipping occurred in the input fft buffer {self._processed_buffer_count}")
                logging.debug(f"FFT buffer before clipping: {fft_frame}\n"
                              f"FFT Frame After Clipping: {fft_frame_clipped}")
            else:
                logging.debug(f"FFT buffer: {fft_frame}")

            if self._inverse_fft:
                output = np.fft.ifft(fft_frame_clipped, self._fft_length)
            else:
                output = np.fft.fft(
                    fft_frame_clipped, self._fft_length) / self._fft_length

            output_buffer = [complex(
                np.int16(np.clip(np.real(o), -(2**15), (2**15)-1)),
                np.int16(np.clip(np.imag(o), -(2**15), (2**15)-1))
            ) for o in output]
            output = [complex(np.int16(np.real(o)), np.int16(np.imag(o)))
                      for o in output]
            if any([a != b for a, b in zip(output_buffer, output)]):
                logging.info(
                    f"clipping occurred in the output fft buffer {self._processed_buffer_count}")
                logging.debug(f"Output Frame before Clipping: {output}\n"
                              f"Output Frame after Clipping: {output_buffer}")
            else:
                logging.debug(f"Output Frame: {output}")

            max_message_length = min(
                self._max_message_samples, self._fft_length)
            logging.debug("output: {} samples".format(len(output_buffer)))
            while len(output_buffer) > 0:
                logging.debug(
                    "opcode: sample len: {}".format(max_message_length))
                messages.append(
                    {"opcode": "sample",
                     "data": output_buffer[:max_message_length]})
                output_buffer = output_buffer[max_message_length:]

            if not flush:
                messages += self._flush_held_opcodes()

            self._processed_buffer_count += 1

        return messages

    def _flush(self):
        """Flush the buffer.

        If the buffer is partially filled then complete with zeros
        and output, then output flush opcode.

        Returns:
            Formatted messages.
        """
        messages = []
        # Flush partially filled buffer with zeros.
        if (0 < len(self._sample_buffer) < self._fft_length):
            samples_until_complete_frame = self._fft_length - \
                len(self._sample_buffer)
            flush_data = [complex(0, 0)] * samples_until_complete_frame
            logging.debug("flush {} samples".format(len(flush_data)))
            messages = self._process_samples(flush_data, True)
        logging.debug("output flush opcode")
        messages.append({"opcode": "flush", "data": 0})
        return messages

    def flush(self, input_):
        """Process an incoming message with the flush opcode.

        Args:
            input_: Ignored.

        Returns:
            Formatted messages.
        """
        logging.debug("flush opcode: buffer: {}._fft_length: {}"
                      .format(len(self._sample_buffer), self._fft_length))
        messages = self._flush()
        messages += self._flush_held_opcodes()
        self.reset()
        return self.output_formatter(messages)

    def _metadata_inserter(self):
        """Create and insert a message with the metadata opcode.

        Returns:
            Metadata opcode tuple.
        """
        metadata = {"opcode": "metadata", "data": {
            "id": self._start_of_fft_block_id, "value": self._start_of_fft_block_value}}
        if self._increment_fft_block_value:
            self._start_of_fft_block_value = (
                self._start_of_fft_block_value+1) % (2**64)
        return metadata

    def sample_interval(self, value):
        """Process an incoming message with the sample_interval opcode.

        Args:
            value (decimal): Sample Interval value.

        Returns:
            Formatted messages.
        """
        messages = []
        self._sample_time.interval = value
        if (0 < len(self._sample_buffer) < self._fft_length):
            messages += self._flush()
        messages.append({"opcode": "sample_interval",
                         "data": self._sample_time.opcode_interval()})

        messages += self._flush_held_opcodes()

        return self.output_formatter(messages)

    def time(self, value):
        """Process an incoming message with the time opcode.

        Args:
            value (decimal): Timestamp value.

        Returns:
            Forwarded time or empty message list.
        """
        if len(self._sample_buffer) == 0:
            self._sample_time.time = None
            return self.output_formatter([{"opcode": "time", "data": value}])

        self._sample_time.time = value
        return self.output_formatter([])

    def discontinuity(self, input_):
        """Resets internal buffers and forward to output.

        Args:
            input_: Ignored.

        Returns:
            Formatted messages.
        """
        messages = self._flush_held_opcodes()
        self.reset()
        messages.append({"opcode": "discontinuity", "data": None})
        return self.output_formatter(messages)

    def metadata(self, value):
        """Process an incoming message with the metadata opcode.

        Args:
            value (tuple): Metadata id/value pair.

        Returns:
            Formatted messages.
        """
        messages = []
        if (0 < len(self._sample_buffer) < self._fft_length):
            # Processing buffer so push onto fifo.
            self._metadata_fifo.append({"opcode": "metadata", "data": value})
            if len(self._metadata_fifo) > self._metadata_fifo_size:
                logging.debug("Metadata FIFO overflowed")
                self._metadata_fifo.pop(0)
        else:
            # Not processing so just forward.
            messages.append({"opcode": "metadata", "data": value})

        return self.output_formatter(messages)

    def end_of_files(self):
        """Generate any additional messages once the inputs have ended.

        Returns:
            Formatted messages.
        """
        logging.debug("end_of_files: buffer: {}._fft_length: {}"
                      .format(len(self._sample_buffer), self._fft_length))
        # Flush out any unfinished FFT frame
        messages = self._flush()
        # But don't send the trailing flush opcode
        messages = messages[:-1]
        return self.output_formatter(messages)
