#!/usr/bin/env python3

# Script to verify output of the half_band_interpolator_xs worker
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Half Band Interpolator verify."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from half_band_interpolator import HalfBandInterpolator_ComplexShort

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case()
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 0))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logging.basicConfig(
    filename=f"{test_id}.half_band_interpolator_xs.py.log",
    filemode="w",
    level=verify_log_level)

rounding_type = os.environ.get("OCPI_TEST_rounding_type", "half_even")
overflow_type = "wrap"
if rounding_type == "truncate_with_saturation":
    rounding_type = "truncate"
    overflow_type = "saturate"

number_of_taps = int(os.environ.get("OCPI_TEST_number_of_taps"))
group_delay_seconds = int(os.environ.get("OCPI_TEST_group_delay_seconds", 0))
group_delay_fractional = int(os.environ.get("OCPI_TEST_group_delay_fractional",
                                            0))

taps_raw_input = os.environ.get("OCPI_TEST_taps").split(",")
# OCPI will trim multiple consecutive zeros from the end of the array, and will
# also add an extra 0 at the end. Initialise tap array to enough values (zeros)
# and then populate with given values.
filter_taps = [0] * number_of_taps
for i, x in enumerate(taps_raw_input):
    if i < number_of_taps:
        filter_taps[i] = int(x)
    else:
        if int(x) != 0:
            raise ValueError(
                "Taps contains too many values (expected: {}, got: {})\n{}"
                .format(number_of_taps, len(taps_raw_input), taps_raw_input))


symmetric_filter_implementation = HalfBandInterpolator_ComplexShort(
    number_of_taps=number_of_taps,
    taps=filter_taps,
    rounding_type=rounding_type,
    overflow_type=overflow_type,
    group_delay_seconds=group_delay_seconds,
    group_delay_fractional=group_delay_fractional,
    logging_level=verify_log_level)

verifier = ocpi_testing.Verifier(symmetric_filter_implementation)
verifier.set_port_types(["complex_short_timed_sample"],
                        ["complex_short_timed_sample"],
                        ["equal"])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
