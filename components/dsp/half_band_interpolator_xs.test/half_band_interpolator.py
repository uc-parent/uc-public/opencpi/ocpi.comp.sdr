#!/usr/bin/env python3

# Python implementation of symmetric fir filter for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Half Band Interpolator."""

import decimal
import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing
from sdr_interface.sample_time import SampleTime
from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric_ComplexInt


class HalfBandInterpolator_ComplexShort(ocpi_testing.Implementation):
    """Half Band Interpolator reference implementation."""

    def __init__(self,
                 number_of_taps,
                 taps=[],
                 rounding_type="half_even",
                 overflow_type="wrap",
                 group_delay_seconds=0,
                 group_delay_fractional=0,
                 logging_level: str = "INFO"):
        """Initialise all members.

        Args:
            number_of_taps (int)    : Number of coefficients.
            taps (list)             : The coefficient values
            rounding_type (string)  : Rounding method to use when scaling.
            overflow_type (string)  : How to overflow result.
            group_delay_seconds (int) : Modelled delay through system (seconds)
            group_delay_fractional (int) : (fraction)
            logging_level (string)  : To control amounts of debug output
        """
        super().__init__(
            number_of_taps=number_of_taps,
            taps=taps,
            rounding_type=rounding_type,
            logging_level=logging_level)

        if (number_of_taps+1) % 4 != 0:
            raise ValueError(
                f"FAILURE: number of taps ({number_of_taps}) is not 4n-1")

        self._fir_filter = FirFilterSymmetric_ComplexInt(
            number_of_taps=number_of_taps,
            taps=taps,
            rounding_type=rounding_type,
            overflow_type=overflow_type,
            half_band_interpolator=True,
            half_band_decimator=False,
            logger_level=logging_level,
            input_depth=16,
            internal_depth=32,
            output_scale=15,
            output_depth=16)

        self._sample_time = SampleTime(
            group_delay_seconds=group_delay_seconds,
            group_delay_fractional=group_delay_fractional,
            interval_up_sampling_factor=2)

    def group_delay_seconds(self):
        """Get group delay seconds value.

        Returns:
            Group delay seconds part (int).
        """
        return self._sample_time.get_group_delay_seconds()

    def group_delay_fractional(self):
        """Get group delay fractional value.

        Returns:
            Group delay fractional part as (int).
        """
        return self._sample_time.get_group_delay_fractional()

    def reset(self):
        """Reset FIR filter."""
        self._fir_filter.reset()

    def sample(self, input_):
        """Top level function to process given sample.

        Args:
            input_ (list)  : Input Data.

        Returns:
            Output messages.
        """
        output = self._fir_filter.process_samples(input_)
        max_message_samples = ocpi_protocols.PROTOCOLS["complex_short_timed_sample"].max_sample_length
        messages = [{
            "opcode": "sample",
            "data": output[index: index + max_message_samples]}
            for index in range(0, len(output), max_message_samples)]
        return self.output_formatter(messages)

    def sample_interval(self, value):
        """Sample Interval opcode handler.

        Args:
            value (int) : Sample interval.

        Returns:
            Formatted interval message.
        """
        # Store the received sample interval
        self._sample_time.interval = decimal.Decimal(value)

        messages = [{"opcode": "sample_interval",
                     "data": self._sample_time.opcode_interval()}]
        return self.output_formatter(messages)

    def time(self, value):
        """Time opcode handler.

        Args:
            value (float) : Time stamp.

        Returns:
            Formatted time stamp message.
        """
        self._sample_time.time = decimal.Decimal(value)
        messages = [
            {"opcode": "time", "data": self._sample_time.opcode_time()}]
        return self.output_formatter(messages)

    def discontinuity(self, _):
        """Process discontinuity message.

        Returns:
            Formatted discontinuity message.
        """
        self.reset()
        messages = [{"opcode": "discontinuity", "data": None}]
        return self.output_formatter(messages)

    def flush(self, _):
        """Process flush message.

        Returns:
            Formatted flush sample (if required) and flush messages.
        """
        messages = []
        if self._fir_filter.data_received:
            flush_length = self.number_of_taps
            output = self._fir_filter.process_samples([0] * flush_length)
            messages.append({"opcode": "sample", "data": output})
        # Add a flush to end of output
        messages.append({"opcode": "flush", "data": None})
        self.reset()
        return self.output_formatter(messages)
