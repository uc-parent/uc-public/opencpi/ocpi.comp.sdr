#!/usr/bin/env python3

# Python implementation of Polyphase Decimator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Complex integer version of the Polyphase Decimator."""

import opencpi.ocpi_testing as ocpi_testing
import logging
from sdr_dsp.polyphase_decimator import PolyphaseDecimator_ComplexInt
from sdr_interface.sample_time import SampleTime


class PolyphaseDecimator_XS(ocpi_testing.Implementation):
    """Complex integer version of the Polyphase Decimator.

    This makes use of fixed point integers (i.e. integer arithmetic
    and shifts) and expects inputs/outputs to be complex integers
    """

    def __init__(self, decimation_factor: int,
                 taps=[],
                 max_taps_per_branch=255,
                 max_decimation=255,
                 rounding_type="half_even",
                 group_delay_seconds=0,
                 group_delay_fractional=0,
                 case="",
                 logging_level: str = "WARN"):
        """Initialises a Polyphase Interpolator class.

        Args:
            decimation_factor (int): The current down-sampling factor
            taps (list of int): The coefficient values
            max_taps_per_branch (int): The maximum number of taps per branch
            max_decimation (int): The maximum down-sampling factor
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate"
            group_delay_seconds (number or string representation):
                Integer seconds part of the UQ32.64 value
                for the group delay to apply to time.
            group_delay_fractional (number or string representation):
                Integer representing the fractional part of the UQ32.64 value
                for the group delay to apply to time.
            case (str): Name to use creating logging file
            logging_level (str): Minimum level of log messages to be generated
        """
        logging.basicConfig(level=logging_level,
                            filename=case+".polyphase_decimator_xs.py.log",
                            filemode="w")

        super().__init__()

        # The main processor of samples
        self._decimator = PolyphaseDecimator_ComplexInt(
            decimation_factor=decimation_factor,
            taps=taps,
            bit_depth=16,
            max_decimation=max_decimation,
            max_taps_per_branch=max_taps_per_branch,
            rounding_type=rounding_type,
            logger_level=logging_level)

        self._sample_time = SampleTime(
            group_delay_seconds=group_delay_seconds,
            group_delay_fractional=group_delay_fractional,
            interval_down_sampling_factor=decimation_factor)

        self._decimation_factor = decimation_factor
        self._missed_timestamp = False
        self._decimated_samples = 0

        self.reset()

    def reset(self):
        """Put the implementation into the same state as at initialisation."""
        self._decimator.reset()
        self._sample_time.time = None
        self._missed_timestamp = False
        self._decimated_samples = 0
        self._data_received = False

    def _is_mid_decimation(self):
        """Is the decimator in the middle of a decimation run?

        If the decimator has received a number of input samples that is not a
        multiple of the decimation factor, there will still be some input
        samples buffered. These will not yet have generated the resulting
        output sample.

        Returns:
            bool: True if a decimation is in progress.
        """
        return (self._decimated_samples % self._decimation_factor) != 0

    def _samples_to_finish_decimation(self):
        """Get the number of samples to finish the current decimation run.

        Returns:
            int: The number of samples required to finish the decimation.
        """
        return (self._decimation_factor - self._decimated_samples) % self._decimation_factor

    def sample(self, input_samples):
        """Get messages resulting from a sample message.

        Args:
            input_samples (list of complex): The incoming samples to process.

        Returns:
            A (potentially empty) list of formatted output messages.
        """
        output_samples = self._decimator.decimate_samples(input_samples)
        self._data_received = True

        ret = [[]]

        if len(output_samples) == 0:
            self._sample_time.advance_time_by(len(input_samples))
        else:
            # we want to check that the timestamp is in this buffer
            # (i.e. we are not 1 sample early), and if a timestamp is expected
            if (self._missed_timestamp):
                # send out the next sample, then timestamp

                n = self._samples_to_finish_decimation()
                self._sample_time.advance_time_by(n)

                logging.debug(
                    f"Inserting time: {self._sample_time.opcode_time()}")
                ret[0].append({"opcode": "sample",
                               "data": output_samples[:1]})
                ret[0].append({"opcode": "time",
                               "data": self._sample_time.opcode_time()})
                if len(output_samples) > 1:
                    ret[0].append({"opcode": "sample",
                                   "data": output_samples[1:]})
                self._missed_timestamp = False
            else:
                # Output samples without time
                ret = [[{"opcode": "sample", "data": output_samples}]]

        # Keep track of if we are in a decimation or not
        self._decimated_samples += len(input_samples)
        self._decimated_samples %= self._decimation_factor

        return self.output_formatter(*ret)

    def time(self, value):
        """Get messages resulting from a time message.

        Args:
            value (decimal): The incoming time value

        Returns:
            A (potentially empty) list of formatted output messages.
        """
        # Update the internal time counter
        self._sample_time.time = value

        # As we are decimating, we need to wait until there is an outgoing
        # sample. No point in sending time-of-next-sample if its going to be
        # decimated away, unless there has been no data, in which case the
        # value can be passed straight through.
        if self._is_mid_decimation():  # partially through a decimation
            self._missed_timestamp = True
            ret = [[]]
        else:
            # Send right away.
            self.missed_timestamp = False
            ret = [
                [{"opcode": "time", "data": self._sample_time.opcode_time()}]]
        return self.output_formatter(*ret)

    def sample_interval(self, value):
        """Get messages resulting from a sample_interval message.

        Args:
            value (decimal): The incoming sample interval value

        Returns:
            A list of formatted output messages.
        """
        # Store the received sample interval (to be scaled
        # by decimation factor on output)
        self._sample_time.interval = value

        ret = [[{"opcode": "sample_interval",
                 "data": self._sample_time.opcode_interval()}]]
        return self.output_formatter(*ret)

    def flush(self, input_):
        """Get messages resulting from a flush message.

        Args:
            input_: Ignored input data

        Returns:
            A list of formatted output messages.
        """
        if self._data_received:
            # Process zeros to flush the decimation process
            ret = self.sample([0] *
                              (self._decimation_factor*self._decimator._taps_per_branch))
            self.reset()
            # Add a flush to end of output
            ret[0].append({"opcode": "flush", "data": None})
        else:
            ret = [[{"opcode": "flush", "data": None}]]

        return self.output_formatter(*ret)

    def discontinuity(self, input_):
        """Get messages resulting from a discontinuity message.

        Args:
            input_: Ignored input data

        Returns:
            A list of formatted output messages.
        """
        ret = [[]]
        if self._missed_timestamp:
            ret[0].append({"opcode": "time",
                           "data": self._sample_time.opcode_time()})

        self.reset()
        # Add a flush to end of output
        ret[0].append({"opcode": "discontinuity", "data": None})
        return self.output_formatter(*ret)

    def end_of_files(self):
        """Generate any additional messages once the inputs have ended.

        Returns:
            Formatted messages.
        """
        messages = []
        if self._missed_timestamp:
            messages.append({"opcode": "time",
                             "data": self._sample_time.opcode_time()})
        return self.output_formatter(messages)
