#!/usr/bin/env python3

# Generates the input binary file for polyphase interpolator testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generator for the Input port for Polyphase Interpolator (Complex Short)."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
interpolation_factor = int(os.environ["OCPI_TEST_interpolation_factor"])
max_interpolation = int(os.environ.get("OCPI_TEST_max_interpolation", 64))
max_taps = int(os.environ.get("OCPI_TEST_max_number_taps", 64))
number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
rounding_type = os.environ["OCPI_TEST_rounding_type"]
group_delay_seconds = int(os.environ["OCPI_TEST_group_delay_seconds"])
group_delay_fractional = int(os.environ["OCPI_TEST_group_delay_fractional"])

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  interpolation_factor,
                                  max_interpolation,
                                  max_taps,
                                  number_of_taps,
                                  rounding_type,
                                  group_delay_seconds,
                                  group_delay_fractional)

generator = ocpi_testing.generator.ComplexShortGenerator()
# Shorten the default messages, as we are interpolating.
generator.SAMPLE_DATA_LENGTH = 25
# Shorten the number of messages in message size testing, as otherwise this
# test can take over 10 minutes.
generator.MESSAGE_SIZE_NUMBER_OF_MESSAGES = 2

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
