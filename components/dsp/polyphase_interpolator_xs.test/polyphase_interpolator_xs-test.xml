<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="taps_shape" type="string" value="kaiser"/>
  <!-- Case 00: Run typical test case -->
  <!-- Group delay values for easy test value checking -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" values="64,4096"/>
    <property name="number_of_taps" values="8,64"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
  </case>
  <!-- Case 01: Property: Interpolation factor -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" values="2,4,6,8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
  </case>
  <!-- Case 02: Property: max_number_taps and number_of_taps  -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" values="4096,32768"/>
    <property name="number_of_taps" values="128,255,682,4096"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
  </case>
  <!-- Case 03: Property: Rounding -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" values="truncate,half_up,half_even"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
  </case>
  <!-- Case 04: Property: Group Delay tests -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" values="0,0x1,0xffffffff"/>
    <property name="group_delay_fractional" values="0,0x4000000001000000,0xffffffffffffffff"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="zero,maximum"/>
  </case>
  <!-- Case 05: Property: Taps tests -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="taps_shape" values="all_ones,random"/>
  </case>
  <!-- Case 06: Test extreme sample values -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,real_zero,imaginary_zero,large_positive,large_negative,near_zero"/>
  </case>
  <!-- Case 07: Stress input port -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="8192" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
  </case>
  <!-- Case 08: Test different message lengths -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 09: Test timestamp -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 10: Test sample interval -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 11: Test flush -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 12: Test discontinuity -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 13: Test metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" value="8"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 14: Soak test -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="interpolation_factor" values="4,32"/>
    <property name="max_number_taps" value="64"/>
    <property name="number_of_taps" values="35,64"/>
    <property name="rounding_type" value="truncate"/>
    <property name="group_delay_seconds" value="0x00000020"/>
    <property name="group_delay_fractional" value="0x4000000080000000"/>
    <property name="taps" generate="generate_taps.py short 15"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
