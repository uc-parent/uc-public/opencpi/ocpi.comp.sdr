#!/usr/bin/env python3

# Generates messages file for the instep port of carrier_mixer_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the instep value for carrier_mixer_xs testing."""

import random
import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class InStepGenerator(ocpi_testing.generator.LongGenerator):
    """Python implementation of carrier mixer instep generator for testing."""

    def typical(self, seed, subcase):
        """Generate the sample data.

        Args:
            seed (int) : generated seed for test message generation
            subcase (string): Subcase for test currently being run

        Returns:
            Sample messages.
        """
        # Typical behaviour for in step is a repeated value not wave, so
        # override the default typical behaviour for this particular case.
        random.seed(seed)

        data = [
            random.randint(-(2**31), (2**31) - 1)] * self.SAMPLE_DATA_LENGTH

        return [{"opcode": "sample", "data": data}]


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
carrier_amplitude = int(os.environ["OCPI_TEST_carrier_amplitude"])
instep_opcode_passthrough = os.environ[
    "OCPI_TEST_instep_opcode_passthrough"].upper() == "TRUE"

seed = ocpi_testing.get_test_seed(arguments.case, subcase, carrier_amplitude,
                                  instep_opcode_passthrough, arguments.seed)

generator = InStepGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "long_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
