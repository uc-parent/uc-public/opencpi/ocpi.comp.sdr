-- HDL Implementation of Symmetric FIR Filter
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.fir_filter_symmetric;

architecture rtl of worker is
  function to_string(rounding_type : in work.fir_filter_symmetric_xs_constants.rounding_type_t) return string is
  begin
    case rounding_type is
      when truncate_with_saturation_e => return "truncate_with_saturation";
      when truncate_e                 => return "truncate";
      when half_up_e                  => return "half_up";
      when half_even_e                => return "half_even";
      when others                     => return "truncate";
    end case;
  end function;

  constant data_width_c         : integer   := input_in.data'length/2;
  constant num_taps_c           : integer   := to_integer(number_of_taps);
  constant fir_mode_c           : string    := "symmetric_fir";
  constant processed_data_mux_c : std_logic := '1';
  constant dsp_slice_delay_c    : positive  := 5;
  -- 3 terms in the dsp48e accumlator, symmetry requires 1/2 MACs
  constant adder_delay_c        : natural   := integer(ceil(log2(ceil(real((num_taps_c+1)/2)/3.0))));
  constant rounding_delay_c     : positive  := 1;
  constant delay_c              : positive  := dsp_slice_delay_c + adder_delay_c + rounding_delay_c;

  signal take            : std_logic;
  signal input_interface : worker_input_in_t;
  signal no_data         : std_logic;
  signal flush           : std_logic;
  signal discontinuity   : std_logic;
  signal flush_length    : unsigned(number_of_taps'range);
  signal fir_data_reset  : std_logic;

  signal tap_data  : signed(15 downto 0);
  signal tap_valid : std_logic;

  signal data_valid_in    : std_logic;
  signal input_ready_real : std_logic;

  signal fir_real_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_imag_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_real_output_valid : std_logic;
  signal processed_data        : std_logic_vector((2*data_width_c)-1 downto 0);

begin

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= '1';
  props_out.raw.error <= '0';

  -- interface handling
  take <= input_ready_real and output_in.ready;

  data_valid_in <= '1' when (input_interface.opcode = complex_short_timed_sample_sample_op_e)
                   and (input_interface.valid = '1') else '0';

  discontinuity <= '1' when (input_interface.ready = '1') and (take = '1')
                   and (input_in.opcode = complex_short_timed_sample_discontinuity_op_e)
                   else '0';

  fir_data_reset <= discontinuity;

  flush <= '1' when (input_interface.ready = '1') and (take = '1')
           and (input_interface.opcode = complex_short_timed_sample_flush_op_e)
           else '0';

  -- Coefficient data is only 16 bits, therefore there are 2 coefficient
  -- messages possible on the raw interface
  tap_data <= signed(props_in.raw.data(15 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(31 downto 16));
  -- only allow coefficient valid high for half of coefficients because other
  -- half are same(symmetric)
  tap_valid <= '1' when its(props_in.raw.is_write) else '0';

  ------------------------------------------------------------------------------
  -- Process to check for flush or discontinuity, when no_data (i.e. no data
  -- samples have been passed to the FIR filters) then the flush length is
  -- overridden to 0.
  ------------------------------------------------------------------------------
  no_data_flag_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        no_data <= '1';
      else
        if input_ready_real = '1' and data_valid_in = '1' then
          no_data <= '0';
        end if;
        if flush = '1' or discontinuity = '1' then
          no_data <= '1';
        end if;
      end if;
    end if;
  end process;

  flush_length <= number_of_taps when no_data = '0' else (others => '0');

  ------------------------------------------------------------------------------
  -- Data flushing
  ------------------------------------------------------------------------------
  complex_short_flush_injector_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output/4)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  fir_filter_symmetric_real_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => fir_mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => fir_data_reset,
      enable           => output_in.ready,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data(data_width_c-1 downto 0),
      input_valid_in   => data_valid_in,
      input_ready_out  => input_ready_real,
      output_data_out  => fir_real_output_data,
      output_valid_out => fir_real_output_valid,
      output_ready_in  => '1'
      );

  fir_filter_symmetric_imag_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => fir_mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1,
      saturation_en_g     => false
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => fir_data_reset,
      enable           => output_in.ready,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data((2*data_width_c)-1 downto data_width_c),
      input_valid_in   => data_valid_in,
      input_ready_out  => open,
      output_data_out  => fir_imag_output_data,
      output_valid_out => open,
      output_ready_in  => '1'
      );

  processed_data <= fir_imag_output_data & fir_real_output_data;

  complex_short_protocol_delay_i : entity work.complex_short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => processed_data_mux_c
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      enable                 => output_in.ready,
      take_in                => input_ready_real,
      input_in               => input_interface,
      processed_stream_in    => processed_data,
      group_delay_seconds    => unsigned(props_in.group_delay_seconds),
      group_delay_fractional => unsigned(props_in.group_delay_fractional),
      output_out             => output_out
      );

end rtl;
