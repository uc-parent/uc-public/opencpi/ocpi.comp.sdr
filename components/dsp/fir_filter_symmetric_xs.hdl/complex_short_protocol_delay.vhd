-- HDL Implementation of complex short protocol delay
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes stream data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.fir_filter_symmetric_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;

entity complex_short_protocol_delay is
  generic (
    delay_g              : integer   := 1;
    data_in_width_g      : integer   := 32;
    processed_data_mux_g : std_logic := '1'
    );
  port (
    clk                    : in  std_logic;
    reset                  : in  std_logic;
    enable                 : in  std_logic;  -- high when output is ready
    take_in                : in  std_logic;  -- high when data is taken from input
    input_in               : in  worker_input_in_t;  -- input streaming interface
    processed_stream_in    : in  std_logic_vector(data_in_width_g - 1 downto 0);
    group_delay_seconds    : in  unsigned(31 downto 0);
    group_delay_fractional : in  unsigned(63 downto 0);
    output_out             : out worker_output_out_t  -- output streaming interface
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is

  constant opcode_width_c : integer := integer(ceil(log2(real(
    complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c  : integer := integer(ceil(log2(real(input_in.byte_enable'length)))) + 1;
  constant time_opcode_stages_c : integer := group_delay_seconds'length + group_delay_fractional'length;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal input_opcode        : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_opcode       : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data         : std_logic_vector(data_in_width_g - 1 downto 0);
  signal output_valid        : std_logic;
  signal output_eom          : std_logic;
  signal output_data_i       : unsigned(data_in_width_g downto 0);
  signal time_opcode_counter : integer range 0 to time_opcode_stages_c-1;
  signal carry_bit           : unsigned(0 downto 0);
begin

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : protocol_interface_delay_v2
    generic map (
      delay_g                 => delay_g,
      data_width_g            => data_in_width_g,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      processed_data_mux_g    => processed_data_mux_g,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e)
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => enable,
      take_in             => take_in,
      processed_stream_in => processed_stream_in,
      input_som           => '0',
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => open,
      output_eom          => output_eom,
      output_eof          => output_out.eof,
      output_valid        => output_valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );

  ----------------------------------------------------------------------------
  -- Time Message Validation
  --
  -- On output of a valid time op-code from the delay component, a counter
  -- flags the current word within the message.
  -- Also captures the carry bit from the previously output time word.
  ----------------------------------------------------------------------------
  time_opcode_valid_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        time_opcode_counter <= 0;
        carry_bit           <= "0";
      elsif enable = '1' then
        if output_valid = '1' and output_opcode = opcode_to_slv(complex_short_timed_sample_time_op_e) then
          if time_opcode_counter = time_opcode_stages_c-data_in_width_g then
            time_opcode_counter <= 0;
            carry_bit           <= "0";
          else
            time_opcode_counter <= time_opcode_counter + data_in_width_g;
            carry_bit           <= output_data_i(output_data_i'high downto output_data_i'high);
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Output Control
  --
  -- Replaces time message data:
  --    Computation on 32-bit seconds and 64-bit fractional values.
  --    16-bit subtraction on each incoming word of the time message.
  --    Additional '0' prepended to the minuend to capture the carry bit
  --    (via 2's complement). This is then subtracted from the next subtraction.
  ------------------------------------------------------------------------------
  output_data_p : process(output_data, output_opcode, group_delay_seconds, group_delay_fractional, time_opcode_counter, carry_bit)
    variable group_delay_v : unsigned(group_delay_seconds'length + group_delay_fractional'length-1 downto 0);
  begin
    -- Default
    output_data_i <= unsigned('0' & output_data);

    -- Time op-code
    if (output_opcode = opcode_to_slv(complex_short_timed_sample_time_op_e)) then

      group_delay_v := unsigned(std_logic_vector(group_delay_seconds) & std_logic_vector(group_delay_fractional));

      output_data_i <= ("0" & unsigned(output_data)) - group_delay_v(time_opcode_counter+data_in_width_g-1 downto time_opcode_counter) - carry_bit;
    end if;
  end process;

  output_out.valid  <= output_valid;
  output_out.data   <= std_logic_vector(output_data_i(output_data_i'high - 1 downto 0));
  output_out.opcode <= slv_to_opcode(output_opcode);
  -- EOM active on last time opcode message. Otherwise EOM passes through.
  output_out.eom    <= '1' when time_opcode_counter = time_opcode_stages_c-1 and output_opcode = opcode_to_slv(complex_short_timed_sample_time_op_e)
                       else output_eom;


end rtl;
