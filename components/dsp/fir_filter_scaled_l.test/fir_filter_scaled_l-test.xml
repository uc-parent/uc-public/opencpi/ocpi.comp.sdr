<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="taps_shape" type="string"/>
  <!-- Worker-specific properties are last in ordering because of OpenCPI issue -->
  <!-- CASE 0: typical -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 1: property (rounding_type) -->
  <!-- Odd and even scale bits -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" values="35,36"/>
    <property name="rounding_type" values="half_up,half_even,truncate,truncate_with_saturation"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="all_ones"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 2: property (scale_factor) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" values="0,1,2,5,10,16,24,36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 3: property (number_of_taps with reduced number_of_multipliers) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="2,16,32,96,127,128,255"/>
    <property name="scale_factor" value="32"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="2"/>
  </case>
  <!-- CASE 4: property (taps) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" values="kaiser,ramp,random,all_ones,all_zeros"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 5: property (number_of_multipliers) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" values="1,2,4,8,16,32,33"/>
  </case>
  <!-- CASE 6: property
  (number_of_multipliers and number_of_taps as non-power-of-2 values) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="127"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="33"/>
  </case>
  <!-- CASE 7: sample -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 8: input_stressing -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 9: message_size -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 10: time and group_delay
                (with all additional/ignored bits set)-->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" values="0,0xffffffff"/>
    <property name="group_delay_fractional" values="0x0000000001000000,0xffffffffff000000"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 11: sample_interval -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 12: flush -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="4,16,255"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="initial,single,consecutive"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 13: discontinuity -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="single,consecutive"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 14: metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="scale_factor" value="36"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0"/>
    <property name="group_delay_fractional" value="0"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" value="4"/>
  </case>
  <!-- CASE 15: soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="128"/>
    <property name="scale_factor" values="11,16"/>
    <property name="rounding_type" value="half_up"/>
    <property name="group_delay_seconds" value="0xffffffff"/>
    <property name="group_delay_fractional" value="0xffffffffff000000"/>
    <property name="taps" generate="generate_taps.py 31"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
    <property name="fir_filter_scaled_l.hdl.number_of_multipliers" values="1,4"/>
  </case>
</tests>
