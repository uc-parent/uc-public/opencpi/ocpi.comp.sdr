#!/usr/bin/env python3

# Generates the input binary file for cic_decimator_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Cascaded Integrator-Comb Decimator complex short generate test data."""

import random
import logging
import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing
from ocpi_block_testing.generator.base_block_generator import BaseBlockGenerator
from ocpi_block_testing.generator.complex_short_block_generator import ComplexShortBlockGenerator
from ocpi_block_testing.get_generate_arguments import get_generate_arguments

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

arguments = get_generate_arguments(generator=BaseBlockGenerator)

subcase = os.environ["OCPI_TEST_subcase"]
cic_order = int(os.environ.get("OCPI_TEST_cic_order"))
delay_factor = int(os.environ.get("OCPI_TEST_cic_differential_delay"))
down_sample_factor = int(os.environ.get("OCPI_TEST_down_sample_factor"))
output_scale_factor = int(os.environ.get("OCPI_TEST_scale_output"))
cic_register_size = int(os.environ.get("OCPI_TEST_cic_register_size"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase, cic_order,
                                  delay_factor, down_sample_factor,
                                  output_scale_factor, cic_register_size)

generator = ComplexShortBlockGenerator(block_length=down_sample_factor)
# Ensure input interval does not cause output interval overflow
generator.SAMPLE_INTERVAL_MAX //= down_sample_factor

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
