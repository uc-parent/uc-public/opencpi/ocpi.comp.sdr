<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <!-- Case 00: Run typical test case-->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 01: Down Sample Factors-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" values="2,4,10,31,48,78,511"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 02: Scale Output-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" values="0,1,2,8,15,20"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 03: CIC Order-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" values="3,4,5"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 04: CIC Differential Delay-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" values="1,2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 05: CIC Register Size-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="48"/>
  </case>
  <!-- Case 06: Sample Values-->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero,real_zero,imaginary_zero"/>
  </case>
  <!-- Case 07: Input Stressing-->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
  </case>
  <!-- Case 08: Message Length-->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 09: Tests the upper 16 bits of high gain messages to ensure the full
       cic register size is available-->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="50"/>
    <property name="scale_output" value="20"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" value="longest"/>
  </case>
  <!-- Case 10: Timestamp Opcode-->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive,intra_consecutive,intra_multiple,eof"/>
  </case>
  <!-- Case 11: Sample Interval Opcode-->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 12: Flush Opcode (relies on down_sample_factor property)-->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" values="2,4,10,31,48,78,511"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="single,consecutive,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 13: Discontinuity Opcode-->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="single,consecutive,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 14: Metadata Opcode-->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="2"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="zero,positive,maximum,consecutive,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 15: Time Calculations
       (with decimation factor to test partial decimation edge-cases)-->
  <case>
    <input port="input" script="generate.py --case time_calculation" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="4"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="interval_change,time_change,minimum_increment,intra_consecutive,intra_multiple"/>
  </case>
  <!-- Case 16: Opcode Interactions
       (with decimation factor to test partial decimation edge-cases)-->
  <case>
    <input port="input" script="generate.py --case opcode_interaction" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" value="4"/>
    <property name="scale_output" value="5"/>
    <property name="cic_order" value="3"/>
    <property name="cic_differential_delay" value="2"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="time_flush,time_discontinuity,time_metadata"/>
  </case>
  <!-- Case 17: Soak Test-->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="down_sample_factor" values="2,5,511"/>
    <property name="scale_output" values="5,20"/>
    <property name="cic_order" values="3,5"/>
    <property name="cic_differential_delay" values="2,5"/>
    <property name="cic_register_size" value="36"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
