-- Delay module for Complex short Protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes stream data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.polyphase_interpolator_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_fifo_v2;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;

library sdr_math;
use sdr_math.sdr_math.non_restoring_divider;

entity complex_short_protocol_delay is
  generic (
    data_in_width_g : integer := 32
    );
  port (
    clk                    : in  std_logic;
    reset                  : in  std_logic;
    enable                 : in  std_logic;  -- high when output is ready
    take_in                : in  std_logic;  -- high when data is taken from input
    pd_hold                : out std_logic;  -- high when data is taken from input
    input_in               : in  worker_input_in_t;  -- input streaming interface
    interpolation_factor   : in  unsigned(7 downto 0);
    group_delay_seconds    : in  unsigned(31 downto 0);
    group_delay_fractional : in  unsigned(63 downto 0);
    processed_stream_in    : in  std_logic_vector(data_in_width_g - 1 downto 0);
    processed_valid_in     : in  std_logic;
    processed_end_in       : in  std_logic;
    output_out             : out worker_output_out_t  -- output streaming interface
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is
  constant opcode_width_c : integer := integer(ceil(log2(real(
    complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := integer(ceil(log2(real(input_in.byte_enable'length)))) + 1;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  constant interval_calc_delay : integer := 4;

  signal input_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal take_in_r    : std_logic_vector(interval_calc_delay-1 downto 0);

  signal up_sample_factor : unsigned(15 downto 0);

  signal pd_enable       : std_logic;
  signal pd_take         : std_logic;
  signal pd_take_out     : std_logic;
  signal pd_hold_in      : std_logic;
  signal pd_hold_out     : std_logic;
  signal divider_ready   : std_logic;
  signal input_si_valid  : std_logic;
  signal output_si_valid : std_logic;
  signal divider_done    : std_logic;
  signal si_valid_pipe   : std_logic_vector(1 downto 0);

  -- Sample interval uses all 64 bits of the fractional data
  signal sample_interval_r        : std_logic_vector(63 downto 0);
  signal sample_interval_input    : signed(96 downto 0);
  signal sample_interval_output   : signed(96 downto 0);
  signal sample_interval_output_r : unsigned(96 downto 0);

  signal interpolation_factor_scaled : signed(96 downto 0);

  signal pd_output        : worker_input_in_t;
  signal pd_output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal pd_input         : worker_input_in_t;
  signal pd_input_opcode  : std_logic_vector(opcode_width_c - 1 downto 0);
  signal pd_input_data    : std_logic_vector(data_in_width_g downto 0);  -- 1 larger for carry

  signal output_valid  : std_logic;
  signal output_data   : std_logic_vector(output_out.data'range);
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

  signal time_valid_pipe : std_logic_vector(2 downto 0);
  signal carry_bit       : unsigned(0 downto 0);
begin

  -- Input control (validation)
  pd_take <= take_in and (not pd_hold_in);

  -- Output control (flow)
  pd_enable <= enable and (divider_ready);

  -- Take pipe to align the protocol delay FIFO take to the incoming data
  take_in_p : process(clk)
  begin
    if rising_edge(clk) then
      if pd_enable = '1' then
        take_in_r <= take_in_r(take_in_r'high-1 downto 0) & pd_take;
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Sample OpCode Capture
  --
  -- Incoming opcode, when valid, captured by shift reg.
  -- Sized to capture fractional bit: seconds are
  -- captured directly by divider.
  ------------------------------------------------------------------------------
  sample_interval_in_p : process(clk)
  begin
    if rising_edge(clk) then
      if (enable = '1') and (input_in.valid = '1') and (pd_hold_in = '0')
        and (input_in.opcode = complex_short_timed_sample_sample_interval_op_e) then
        sample_interval_r <= input_in.data & sample_interval_r(sample_interval_r'high downto data_in_width_g);
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Divider
  --
  -- Input validated by end of sample-interval message.
  -- Clock enabled by down-stream component.
  ------------------------------------------------------------------------------

  -- Input control
  input_si_valid <= '1' when ((input_in.opcode = complex_short_timed_sample_sample_interval_op_e)
                              and (input_in.valid = '1') and (input_in.eom = '1') and (enable = '1'))
                    else '0';

  -- Port assignments
  interpolation_factor_scaled(interpolation_factor_scaled'high downto (interpolation_factor'high + 1)) <= (others => '0');
  interpolation_factor_scaled(interpolation_factor'range)                                              <= signed(std_logic_vector(interpolation_factor));
  sample_interval_input                                                                                <= signed('0' & input_in.data & sample_interval_r);  -- Signed bit always set to '0'

  division_i : non_restoring_divider
    generic map (
      data_width_g => 97,
      method_g     => "floor"
      )
    port map(
      clk            => clk,
      reset          => reset,
      clk_en         => enable,
      data_in_valid  => input_si_valid,
      data_in_ready  => divider_ready,
      numerator      => sample_interval_input,
      denominator    => interpolation_factor_scaled,
      data_valid_out => output_si_valid,
      quotient       => sample_interval_output,
      remainder      => open
      );


  ------------------------------------------------------------------------------
  -- Flow Control
  --
  -- The main flow control (pd_hold_in) halts/invalidates the input from the
  -- point that the divider is active, until a valid sample-interval op-code is
  -- output by the delay component.
  -- If a valid sample-interval op-code is output by the delay component while
  -- the divider component is active, further flow control (pd_hold_out)
  -- disables the delay component output until the divider component is inactive
  ------------------------------------------------------------------------------

  pd_hold_in <= (not divider_ready) or divider_done or output_si_valid;

  pd_hold_out <= '1' when ((pd_output_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e)
                            and (pd_output.valid = '1')) and (pd_hold_in = '1'))
                 else '0';

  pd_hold <= pd_hold_in;


  ------------------------------------------------------------------------------
  -- Sample Interval Message Validation
  --
  -- Valid output from the divider component is flagged (divider_done) until a
  -- valid sample-interval op-code is output by the delay component. This
  -- affects flow control via pd_hold_in and pd_hold_out.
  -- by the delay component.
  -- A shift reg flags the current word within the message.
  ------------------------------------------------------------------------------
  sample_interval_out_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        divider_done  <= '0';
        si_valid_pipe <= (others => '0');
      elsif (enable = '1') then
        if (pd_output.valid = '1') and (pd_output_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e)) then
          si_valid_pipe <= si_valid_pipe(si_valid_pipe'high -1 downto 0) & divider_done;
          divider_done  <= '0';
        end if;

        if output_si_valid = '1' then
          divider_done             <= '1';
          sample_interval_output_r <= unsigned(sample_interval_output);
        end if;
      end if;
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Delay
  --
  -- Input invalidated while divider is active, up until the point that a valid
  -- sample-interval op-code is output.
  -- Output halted if a valid sample-interval op-code is output, but divider is
  -- still active.
  ------------------------------------------------------------------------------

  -- Port assignments
  input_opcode     <= opcode_to_slv(input_in.opcode);
  up_sample_factor <= x"00" & interpolation_factor;

  -- Short protocol delay to allow sample interval to be calculated (therefore
  -- the processed data is not muxed in)
  protocol_delay_i : protocol_interface_delay_v2
    generic map (
      delay_g                 => interval_calc_delay,
      data_width_g            => input_in.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      processed_data_mux_g    => '0',
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e)
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => pd_enable,
      take_in             => take_in,
      processed_stream_in => (others => '0'),
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => pd_output.som,
      output_eom          => pd_output.eom,
      output_eof          => pd_output.eof,
      output_valid        => pd_output.valid,
      output_give         => pd_output.ready,
      output_byte_enable  => pd_output.byte_enable,
      output_opcode       => pd_output_opcode,
      output_data         => pd_output.data
      );

  ------------------------------------------------------------------------------
  -- Output Control
  --
  -- Replaces sample interval and time message data:
  --  Sample interval: Output from divider component.
  --  Time: 32-bit subtraction on each incoming word of the time message.
  --        Additional '0' prepended to the miniuend to capture the carry bit
  --        (via 2's complement). This is then subtracted from the next
  --        subtraction.
  ------------------------------------------------------------------------------
  output_data_p : process(pd_output_opcode, pd_output, pd_input_data, divider_done,
                          group_delay_fractional, group_delay_seconds, time_valid_pipe,
                          carry_bit, sample_interval_output_r, si_valid_pipe)
  begin
    pd_input_opcode      <= pd_output_opcode;
    pd_input.byte_enable <= pd_output.byte_enable;
    pd_input.ready       <= pd_output.ready;
    pd_input.valid       <= pd_output.valid;
    pd_input.data        <= pd_output.data;
    pd_input.som         <= pd_output.som;
    pd_input.eom         <= pd_output.eom;
    pd_input.eof         <= pd_output.eof;

    -- Sample interval
    if (pd_output_opcode = opcode_to_slv(complex_short_timed_sample_sample_interval_op_e)) then
      pd_input.ready <= '0';
      pd_input.valid <= '0';

      -- Fractional (31:0)
      if (divider_done = '1') then
        pd_input.som   <= '1';
        pd_input.ready <= pd_output.ready;
        pd_input.valid <= pd_output.valid;
        pd_input.data  <= (std_logic_vector(sample_interval_output_r(31 downto 0)));
      -- Fractional (63:32)
      elsif (si_valid_pipe(0) = '1') then
        pd_input.ready <= pd_output.ready;
        pd_input.valid <= pd_output.valid;
        pd_input.data  <= (std_logic_vector(sample_interval_output_r(63 downto 32)));
      -- Seconds
      elsif (si_valid_pipe(1) = '1') then
        pd_input.ready <= pd_output.ready;
        pd_input.valid <= pd_output.valid;
        pd_input.data  <= (std_logic_vector(sample_interval_output_r(95 downto 64)));
        pd_input.eom   <= '1';
      end if;

    -- Time
    elsif (pd_output_opcode = opcode_to_slv(complex_short_timed_sample_time_op_e)) then
      pd_input.ready <= pd_output.ready;
      pd_input.valid <= pd_output.valid;
      pd_input.som   <= pd_output.som;
      pd_input.eom   <= pd_output.eom;

      -- Fractional (31:0): Incoming fractional time and group-delay values
      if (time_valid_pipe(0) = '1') then
        pd_input_data <= std_logic_vector(("0" & unsigned(pd_output.data(31 downto 0))) - group_delay_fractional(31 downto 0));
      -- Fractional (63:32)
      elsif (time_valid_pipe(1) = '1') then
        pd_input_data <= std_logic_vector(("0" & unsigned(pd_output.data)) - group_delay_fractional(63 downto 32) - carry_bit);
      -- Seconds (95:64)
      elsif (time_valid_pipe(2) = '1') then
        pd_input_data <= std_logic_vector(("0" & unsigned(pd_output.data)) - group_delay_seconds - carry_bit);
        pd_input.eom  <= '1';
      end if;

      pd_input.data <= pd_input_data(pd_input.data'range);
    end if;
  end process;


  ------------------------------------------------------------------------------
  -- Time Message Validation
  --
  -- On output of a valid time time op-code from the delay component, a shift
  -- reg flags the current word within the message.
  -- Also captures the carry bit from the previously output word.
  ------------------------------------------------------------------------------
  time_out_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        time_valid_pipe <= "001";
      elsif (enable = '1') then
        if (pd_output.valid = '1') and (pd_output_opcode = opcode_to_slv(complex_short_timed_sample_time_op_e)) then
          time_valid_pipe <= time_valid_pipe(1 downto 0) & time_valid_pipe(2);
          carry_bit       <= unsigned(pd_input_data(32 downto 32));
        end if;
      end if;
    end if;
  end process;


  protocol_delay_fifo_i : protocol_interface_fifo_v2
    generic map (
      enforce_msg_boundary_g  => true,
      data_in_width_g         => data_in_width_g,
      data_out_width_g        => data_in_width_g,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      fifo_depth_g            => 8,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e),
      metadata_opcode_g       => opcode_to_slv(complex_short_timed_sample_metadata_op_e)
      )
    port map (
      clk     => clk,
      reset   => reset,
      enable  => enable,
      take_in => take_in_r(take_in_r'high),

      processed_stream_in => processed_stream_in,
      processed_valid_in  => processed_valid_in,
      processed_end_in    => processed_end_in,
      processed_ready_out => open,
      -- Input interface signals
      input_som           => pd_input.som,
      input_eom           => pd_input.eom,
      input_eof           => pd_input.eof,
      input_valid         => pd_input.valid,
      input_ready         => pd_input.ready,
      input_byte_enable   => pd_input.byte_enable,
      input_opcode        => pd_input_opcode,
      input_data          => pd_input.data,
      take_out            => pd_take_out,
      -- Output interface signals
      output_som          => output_out.som,
      output_eom          => output_out.eom,
      output_eof          => output_out.eof,
      output_valid        => output_valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );
  output_out.valid  <= output_valid;
  output_out.opcode <= slv_to_opcode(output_opcode);

  output_out.data <= output_data;

end rtl;
