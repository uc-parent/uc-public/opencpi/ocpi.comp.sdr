.. fast_fourier_transform_xs RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _fast_fourier_transform_xs-RCC-worker:


``fast_fourier_transform_xs`` RCC Worker
========================================

Detail
------

.. ocpi_documentation_worker::

   fft_core_implementation: Setting the parameter to ``kiss_fft`` will instruct the component to build and use the open source kiss FFT, and setting the parameter to ``built_in`` will instruct the component to build and use the embedded FFT Core.

Kiss FFT Core
-------------
If the Kiss FFT core implementation has been selected for use (i.e. if ``fft_core_implementation`` is set to ``kiss_fft``) then the Kiss FFT core is required to be downloaded prior to building the component. When calling the ``ocpidev build`` command this is done automatically via a prerequisites script that obtains the source files from the local prerequisites cache or directly from GitHub.

If the Kiss FFT core is not present on the system, this component can still be built using the ``built_in`` FFT core.  To do so, ensure the ``fast_fourier_transform_xs-build.xml`` build configuration file is updated to remove any ``kiss_fft`` configurations. For example:

.. code-block:: XML
   :caption: Example build configuration file to build *without* Kiss FFT present.

   <build>
     <configuration id="0">
       <parameter name="fft_core_implementation" value="built_in"/>
     </configuration>
   </build>
