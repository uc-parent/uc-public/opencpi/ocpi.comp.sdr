<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="taps_shape" type="string"/>
  <!-- Case 00: Typical Case -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 01: Property: Number of taps -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="1,8,9,11,13,25,64,255"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 02: Property: Group Delay (subcase specialised in generator) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="0xffffffff"/>
    <property name="group_delay_fractional" values="0x8000000000000000,0x0000000001000000,0xffffffffffffffff"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" value="group_delay"/>
  </case>
  <!-- Case 03: Property: Taps (shape and values) -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" values="ramp,random,all_ones,all_zeros"/>
  </case>
  <!-- Case 04: Sample values -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero,positive_infinity,negative_infinity,
        not_a_number"/>
  </case>
  <!-- Case 05: Input stressing -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
  </case>
  <!-- Case 06: Message size -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 07: Time opcode -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 08: Sample Interval opcode -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 09: Flush opcode (reliant on number_of_taps) -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="1,8,9,11,13,25,64,255"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="initial,single,consecutive"/>
  </case>
  <!-- Case 10: Discontinuity opcode -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 11: Metadata opcode -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="64"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 12: Soak test -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="1,8,9,11,13,25,64,255"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
