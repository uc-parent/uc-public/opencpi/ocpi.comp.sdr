#!/usr/bin/env python3

# Generates the input binary file for fir_filter_f testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for fir_filter_f testing."""

import os
import numpy
import random
import logging

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class FirFilterGenerator(ocpi_testing.generator.FloatGenerator):
    """Generate test cases for Fir Filter."""

    def property(self, seed, subcase):
        """Property case for FirFilter generator.

        Overrides the group_delay subcase to include sample_interval and time
        opcodes to test the group delay is applied. All other subcases are
        handled by the default generator.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        random.seed(seed)

        if subcase == "group_delay":
            # Ensure time and interval are included.
            messages = [{"opcode": "sample_interval", "data": 1.0},
                        {"opcode": "time", "data": 10000.0},
                        {"opcode": "sample", "data": self._get_sample_values()},
                        {"opcode": "time", "data": 20000.0},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        return super().property(seed, subcase)

    def flush(self, seed, subcase):
        """Override of base adds "initial" case then defers to super().

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        if subcase == "initial":
            # Insert an initial flush opcode before data
            messages = [{"opcode": "flush", "data": None},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        return super().flush(seed, subcase)


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

arguments = ocpi_testing.get_generate_arguments()

taps_property = [
    float(tap) for tap in os.environ.get("OCPI_TEST_taps").split(",")]
number_of_taps_property = int(os.environ.get("OCPI_TEST_number_of_taps"))
subcase = os.environ.get("OCPI_TEST_subcase")

# OCPI will trim multiple zeros from the end, but more taps can be set than
# needed, so append zeros and remove excess.
taps_property.extend([0]*number_of_taps_property)
taps_property = taps_property[:number_of_taps_property]

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  number_of_taps_property, taps_property)

generator = FirFilterGenerator()
# Reduce the input range so fir tap calculations do not overflow.
generator.FLOAT_MINIMUM = float(numpy.float32(numpy.finfo(numpy.float16).min))
generator.FLOAT_MAXIMUM = float(numpy.float32(numpy.finfo(numpy.float16).max))
generator.TYPICAL_AMPLITUDE_MEAN = generator.FLOAT_MAXIMUM/10
generator.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH = generator.FLOAT_MAXIMUM / 50
generator.TYPICAL_MAXIMUM_AMPLITUDE = generator.FLOAT_MAXIMUM * \
    generator.LIMITED_SCALE_FACTOR

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "float_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
