// Implementation of the half_band_decimator_xs worker in C++.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../../math/common/time_utils.hh"
#include "../common/fir/symmetric_fir_core.hh"
#include "half_band_decimator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Half_band_decimator_xsWorkerTypes;

class Half_band_decimator_xsWorker : public Half_band_decimator_xsWorkerBase {
  static_assert((HALF_BAND_DECIMATOR_XS_NUMBER_OF_TAPS + 1) % 4 == 0,
                "number_of_taps must be 4n-1");
  const uint8_t number_of_taps = HALF_BAND_DECIMATOR_XS_NUMBER_OF_TAPS;

  // symmetric_fir_core <SAMPLE_TYPE, ACCUMULATOR_TYPE>
  // Note : The ACCUMULATOR_TYPE needs to be sized appropriately to allow the
  // roll over of any internal mathematical calculations. The size of the input
  // data or the taps can require an internal accumulator size 4 times the
  // size of the SAMPLE_TYPE.
  // Use 16 bit sample/output, 64 bit working size
  symmetric_fir_core<int16_t, int64_t> fir{};

  // Flags when data has been received and will need flushing out prior to
  // passing through a flush opcode.
  bool received_data = false;
  // Count of the samples that need to be flushed through.
  size_t flush_samples = 0;
  // Zero filled buffer for flush opcode.
  std::vector<Complex_short_timed_sampleSampleData> zero_input;

  uint32_t group_delay_time_seconds = 0;   // group delay time seconds part
  uint64_t group_delay_time_fraction = 0;  // group delay time fractional part
  uint32_t output_time_seconds = 0;        // output time seconds part
  uint64_t output_time_fraction = 0;       // output time fractional part
  uint32_t input_interval_seconds = 0;
  uint64_t input_interval_fraction = 0;
  RCCBoolean missed_timestamp = false;  // Flags when a timestamp is being held
  RCCBoolean ready_to_send_time =
      false;  // Indicates when time is ready to be sent

  // Buffer to hold odd samples
  Complex_short_timed_sampleSampleData held_sample;
  RCCBoolean held_odd_sample = false;

  size_t sample_offset = 0;  // Input port sample offset.
  // Create 16 bit containers to hold input data values (with +1 for the
  // potentially held sample)
  int16_t input_data_buffer[2 * HALF_BAND_DECIMATOR_XS_OCPI_MAX_BYTES_INPUT /
                                sizeof(Complex_short_timed_sampleSampleData) +
                            1] = {0};

  // Create 16 bit containers to hold output data values
  int16_t output_data_buffer[2 * HALF_BAND_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
                             sizeof(Complex_short_timed_sampleSampleData)] =
      {0};

  // Set fir taps and rounding from current properties
  void set_taps_from_properties() {

    // Convert properties().rounding_type into dsp enum types
    // Default is convergent rounding with wrapping.
    sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    sdr_math::overflow_type overflow = sdr_math::overflow_type::MATH_WRAP;

    Rounding_type property_value = properties().rounding_type;

    if (property_value == ROUNDING_TYPE_HALF_EVEN) {
      rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    } else if (property_value == ROUNDING_TYPE_HALF_UP) {
      rounding = sdr_math::rounding_type::MATH_HALF_UP;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE_WITH_SATURATION) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
      overflow = sdr_math::overflow_type::MATH_SATURATE;
    }

    this->fir.set(this->number_of_taps, properties().taps, rounding, overflow,
                  true);
  }

  RCCResult start() {
    this->set_taps_from_properties();
    this->flush_samples = this->number_of_taps;
    this->zero_input.resize(this->number_of_taps + 1,
                            Complex_short_timed_sampleSampleData());
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    this->flush_samples = this->number_of_taps;
    this->set_taps_from_properties();
    return RCC_OK;
  }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_time_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_time_fraction = properties().group_delay_fractional;
    return RCC_OK;
  }

  // Reset fir core and data flags to initial state.
  void reset() {
    this->fir.reset();
    this->received_data = false;
    this->missed_timestamp = false;
    this->ready_to_send_time = false;
    this->held_odd_sample = false;
    this->sample_offset = 0;
    this->flush_samples = this->number_of_taps;
  }

  // Add sample interval to the output time.
  void increment_output_time() {
    time_utils::add(&this->output_time_seconds, &this->output_time_fraction,
                    this->input_interval_seconds,
                    this->input_interval_fraction);
  }

  // Output time and advance the output buffer
  void send_missed_time_to_output(RCCBoolean add_sample_interval) {

    if (add_sample_interval) {
      // Add sample interval for the held sample.
      increment_output_time();
    }

    output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
    output.time().seconds() = this->output_time_seconds;
    output.time().fraction() = this->output_time_fraction;

    // Clear flags so time is not output again later.
    this->missed_timestamp = false;
    this->ready_to_send_time = false;

    // We don't want to advance input - advance output only.
    output.advance();
  }

  // Process held sample and the incoming samples and send to the output port
  RCCResult handle_samples(
      const Complex_short_timed_sampleSampleData *input_data, size_t length,
      bool advance_input) {
    size_t offset = 0;

    // Insert held sample at start of buffer if required
    if (this->held_odd_sample) {
      this->input_data_buffer[0] = this->held_sample.real;
      this->input_data_buffer[1] = this->held_sample.imaginary;
      this->held_odd_sample = false;
      offset = 1;
    }

    // If have a odd length, hold the last sample until next time
    if ((length + offset) % 2 != 0) {
      this->held_sample = input_data[length - 1];
      --length;
      this->held_odd_sample = true;
    }

    if (length == 0) {
      // Given only one sample, which has been held for the next run
      if (advance_input) {
        input.advance();
      }
      return RCC_OK;
    }

    // Save contents of 16 bit input data into a container
    for (size_t index = 0; index < length; index++) {
      this->input_data_buffer[(index + offset) * 2] = input_data[index].real;
      this->input_data_buffer[((index + offset) * 2) + 1] =
          input_data[index].imaginary;
    }

    length += offset;
    this->fir.do_work_complex(this->input_data_buffer, length,
                              this->output_data_buffer);

    output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
    output.sample().data().resize(length / 2);
    Complex_short_timed_sampleSampleData *output_data =
        output.sample().data().data();
    for (uint16_t index = 0; index < length / 2; index++) {
      // Copy filter data into output, discard every other
      // sample, starting with first.
      // Step size=4 as 2 real and imag values
      // +0=1st real, +1=1st imag, +2=2nd real, +3=2nd imag
      output_data[index].real = this->output_data_buffer[(index * 4) + 2];
      output_data[index].imaginary = this->output_data_buffer[(index * 4) + 3];
    }

    if (advance_input) {
      // Finished with this input, move onto the next.
      this->sample_offset = 0;
      return RCC_ADVANCE;
    }

    output.advance();
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.hasBuffer() && input.getEOF()) {
      // Flush the held timestamp
      if (this->missed_timestamp) {
        send_missed_time_to_output(false);
        return RCC_OK;
      }

      output.setEOF();
      return RCC_ADVANCE_DONE;
    }
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *input_data =
          input.sample().data().data();

      if (this->missed_timestamp) {
        if (this->ready_to_send_time) {
          // Input buffer only contained a single sample
          // advance the input ready for the next opcode
          // operation once the missed timestamp is sent.
          if (length == 1) {
            input.advance();
          }

          // Send time to the output
          send_missed_time_to_output(true);
          return RCC_OK;
        } else {
          // Indicates that time should be sent in the next software cycle
          this->ready_to_send_time = true;
          // Move past the first element of the input buffer next time as this
          // has already been processed.
          if (length > 1) {
            this->sample_offset = 1;
          }
          // Process only 1 input sample, this will be
          // added to the currently held sample.
          return handle_samples(input_data, 1, false);
        }
      } else {
        if (this->sample_offset > 0) {
          // Already processed a sample as part of the missed timestamp,
          // move past it and reduce the length to process the next samples.
          input_data += this->sample_offset;
          length -= this->sample_offset;
        }
      }

      this->received_data = true;
      return handle_samples(input_data, length, true);

    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      this->output_time_seconds = input.time().seconds();
      this->output_time_fraction = input.time().fraction();

      // Subtract group delay from time.
      time_utils::subtract(
          &this->output_time_seconds, &this->output_time_fraction,
          this->group_delay_time_seconds, this->group_delay_time_fraction);

      // Hold onto the timestamp, ready to be
      // sent once another sample is received.
      if (this->held_odd_sample) {
        this->missed_timestamp = true;
        input.advance();
        return RCC_OK;
      }
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().seconds() = this->output_time_seconds;
      output.time().fraction() = this->output_time_fraction;
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      const uint8_t decimation_factor = 2;

      this->input_interval_seconds = input.sample_interval().seconds();
      this->input_interval_fraction = input.sample_interval().fraction();

      uint32_t output_interval_seconds = this->input_interval_seconds;
      uint64_t output_interval_fraction = this->input_interval_fraction;
      time_utils::multiply(&output_interval_seconds, &output_interval_fraction,
                           decimation_factor);

      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = output_interval_seconds;
      output.sample_interval().fraction() = output_interval_fraction;

      return RCC_ADVANCE;

    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (this->received_data) {
        if (this->missed_timestamp) {
          if (this->held_odd_sample) {
            // Add sample interval for the held sample.
            increment_output_time();
            // Flushing the held sample so reduce flush_samples
            // by one ready to clear the taps in the next cycle.
            --this->flush_samples;
            // and then flush it through
            return handle_samples(zero_input.data(), 1, false);
          } else {
            // The internal buffer is clear, send the missed timestamp
            send_missed_time_to_output(false);
            return RCC_OK;
          }
        }

        // Determine the number of flush samples ensuring flush ends on even
        // sample so the last is not decimated away.
        if ((flush_samples + this->held_odd_sample) % 2 == 1) {
          ++this->flush_samples;
        }

        this->received_data = false;
        return handle_samples(this->zero_input.data(), this->flush_samples,
                              false);
      }

      this->reset();
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;

    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      if (this->missed_timestamp) {
        // Output the stored time with no addition of the sample interval.
        send_missed_time_to_output(false);
        return RCC_OK;
      }
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      this->reset();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }
    setError("Unknown OpCode Received");
    return RCC_FATAL;
  }
};

HALF_BAND_DECIMATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
HALF_BAND_DECIMATOR_XS_END_INFO
