.. cic_interpolator_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: filter up sample interpolator CIC


.. _cic_interpolator_xs:


CIC Interpolator (``cic_interpolator_xs``)
==========================================
CIC (cascading integrating comb) filter combined with interpolator.

Design
------
.. ocpi_documentation_include:: ../doc/cic_interpolator/introduction.inc

   |component_name|: cic_interpolator_xs
   |include_dir|: ../doc/cic_interpolator/

Interface
---------
.. literalinclude:: ../specs/cic_interpolator_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~

.. ocpi_documentation_include:: ../doc/cic_interpolator/opcodes.inc

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   up_sample_factor: Must not be set to zero. Changing will trigger a reset of the processed data path.
   scale_output: Must not be set greater than :math:`\texttt{cic_register_size} - 16`.
   cic_order: Must not be set to zero.
   cic_differential_delay: Must not be set to zero.
   cic_register_size: Must meet the requirements given in :eq:`cic_interpolator_xs-cic_int_gain_width-equation`

Implementations
---------------
.. ocpi_documentation_implementations:: ../cic_interpolator_xs.hdl ../cic_interpolator_xs.rcc

CIC Compensation Filters
------------------------

.. ocpi_documentation_include:: ../doc/cic_interpolator/compensation_filters.inc

   |component_name|: cic_interpolator_xs
   |include_dir|: ../doc/cic_interpolator/

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CIC interpolator primitive <cic_int-primitive>`

 * :ref:`Halfup rounding primitive <rounding_halfup-primitive>`

 * :ref:`Upsample Protocol Interface Delay primitive v2 <upsample_protocol_interface_delay_v2-primitive>`

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

 * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`

 * ``components/dsp/common/cic/cic_core.hh``

 * ``components/math/common/time_utils.hh``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``cic_interpolator_xs`` are:

 * Up-sample factor must be greater than or equal to 2.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
