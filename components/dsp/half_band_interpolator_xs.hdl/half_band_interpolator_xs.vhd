-- HDL Implementation of Half Band Interpolator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.fir_filter_symmetric;

architecture rtl of worker is
  function to_string(rounding_type : in work.half_band_interpolator_xs_constants.rounding_type_t) return string is
  begin
    case rounding_type is
      when truncate_with_saturation_e => return "truncate_with_saturation";
      when truncate_e                 => return "truncate";
      when half_up_e                  => return "half_up";
      when half_even_e                => return "half_even";
      when others                     => return "truncate";
    end case;
  end function;

  constant data_width_c : integer := input_in.data'length/2;
  constant num_taps_c   : integer := to_integer(number_of_taps);
  constant mode_c       : string  := "half_band_interpolate";

  signal input_interface : worker_input_in_t;

  signal pd_take_in        : std_logic;
  signal pd_take_out       : std_logic;
  signal fir_busy          : std_logic := '0';
  signal fir_enable        : std_logic;
  signal hold_ip_output    : std_logic;
  signal input_sample_data : std_logic;
  signal input_opcode_data : std_logic;
  signal no_data           : std_logic;
  signal flush             : std_logic;
  signal discontinuity     : std_logic;
  signal flush_length      : unsigned(number_of_taps'range);

  signal tap_data  : signed(15 downto 0);
  signal tap_valid : std_logic;

  signal data_valid_in    : std_logic;
  signal input_ready_real : std_logic;

  signal fir_real_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_imag_output_data  : std_logic_vector(data_width_c-1 downto 0);
  signal fir_real_output_valid : std_logic;
  signal fir_real_output_last  : std_logic;

  signal processed_data : std_logic_vector((2*data_width_c)-1 downto 0);

begin

  -- props_out set to default, accept data on the raw interface straight away
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= '1';
  props_out.raw.error <= '0';

  -- Interface handling
  input_sample_data <= '1' when (input_interface.opcode = complex_short_timed_sample_sample_op_e and
                                 (input_interface.valid = '1' or input_interface.eof = '1')
                                 and output_in.ready = '1')
                       else '0';

  input_opcode_data <= '1' when (input_interface.opcode /= complex_short_timed_sample_sample_op_e
                                 and input_interface.ready = '1') and output_in.ready
                       else '0';
  fir_enable <= output_in.ready and not hold_ip_output;

  data_valid_in <= '1' when (input_interface.opcode = complex_short_timed_sample_sample_op_e
                             and input_interface.valid = '1')
                   else '0';

  discontinuity <= '1' when (input_interface.ready = '1' and input_opcode_data = '1'
                             and input_interface.opcode = complex_short_timed_sample_discontinuity_op_e)
                   else '0';

  flush <= '1' when (input_interface.ready = '1' and input_opcode_data = '1'
                     and input_interface.opcode = complex_short_timed_sample_flush_op_e)
           else '0';

  -- Coefficient data is only 16 bits, therefore there are 2 coefficient
  -- messages possible on the raw interface
  tap_data <= signed(props_in.raw.data(15 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(31 downto 16));
  -- only allow coefficient valid high for half of coefficients because other
  -- half are same(symmetric)
  tap_valid <= '1' when its(props_in.raw.is_write) else '0';

  ------------------------------------------------------------------------------
  -- Process to check for flush or discontinuity, when no_data (i.e. no data
  -- samples have been passed to the FIR filters) then the flush length is
  -- overridden to 0.
  ------------------------------------------------------------------------------
  no_data_flag_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        no_data <= '1';
      else
        if fir_busy = '1' and data_valid_in = '1' then
          no_data <= '0';
        end if;
        if flush = '1' or discontinuity = '1' then
          no_data <= '1';
        end if;
      end if;
    end if;
  end process;

  flush_length <= number_of_taps when no_data = '0' else (others => '0');

  ------------------------------------------------------------------------------
  -- Data flushing
  ------------------------------------------------------------------------------
  complex_short_flush_injector_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output/4)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => pd_take_out,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  ------------------------------------------------------------------------------
  -- Real half band filter constructed from the symmetric FIR filter by setting
  -- the generic mode_g
  ------------------------------------------------------------------------------
  fir_filter_symmetric_real_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1,
      saturation_en_g     => false
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => discontinuity,
      enable           => fir_enable,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data(data_width_c-1 downto 0),
      input_valid_in   => data_valid_in,
      input_last_in    => input_interface.eom,
      input_ready_out  => input_ready_real,
      output_data_out  => fir_real_output_data,
      output_valid_out => fir_real_output_valid,
      output_last_out  => fir_real_output_last,
      output_ready_in  => '1'
      );

  ------------------------------------------------------------------------------
  -- Imaginary Half band filter constructed from the symmetric FIR filter
  -- The real FIR is used for output control signals (ready, valid and last)
  ------------------------------------------------------------------------------
  fir_filter_symmetric_imag_i : fir_filter_symmetric
    generic map(
      num_taps_g          => num_taps_c,
      input_data_width_g  => data_width_c,
      output_data_width_g => data_width_c,
      mode_g              => mode_c,
      rounding_type_g     => to_string(rounding_type),
      binary_point_g      => data_width_c-1
      )
    port map(
      clk              => ctl_in.clk,
      reset            => ctl_in.reset,
      data_reset       => discontinuity,
      enable           => fir_enable,
      tap_data_in      => std_logic_vector(tap_data),
      tap_valid_in     => tap_valid,
      tap_ready_out    => open,
      input_data_in    => input_interface.data((2*data_width_c)-1 downto data_width_c),
      input_valid_in   => data_valid_in,
      input_last_in    => '0',
      input_ready_out  => open,
      output_data_out  => fir_imag_output_data,
      output_valid_out => open,
      output_last_out  => open,
      output_ready_in  => '1'
      );

  -- Create the data stream by concatenating the real and imaginary(Imag:Real)
  processed_data <= fir_imag_output_data & fir_real_output_data;


  complex_short_protocol_delay_i : entity work.complex_short_protocol_delay
    generic map (
      enforce_msg_boundary_g => true,
      data_in_width_g        => input_in.data'length
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      enable                 => output_in.ready,
      take_in                => pd_take_in,
      input_in               => input_interface,
      -- Connect output from data processing module.
      processed_stream_in    => processed_data,
      processed_valid_in     => fir_real_output_valid,
      processed_end_in       => fir_real_output_last,
      group_delay_seconds    => unsigned(props_in.group_delay_seconds),
      group_delay_fractional => unsigned(props_in.group_delay_fractional),
      take_out               => pd_take_out,  -- Qualifies _valid and _ready.
      hold_ip_output         => hold_ip_output,
      output_out             => output_out    -- Output streaming interface
      );

  ---------------------------------
  -- Flags when the FIR filter is
  -- processing data in order to
  -- control the take signal
  ---------------------------------
  fir_busy_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        fir_busy <= '0';
      else
        if input_sample_data = '1' then
          fir_busy <= '1';
        elsif fir_real_output_last = '1' then
          fir_busy <= '0';
        end if;
      end if;
    end if;
  end process;

  pd_take_in <= output_in.ready when (fir_busy = '0' and input_interface.opcode /= complex_short_timed_sample_sample_op_e) else input_ready_real;

end rtl;
