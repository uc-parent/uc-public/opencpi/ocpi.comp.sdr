-- HDL Implementation of complex short protocol delay
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes sample data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes sample data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.half_band_interpolator_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_fifo_v2;

entity complex_short_protocol_delay is
  generic (
    enforce_msg_boundary_g : boolean := true;
    data_in_width_g        : integer := 32
    );
  port (
    clk                    : in  std_logic;
    reset                  : in  std_logic;
    enable                 : in  std_logic;  -- High when output is ready
    take_in                : in  std_logic;  -- High when data is taken from input
    input_in               : in  worker_input_in_t;  -- Input streaming interface
    -- Connect output from data processing module.
    processed_stream_in    : in  std_logic_vector(data_in_width_g - 1 downto 0);
    processed_valid_in     : in  std_logic;
    processed_end_in       : in  std_logic;
    group_delay_seconds    : in  unsigned(31 downto 0);
    group_delay_fractional : in  unsigned(63 downto 0);
    take_out               : out std_logic;
    hold_ip_output         : out std_logic;
    output_out             : out worker_output_out_t  -- Output streaming interface
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is

  constant opcode_width_c : integer := integer(ceil(log2(real(
    complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := integer(ceil(log2(real(input_in.byte_enable'length)))) + 1;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal input_opcode                 : std_logic_vector(opcode_width_c - 1 downto 0);
  signal protocol_delay               : worker_output_out_t;
  signal protocol_delay_opcode        : std_logic_vector(opcode_width_c - 1 downto 0);
  signal protocol_delay_data          : std_logic_vector(data_in_width_g - 1 downto 0);
  signal protocol_delay_give_r        : std_logic;
  signal protocol_delay_som_r         : std_logic;
  signal protocol_delay_eom_r         : std_logic;
  signal protocol_delay_eof_r         : std_logic;
  signal protocol_delay_valid_r       : std_logic;
  signal protocol_delay_byte_enable_r : std_logic_vector(byte_enable_width_c - 1 downto 0);
  signal protocol_delay_opcode_r      : std_logic_vector(opcode_width_c - 1 downto 0);
  signal protocol_delay_data_r        : std_logic_vector(data_in_width_g - 1 downto 0);

  signal take_out_i          : std_logic;
  signal processed_ready_out : std_logic;
  signal output_data_i       : unsigned(data_in_width_g downto 0);

  signal out_time_idx : std_logic_vector(2 downto 0);
  signal data_carry   : std_logic_vector(0 downto 0);

begin

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : protocol_interface_fifo_v2
    generic map (
      enforce_msg_boundary_g  => enforce_msg_boundary_g,
      data_in_width_g         => data_in_width_g,
      data_out_width_g        => data_in_width_g,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e),
      metadata_opcode_g       => opcode_to_slv(complex_short_timed_sample_metadata_op_e)
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => enable,
      take_in             => take_in,
      take_out            => take_out_i,
      processed_stream_in => processed_stream_in,
      processed_valid_in  => processed_valid_in,
      processed_end_in    => processed_end_in,
      processed_ready_out => processed_ready_out,
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => protocol_delay.som,
      output_eom          => protocol_delay.eom,
      output_eof          => protocol_delay.eof,
      output_valid        => protocol_delay.valid,
      output_give         => protocol_delay.give,
      output_byte_enable  => protocol_delay.byte_enable,
      output_opcode       => protocol_delay_opcode,
      output_data         => protocol_delay.data
      );

  -- Add delay to take a slice across 2 samples for the sample interval/2
  delay_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        protocol_delay_give_r  <= '0';
        protocol_delay_valid_r <= '0';
      elsif(enable = '1') then
        protocol_delay_give_r  <= protocol_delay.give;
        protocol_delay_valid_r <= protocol_delay.valid;
      end if;
      -- other registers don't need to be reset as gated by
      -- protocol_delay_give_r..
      if(enable = '1') then
        protocol_delay_som_r         <= protocol_delay.som;
        protocol_delay_eom_r         <= protocol_delay.eom;
        protocol_delay_eof_r         <= protocol_delay.eof;
        protocol_delay_byte_enable_r <= protocol_delay.byte_enable;
        protocol_delay_opcode_r      <= protocol_delay_opcode;
        protocol_delay_data_r        <= protocol_delay.data;
      end if;
    end if;
  end process;

  take_out       <= take_out_i;
  hold_ip_output <= not processed_ready_out;

  -- Process captures the carry bit between each cycle of data (also controls
  -- the mux for each cycle of the combinatorial process output_data_p)
  out_time_idx_1hot_p : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        out_time_idx <= "001";
        data_carry   <= "0";
      else
        if protocol_delay_valid_r = '1' and enable = '1'
          and (slv_to_opcode(protocol_delay_opcode_r) = complex_short_timed_sample_time_op_e or
               slv_to_opcode(protocol_delay_opcode_r) = complex_short_timed_sample_sample_interval_op_e) then
          out_time_idx  <= out_time_idx(out_time_idx'high-1 downto 0) & out_time_idx(out_time_idx'high);
          data_carry(0) <= (output_data_i(output_data_i'high));
        end if;
      end if;
    end if;
  end process;

  -- This process currently assumes a 32 bit data width, as such there will be
  -- 3 output samples, for a time or sample interval opcode.
  output_data_p : process (protocol_delay.data, protocol_delay_data_r, protocol_delay_opcode_r,
                           group_delay_seconds, group_delay_fractional, out_time_idx,
                           data_carry, input_in.data)
  begin
    if slv_to_opcode(protocol_delay_opcode_r) = complex_short_timed_sample_time_op_e then
      if out_time_idx(2) = '1' then
        output_data_i <= unsigned('0' & protocol_delay_data_r) - (group_delay_seconds) - unsigned(data_carry);
      elsif out_time_idx(1) = '1' then
        output_data_i <= unsigned('0' & protocol_delay_data_r) - unsigned(group_delay_fractional(63 downto 32)) - unsigned(data_carry);
      else
        output_data_i <= unsigned('0' & protocol_delay_data_r) - unsigned(group_delay_fractional(31 downto 0)) - unsigned(data_carry);
      end if;
    elsif slv_to_opcode(protocol_delay_opcode_r) = complex_short_timed_sample_sample_interval_op_e then
      -- Take sample interval divided by 2, as half band interpolator
      if out_time_idx(2) = '1' then
        output_data_i <= to_unsigned(0, 2) & unsigned(protocol_delay_data_r(31 downto 1));
      else
        output_data_i <= '0' & unsigned(protocol_delay.data(0) & protocol_delay_data_r(31 downto 1));
      end if;
    else
      output_data_i <= unsigned('0' & protocol_delay_data_r);
    end if;
  end process;

  output_out.valid       <= protocol_delay_valid_r;
  output_out.data        <= std_logic_vector(output_data_i(output_out.data'range));
  output_out.opcode      <= slv_to_opcode(protocol_delay_opcode_r);
  output_out.give        <= protocol_delay_give_r;
  output_out.som         <= protocol_delay_som_r;
  output_out.eom         <= protocol_delay_eom_r;
  output_out.eof         <= protocol_delay_eof_r;
  output_out.byte_enable <= protocol_delay_byte_enable_r;

end rtl;
