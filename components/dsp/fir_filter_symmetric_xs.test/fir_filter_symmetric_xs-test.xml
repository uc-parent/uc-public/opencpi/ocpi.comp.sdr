<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<!-- This is the test xml for testing component "fir_filter_symmetric_xs" -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="taps_shape" type="string" value="kaiser"/>
  <property test="true" name="taps_symmetric" type="bool" value="true"/>
  <!-- Case 00: Typical-->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
  </case>
  <!-- Case 01: Number of taps-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="1,7,8,11,25,255"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
  </case>
  <!-- Case 02: Rounding -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" values="half_even,truncate,truncate_with_saturation,half_up"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
  </case>
  <!-- Case 03: Group Delay -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="1"/>
    <property name="group_delay_fractional" value="2277375790844965432"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" value="group_delay"/>
  </case>
  <!-- Case 04: Asymmetric taps (varies with number of taps)-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="7,8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" values="kaiser,random,half_band_corrupted"/>
    <property name="taps_symmetric" values="true,false"/>
  </case>
  <!-- Case 05: Extreme samples -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,real_zero,imaginary_zero,large_positive,large_negative,near_zero"/>
  </case>
  <!-- Case 06: Stress input port -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
  </case>
  <!-- Case 07: Message lengths -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 08: Timestamp -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 09: Sample Interval -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 10: Flush (varies with number_of_taps) -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="7,8,255"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="initial,single,consecutive"/>
  </case>
  <!-- Case 11: Discontinuity -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 12: Metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" value="8"/>
    <property name="rounding_type" value="half_even"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 13: Soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="number_of_taps" values="1,11,13,25,255"/>
    <property name="rounding_type" values="half_even,truncate,truncate_with_saturation,half_up"/>
    <property name="group_delay_seconds" value="8"/>
    <property name="group_delay_fractional" value="2277375790838448128"/>
    <property name="taps" generate="generate_taps.py 15"/>
    <property name="taps_shape" value="kaiser"/>
    <property name="taps_symmetric" value="true"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
  <!-- EOF -->
</tests>
