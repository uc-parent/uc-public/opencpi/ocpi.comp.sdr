// Implementation of the fir_filter_symmetric_xs worker in C++.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "fir_filter_symmetric_xs-worker.hh"
#include "../common/fir/symmetric_fir_core.hh"
#include "../../math/common/time_utils.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fir_filter_symmetric_xsWorkerTypes;

class Fir_filter_symmetric_xsWorker : public Fir_filter_symmetric_xsWorkerBase {
  // symmetric_fir_core <SAMPLE_TYPE, ACCUMULATOR_TYPE>
  // Note : The ACCUMULATOR_TYPE needs to be sized appropriately to allow the
  // roll over of any internal mathematical calculations. The size of the input
  // data or the taps can require an internal accumulator size 4 times the
  // size of the SAMPLE_TYPE.
  // Use 16 bit sample/output, 64 bit working size
  symmetric_fir_core<int16_t, int64_t> fir{};

  bool received_data = false;  // Flags when data has been received and will
                               // need flushing out prior to passing through
                               // a flush opcode, if received.

  uint32_t group_delay_time_seconds = 0;   // group delay time seconds part
  uint64_t group_delay_time_fraction = 0;  // group delay time fractional part
  uint32_t output_time_seconds = 0;        // output time seconds part
  uint64_t output_time_fraction = 0;       // output time fractional part

  RCCResult start() {
    if (FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS == 0) {
      setError("Number of taps must be > 0");
      return RCC_FATAL;
    }

    // Convert properties().rounding_type into dsp enum types
    // Default is convergent rounding with wrapping.
    sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    sdr_math::overflow_type overflow = sdr_math::overflow_type::MATH_WRAP;

    Rounding_type property_value = properties().rounding_type;

    if (property_value == ROUNDING_TYPE_HALF_EVEN) {
      rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    } else if (property_value == ROUNDING_TYPE_HALF_UP) {
      rounding = sdr_math::rounding_type::MATH_HALF_UP;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE_WITH_SATURATION) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
      overflow = sdr_math::overflow_type::MATH_SATURATE;
    }

    this->fir.set(FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS, properties().taps,
                  rounding, overflow, false);
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    this->fir.set_taps(FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS,
                       properties().taps);
    return RCC_OK;
  }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_time_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_time_fraction = properties().group_delay_fractional;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {

      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *input_data =
          input.sample().data().data();

      if (length > 0) {
        this->received_data = true;
        Complex_short_timed_sampleSampleData *output_data =
            output.sample().data().data();
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);
        this->fir.do_work_complex(&input_data->real, length,
                                  &output_data->real);
      }

      return RCC_ADVANCE;

    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output_time_seconds = input.time().seconds();
      output_time_fraction = input.time().fraction();

      // Subtract group delay from time, adjust time_to_output if wrapping
      // around from zero to max time
      time_utils::subtract(&output_time_seconds, &output_time_fraction,
                           this->group_delay_time_seconds,
                           this->group_delay_time_fraction);
      output.time().seconds() = output_time_seconds;
      output.time().fraction() = output_time_fraction;
      return RCC_ADVANCE;

    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;

    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (this->received_data) {

        // N.B. array expected to be twice stated length, as it contains
        // interleaved complex data (I,Q,I,Q) and length is number of samples
        const Complex_short_timed_sampleSampleData zero_input_data
            [2 * FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS] = {{0, 0}};

        Complex_short_timed_sampleSampleData *output_data =
            output.sample().data().data();
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        output.sample().data().resize(FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS);

        this->fir.do_work_complex(&zero_input_data->real,
                                  FIR_FILTER_SYMMETRIC_XS_NUMBER_OF_TAPS,
                                  &output_data->real);

        this->received_data = false;

        output.advance();
        return RCC_OK;
      }

      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;

    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      this->fir.reset();
      this->received_data = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }

    setError("Unknown OpCode Received");
    return RCC_FATAL;
  }
};

FIR_FILTER_SYMMETRIC_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FIR_FILTER_SYMMETRIC_XS_END_INFO
