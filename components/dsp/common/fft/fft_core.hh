// RCC fast fourier transform core.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_FFT_FFT_CORE_HH_
#define COMPONENTS_DSP_COMMON_FFT_FFT_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>  // for size_t

// FFT and Inverse FFT
template <class FLOATING_TYPE>
// LINT EXCEPTION: cpp_011: 2: New class definition allowed in common
// implementation
class fft_core {
 public:
  const FLOATING_TYPE PI = (3.1415926535897932384626433832795);

  // Fast Fourier Transform
  // Takes a complex time-domain signal of the specified size and returns a
  // complex frequency domain signal of the same size.
  // Ensure that the signal size (same as FFT Size) is a power of 2.
  void fft(FLOATING_TYPE tdom_r[], FLOATING_TYPE tdom_i[], size_t FFTPoints) {
    this->generateDecomposedFrequencyDomain(tdom_r, tdom_i, FFTPoints);

    // Now we've got the individual frequency elements, we'll need to synthesize
    size_t fft_length_bitdepth = floor_log2(FFTPoints);
    // Shorthand pointers
    FLOATING_TYPE* Xr = tdom_r;
    FLOATING_TYPE* Xi = tdom_i;

    for (size_t bit_position = 1; bit_position <= fft_length_bitdepth;
         bit_position++) {
      size_t bitdepth = size_t(1) << bit_position;
      size_t bitdepth2 = bitdepth >> 1;  // Divide by 2
      FLOATING_TYPE ur = 1;
      FLOATING_TYPE ui = 0;
      FLOATING_TYPE sr = cos(PI / bitdepth2);
      FLOATING_TYPE si = -sin(PI / bitdepth2);

      for (size_t j = 1; j <= bitdepth2; j++) {
        FLOATING_TYPE tr;
        for (size_t i = (j - 1); i < FFTPoints; i = i + bitdepth) {
          size_t ip = i + bitdepth2;

          // t = X[ip] x u
          tr = Xr[ip] * ur - Xi[ip] * ui;
          FLOATING_TYPE ti = Xr[ip] * ui + Xi[ip] * ur;

          // X[ip] = X[ir] - t
          // where t = previous X[ip] x u
          Xr[ip] = Xr[i] - tr;
          Xi[ip] = Xi[i] - ti;

          Xr[i] += tr;
          Xi[i] += ti;
        }
        tr = ur;
        // u = t x s
        ur = tr * sr - ui * si;
        ui = tr * si + ui * sr;
      }
    }
  }

  void fftInterleaved(FLOATING_TYPE tdom[], size_t FFTPoints) {
    this->generateDecomposedFrequencyDomainInterleaved(tdom, FFTPoints);

    // Now we've got the individual frequency elements, we'll need to synthesize
    size_t fft_length_bitdepth = floor_log2(FFTPoints);
    // Shorthand pointer
    FLOATING_TYPE* X = tdom;

    for (size_t bit_position = 1; bit_position <= fft_length_bitdepth;
         bit_position++) {
      size_t bitdepth = size_t(1) << bit_position;
      size_t bitdepth2 = bitdepth >> 1;  // Divide by 2
      FLOATING_TYPE ur = 1;
      FLOATING_TYPE ui = 0;
      FLOATING_TYPE sr = cos(PI / bitdepth2);
      FLOATING_TYPE si = -sin(PI / bitdepth2);

      for (size_t j = 1; j <= bitdepth2; j++) {
        FLOATING_TYPE tr = 0.0;
        for (size_t i = (j - 1); i < FFTPoints; i = i + bitdepth) {
          size_t ip = i + bitdepth2;

          // t = X[ip] x u
          tr = X[ip * 2] * ur - X[ip * 2 + 1] * ui;
          FLOATING_TYPE ti = X[ip * 2] * ui + X[ip * 2 + 1] * ur;

          // X[ip] = X[ir] - t
          // where t = previous X[ip] x u
          X[ip * 2] = X[i * 2] - tr;
          X[ip * 2 + 1] = X[i * 2 + 1] - ti;

          X[i * 2] += tr;
          X[i * 2 + 1] += ti;
        }
        tr = ur;
        // u = t x s
        ur = tr * sr - ui * si;
        ui = tr * si + ui * sr;
      }
    }
  }

  void ifft(FLOATING_TYPE* r_fdom, FLOATING_TYPE* i_fdom, size_t FFTPoints) {
    // Perform the 1/N scaling and the changing of the sign of the imaginary
    // point prior to input to the FFT.
    // Cast to double to avoid integer division for integer types if made
    // generic later.
    for (size_t i = 0; i < FFTPoints; i++) {
      r_fdom[i] /= (FLOATING_TYPE)FFTPoints;
      i_fdom[i] /= -(FLOATING_TYPE)FFTPoints;
    }

    FLOATING_TYPE* r_tdom = r_fdom;
    FLOATING_TYPE* i_tdom = i_fdom;

    fft(r_tdom, i_tdom, FFTPoints);

    // Finally change the sign of the imaginary part of the output of the FFT.
    for (size_t i = 0; i < FFTPoints; i++) {
      i_tdom[i] = -i_tdom[i];
    }
  }

  void ifftInterleaved(FLOATING_TYPE* fdom, size_t FFTPoints) {
    // Perform the 1/N scaling and the changing of the sign of the imaginary
    // point prior to input to the FFT.
    // Cast to double to avoid integer division for integer types if made
    // generic later.
    for (size_t i = 0; i < FFTPoints; i++) {
      fdom[i * 2] /= (FLOATING_TYPE)FFTPoints;
      fdom[i * 2 + 1] /= -(FLOATING_TYPE)FFTPoints;
    }

    fftInterleaved(fdom, FFTPoints);

    // Finally change the sign of the imaginary part of the output of the FFT.
    for (size_t i = 0; i < FFTPoints; i++) {
      fdom[i * 2 + 1] = -fdom[i * 2 + 1];
    }
  }

  // Implementation functions
 private:
  // Calculate the floored log base 2 of the passed number
  inline static size_t floor_log2(size_t number) {
    size_t result = 0;
    while (number >>= 1) {
      result++;
    }
    return result;
  }

  // Generate frequency domain here using index reversal - using
  // separate arrays for real and imaginary
  static void generateDecomposedFrequencyDomain(FLOATING_TYPE r_tdom[],
                                                FLOATING_TYPE i_tdom[],
                                                size_t FFTPoints) {
    bitReversalSort(r_tdom, FFTPoints);
    bitReversalSort(i_tdom, FFTPoints);
  }

  // Generate frequency domain here using index reversal for
  // interleaved data
  static void generateDecomposedFrequencyDomainInterleaved(FLOATING_TYPE tdom[],
                                                           size_t FFTPoints) {
    bitReversalSortInterleaved(tdom, FFTPoints);
  }

  // An in-place bit reversal sort algorithm
  static void bitReversalSort(FLOATING_TYPE inOutArray[],
                              size_t inOutArrayItemCount) {
    size_t j = size_t(inOutArrayItemCount) >> 1;

    for (size_t i = 1; i <= inOutArrayItemCount - 2; i++) {
      size_t k;
      // Rationalise: k is ontology
      if (i >= j) {
        k = inOutArrayItemCount >> 1;
      } else {
        // Use temp to avoid overwriting if inArray and outArray are the same
        // location (in-place) i.e classic swap algorithm.
        FLOATING_TYPE temp = inOutArray[i];
        inOutArray[i] = inOutArray[j];
        inOutArray[j] = temp;
        k = inOutArrayItemCount >> 1;
      }
      while (k <= j) {
        j = j - k;
        k = k >> 1;
      }
      j = j + k;
    }
  }

  // An in-place bit reversal sort algorithm for interleaved IQ data
  static void bitReversalSortInterleaved(FLOATING_TYPE inOutArray[],
                                         size_t inOutArrayItemCount) {
    for (size_t a = 0; a < 2; a++) {
      size_t k = 0;
      size_t j = inOutArrayItemCount >> 1;

      for (size_t i = 1; i <= inOutArrayItemCount - 2; i++) {
        // Rationalise: k is ontology
        if (i >= j) {
          k = inOutArrayItemCount >> 1;
        } else {
          // Use temp to avoid overwriting if inArray and outArray are the same
          // location (in-place) i.e classic swap algorithm.
          FLOATING_TYPE temp = inOutArray[i * 2 + a];
          inOutArray[i * 2 + a] = inOutArray[j * 2 + a];
          inOutArray[j * 2 + a] = temp;
          k = inOutArrayItemCount >> 1;
        }
        while (k <= j) {
          j = j - k;
          k = k >> 1;
        }
        j = j + k;
      }
    }
  }
};

#endif  // COMPONENTS_DSP_COMMON_FFT_FFT_CORE_HH_
