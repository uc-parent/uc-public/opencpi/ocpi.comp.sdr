// Symmetric FIR Filter Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_FIR_SYMMETRIC_FIR_CORE_HH_
#define COMPONENTS_DSP_COMMON_FIR_SYMMETRIC_FIR_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <vector>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <iterator>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "../../../math/common/scale_and_round.hh"

// LINT EXCEPTION: cpp_011: 3: New class definition allowed in common
// implementation
template <class SAMPLE_TYPE, class ACCUMULATOR_TYPE>
class symmetric_fir_core {
  static_assert(sizeof(ACCUMULATOR_TYPE) >= sizeof(SAMPLE_TYPE),
                "ACCUMULATOR_TYPE type size must be greater than or equal to "
                "the SAMPLE_TYPE");

 public:
  symmetric_fir_core() {}

  void set(size_t length, const SAMPLE_TYPE taps[],
           sdr_math::rounding_type rounding, sdr_math::overflow_type overflow,
           bool decimate = false) {
    this->rounding = rounding;
    this->overflow = overflow;
    this->decimate = decimate;
    set_taps(length, taps);
  }

  // Reset the histories
  void reset() {
    if (!this->history_real.empty()) {
      this->history_real.clear();
    }
    this->history_real.resize(this->length, 0);
    this->history_real_position_iterator = this->history_real.begin();

    if (!this->history_imag.empty()) {
      this->history_imag.clear();
    }
    this->history_imag.resize(this->length, 0);
    this->history_imag_position_iterator = this->history_imag.begin();
  }

  // Set the taps and reset to create correct length memory
  void set_taps(size_t length, const SAMPLE_TYPE taps[]) {
    this->length = length;
    // reserve and clear any existing taps
    this->taps_buffer.clear();
    this->taps_buffer.reserve(length);
    std::copy(taps, taps + length, std::back_inserter(this->taps_buffer));
    reset();
  }

  SAMPLE_TYPE _set_output(ACCUMULATOR_TYPE outputTotal) {
    SAMPLE_TYPE output =
        sdr_math::scale_and_round<ACCUMULATOR_TYPE, SAMPLE_TYPE>(
            outputTotal, this->rounding,
            std::numeric_limits<SAMPLE_TYPE>::digits, this->overflow);

    return output;
  }

  SAMPLE_TYPE get_real_history(size_t age) const {
    size_t current_position =
        (this->history_real_position_iterator - this->history_real.begin());
    size_t index = (current_position + this->length - age) % this->length;
    return this->history_real.at(index);
  }

  SAMPLE_TYPE get_imag_history(size_t age) const {
    size_t current_position =
        (this->history_imag_position_iterator - this->history_imag.begin());
    size_t index = (current_position + this->length - age) % this->length;
    return this->history_imag.at(index);
  }

  void push_real_history(SAMPLE_TYPE sample) {
    ++this->history_real_position_iterator;
    if (this->history_real_position_iterator == this->history_real.end()) {
      this->history_real_position_iterator = this->history_real.begin();
    }
    *this->history_real_position_iterator = sample;
  }

  void push_imag_history(SAMPLE_TYPE sample) {
    ++this->history_imag_position_iterator;
    if (this->history_imag_position_iterator == this->history_imag.end()) {
      this->history_imag_position_iterator = this->history_imag.begin();
    }
    *this->history_imag_position_iterator = sample;
  }

  void do_work(const SAMPLE_TYPE input[], size_t samples,
               SAMPLE_TYPE output[]) {
    // Loop over each sample
    for (size_t sample = 0; sample < samples; ++sample) {
      // Assign the new sample to the history
      this->push_real_history(input[sample]);

      // Perform the filter working from the outside to the middle of the
      // taps/history.
      ACCUMULATOR_TYPE outputTotal = 0;
      size_t newest = 0;
      size_t oldest = this->length - 1;

      while (newest < oldest) {
        outputTotal += this->taps_buffer.at(oldest) *
                       (get_real_history(newest) + get_real_history(oldest));
        ++newest;
        --oldest;
        // Skip over zero taps (odd taps except centre tap).
        if (this->decimate && newest != oldest) {
          ++newest;
          --oldest;
        }
      }

      if (newest == oldest) {
        // odd taps - just add middle tap/history.
        outputTotal += this->taps_buffer.at(newest) * get_real_history(newest);
      }

      output[sample] = _set_output(outputTotal);
    }  // For loop over samples
  }    // do_work

  // If this is a complex filter, do complex work. input[] contains
  // real/imaginary data interleaved into an IQIQIQ... array that is
  // samples*2 in length.
  void do_work_complex(const SAMPLE_TYPE input[], size_t samples,
                       SAMPLE_TYPE output[]) {

    // Loop over each sample
    for (size_t sample = 0; sample < (samples * 2); sample += 2) {
      // Assign the new sample to the history
      this->push_real_history(input[sample]);
      this->push_imag_history(input[sample + 1]);

      // Reset the real and imaginary accumulators
      ACCUMULATOR_TYPE output_total_real = 0;
      ACCUMULATOR_TYPE output_total_imag = 0;
      size_t newest = 0;
      size_t oldest = this->length - 1;

      while (newest < oldest) {
        output_total_real +=
            this->taps_buffer.at(oldest) *
            (get_real_history(newest) + get_real_history(oldest));
        output_total_imag +=
            this->taps_buffer.at(oldest) *
            (get_imag_history(newest) + get_imag_history(oldest));
        ++newest;
        --oldest;
        // Skip over zero taps (odd taps except centre tap).
        if (this->decimate && newest != oldest) {
          ++newest;
          --oldest;
        }
      }

      if (newest == oldest) {
        // odd taps - just add middle tap/history.
        output_total_real +=
            this->taps_buffer.at(newest) * get_real_history(newest);
        output_total_imag +=
            this->taps_buffer.at(newest) * get_imag_history(newest);
      }

      output[sample] = _set_output(output_total_real);
      output[sample + 1] = _set_output(output_total_imag);
    }  // For loop over samples
  }    // do_work_complex

 private:
  size_t length{0};
  std::vector<ACCUMULATOR_TYPE> taps_buffer;
  std::vector<ACCUMULATOR_TYPE> history_real;
  std::vector<ACCUMULATOR_TYPE> history_imag;
  typename std::vector<ACCUMULATOR_TYPE>::iterator
      history_real_position_iterator;
  typename std::vector<ACCUMULATOR_TYPE>::iterator
      history_imag_position_iterator;
  sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
  sdr_math::overflow_type overflow{sdr_math::overflow_type::MATH_WRAP};
  bool decimate = false;
};

#endif  // COMPONENTS_DSP_COMMON_FIR_SYMMETRIC_FIR_CORE_HH_
