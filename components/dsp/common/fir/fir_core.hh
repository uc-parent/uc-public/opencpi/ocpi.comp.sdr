// FIR Filter Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_
#define COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <limits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <vector>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "../../../math/common/scale_and_round.hh"

// LINT EXCEPTION: cpp_011: 3: New class definition allowed in common
// implementation
template <class SAMPLE_TYPE, class ACCUMULATOR_TYPE>
class fir_core {
  static_assert(sizeof(ACCUMULATOR_TYPE) >= sizeof(SAMPLE_TYPE),
                "ACCUMULATOR_TYPE type size must be greater than or equal to "
                "the SAMPLE_TYPE");

 public:
  fir_core() {}

  void set(size_t length, const SAMPLE_TYPE taps[],
           sdr_math::rounding_type rounding, sdr_math::overflow_type overflow) {
    this->rounding = rounding;
    this->overflow = overflow;
    set_taps(length, taps);
  }

  // Reset the histories
  void reset() {
    if (!this->history_real.empty()) {
      this->history_real.clear();
    }
    if (!this->history_imag.empty()) {
      this->history_imag.clear();
    }

    this->history_real.resize(this->length, 0);
    this->history_imag.resize(this->length, 0);
    this->history_pos = this->length - 1;
  }

  // Set the taps and reset to create correct length memory
  void set_taps(size_t length, const SAMPLE_TYPE taps[]) {
    this->length = length;
    // reserve and clear any existing taps
    this->taps_buffer.clear();
    this->taps_buffer.reserve(length);
    std::copy(taps, taps + length, std::back_inserter(this->taps_buffer));
    reset();
  }

  void set_scale_factor(const uint8_t scale_factor) {
    this->scale_factor = scale_factor;
  }

  template <typename T = ACCUMULATOR_TYPE,
            typename std::enable_if<std::is_floating_point<T>::value,
                                    T>::type* = nullptr>
  SAMPLE_TYPE _set_output(T outputTotal) {

    SAMPLE_TYPE output =
        sdr_math::constrain_value<T, SAMPLE_TYPE>(outputTotal, this->overflow);
    return output;
  }

  template <typename T = ACCUMULATOR_TYPE,
            typename std::enable_if<std::numeric_limits<T>::is_integer,
                                    T>::type* = nullptr>
  SAMPLE_TYPE _set_output(T output_total) {

    SAMPLE_TYPE output = sdr_math::scale_and_round<T, SAMPLE_TYPE>(
        output_total, this->rounding, this->scale_factor, this->overflow);
    return output;
  }

  // If this is a non-complex filter, do real work
  void do_work(const SAMPLE_TYPE input[], size_t samples,
               SAMPLE_TYPE output[]) {
    // Loop over each sample
    for (size_t sample = 0; sample < samples; sample++) {

      // Assign the new sample to the history
      this->history_real.at(this->history_pos) = input[sample];

      // With the most recent first (this->history_pos), go through
      // the history and do the multiply accumulate operation.
      ACCUMULATOR_TYPE output_total = 0;
      for (size_t i = 0; i < this->length; i++) {
        output_total +=
            this->taps_buffer.at(i) *
            this->history_real.at((this->history_pos + i) % this->length);
      }

      output[sample] = _set_output(output_total);

      // Increment the current position and wrap
      if (this->history_pos == 0) {
        this->history_pos = this->length - 1;
      } else {
        this->history_pos--;
      }
    }  // For loop over samples
  }    // do_work

  // If this is a complex filter, do complex work. input[] contains
  // real/imaginary data interleaved into an IQIQIQ... array that is
  // samples*2 in length.
  void do_work_complex(const SAMPLE_TYPE input[], size_t samples,
                       SAMPLE_TYPE output[]) {
    // Loop over each sample
    for (size_t sample = 0; sample < (samples * 2); sample += 2) {
      // Assign the new sample to the history
      this->history_real.at(this->history_pos) = input[sample];
      this->history_imag.at(this->history_pos) = input[sample + 1];
      // Reset the real and imaginary accumulators
      ACCUMULATOR_TYPE output_total_real = 0;
      ACCUMULATOR_TYPE output_total_imag = 0;

      // With the most recent first (this->history_pos), go through
      // the history and do the multiply accumulate operation.
      for (size_t i = 0; i < this->length; i++) {
        output_total_real +=
            this->taps_buffer.at(i) *
            this->history_real.at((this->history_pos + i) % this->length);
        output_total_imag +=
            this->taps_buffer.at(i) *
            this->history_imag.at((this->history_pos + i) % this->length);
      }

      output[sample] = _set_output(output_total_real);
      output[sample + 1] = _set_output(output_total_imag);

      // Increment the current position and wrap
      if (this->history_pos == 0) {
        this->history_pos = this->length - 1;
      } else {
        this->history_pos--;
      }
    }  // For loop over samples
  }    // do_work_complex

 private:
  uint8_t scale_factor{0};
  size_t length{0};
  std::vector<ACCUMULATOR_TYPE> taps_buffer;
  std::vector<ACCUMULATOR_TYPE> history_real;
  std::vector<ACCUMULATOR_TYPE> history_imag;
  size_t history_pos{0};
  sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
  sdr_math::overflow_type overflow{sdr_math::overflow_type::MATH_WRAP};
};

#endif  // COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_
