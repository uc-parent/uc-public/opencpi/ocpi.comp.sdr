-- HDL Implementation of a equal to component.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  constant delay_c : integer := 0;

  signal input_hold    : std_logic;
  signal component_out : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Interface signals
  ------------------------------------------------------------------------------

  -- Take input when output ready and input_hold is low.
  -- Input hold is high when non-sample opcodes with valid data are being
  -- output from the component, as this takes multiple clock cycles per
  -- input due to the difference in input and output width.
  input_out.take <= output_in.ready and not input_hold;

  -- This handles the translation of non-sample message data from
  -- the wide input to narrower output.
  -- It has zero delay to match the calculation below.
  interface_delay_i : entity work.ulong_to_bool_protocol_delay
    generic map (
      delay_g          => delay_c,
      data_in_width_g  => input_in.data'length,
      data_out_width_g => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => component_out,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  ------------------------------------------------------------------------------
  -- Comparator Logic
  ------------------------------------------------------------------------------
  -- Test if the input is greater than the comparator value
  equal_to_process : process(input_in.data, props_in.value, props_in.invert_output)
  begin
    if (unsigned(input_in.data) = props_in.value) then
      if (props_in.invert_output = '1') then
        component_out <= x"00";
      else
        component_out <= x"01";
      end if;
    else
      if (props_in.invert_output = '1') then
        component_out <= x"01";
      else
        component_out <= x"00";
      end if;
    end if;
  end process;

end rtl;
