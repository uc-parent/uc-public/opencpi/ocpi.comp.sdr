-- Golay Decoder
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions
library cdc;

-------------------------------------------------------------------------------
-- Golay Decoder
-------------------------------------------------------------------------------
--
-- Description:
--
-- Implementation of a Golay Decoder that decodes Golay [24,12,8] codewords
-- generated with characteristic polynomial:
-- x^11 + x^9 + x^7 + x^6 + x^5 + x + 1 (0xAE3)
--
-- The input port data is a potential Golay codeword.
--
-- This worker detects if there was a parity bit error and/or codeword error.
-- For codeword errors it only checks if there was an error not the amount
-- of bits that have an error.
--
-- If there is an error it can only correct up to 3 bit errors. If the
-- bit errors in a codeword is greater than 3, then the wrong data may
-- be decoded.
--
-- This implementation uses a ROM of precomputed Golay error patterns
-- for 3 or fewer bit errors. The syndrome is calculated and then used
-- as an address to the error pattern ROM.
--
-- It only can process 24 bit data at a time, so it only
-- looks at the 24 LSB bits of the input data port.
--
-- The codewords that are decoded are 12 bits wide, placed
-- in the 12 LSB bits of the output data port.
--
-- Latency:
-- The output has a latency of one input_in.clk clock cycle
--
-- References:
-- http://aqdi.com/articles/using-the-golay-error-detection-and-correction-code-3/
-- https://www.researchgate.net/profile/Satyabrata-Sarangi/publication/264977426_Efficient_Hardware_Implementation_of_Encoder_and_Decoder_for_Golay_Code/links/5405fdd00cf23d9765a7cbca/Efficient-Hardware-Implementation-of-Encoder-and-Decoder-for-Golay-Code.pdf
-- https://www.sciencedirect.com/science/article/pii/S1665642313715438
-- https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.323.9943&rep=rep1&type=pdf
-- http://www.rajivchakravorty.com/source-code/uncertainty/multimedia-sim/html/golay_8c-source.html

-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length

-- TODO: Add properties for how many bits in a codeword have errors, number of
-- codeword errors seen, and number of parity bit errors seen.
-- And add marshalling and demarshalling primitves to handle the protocols used
-- for the input and output ports. Some of this code will have to be rewritten
-- when the marshalling primitives are added. Note that only the data associated
-- with the sample opcode is decoded by this worker.
-- All other opcodes pass through this component without any effect (with 1 clk
-- cycle delay as well).


architecture rtl of worker is

  signal checkbits_mask     : std_logic_vector(15 downto 0);
  signal syndrome           : std_logic_vector(10 downto 0);
  signal error_pattern      : std_logic_vector(22 downto 0);
  signal parity_bit_error   : std_logic;
  signal codeword_error     : std_logic;
  signal codeword           : std_logic_vector(23 downto 0);
  signal recovered_codeword : std_logic_vector(22 downto 0);
  signal codeword_r         : std_logic_vector(23 downto 0);
  signal last_data_r        : std_logic;
  signal take               : std_logic;
  signal give               : std_logic;
  signal first_data_r       : std_logic;
  signal opcode_r           : ulong_timed_sample_OpCode_t;
  signal output_opcode      : ushort_timed_sample_opcode_t;
  signal eof_r              : std_logic;
  signal ivld               : std_logic;
  signal valid_r            : std_logic;
  signal som_r              : std_logic;
  signal eom_r              : std_logic;
  signal samples_vld        : std_logic;
  signal output_data        : std_logic_vector(output_out.data'range);
  signal input_data_r       : std_logic_vector(input_in.data'range);

begin

  checkbits_mask <= from_ushort(props_in.checkbits_mask);

  -- Undo checkbit toggling done by Golay Encoder
  codeword <= input_in.data(23) & (input_in.data(22 downto 12) xor checkbits_mask(10 downto 0)) & input_in.data(11 downto 0);

  -- Check if the parity bit matches the computed parity. If they don't match
  -- then there is an error
  parity_bit_error <= xor_reduce(codeword(22 downto 0)) xor codeword(23);

  -- If the syndrome is greater than 0, then there is a codeword error
  codeword_error <= '1' when (unsigned(syndrome) > 0) else '0';

  -- Recovered codeword without the parity bit
  recovered_codeword <= error_pattern xor codeword_r(22 downto 0);

  ctrl_parity_bit_error_cdc : cdc.cdc.fast_pulse_to_slow_sticky
    port map(
      -- fast clock domain
      fast_clk    => input_in.clk,
      fast_rst    => input_in.reset,
      fast_pulse  => parity_bit_error,
      -- slow clock domain
      slow_clk    => ctl_in.clk,
      slow_rst    => ctl_in.reset,
      slow_clr    => props_in.clr_parity_bit_sticky_error,
      slow_sticky => props_out.parity_bit_sticky_error);

  ctrl_codeword_error_cdc : cdc.cdc.fast_pulse_to_slow_sticky
    port map(
      -- fast clock domain
      fast_clk    => input_in.clk,
      fast_rst    => input_in.reset,
      fast_pulse  => codeword_error,
      -- slow clock domain
      slow_clk    => ctl_in.clk,
      slow_rst    => ctl_in.reset,
      slow_clr    => props_in.clr_codeword_sticky_error,
      slow_sticky => props_out.codeword_sticky_error);

  -- This is a simplified binary matrix multiplication.
  -- In each row of the systematic parity check matrix, H, find the position
  -- where all the bits are 1 and xor together all the bits in the codeword at
  -- those corresponding positions.
  --  e.g. for a row like this: 1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 1 0 0 1 0
  -- with the position 22 being the left most bit and position 0 being the
  -- right most bit, since positions 22, 11, 10, 9, 8, 7, 4, and 1 are set to 1,
  -- you would xor positions 22, 11, 10, 9, 8, 7, 4, and 1 of the codeword
  -- together. The systematic parity check matrix looks like this:
  -- |1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 1 0 0 1 0|
  -- |0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 1 0 0 1|
  -- |0 0 1 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 1 0 1 1 0|
  -- |0 0 0 1 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 1 0 1 1|
  -- |0 0 0 0 1 0 0 0 0 0 0 1 1 0 0 1 0 0 0 1 1 1 1|
  -- |0 0 0 0 0 1 0 0 0 0 0 1 0 0 1 1 1 0 1 0 1 0 1|
  -- |0 0 0 0 0 0 1 0 0 0 0 1 0 1 1 0 1 1 1 1 0 0 0]
  -- |0 0 0 0 0 0 0 1 0 0 0 0 1 0 1 1 0 1 1 1 1 0 0|
  -- |0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 1 1 0 1 1 1 1 0|
  -- |0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 1 1 0 1 1 1 1|
  -- |0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 1 0 0 1 0 1|
  syndrome(10) <= codeword(22) xor codeword(11) xor codeword(10) xor codeword(9)
                  xor codeword(8) xor codeword(7) xor codeword(4) xor codeword(1);

  syndrome(9) <= codeword(21) xor codeword(10) xor codeword(9) xor codeword(8)
                 xor codeword(7) xor codeword(6) xor codeword(3) xor codeword(0);

  syndrome(8) <= codeword(20) xor codeword(11) xor codeword(10) xor codeword(6)
                 xor codeword(5) xor codeword(4) xor codeword(2) xor codeword(1);

  syndrome(7) <= codeword(19) xor codeword(10) xor codeword(9) xor codeword(5)
                 xor codeword(4) xor codeword(3) xor codeword(1) xor codeword(0);

  syndrome(6) <= codeword(18) xor codeword(11) xor codeword(10) xor codeword(7)
                 xor codeword(3) xor codeword(2) xor codeword(1) xor codeword(0);

  syndrome(5) <= codeword(17) xor codeword(11) xor codeword(8) xor codeword(7)
                 xor codeword(6) xor codeword(4) xor codeword(2) xor codeword(0);

  syndrome(4) <= codeword(16) xor codeword(11) xor codeword(9) xor codeword(8)
                 xor codeword(6) xor codeword(5) xor codeword(4) xor codeword(3);

  syndrome(3) <= codeword(15) xor codeword(10) xor codeword(8) xor codeword(7)
                 xor codeword(5) xor codeword(4) xor codeword(3) xor codeword(2);

  syndrome(2) <= codeword(14) xor codeword(9) xor codeword(7) xor codeword(6)
                 xor codeword(4) xor codeword(3) xor codeword(2) xor codeword(1);

  syndrome(1) <= codeword(13) xor codeword(8) xor codeword(6) xor codeword(5)
                 xor codeword(3) xor codeword(2) xor codeword(1) xor codeword(0);

  syndrome(0) <= codeword(12) xor codeword(11) xor codeword(10) xor codeword(9)
                 xor codeword(8) xor codeword(5) xor codeword(2) xor codeword(0);

  golay_error_patterns_inst : entity work.golay_error_patterns_rom
    port map (
      clk           => input_in.clk,
      ivld          => ivld,
      syndrome      => syndrome,
      error_pattern => error_pattern);


  -- Takes the MSB 16 bits of the input data if not a sample opcode
  output_data <= x"0" & recovered_codeword(11 downto 0) when (opcode_r = ulong_timed_sample_sample_op_e) else
                 input_data_r(31 downto 16);

  -- Remove the below line when the marshalling and demarshalling
  -- primitves are added
  samples_vld <= input_in.valid when (input_in.opcode = ulong_timed_sample_sample_op_e) else '0';

  ivld             <= output_in.ready and samples_vld;
  take             <= input_in.ready and output_in.ready;
  give             <= (input_in.ready or last_data_r) and first_data_r and output_in.ready;
  output_out.data  <= output_data;
  input_out.take   <= take;
  output_out.give  <= give;
  output_out.valid <= give and valid_r;
  output_out.som   <= give and som_r;
  output_out.eom   <= give and eom_r;
  output_out.eof   <= eof_r;

  output_opcode <= ushort_timed_sample_sample_op_e when (opcode_r = ulong_timed_sample_sample_op_e) else
                   ushort_timed_sample_time_op_e            when (opcode_r = ulong_timed_sample_time_op_e) else
                   ushort_timed_sample_sample_interval_op_e when (opcode_r = ulong_timed_sample_sample_interval_op_e) else
                   ushort_timed_sample_flush_op_e           when (opcode_r = ulong_timed_sample_flush_op_e) else
                   ushort_timed_sample_discontinuity_op_e   when (opcode_r = ulong_timed_sample_discontinuity_op_e) else
                   ushort_timed_sample_metadata_op_e        when (opcode_r = ulong_timed_sample_metadata_op_e) else
                   ushort_timed_sample_sample_op_e;

  output_out.opcode <= output_opcode;

  -- Since it takes a clock cycle for output to change have
  -- to delay these by one clock cycle
  delay_regs : process (input_in.clk)
  begin
    if rising_edge(input_in.clk) then
      if input_in.reset = '1' then
        first_data_r <= '0';
        last_data_r  <= '0';
        opcode_r     <= ulong_timed_sample_sample_op_e;
        valid_r      <= '0';
        som_r        <= '0';
        eom_r        <= '0';
        eof_r        <= '0';
        codeword_r   <= (others => '0');
        input_data_r <= (others => '0');
      else
        if take = '1' then
          codeword_r   <= codeword;
          valid_r      <= input_in.valid;
          som_r        <= input_in.som;
          eom_r        <= input_in.eom;
          opcode_r     <= input_in.opcode;
          input_data_r <= input_in.data;
        end if;
        -- To handle initial output from golay_encoder_rom when input_in.ready
        -- goes high for the first time
        if (first_data_r = '0' and input_in.ready = '1') then
          first_data_r <= '1';
        end if;
        -- To handle when input_in.ready goes low after an eof arrives
        if (input_in.eof = '1' and eof_r = '0' and output_in.ready = '1') then
          last_data_r <= '1';
        end if;
        if last_data_r = '1' then
          last_data_r <= '0';
          eof_r       <= input_in.eof;
        end if;
      end if;
    end if;
  end process delay_regs;

end rtl;
