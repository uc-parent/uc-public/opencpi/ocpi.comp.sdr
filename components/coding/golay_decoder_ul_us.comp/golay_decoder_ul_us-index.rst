.. golay_decoder_ul_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _golay_decoder_ul_us:


Golay Decoder (``golay_decoder_ul_us``)
=======================================
Takes a Golay codeword and decodes it into the original data.

Tested platforms include ``xsim``, ``e31x``.

Design
------
A Golay code is an error correcting code used for forward error correction in digital communications.

Given a codeword that is a complex long, the Golay decoder component looks at the LSB bits (24 LSB bits if the extended Golay code is implemented or 23 LSB bits if the perfect Golay code is implemented) and attempts to correct (if there are any bit errors) and decode the original data using a Golay correction and decoding algorithm.

A block diagram representation of Golay [24,12,8] is given in :numref:`golay_decoder-diagram`.

.. _golay_decoder-diagram:

.. figure:: golay_decoder.svg
   :alt: Block diagram outlining Golay [24,12,8].
   :align: center

   Block diagram outlining Golay [24,12,8].

Interface
---------
.. literalinclude:: ../specs/golay_decoder_ul_us-spec.xml
   :language: xml

Ports
~~~~~
.. ocpi_documentation_ports::

   input:  Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The Golay decoder decodes data with sample opcodes only.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../golay_decoder_ul_us.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependency (generated via ``gen_golay_error_patterns_rom.py``) for this worker is:

 * ``golay_error_patterns_rom.vhd``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``golay_decoder_ul_us`` are:

 * None.

Testing
-------
This component test suite uses the OpenCPI unit test framework. The unit test sends all possible Golay codewords with and without errors and checks that output data is the expected original data prior to encoding.

.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
