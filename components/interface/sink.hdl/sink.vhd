-- HDL Implementation of a sink component.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

library ocpi;
use ocpi.types.all;

architecture rtl of worker is
begin
  -- Take all input data when ready and take_data property is true
  input_out.take   <= (input_in.ready or input_in.valid) and props_in.take_data;
  -- Move to finished control state when take_data property is true and the
  -- EOF flag on the input port is set high.
  ctl_out.finished <= props_in.take_data and input_in.eof;
end rtl;
