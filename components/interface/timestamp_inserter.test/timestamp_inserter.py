#!/usr/bin/env python3

# Python implementation of Timestamp Inserter.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Timestamp Inserter implementation."""

import logging

import opencpi.ocpi_testing as ocpi_testing

from sdr_interface.sample_time import SampleTime


class TimestampInserter(ocpi_testing.Implementation):
    """Timestamp Inserter reference implementation."""

    def __init__(self, continuous_mode=False, timestamp_pending=False, leap_seconds=0):
        """Timestamp Inserter class.

        Args:
            continuous_mode (bool): Enables the prepending of a timestamp
                before each data message.
            timestamp_pending (bool): Signals that a timestamp is to be
                prepended before a data message.
            leap_seconds (int): Leap seconds to subtract from the GPS epoch.
        """
        super().__init__()
        self._continuous_mode = continuous_mode
        self._timestamp_pending = timestamp_pending

        logging.info("Timestamp Inserter settings are:"
                     f"Continuous Mode: {continuous_mode} "
                     f"Timestamp Pending: {timestamp_pending} "
                     f"Leap Seconds: {leap_seconds}")

        # Instantiate SampleTime class
        self._sample_time = SampleTime(leap_seconds=leap_seconds)

        # Track which time messages were inserted opcodes
        self._time_opcode_count = 0
        self._inserted_time_opcodes = []

    @property
    def inserted_time_opcodes(self):
        """Get inserted time opcodes property.

        A list containing the index (starting from 0) of the inserted time opcodes.

        Returns:
            List: Index of inserted timestamp opcodes.
        """
        return self._inserted_time_opcodes

    def _create_timestamp_opcode(self):
        """Created time opcode containing GPS timestamp.

        Returns:
            Formatted time opcode.
        """
        opcode = {"opcode": "time",
                  "data": self._sample_time.get_gps_timestamp()}

        self._inserted_time_opcodes.append(self._time_opcode_count)
        self._time_opcode_count += 1
        return opcode

    def reset(self):
        """Reset to initial state."""
        pass

    def time(self, *inputs):
        """Get messages resulting from a time message.

        Overloaded simply to count all output time opcodes. Allows later
        checks to know which time opcodes were passed through and which were
        inserted by this component.

        Args:
            inputs* (list of float): Time values for the input ports.

        Returns:
            Formatted opcodes for output.
        """
        self._time_opcode_count += 1
        return self._time_default(*inputs)

    def sample(self, _input):
        """Sample opcode handling.

        Args:
            input (list): List of sample values from the input port.

        Returns:
            Formatted opcodes for output.
        """
        messages = []

        if self._continuous_mode:
            opcode = self._create_timestamp_opcode()
            messages.append(opcode)
            logging.info("Continuous Mode Time Now: " + str(opcode["data"]))
            self._timestamp_pending = False

        if self._timestamp_pending:
            opcode = self._create_timestamp_opcode()
            messages.append(opcode)
            logging.info("Timestamp Pending Time Now: " + str(opcode["data"]))
            self._timestamp_pending = False

        messages.append({"opcode": "sample", "data": _input})
        return self.output_formatter(messages)
