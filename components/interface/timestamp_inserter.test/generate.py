#!/usr/bin/env python3

# Generates the input binary file for timestamp inserter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for timestamp_inserter tests."""

import os
import random
import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
continuous_mode = os.environ["OCPI_TEST_continuous_mode"].upper() == "TRUE"
timestamp_pending = os.environ["OCPI_TEST_timestamp_pending"].upper() == "TRUE"
leap_seconds = int(os.environ["OCPI_TEST_leap_seconds"])
port_width = int(os.environ["OCPI_TEST_port_width"])

seed = ocpi_testing.get_test_seed(arguments.case, subcase, port_width,
                                  continuous_mode, timestamp_pending,
                                  leap_seconds)

if port_width == 8:
    protocol = "uchar_timed_sample"
    basegenerator = ocpi_testing.generator.UnsignedCharacterGenerator
elif port_width == 16:
    protocol = "ushort_timed_sample"
    basegenerator = ocpi_testing.generator.UnsignedShortGenerator
elif port_width == 32:
    protocol = "ulong_timed_sample"
    basegenerator = ocpi_testing.generator.UnsignedLongGenerator
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
    basegenerator = ocpi_testing.generator.UnsignedLongLongGenerator
else:
    raise ValueError(f"Unsupported port length: {port_width}")


class TimestampInserterGenerator(basegenerator):
    """Customised protocol test data generator.

    Adds custom tests, with subcases for different combinations of opcodes and
    timestamp insertion properties.
    """

    def custom(self, seed, subcase):
        """Generate various combinations of opcode sequences.

        Usually called via ``self.generate()``.

        Accepts the following subcases: "time", "sample_interval",
        "flush", "discontinuity", "metadata". Generates an input
        opcode sequence that is the subcase opcode plus typical
        sample opcode. This tests that timestamps are not inserted
        before non-sample opcodes.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        messages = []
        if subcase == "time":
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
        elif subcase == "sample_interval":
            messages.append({"opcode": "sample_interval",
                             "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                                    float(self.SAMPLE_INTERVAL_MAX))})
        elif subcase == "flush":
            messages.append({"opcode": "flush", "data": None})
        elif subcase == "discontinuity":
            messages.append({"opcode": "discontinuity", "data": None})
        elif subcase == "metadata":
            messages.append({"opcode": "metadata",
                             "data": {"id": random.randint(0, self.METADATA_ID_MAX),
                                      "value": random.randint(0, self.METADATA_VALUE_MAX)}})
        else:
            raise ValueError(f"Unexpected subcase of {subcase} for custom()")

        messages += super().typical(seed, subcase)

        return messages


generator = TimestampInserterGenerator()

# Increase number of messages, so that the soak tests contain more
# messages and therefore more timestamps are generated.
generator.SOAK_ALL_OPCODE_AVERAGE_NUMBER_OF_MESSAGES *= 8

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, protocol) as file_id:
    file_id.write_dict_messages(messages)
