#!/usr/bin/env python3

# Generates the input binary file for fifo testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for fifo_message tests."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
data_width = int(os.environ["OCPI_TEST_data_width"])
data_depth = int(os.environ["OCPI_TEST_data_depth"])
message_depth = int(os.environ["OCPI_TEST_message_depth"])

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, data_width, data_depth, message_depth)

if data_width == 8:
    protocol = "char_timed_sample"
    generator = ocpi_testing.generator.CharacterGenerator()
elif data_width == 16:
    protocol = "short_timed_sample"
    generator = ocpi_testing.generator.ShortGenerator()
elif data_width == 32:
    protocol = "long_timed_sample"
    generator = ocpi_testing.generator.LongGenerator()
elif data_width == 64:
    protocol = "longlong_timed_sample"
    generator = ocpi_testing.generator.LongLongGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, protocol) as file_id:
    file_id.write_dict_messages(messages)
