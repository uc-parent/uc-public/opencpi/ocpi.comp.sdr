<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <!-- Case 00: Typical -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 01: Width property test -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" values="8,16,32,64"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 02: Wide width property test with modified properties for HDL worker -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="64"/>
    <property name="data_depth" value="8192"/>
    <property name="message_depth" value="1"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 03: Data depth property test -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" values="8192,16000,32000"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 04: Data depth property test (Run only on RCC as limit too large for FPGA resources) -->
  <case excludeworkers="fifo_message.hdl">
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="0xffffffff"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 05: Message depth property test -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" values="1,2,100"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 06: Message depth property test (Run only on RCC as limit too large for FPGA resources) -->
  <case excludeworkers="fifo_message.hdl">
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="0xffff"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 07: Store all controls property test -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="true"/>
  </case>
  <!-- Case 08: Sample case test -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="all_zero,all_maximum,large_positive,near_zero"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 09: Input stressing -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 10: Message size -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 11: Time opcodes -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 12: Sample interval opcodes -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 13: Flush opcodes -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="single,consecutive"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 14: Discontinuity opcodes -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="single,consecutive"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 15: Metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" value="16"/>
    <property name="data_depth" value="16384"/>
    <property name="message_depth" value="512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
  <!-- Case 16: Soak test -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="data_width" values="8,16,32,64"/>
    <property name="data_depth" values="16384,100000"/>
    <property name="message_depth" values="1,512"/>
    <property name="empty_count" value="0"/>
    <property name="full_data_count" value="0"/>
    <property name="full_message_count" value="0"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
    <property name="fifo_message.hdl.store_all_controls" value="false"/>
  </case>
</tests>
