-- HDL Implementation of a multiplexer component.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal take_1_d         : std_logic;
  signal take_2_d         : std_logic;
  signal input_1_eom      : std_logic;
  signal input_2_eom      : std_logic;
  signal inselect_valid   : std_logic;
  signal inselect_value   : std_logic;
  signal inselect_value_d : std_logic;
  signal ready_to_switch  : std_logic;
  signal port_select      : std_logic;
  signal port_select_d    : std_logic;
  signal ignore           : std_logic;
  signal ignore_d         : std_logic;
  signal gate             : std_logic;
  signal input_select     : std_logic;

begin

  input_1_out.take  <= output_in.ready;
  input_2_out.take  <= output_in.ready;
  -- Take data from inselect whenever it is available
  inselect_out.take <= '1';

  -- Signal end of message for inputs
  input_1_eom <= (input_1_in.ready or input_1_in.valid) and input_1_in.eom;
  input_2_eom <= (input_2_in.ready or input_2_in.valid) and input_2_in.eom;

  -- Signal high when messages are in progress
  take_inputs_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        take_1_d <= '0';
        take_2_d <= '0';
      elsif output_in.ready = '1' then
        -- Take input_1
        if input_1_eom = '1' then
          take_1_d <= '0';
        else
          take_1_d <= '1';
        end if;
        -- Take input_2
        if input_2_eom = '1' then
          take_2_d <= '0';
        else
          take_2_d <= '1';
        end if;
      end if;
    end if;
  end process;

  -- Signal valid data on inselect port
  inselect_valid <= '1' when (inselect_in.valid = '1'
                              and inselect_in.opcode = bool_timed_sample_sample_op_e)
                    else '0';

  -- Register the inselect values
  inselect_reg_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        inselect_value_d <= '0';
      elsif inselect_valid = '1' then
        inselect_value_d <= inselect_in.data(0);
      end if;
    end if;
  end process;

  inselect_value <= inselect_in.data(0) when inselect_valid = '1' else inselect_value_d;

  -- Use property port_select_enable to determine whether a property or a port
  -- specifies which input to pass through
  port_select <= props_in.port_select when props_in.port_select_enable = '0'
                 else inselect_value;

  -- Ensure switching only occurs when the currently selected input is
  -- between messages
  ready_to_switch <= (not port_select_d and not take_1_d)
                     or (port_select_d and not take_2_d);

  -- Register port select value for duration of passed through message
  port_select_reg_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        port_select_d <= '0';
      elsif output_in.ready = '1' then
        port_select_d <= input_select;
      end if;
    end if;
  end process;

  input_select <= port_select when ready_to_switch = '1' else port_select_d;

  -- Register ignore signal until eom is detected on selected port
  -- Gate signal used to "ignore" input data if a switch occurred mid-message
  ignore_reg_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        ignore_d <= '0';
      elsif output_in.ready = '1' then
        if (input_select = '0' and input_1_eom = '1')
          or (input_select = '1' and input_2_eom = '1') then
          ignore_d <= '0';
        elsif ready_to_switch = '1' then
          ignore_d <= ignore;
        end if;
      end if;
    end if;
  end process;

  -- Flag when selected input is mid-message
  ignore <= (not input_select and take_1_d)
            or (input_select and take_2_d);

  gate <= ignore when ready_to_switch = '1' else ignore_d;

  -- Streaming interface output
  output_mux_p : process(gate, input_select,
                         input_1_in, input_2_in)
  begin
    if gate = '1' then
      output_out.give  <= '0';
      output_out.valid <= '0';
    elsif input_select = '0' then
      output_out.give        <= input_1_in.ready;
      output_out.valid       <= input_1_in.valid;
      output_out.eom         <= input_1_in.eom;
      output_out.data        <= input_1_in.data;
      output_out.opcode      <= input_1_in.opcode;
      output_out.byte_enable <= input_1_in.byte_enable;
    else
      output_out.give        <= input_2_in.ready;
      output_out.valid       <= input_2_in.valid;
      output_out.eom         <= input_2_in.eom;
      output_out.data        <= input_2_in.data;
      output_out.opcode      <= input_2_in.opcode;
      output_out.byte_enable <= input_2_in.byte_enable;
    end if;
  end process;

end rtl;
