<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
  </case>
  <!-- Note, testing with low values for step_size will take 100s of hours
       to complete. Limiting to larger values for speed. -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"
      values="6144170,4294967294,4294967295,2147483548,1073741824,3651978334,1604137366,4061567421,3936925831,3299432489"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="step_size"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" values="true,false"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="enable"/>
  </case>
  <!-- Have no way to know when worker was lacking input data when testing. So
       cannot verify testing when zero_pad_when_not_ready is zero, therefore
       not including test. -->
  <!--
  <case>
    <input port="input" script="generate.py -case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="43049876"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" values="true,false"/>
    <property name="subcase" value="zero_pad_when_not_ready"/>
  </case>
  -->
  <!-- Cannot currently verify as no way to determine when property changed in
       relation to data. -->
  <!--
  <case>
    <input port="input" script="generate.py -case property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size">
      <set delay="0" value="2147483548"/>
      <set delay="1" value="4061567421"/>
    </property>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="step_size"/>
  </case>
  <case>
    <input port="input" script="generate.py -case property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable">
      <set delay="0" value="true"/>
      <set delay="1" value="false"/>
    </property>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="enable"/>
  <case>
    <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready">
      <set delay="0" value="false"/>
      <set delay="1" value="true"/>
    </property>
    <property name="subcase" value="zero_pad_when_not_ready"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero"/>
  </case>
  <!-- Test never terminates when attribute stressormode is set to full since
       a ZLM is not sent at the end in OpenCPI 1.4. -->
  <!--
  <case>
    <input port="input" script="generate.py -case input_stressing" messagesinfile="true" messagesize="16384"
      stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Note, testing with low values for step_size will take 100s of hours
       to complete. Limiting to larger values for speed. -->
  <!-- Cannot test with zero_pad_when_not_ready is true, as have no way of
       knowing when a worker was lacking input data when testing. So cannot
       verify testing, therefore not including test.-->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"
      values="4016393383,4066649688,1146601149,949900932,1336895829,1457318339,263504573,168155675,1691086021,1011734895"/>
    <property name="enable" values="true,false"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
