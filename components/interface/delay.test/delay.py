#!/usr/bin/env python3

# Python implementation of delay block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of Delay for verification."""

import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class Delay(ocpi_testing.Implementation):
    """Protocol-less version of the delay implementation."""

    def __init__(self):
        """Initialise Delay class."""
        super().__init__()

    def reset(self):
        """Component is stateless, therefore no reset conditions needed."""
        # It is a requirement for the verification classes that all
        # implementations have a reset function, however for delay this doesn't
        # make much sense so just pass.
        pass

    def sample(self, values):
        """Sample opcode handling.

        Args:
            values (list): Input samples.

        Returns:
            dict: Formatted sample messages.
        """
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])
