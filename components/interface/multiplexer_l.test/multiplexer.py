#!/usr/bin/env python3

# Python implementation of multiplexer block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of multiplexer block for verification."""

import opencpi.ocpi_testing as ocpi_testing


class Multiplexer(ocpi_testing.Implementation):
    """A Python Multiplexer for verification."""

    def __init__(self, port_select):
        """Initialises the Multiplexer class.

        Args:
            port_select (bool): Selects which input port data is passed to
                                the output. When false, data on ``input_1``
                                is passed to the output
        """
        super().__init__(port_select=port_select)

        self.input_ports = ["input_1", "input_2"]

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def select_input(self, input_1, input_2):
        """Determine which input port should be advanced.

        Args:
            input_1 (str): Name of the next opcode on the input_1 port.
            input_2 (str): Name of the next opcode on the input_2 port.

        Returns:
            An integer of the input port to be advanced, indexed from zero.
        """
        if not self.port_select:
            return 0
        else:
            return 1

    def sample(self, input_1, input_2):
        """Handle incoming Sample opcode.

        Args:
            input_1 (list): Incoming sample values from the input_1 port.
            input_2 (list): Incoming sample values from the input_2 port.

        Returns:
            Formatted output opcode messages.
        """
        if not self.port_select:
            output_values = input_1
        else:
            output_values = input_2

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
