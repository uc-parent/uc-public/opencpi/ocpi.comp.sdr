.. timestamp_inserter HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _timestamp_inserter-HDL-worker:

``timestamp_inserter`` HDL Worker
=================================

Detail
------
Data is read from the input port when the output is ready and no timestamps are currently being inserted.

Input data is added to the last used pipeline stage of an internal skid buffer if the pipeline is being shifted, else it is added to the next available stage. The skid buffer is dimensioned to equal the propagation delay of the ready being de-asserted on the output reaching the input, and the delay of the data reaching the output port. Therefore, there will always be a free slot in the skid buffer if required. Data is passed to the output port from the first address of the skid buffer when the output port is ready and no timestamps are being inserted. The skid buffer is then shifted.

Timestamps are provided as an input to the component on the time interface. The timestamp is a 64-bit number with the first 32 bits corresponding to seconds and the last 32 bits corresponding to fractions of a second. When a timestamp is to be inserted and a valid sample opcode message is detected on the input, the timestamp is registered and put to the output port, seconds first followed by fractions of a second. Timestamps will be output as a Time opcode.

The time interface from which the timestamps are generated originates from the OpenCPI time-server, which is instanced as part of the platform worker. Furthermore, an additional component (the time-client) is dynamically instanced by the framework for all components that declare time interfaces. The time-client communicates with the time-server and produces the time interface seen by the component.

.. ocpi_documentation_worker::


Utilisation
-----------
.. ocpi_documentation_utilization::
