-- HDL implementation of the protocol delay module
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes sample data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes sample data.

library ieee;
use ieee.std_logic_1164.all;
library work;
use work.delay_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;

entity protocol_delay is
  generic (
    delay_g              : integer   := 1;
    data_in_width_g      : integer   := 16;
    processed_data_mux_g : std_logic := '1'
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    input_in            : in  worker_input_in_t;   -- Input streaming interface
    -- Connect output from data processing module.
    processed_stream_in : in  std_logic_vector(data_in_width_g - 1 downto 0);
    output_out          : out worker_output_out_t  -- Output streaming interface
    );
end protocol_delay;

architecture rtl of protocol_delay is
  signal output_data : std_logic_vector(data_in_width_g - 1 downto 0);
begin

  protocol_delay_i : protocol_interface_delay_v2
    generic map (
      delay_g                 => delay_g,
      data_width_g            => data_in_width_g,
      opcode_width_g          => input_in.opcode'length,
      byte_enable_width_g     => input_in.byte_enable'length,
      processed_data_mux_g    => processed_data_mux_g,
      processed_data_opcode_g => x"00"
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => enable,
      take_in             => take_in,
      processed_stream_in => processed_stream_in,
      input_som           => '0',
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_in.opcode,
      input_data          => input_in.data,
      output_som          => open,
      output_eom          => output_out.eom,
      output_eof          => output_out.eof,
      output_valid        => output_out.valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_out.opcode,
      output_data         => output_data
      );

  output_out.data <= output_data;

end rtl;
