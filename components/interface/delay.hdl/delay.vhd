-- HDL implementation of Delay worker
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is
  constant latency_c : natural := to_integer(latency);
begin

  input_out.take <= output_in.ready;

  delay_zero_gen : if latency_c = 0 generate
    output_out.data        <= input_in.data;
    output_out.eom         <= input_in.eom;
    output_out.valid       <= input_in.valid;
    output_out.give        <= input_in.ready;
    output_out.opcode      <= input_in.opcode;
    output_out.byte_enable <= input_in.byte_enable;
    output_out.eof         <= input_in.eof;
  end generate;

  delay_not_zero_gen : if latency_c > 0 generate
    interface_delay_i : entity work.protocol_delay
      generic map (
        delay_g              => latency_c,
        data_in_width_g      => input_in.data'length,
        processed_data_mux_g => '0'
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => output_in.ready,
        take_in             => '1',
        input_in            => input_in,
        processed_stream_in => (others => '0'),
        output_out          => output_out
        );
  end generate;

end rtl;
