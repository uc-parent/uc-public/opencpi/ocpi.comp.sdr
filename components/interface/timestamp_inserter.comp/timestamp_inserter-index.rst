.. timestamp_inserter documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _timestamp_inserter:

Timestamp Inserter (``timestamp_inserter``)
===========================================
Inserts timestamps before sample opcode messages. Can be configured to insert a timestamp before every sample opcode, or only when requested.

Design
------
Passes all opcode messages received on the input port to the output port. When a Sample opcode message is received, a timestamp is optionally output via a Time opcode message before the Sample opcode message is passed onto the output.

On reception of a Sample opcode, if the property ``continuous_mode`` is true, a timestamp is generated and inserted as a Time opcode before the Sample opcode message. If ``continuous_mode`` is false, the timestamp is only generated and inserted if the property ``timestamp_pending`` is true.

When the worker inserts a Time opcode, it clears the ``timestamp_pending`` property to false. This also indicates that a timestamp insertion action has been been completed.

Thus if both ``continuous_mode`` and ``timestamp_pending`` are false, no timestamp insertion action shall occur, and the component just passes data through unchanged.

A block diagram of the component is as follows:-

.. timestamp_inserter-timestamp_inserter_block_diagram:

.. figure:: ./timestamp_inserter_blk.svg
   :alt: Block Diagram
   :align: center

   Block Diagram

Interface
---------
.. literalinclude:: ../specs/timestamp_inserter-spec.xml
   :language: xml
   :lines: 1,19-

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   continuous_mode: When true, timestamps are inserted before every sample opcode in the data stream. When false, single timestamps are inserted as governed by the ``timestamp_pending`` property.

   timestamp_pending:  When ``continuous_mode`` is false, setting ``timestamp_pending`` to true triggers a timestamp to be sent before the next sample opcode. The worker will set ``timestamp_pending`` to false when it has completed the insertion of the timestamp. When ``continuous_mode`` is true, the ``timestamp_pending`` value is ignored and the worker will set it to false.


Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../timestamp_inserter.hdl ../timestamp_inserter.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

  * HDL OpenCPI dependencies:

   * ``Time-server``.

   * ``Time-client``.

   * :ref:`SDR Utilities Library <sdr_utilities_library>`

  * RCC OpenCPI dependencies:

   * ``components/util/common/opcodes.hh``

There is also a dependency on:

  * HDL specific dependencies:

   * ``ieee.std_logic_1164``

   * ``ieee.numeric_std``

   * ``ieee.math_real``

  * RCC specific dependencies:

   * ``<chrono>``

Limitations
-----------
The limitations of ``timestamp_inserter`` are:

 * Multiple triggering of the ``timestamp_pending`` property will result in only one timestamp being inserted before an input message.

 * Whilst the component is "protocol-less", it is assumed it will be used with one of the the :ref:`timed sample protocols <timed_sample_protocols>`.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
