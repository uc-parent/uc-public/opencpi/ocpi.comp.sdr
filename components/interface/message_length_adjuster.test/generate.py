#!/usr/bin/env python3

# Generates the input binary file for message_length_adjuster testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for message_length_adjuster testing."""

import os
import random

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
message_size = int(os.environ.get("OCPI_TEST_message_size"))
timeout = int(os.environ.get("OCPI_TEST_timeout"))
width = int(os.environ.get("OCPI_TEST_width"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  message_size, timeout, width)

if width == 8:
    protocol = "uchar_timed_sample"
    generator = ocpi_testing.generator.UnsignedCharacterGenerator
elif width == 16:
    protocol = "ushort_timed_sample"
    generator = ocpi_testing.generator.UnsignedShortGenerator
elif width == 32:
    protocol = "ulong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongGenerator
elif width == 64:
    protocol = "ulonglong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongLongGenerator
else:
    raise ValueError(f"Unsupported width: {width}")


class MessageLengthAdjusterGenerator(generator):
    """Message Length Adjuster test case generator."""

    def __init__(self):
        """Initialise."""
        super().__init__()
        self.SAMPLE_DATA_LENGTH = self.MESSAGE_SIZE_LONGEST

        if subcase == "shortest":
            # Test data will combine into more than one output message
            self.MESSAGE_SIZE_NUMBER_OF_MESSAGES = int(max(
                self.MESSAGE_SIZE_NUMBER_OF_MESSAGES,
                (message_size * 1.5) // self.MESSAGE_SIZE_SHORTEST))

    def property(self, seed, subcase):
        """Messages when testing a port's handling of different message widths.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the width size testing.
        """
        random.seed(seed)
        # This test case generates the longest sized sample opcode as possible.
        if subcase == "sample_width":
            return [{"opcode": "sample",
                     "data": self._get_sample_values(self.MESSAGE_SIZE_LONGEST)
                     } for _ in range(self.MESSAGE_SIZE_NUMBER_OF_MESSAGES)]

        return super().property(seed, subcase)


generator = MessageLengthAdjusterGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, protocol) as file_id:
    file_id.write_dict_messages(messages)
