#!/usr/bin/env python3

# Runs on correct output for message_length_adjuster implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verification script for message length adjuster unit tests."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from message_length_adjuster import MessageLengthAdjuster

message_size = int(os.environ.get("OCPI_TEST_message_size"))
timeout = int(os.environ.get("OCPI_TEST_timeout"))
width = int(os.environ.get("OCPI_TEST_width", 8))

input_file = str(sys.argv[-1])
output_file = str(sys.argv[-2])

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 0))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

test_id = ocpi_testing.get_test_case()
logging.basicConfig(
    level=verify_log_level,
    filename=f"{test_id}.message_length_adjuster.py.log",
    filemode="w")

message_length_adjuster_implementation = MessageLengthAdjuster(message_size)

test_id = ocpi_testing.get_test_case()

if width == 8:
    protocol = "uchar_timed_sample"
elif width == 16:
    protocol = "ushort_timed_sample"
elif width == 32:
    protocol = "ulong_timed_sample"
elif width == 64:
    protocol = "ulonglong_timed_sample"
else:
    raise ValueError(f"Unsupported width: {width}")

verifier = ocpi_testing.Verifier(message_length_adjuster_implementation)
verifier.set_port_types([protocol], [protocol], ["equal"])
if not verifier.verify(test_id, [input_file], [output_file]):
    sys.exit(1)
