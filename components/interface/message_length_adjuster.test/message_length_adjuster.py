#!/usr/bin/env python3

# Python implementation of message_length_adjuster block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of message_length_adjuster block for verification."""

import opencpi.ocpi_testing as ocpi_testing
import logging


class MessageLengthAdjuster(ocpi_testing.Implementation):
    """Python implementation of message_length_adjuster functionality."""

    def __init__(self, message_size):
        """Message Length Adjuster.

        Args:
            message_size (int): Sets the maximum number of samples per message.
        """
        super().__init__(message_size=message_size)
        self.reset()

    def reset(self):
        """Initialises the internal state."""
        self._input_buffer = []

    def process_message(self, opcode, data):
        """Joins messages according to the maximum allowed message size.

        Args:
            opcode (str): Message type.
            data (list): Message data.

        Returns:
            List of unformatted messages.
        """
        if opcode == "sample":
            logging.debug(f"Adding {len(data)} samples to buffer.")
            self._input_buffer += list(data)

        # Sample messages cannot have more than message_size samples in each
        # message
        messages = [{
            "opcode": "sample",
            "data": self._input_buffer[index:
                                       index + self.message_size]}
                    for index in range(0, len(self._input_buffer),
                                       self.message_size)]
        # When sample messages are received ensure only complete messages are
        # output. When a non-sample message is received all of the buffered
        # samples are output ahead of it.
        number_of_messages, remaining_samples = divmod(len(self._input_buffer),
                                                       self.message_size)
        if opcode == "sample":
            if number_of_messages == 0:
                logging.debug(
                    f"Not filled a sample opcode to {self.message_size} samples - output nothing.")
                messages = []
            else:
                if remaining_samples == 0:
                    self._input_buffer = []
                else:
                    messages = messages[:-1]
                    self._input_buffer = self._input_buffer[-remaining_samples:]
                logging.debug(
                    f"Outputting {len(messages)} full sample opcodes")
                logging.debug(f"{remaining_samples} remain to be output")
        else:
            if len(messages):
                logging.debug(f"Outputting partially filled sample opcode")
            messages.append({"opcode": opcode, "data": data})
            logging.debug(f"Passing through {opcode} opcode")
            self._input_buffer = []
        return messages

    def sample(self, data):
        """Process a sample message.

        Args:
            data (list): Message data.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("sample", data)
        return self.output_formatter(messages)

    def time(self, value):
        """Process a time message.

        Args:
            value (decimal): Time message value.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("time", value)
        return self.output_formatter(messages)

    def sample_interval(self, value):
        """Process a sample interval message.

        Args:
            value (decimal): Sample interval message value.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("sample_interval", value)
        return self.output_formatter(messages)

    def discontinuity(self, input_):
        """Process a discontinuity message.

        Args:
            input_: Ignored input port data.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("discontinuity", None)
        return self.output_formatter(messages)

    def flush(self, input_):
        """Process a flush message.

        Args:
            input_: Ignored input port data.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("flush", None)
        return self.output_formatter(messages)

    def metadata(self, value):
        """Process a metadata message.

        Args:
            value (dict): Metadata message ID / value pair.

        Returns:
            Formatted messages.
        """
        messages = self.process_message("metadata", value)
        return self.output_formatter(messages)

    def end_of_files(self):
        """Generate any additional messages when the input data has ended.

        Returns:
            Formatted messages.
        """
        logging.debug("End of input, flush any other data")
        # Flush out remaining buffered data
        messages = self.process_message("flush", None)
        # But do not send the trailing flush opcode
        messages = messages[:-1]
        return self.output_formatter(messages)
