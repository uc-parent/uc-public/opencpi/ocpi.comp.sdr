.. fifo_message documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. meta::
   :keywords: message fifo buffer

.. _fifo_message:

FIFO Message Buffer (``fifo_message``)
======================================
Performs First In First Out (FIFO) buffering of messages.

A FIFO is a data buffering component that provides a degree of isolation between the input and output ports. By setting the depth to an appropriate size, the input can receive data without concern for back-pressure.

Design
------
Samples are read from the input port. They will be sent to the output unless the output is applying back-pressure:

 * The component will receive data when supplied by the input port, otherwise it will process any buffered data to the output port.

 * The component will send data on the output port whenever there is no output back-pressure and data is available from the FIFO.

If the FIFO becomes full, either by reaching the maximum number of messages or the maximum words of `data_width`, back-pressure will be applied to the input port until there has been sufficient output to make room. To avoid back-pressure on the input port, the data depth and message depth should be chosen to cater for the worst case difference in rate between the input and output ports.

In the absence of back-pressure and no backlog in the FIFO, messages will be passed from the input to output with the minimum of delay. It is an implementation detail whether this passes through or by-passes the FIFO store.

The FIFO provides several properties to control and evaluate the effectiveness of the buffer:

* Current data fill.

* Current message fill.

* Empty count.

* Full data count.

* Full message count.

As a FIFO full condition will cause back-pressure on the input port, ``full_data_count`` and ``full_message_count`` will indicate the number of times back-pressure has been applied preventing input data being consumed.


Interface
---------
.. literalinclude:: ../specs/fifo_message-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
Whilst this component has been written without reference to a specific protocol, it has been assumed it will be used with the :ref:`timed sample protocols <timed_sample_protocols>`. As such opcode 0 is assumed to be a sample opcode.

All opcodes will be passed to the output in the same order as they arrived, unchanged.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   data_width: Must be a multiple of 8 - i.e. a whole number of bytes.

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../fifo_message.hdl ../fifo_message.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------

The dependencies to other elements in OpenCPI are:

 * HDL OpenCPI dependencies:

   * :ref:`FIFO Instant Read Primitive <fifo_instant_read-primitive>`.

 * RCC OpenCPI dependencies:

   * None.

There are also dependencies on:

 * HDL specific dependencies:

   * ``ieee.std_logic_1164``

   * ``ieee.numeric_std``

   * ``ieee.math_real``

 * RCC specific dependencies:

   * <utility>

   * <queue>

Limitations
-----------
Limitations of ``fifo_message`` are:

 * The HDL worker is limited to a ``data_depth`` of :math:`(2^{31})-1` although the device resources are liable to limit this further.

 * Only ``data_width`` sizes of 8, 16, 32, and 64 are supported.

 * The component only protects the input port from back-pressure if the data and message depths are sufficiently sized to accommodate the worst-case rate difference between input and output.

 * The ``data_depth`` must be large enough to accept a maximum size input message.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
