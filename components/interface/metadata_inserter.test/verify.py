#!/usr/bin/env python3

# Runs checks for metadata inserter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Metadata Inserter OCPI Verification script."""

import os
import sys
import logging
from os.path import exists
from xml.dom import minidom

import opencpi.ocpi_testing as ocpi_testing

from metadata_inserter import MetadataInserter

input_file = str(sys.argv[-1])
trigger_file = str(sys.argv[-2])
output_file = str(sys.argv[-3])

case_text = str(os.environ.get("OCPI_TESTCASE"))
case = int(case_text[-2:])

subcase_text = str(os.environ.get("OCPI_TESTSUBCASE"))
subcase = int(subcase_text)

# Get the application xml, and use this to find the initial value for
# metadata_value and insert_metadata_message.
# Note: this is done as the OCPI_TEST_xxx is modified by the UUT, and the value
# present at the end of the application run is passed to this verify script.
app = minidom.parse(f"../../gen/applications/case{case:02}.{subcase:02}.xml")
metadata_value = None
for inst in app.getElementsByTagName("instance"):
    if inst.getAttribute("name") == "metadata_inserter":
        component = inst
        for prop in inst.getElementsByTagName("property"):
            if prop.getAttribute("name") == "metadata_value":
                metadata_value = int(prop.getAttribute("value"))
            if prop.getAttribute("name") == "insert_metadata_message":
                insert_metadata_message = prop.getAttribute(
                    "value").upper() == "TRUE"

test_id = ocpi_testing.get_test_case()

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
metadata_id = int(os.environ["OCPI_TEST_metadata_id"])
trigger_disconnected = (os.environ["OCPI_TEST_trigger_disconnected"]
                        .upper() == "TRUE")
increment_value = os.environ["OCPI_TEST_increment_value"].upper() == "TRUE"

port_width = int(os.environ["OCPI_TEST_port_width"])

if port_width == 8:
    protocol = "uchar_timed_sample"
elif port_width == 16:
    protocol = "ushort_timed_sample"
elif port_width == 32:
    protocol = "ulong_timed_sample"
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
else:
    raise ValueError(f"Unsupported port length: {port_width}")

if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logfile = os.path.dirname(output_file)
logfile = os.path.join(logfile, f"{test_id}.metadata_inserter.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")

metadata_inserter_implementation = MetadataInserter(
    metadata_id, metadata_value, increment_value, insert_metadata_message)

verifier = ocpi_testing.Verifier(metadata_inserter_implementation)

if not exists(trigger_file) or trigger_disconnected:
    logging.info("Trigger port not connected")
    trigger_file = "/dev/null"

verifier.set_port_types([protocol, "bool_timed_sample"], [protocol], ["equal"])

if not verifier.verify(test_id, [input_file, trigger_file], [output_file]):
    sys.exit(1)
