#!/usr/bin/env python3

# Generates the input binary file for metadata inserter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI trigger generator for metadata_inserter tests."""

import os
import random

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
metadata_id = int(os.environ["OCPI_TEST_metadata_id"])
metadata_value = int(os.environ["OCPI_TEST_metadata_value"])
increment_value = os.environ["OCPI_TEST_increment_value"].upper() == "TRUE"
insert_metadata_message = (os.environ["OCPI_TEST_insert_metadata_message"]
                           .upper() == "TRUE")

# Use the same seed as the main message generator
seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  metadata_id, metadata_value, increment_value, insert_metadata_message)
random.seed(seed)

generator = ocpi_testing.generator.BooleanGenerator()
# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)

# Overwrite the random true/false with a small number of triggers in each
# "sample" opcode, ensuring there are varying size blocks of "samples"
# dispersed between each trigger. Also limits the number of triggers to a
# maximum of 10 per "sample" opcode.
for i, item in enumerate(messages):
    if item["opcode"] == "sample":
        msg_len = len(item["data"])
        # Assign all samples to false
        item["data"] = [False] * msg_len
        # Set up to 10 random samples to True
        for _ in range(random.randint(1, 10)):
            item["data"][random.randint(0, msg_len) - 1] = True

with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
