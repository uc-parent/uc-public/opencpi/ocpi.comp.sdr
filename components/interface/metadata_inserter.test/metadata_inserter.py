#!/usr/bin/env python3

# Python implementation of metadata inserter block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of Metadata Inserter for verification."""

import logging

import opencpi.ocpi_testing as ocpi_testing


class MetadataInserter(ocpi_testing.Implementation):
    """Protocol-less version of the Metadata Inserter ocpi implementation."""

    def __init__(self, metadata_id=0, metadata_value=0, increment_value=False,
                 insert_metadata_message=False):
        """Metadata Inserter.

        Args:
            metadata_id (uint32): Metadata opcode id.
            metadata_value (uint64): Metadata opcode value.
            increment_value (bool): Increment metadata value after each insert.
            insert_metadata_message (bool): If true initially will insert a
                metadata opcode before processing any buffers. Subsequently will
                cause a single metadata opcode to be inserted.
        """
        super().__init__()
        self.input_ports.append("trigger")
        self._metadata_id = metadata_id
        self._metadata_value = metadata_value
        self._increment_value = increment_value
        self._insert_metadata_message = insert_metadata_message

        logging.info("Metadata settings are:"
                     f"id: {metadata_id}, value: {metadata_value} "
                     f"increment: {increment_value}")

        self.messages = []        # List of opcode messages to output
        self.triggers = []        # List of triggers received on trigger port
        self.output_samples = []  # List of samples to be sent out
        self.input_data = []      # List of opcodes received on input port
        # This is either samples waiting for a trigger sample opcode before they
        # can be processed, or other opcodes that can't overtake input samples.

    def reset(self):
        """Reset to initial state."""
        self.messages = []
        self.triggers = []
        self.output_samples = []
        self.input_data = []

        # The _insert_metadata_message property is only set at
        # the start, therefore add that message if required.
        if self._insert_metadata_message:
            self._generate_metadata_opcode()
            self._insert_metadata_message = False

    def select_input(self, input, trigger):
        """Select which input port should be sampled next.

        Args:
            input (str): Opcode name of the next message for the input port.
            trigger (str): Opcode name of the next message for the trigger port.

        Returns:
            int: input port to prioritise (zero based off named inputs).
        """
        if trigger is not None:
            # Prefer trigger port, to allow dropping of non-sample opcodes
            # from the trigger port early.
            # If sample opcode, will either process or store, depending if
            # the input port has samples too.
            return 1
        if input is not None:
            return 0

    def _generate_metadata_opcode(self):
        """Generate Metadata opcode, and insert into list of message to send.

        Updates the Metadata value if required.
        """
        logging.debug("Inserting a metadata message")
        opcode = {"opcode": "metadata",
                  "data": {"id": self._metadata_id,
                           "value": self._metadata_value}}
        self.messages.append(opcode)
        if self._increment_value:
            self._metadata_value = (self._metadata_value+1) % (2**64)

    def _format_messages(self):
        """Format current list of messages for output, then clear the list.

        Returns:
            Formatted messages
        """
        output = self.output_formatter(self.messages)
        self.messages = []
        return output

    def sample(self, input, trigger_port):
        """Sample opcode handling.

        Args:
            input (list): List of sample values from the input port.
            trigger (list): List of sample values from the trigger port.

        Returns:
            dict: Formatted messages.
        """
        # If the trigger port has samples, add them to the triggers list.
        if trigger_port:
            logging.debug(
                f"Trigger port received : {len(trigger_port)} samples.")
            self.triggers.extend(trigger_port)
            logging.debug(f"self.triggers length {len(self.triggers)}")

        # If the input port has a samples opcode, add that to the input list.
        if input:
            logging.debug(f"Input port received : {len(input)} samples.")
            self.input_data.append({"opcode": "sample", "data": input})

        # Process stored list of opcodes
        while len(self.input_data):
            opcode = self.input_data[0]["opcode"]
            if opcode != "sample":
                # Next opcode is non-sample, pass it through.
                logging.debug(f"Forwarding a delayed {opcode} message.")
                data = self.input_data[0]["data"]
                self.messages.append({"opcode": opcode, "data": data})
                self.input_data = self.input_data[1:]
            elif len(self.triggers):
                # Process samples and triggers
                self._process_sample_trigger_pairs()
            else:
                # Next stored opcode is sample, but need more triggers
                # to process it
                break

        # Format and output any generated opcodes
        return self._format_messages()

    def _process_sample_trigger_pairs(self):
        """Process samples and triggers into samples and inserted metadata.

        While the first stored opcode has samples, and there are trigger to
        pair with them, check the triggers and add the samples, and any metadata
        to be inserted, to the list of opcodes to output.
        """
        # For each sample/trigger pair in the input lists
        while ((len(self.input_data[0]["data"]) > 0) and
               (len(self.triggers) > 0)):
            # Check trigger
            if self.triggers[0]:
                # Trigger is true, insert current samples
                # then insert metadata
                if len(self.output_samples) > 0:
                    logging.debug(f"Inserting {len(self.output_samples)}"
                                  " samples before trigger")
                    self.messages.append({"opcode": "sample",
                                          "data": self.output_samples})
                    self.output_samples = []
                self._generate_metadata_opcode()

            # Add sample to output list
            self.output_samples.append(self.input_data[0]["data"][0])

            # Remove processed sample/trigger pair from input lists
            self.input_data[0]["data"] = self.input_data[0]["data"][1:]
            self.triggers = self.triggers[1:]

        # No more sample/trigger pairs
        # Are there still sample to process in this opcode?
        if len(self.input_data[0]["data"]) > 0:
            # Processed all triggers, but still have samples.
            # Don't add to output opcode yet, wait for more triggers
            return
        else:
            # Processed all samples in this input opcode
            # Add them to opcodes to output
            if len(self.output_samples) > 0:
                logging.debug(f"Inserting {len(self.output_samples)}"
                              " samples as EOM on input")
                self.messages.append({"opcode": "sample",
                                      "data": self.output_samples})
                self.output_samples = []
            # Remove now empty input opcode from the list
            self.input_data = self.input_data[1:]

    def flush(self, input, trigger):
        """Flush opcode handler: Pass through flush opcode.

        Args:
            input (dict): Input port data.
            trigger (dict): Trigger port data.

        Returns:
            Messages to output (may be empty).
        """
        self._non_sample_opcode("flush", input, trigger)
        return self._format_messages()

    def discontinuity(self, input, trigger):
        """Discontinuity opcode handler: Pass through discontinuity opcode.

        Args:
            input  (dict): Input port data.
            trigger (dict): Trigger port data.

        Returns:
            Messages to output (may be empty).
        """
        self._non_sample_opcode("discontinuity", input, trigger)
        return self._format_messages()

    def sample_interval(self, input, trigger):
        """Sample Interval opcode handler: Pass through sample_interval opcode.

        Args:
            input  (dict): Input port data.
            trigger (dict): Trigger port data.

        Returns:
            List of messages to output (may be empty).
        """
        self._non_sample_opcode("sample_interval", input, trigger)
        return self._format_messages()

    def time(self, input, trigger):
        """Time opcode handler: Pass through time opcode.

        Args:
            input  (dict): Input port data.
            trigger (dict): Trigger port data.

        Returns:
            List of messages to output (may be empty).
        """
        self._non_sample_opcode("time", input, trigger)
        return self._format_messages()

    def metadata(self, input, trigger):
        """Metadata opcode handler: Pass through metadata opcode.

        Args:
            input  (dict): Input port data.
            trigger (dict): Trigger port data.

        Returns:
            Messages to output (may be empty).
        """
        self._non_sample_opcode("metadata", input, trigger)
        return self._format_messages()

    def _non_sample_opcode(self, opcode, input, trigger):
        """Process a non-sample opcode by passing through input opcodes.

        May hang onto opcodes from the input port if there are samples still
        waiting for triggers before they can be processed. Always discards any
        non-sample opcode from the trigger port.

        Args:
            opcode (str): Opcode name of this non-sample message.
            input (any/None): Input port data.
            trigger (any/None): Trigger port data.
        """
        if input is not None and input is not False:
            if opcode == "flush" or opcode == "discontinuity":
                # Replace boolean flag with None for these ZLM opcodes.
                input = None
            if len(self.input_data):
                # Received a non-sample opcode - whilst still processing
                # previous opcodes (waiting for trigger samples).
                # Hold onto input opcode to process again later.
                logging.debug(f"Delaying a {opcode} message.")
                self.input_data.append({"opcode": opcode, "data": input})
            else:
                logging.debug(f"Received a {opcode} message.")
                self.messages.append({"opcode": opcode, "data": input})

        if trigger is not None and trigger is not False:
            logging.debug(f"Ignoring a {opcode} from the trigger port.")

    def end_of_files(self):
        """Generate any additional messages when all input has ended.

        Returns:
            Messages to output (may be empty)
        """
        logging.debug(
            f"end_of_files: len(self.input_data)={len(self.input_data)}")

        # Add any output samples not yet sent
        if len(self.output_samples) > 0:
            logging.debug(f"Inserting {len(self.output_samples)}"
                          " samples from output_samples.")
            self.messages.append({"opcode": "sample",
                                  "data": self.output_samples})
            self.output_samples = []

        # Add any left over opcodes, without trigger input
        while len(self.input_data) > 0:
            self.messages.append({"opcode": self.input_data[0]["opcode"],
                                  "data": self.input_data[0]["data"]})
            self.input_data = self.input_data[1:]

        logging.debug(self.messages)

        return self._format_messages()
