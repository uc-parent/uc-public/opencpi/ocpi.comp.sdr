.. multiplexer_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _multiplexer_l:


Multiplexer (``multiplexer_l``)
===============================
Uses either a port or property to control which input passes through data to the output.

Design
------
Passes data through from a selected input to the output. The selected input is specified either by the ``port_select`` property or by a sample received on ``inselect`` port.

When ``port_select_enable`` is ``true``, the ``inselect`` port sets whether ``input_1`` or ``input_2`` is passed through. If ``false``, the property ``port_select`` sets which input is passed through.

A value of ``false`` for property ``port_select`` or port ``inselect`` results in ``input_1`` being passed through, otherwise ``input_2`` is passed through.

Data on ``input_1`` and ``input_2`` is taken at the output sample rate. Data on ``inselect`` is taken as soon as it is available.

The last received sample value on the ``inselect`` port is stored and used thereafter for all further processing until another sample is received. All non-sample opcodes passed to the ``inselect`` port are discarded. An entire message on the selected input is passed through for each ``inselect`` sample value. If a message exists on the non-selected port it is taken but not passed through.

Switching between inputs only occurs between messages on the selected input. If a switch occurs and the port that is switched to is mid-message, the output advances no data until the entire message on that port has been taken.

A block diagram representation of the implementation is given in :numref:`multiplexer_ul-diagram`.

.. _multiplexer_l-diagram:

.. figure:: multiplexer_l.svg
   :alt: Block diagram outlining multiplexer implementation.
   :align: center

   Multiplexer implementation.

Interface
---------
.. literalinclude:: ../specs/multiplexer_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input_1: Input samples port.
   input_2: Input samples port.
   output: Primary output samples port.
   inselect: Optional input select port.

Opcode Handling
~~~~~~~~~~~~~~~
The multiplexer function is applied on all data, regardless of opcode type.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../multiplexer_l.hdl ../multiplexer_l.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
There is a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``multiplexer_l`` are:

 * Setting property ``port_select_enable`` to ``true`` is untested.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
