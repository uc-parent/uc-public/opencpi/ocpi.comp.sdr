-- HDL implementation of metadata_inserter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

library ocpi;
use ocpi.types.all;

library sdr_interface;
use sdr_interface.sdr_interface.metadata_inserter_v2;

library sdr_util;
use sdr_util.sdr_util.opcode_t;
use sdr_util.sdr_util.slv_to_opcode;
use sdr_util.sdr_util.opcode_to_slv;

architecture rtl of worker is

  signal metadata_id               : unsigned(props_in.metadata_id'range);
  signal metadata_value            : unsigned(props_in.metadata_value'range);
  signal trigger                   : std_logic;
  signal trigger_r                 : std_logic;
  signal input_take                : std_logic;
  signal input_valid               : std_logic;
  signal input_ready               : std_logic;
  signal output_valid              : std_logic;
  signal output_give               : std_logic;
  signal message_complete          : std_logic;
  signal trigger_out_take          : std_logic;
  signal insert_metadata_message_i : std_logic;

begin

  -----------------------------------------------------------------------------
  -- Detect the two possible trigger conditions
  -----------------------------------------------------------------------------
  detect_trigger_p : process(input_take, input_in.ready,
                             props_in.insert_metadata_message_written,
                             props_in.insert_metadata_message,
                             trigger_in.data(0), trigger_in.opcode,
                             trigger_r, trigger_out_take)
  begin
    -- Default the trigger state
    trigger <= '0';

    -- Trigger from trigger port
    if trigger_out_take = '1' and (trigger_in.opcode = bool_timed_sample_sample_op_e and trigger_in.data(0) = '1') then
      trigger <= '1';

    -- Trigger from properties
    elsif input_take = '1' and input_in.ready = '1' then
      if trigger_r = '1' then
        trigger <= '1';
      elsif props_in.insert_metadata_message_written = '1' then
        trigger <= props_in.insert_metadata_message;
      end if;
    end if;
  end process;

  catch_triggers_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then

      -- Clear captured trigger
      if ctl_in.reset = '1' or trigger = '1' then
        trigger_r <= '0';

      -- Capture trigger from properties.
      -- NOTE: this will be cleared if capture isn't neccessary.
      elsif props_in.insert_metadata_message_written = '1' then
        trigger_r <= props_in.insert_metadata_message;
      end if;
    end if;
  end process;


  -----------------------------------------------------------------------------
  -- Load and increment the meta value
  -----------------------------------------------------------------------------
  manage_metavalue_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      -- Sample the metadata value when in reset or
      -- when the value has been updated
      if ctl_in.reset = '1' or props_in.metadata_value_written = '1' then
        metadata_value <= props_in.metadata_value;
      -- When auto incrementing the metavalue, increment the local version
      -- after the message is complete
      elsif props_in.increment_value = '1' and message_complete = '1' then
        metadata_value <= metadata_value + 1;
      end if;
    end if;
  end process;

  insert_metadata_message_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        insert_metadata_message_i <= '0';
      else
        -- high when message waiting to be inserted
        if trigger = '1' then
          insert_metadata_message_i <= '1';
        end if;
        -- clear after message finished
        if message_complete = '1' then
          insert_metadata_message_i <= '0';
        end if;
      end if;
    end if;
  end process;

  metadata_id                       <= props_in.metadata_id;
  -- Feed back the current metadata value
  props_out.metadata_value          <= metadata_value;
  -- High when metadata message is being inserted
  props_out.insert_metadata_message <= trigger or insert_metadata_message_i;

  -----------------------------------------------------------------------------
  -- Instantiate the protocol independent primitive to do the work
  -----------------------------------------------------------------------------
  meta_inserter_i : metadata_inserter_v2
    generic map (
      data_width_g        => input_in.data'length,
      opcode_width_g      => input_in.opcode'length,
      byte_enable_width_g => input_in.byte_enable'length,
      metadata_opcode_g   => opcode_to_slv(metadata_op_e, input_in.opcode'length),
      data_opcode_g       => opcode_to_slv(sample_op_e, input_in.opcode'length)
      )
    port map (
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      metadata_id        => std_logic_vector(metadata_id),
      metadata_value     => std_logic_vector(metadata_value),
      trigger            => trigger,
      message_complete   => message_complete,
      take_in            => output_in.ready,
      take_out           => input_take,
      input_data         => input_in.data,
      input_byte_enable  => input_in.byte_enable,
      input_opcode       => input_in.opcode,
      input_som          => input_in.som,
      input_eom          => input_in.eom,
      input_eof          => input_in.eof,
      input_valid        => input_valid,
      input_ready        => input_ready,
      output_data        => output_out.data,
      output_byte_enable => output_out.byte_enable,
      output_opcode      => output_out.opcode,
      output_eom         => output_out.eom,
      output_som         => output_out.som,
      output_eof         => output_out.eof,
      output_valid       => output_valid,
      output_give        => output_give
      );

  protocol_control_p : process(trigger_in.is_connected, trigger_in.ready,
                               trigger_in.eof, trigger_in.valid,
                               input_take, input_in.ready,
                               input_in.valid, output_valid,
                               output_give, input_in.opcode,
                               trigger_in.opcode)
  begin
    if trigger_in.is_connected = '1' then
      output_out.valid <= output_valid;
      output_out.give  <= output_give;

      -- Trigger and input port streams are not consumed at the same rate.
      -- Rate is dependent on opcode:

      --    TRIG        INPUT           STREAM CONTROL
      --    ---------------------------------------------------------------
      --    sample      sample          Both streams consumed at same rate.

      --    sample      non-sample      Trigger stream blocked;
      --                                input stream consumed.

      --    non-sample  sample          Trigger stream consumed;
      --                                input stream blocked.

      --    non-sample  non-sample      Both streams consumed.

      if input_take = '1' and input_in.ready = '1' and (trigger_in.eof = '1' or (trigger_in.ready = '1'
                                                                                 and (trigger_in.opcode = bool_timed_sample_sample_op_e or slv_to_opcode(input_in.opcode) /= sample_op_e))) then
        input_out.take <= '1';
      else
        input_out.take <= '0';
      end if;

      if input_in.ready = '1' and (trigger_in.eof = '1' or ((trigger_in.valid = '1' or trigger_in.ready = '1')
                                                            and (trigger_in.opcode = bool_timed_sample_sample_op_e or slv_to_opcode(input_in.opcode) /= sample_op_e))) then
        input_ready <= '1';
      else
        input_ready <= '0';
      end if;

      if input_in.valid = '1' and (trigger_in.eof = '1' or ((trigger_in.valid = '1' or trigger_in.ready = '1')
                                                            and (trigger_in.opcode = bool_timed_sample_sample_op_e or slv_to_opcode(input_in.opcode) /= sample_op_e))) then
        input_valid <= '1';
      else
        input_valid <= '0';
      end if;

      -- Trigger port control: Data consumed when input port is active.
      if input_take = '1' and input_in.ready = '1' and trigger_in.ready = '1'
        and (trigger_in.opcode /= bool_timed_sample_sample_op_e or slv_to_opcode(input_in.opcode) = sample_op_e) then
        trigger_out_take <= '1';
      else
        trigger_out_take <= '0';
      end if;

    else
      -- The input and trigger streams are consumed at the same rate,
      input_out.take   <= input_take;
      output_out.valid <= output_valid;
      output_out.give  <= output_give;
      trigger_out_take <= '0';
      input_ready      <= input_in.ready;
      input_valid      <= input_in.valid;
    end if;
  end process;

  trigger_out.take <= trigger_out_take;

end rtl;
