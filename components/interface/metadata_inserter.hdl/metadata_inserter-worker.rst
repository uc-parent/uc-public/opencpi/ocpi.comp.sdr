.. metadata_inserter HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _metadata_inserter-hdl-worker:


``metadata_inserter`` HDL Worker
================================

Detail
------
The :ref:`Metadata inserter primitive v2 <metadata_inserter_v2-primitive>`
implements the core functionality for all HDL workers. The workers then provide
data type conversion and interface handling for the trigger port.

The HDL worker input and output ports have data widths matching the underlying
protocol to provide throughput up to 1 sample per cycle.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilization::
