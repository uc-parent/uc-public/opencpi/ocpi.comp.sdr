.. message_length_adjuster RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _message_length_adjuster-RCC-worker:


``message_length_adjuster`` RCC Worker
======================================

Detail
------
.. ocpi_documentation_worker::

Limitations
-----------

Any value of ``timeout`` between the range :math:`0` and :math:`4295` will be set to the minimum possible time out in an RCC worker, which is 1 microsecond.

This is due to the way RCC workers in OpenCPI work, any value set in the ``timeout`` property will be rounded to the nearest microsecond. Therefore, any value of ``timeout`` that is less than 4295 would become 0 when calculating the fractional second value.
