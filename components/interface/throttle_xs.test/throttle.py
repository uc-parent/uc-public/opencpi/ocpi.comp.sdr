#!/usr/bin/env python3

# Python implementation of throttle for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of throttle for verification."""

import opencpi.ocpi_testing as ocpi_testing


class Throttle(ocpi_testing.Implementation):
    """Python implementation of throttle for verification."""

    def __init__(self, step_size, enable, zero_pad_when_not_ready):
        """Initialise the Throttle class.

        Args:
            step_size (int): Step size of the throttle phase accumulator.
            enable (bool): Enables or disables the throttle.
            zero_pad_when_not_ready (bool): When enabled the component will
                           enforce the output data rate by padding the output
                           with zeros when there is no valid input data.
        """
        super().__init__(step_size=step_size, enable=enable,
                         zero_pad_when_not_ready=zero_pad_when_not_ready)

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle incoming Sample opcode.

        Args:
            values (list): Incoming sample values from the input port.

        Returns:
            Formatted output opcode messages.
        """
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])
