.. delay documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _delay:


Delay (``delay``)
=================
Uses an internal delay line to delay incoming samples of a variable width bus by a specified number of output samples.

Design
------
The worker has a ``latency`` property, that is a build time parameter, that sets the number of output samples that should occur before the data makes it through an internal delay line between the input and output. For example, if the ``latency`` property is set to 8 and in periodic intervals the output is ready, 8 intervals will pass before an input sample makes it to the output.


Interface
---------
.. literalinclude:: ../specs/delay-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
All opcodes are passed through this component without modification.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.


Implementations
---------------
.. ocpi_documentation_implementations:: ../delay.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

* :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``delay`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
