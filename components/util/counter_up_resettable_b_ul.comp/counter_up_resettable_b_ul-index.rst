.. counter_up_resettable_b_ul documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _counter_up_resettable_b_ul:


Counter Up Resettable (``counter_up_resettable_b_ul``)
======================================================
Resettable counter for input samples.

Design
------
Counts the number of successive input samples that are 0. The count is set to ``reset_count`` when a 1 is received, otherwise it increments.
If the count value exceeds ``period`` it will stop or roll over depending on property value ``auto reset``.
The count value is output on the ``output`` port whenever an input sample is received.
A 1 is output on ``outtrigger`` whenever count is equal to ``period`` - 1, otherwise a 0  is output.
A volatile property ``count`` allows for the count value to be both set and read during run time.

The truth tables below demonstrate the behaviour of the counter under different configurations:

  * ``auto_reset`` set to 1, ``period`` set to 4, ``reset_count`` set to 0

+-----------+------------+----------------+
| ``input`` | ``output`` | ``outtrigger`` |
+===========+============+================+
| | RESET   | | RESET    | | RESET        |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 0       | | 3        | | 1            |
| | 0       | | 0        | | 0            |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 1       | | 0        | | 0            |
| | 0       | | 1        | | 0            |
+-----------+------------+----------------+

  * ``auto_reset`` set to 1, ``period`` set to 4, ``reset_count`` set to 2

+-----------+------------+----------------+
| ``input`` | ``output`` | ``outtrigger`` |
+============+============+===============+
| | RESET   | | RESET    | | RESET        |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 0       | | 3        | | 1            |
| | 0       | | 0        | | 0            |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 1       | | 2        | | 0            |
| | 0       | | 3        | | 0            |
+-----------+------------+----------------+

  * ``auto_reset`` set to 0, ``period`` set to 4, ``reset_count`` set to 0

+-----------+------------+----------------+
| ``input`` | ``output`` | ``outtrigger`` |
+===========+============+================+
| | RESET   | | RESET    | | RESET        |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 1       | | 0        | | 0            |
| | 0       | | 1        | | 0            |
+-----------+------------+----------------+

  * ``auto_reset`` set to 0, ``period`` set to 4, ``reset_count`` set to 512

+-----------+------------+----------------+
| ``input`` | ``output`` | ``outtrigger`` |
+===========+============+================+
| | RESET   | | RESET    | | RESET        |
| | 0       | | 1        | | 0            |
| | 0       | | 2        | | 0            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 0       | | 3        | | 1            |
| | 1       | | 512      | | 1            |
| | 0       | | 512      | | 1            |
+-----------+------------+----------------+

Note that the HDL implementation of this counter is able to process samples at the FPGA clock speed. Connected in this way it can function as both a timer and watchdog timer for an application.

Interface
---------
.. literalinclude:: ../specs/counter_up_resettable_b_ul-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input port.
   output: Optional output count port.
   outtrigger: Optional output trigger port.

Opcode Handling
~~~~~~~~~~~~~~~
Only values within a sample opcode message are counted.

Discontinuity and flush opcode messages reset the counter to zero. All other opcodes pass through this component without any effect.
Which output forwards non-sample messages is determined by the parameter ``nonsample_output_select``.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../counter_up_resettable_b_ul.hdl ../counter_up_resettable_b_ul.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Counter up-down primitive <counter_updown-primitive>`

 * :ref:`Protocol interface delay (narrow to wide) primitive v2 <narrow_to_wide_protocol_interface_delay_v2-primitive>`

 * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``counter_up_resettable_b_ul`` are:

 * Testing can only be done with all inputs and outputs connected. This means that this component has not been tested with any of the optional outputs disconnected.

 * To use the maximum value of ``size`` it must be set to 0.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
