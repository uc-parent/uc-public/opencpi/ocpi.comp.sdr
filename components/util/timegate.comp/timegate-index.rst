.. timegate documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _timegate:


Timegate (``timegate``)
=======================
Timed transmit of data using the timed sample protocol.

Design
------
If the ``bypass`` property is set to ``True``, sample messages are transferred from the input to the output port. Alternatively, when set to ``False`` the transfer of sample messages is determined based on the time.

Until a Time message is received, the component remains in its default state where timed samples on the input are transferred to the output port without delay. Once a Time message is received on the input port, the Time will be stored. If the ``gate_all_opcodes`` property is set to ``True`` the next received Sample, Sample Interval, Flush or Metadata message will cause the gate to close, otherwise when set to ``False`` only a sample message will cause the gate to close. Once closed no further input messages will be consumed on the input port, until the current time matches or exceeds the stored (gated) time minus the ``time_correction`` property. This will then result in the timed samples being transferred to the output port.

The :math:`\text{time}_\text{current} - (\text{time}_\text{stored} - \texttt{time_correction})` will be captured when the gate is opened and provided as the ``actual_time_to_requested_time_delta`` (Q32.32) property. If the current time already exceeds the stored (gated) time minus the ``time_correction`` property when the subsequent message is received, the ``late_time_sticky`` property will be set ``True`` and the ``actual_time_to_requested_time_delta`` will be non-zero. The ``late_time_sticky`` will remain ``True`` until the application writes a ``True`` to clear it.

A Discontinuity message will return the Timegate to its default state where timed samples on the input are transferred to the output port without delay. If this message is queued behind a Sample message then it would not be actioned until the time is reached and the preceding samples are transferred through the Timegate.

To determine the time to open the gate, the  ``time_correction`` (Q0.32) is deducted from the timestamp (Q32.64) and, with the lower 32 bits ignored, compared against the current time (Q32.32). Note, the ARM Q notation has been used, where the sign bit is included in the stated integer bits.

Interface
---------
.. literalinclude:: ../specs/timegate-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
Refer to the `design`_ section for detailed behaviour on opcode handling.


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   leap_seconds: The default value has been set to ``0`` to enforce the setting to the latest published leap second value. At the end of 2016, the number of leap seconds was incremented to ``18``. This value is only required for the RCC worker since the time service will provide GPS time for the HDL worker.


Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.


Implementations
---------------
.. ocpi_documentation_implementations:: ../timegate.hdl ../timegate.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * HDL OpenCPI dependencies:

   * ``ocpi.wci.control_op_t``

   * ``ocpi.util.max``

   * ``sdr_util.sdr_util.slv_to_opcode``

   * ``sdr_util.sdr_util.opcode_t``

   * :ref:`Clock Domain Crossing - Fast pulse to slow sticky primitive <cdc_fast_pulse_to_slow_sticky-primitive>`.

   * :ref:`Clock Domain Crossing - Single bit primitive <cdc_single_bit-primitive>`.

   * :ref:`Clock Domain Crossing - Bits feedback primitive <cdc_bits_feedback-primitive>`.

   * :ref:`Clock Domain Crossing - First In First Out memory primitive <cdc_fifo-primitive>`.

 * RCC OpenCPI dependencies:

   * ``components/math/common/time_utils.hh``

   * ``components/util/common/opcodes.hh``

There is also a dependency on:

 * HDL specific dependencies:

   * ``ieee.std_logic_1164``

   * ``ieee.numeric_std``

   * ``ieee.math_real``

 * RCC specific dependencies:

   * ``<cmath>``

   * ``<cstdint>``

   * ``<chrono>``


Limitations
-----------
Limitations of ``timegate`` are:

 * The time client only provides 32-bits for the fractional part whereas the timed sample protocol supports 64-bits.

 * Due to the way the RCC generates the GPS time from UTC time, there is a ``leap_seconds`` property for users to manually set this offset value.

 * All unit test cases are implemented to validate the timegate component, however whilst running virtually and with the limitations of the framework the ``bypass`` property is set to ``True`` rendering the component to be non-gated, allowing all data and opcodes to pass from input to output.

 * Tested on a PlutoSDR, data was gated as expected and the properties ``actual_time_to_requested_time_delta`` and ``late_time_sticky`` returned sensible results.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
