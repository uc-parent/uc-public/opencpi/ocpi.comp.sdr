-- counter_up_resettable_b_ul HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_util;
use sdr_util.sdr_util.counter_updown;

architecture rtl of worker is

  constant delay_c : integer := 1;

  signal output_ready     : std_logic;
  signal outtrigger_ready : std_logic;
  signal out_ready        : std_logic;
  signal flush            : std_logic;
  signal discontinuity    : std_logic;
  signal valid            : std_logic;
  signal input_bit        : std_logic;
  signal trigger_out      : std_logic;
  signal trigger_slv      : std_logic_vector(7 downto 0);

  signal counter_enable : std_logic;
  signal counter_reset  : std_logic;
  signal load_trigger   : std_logic;
  signal load_value     : unsigned(output_out.data'length - 1 downto 0);
  signal counter_value  : unsigned(output_out.data'length - 1 downto 0);

  signal output_delayed     : worker_output_out_t;
  signal outtrigger_delayed : worker_outtrigger_out_t;

begin

  -- Take data when both outputs are ready or not connected
  input_out.take <= out_ready;

  -- Enable output when either output is ready or not connected.
  output_ready     <= output_in.ready or output_in.reset;
  outtrigger_ready <= outtrigger_in.ready or outtrigger_in.reset;
  out_ready        <= output_ready and outtrigger_ready;

  -- Reset counter
  flush <= '1' when input_in.ready = '1' and out_ready = '1'
           and input_in.opcode = bool_timed_sample_flush_op_e else '0';
  discontinuity <= '1' when input_in.ready = '1' and out_ready = '1'
                   and input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';
  counter_reset <= ctl_in.reset or flush or discontinuity;

  -- Only enable counter when data is valid
  valid <= '1' when out_ready = '1' and input_in.valid = '1'
           and props_in.enable = '1'
           and input_in.opcode = bool_timed_sample_sample_op_e
           else '0';

  -- Data bit is LSB of input data interface
  input_bit <= input_in.data(0);

  load_trigger <= '1' when props_in.count_written = '1' or (valid = '1' and input_bit = '1')
                  else '0';

  load_value <= props_in.count when props_in.count_written = '1' else props_in.reset_count;

  counter_enable <= '1' when valid = '1' and not (props_in.auto_reset = '0' and trigger_out = '1')
                    else '0';

  -- Signal when counter has reached upper limit
  trigger_out <= '1' when counter_value >= props_in.period - 1 else '0';

  -- Instantiate updown counter
  counter_updown_i : counter_updown
    generic map (
      counter_size_g => output_out.data'length
      )
    port map (
      clk        => ctl_in.clk,
      reset      => counter_reset,
      enable     => counter_enable,
      direction  => '1',
      load_trig  => load_trigger,
      load_value => load_value,
      length     => props_in.period,
      count_out  => counter_value
      );

  props_out.count <= counter_value;

  -- Interface delay module for output
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_counter_i : entity work.bool_to_ulong_protocol_delay
    generic map (
      delay_g          => delay_c,
      data_in_width_g  => input_in.data'length,
      data_out_width_g => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => outtrigger_ready,
      input_in            => input_in,
      processed_stream_in => std_logic_vector(counter_value),
      output_out          => output_delayed
      );

  trigger_slv <= (0 => trigger_out, others => '0');

  -- Interface delay module for outtrigger
  interface_delay_trigger_i : entity work.bool_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => outtrigger_in.ready,
      take_in             => output_ready,
      input_in            => input_in,
      processed_stream_in => trigger_slv,
      output_out          => outtrigger_delayed
      );

-- For the generic nonsample_output_select:
-- When = 0 opcodes other than sample are sent through the output port
-- When = 1 opcodes other than sample are sent through the outtrigger port
  outtrigger_out.give <= '0' when (nonsample_output_select = '0'
                                   and outtrigger_delayed.opcode /= bool_timed_sample_sample_op_e)
                         else outtrigger_delayed.give;
  outtrigger_out.valid <= '0' when (nonsample_output_select = '0'
                                    and outtrigger_delayed.opcode /= bool_timed_sample_sample_op_e)
                          else outtrigger_delayed.valid;
  outtrigger_out.opcode <= bool_timed_sample_sample_op_e when nonsample_output_select = '0'
                           else outtrigger_delayed.opcode;

  outtrigger_out.eom         <= outtrigger_delayed.eom;
  outtrigger_out.byte_enable <= outtrigger_delayed.byte_enable;
  outtrigger_out.data        <= outtrigger_delayed.data;


  output_out.give <= '0' when (nonsample_output_select = '1'
                               and output_delayed.opcode /= ulong_timed_sample_sample_op_e)
                     else output_delayed.give;
  output_out.valid <= '0' when (nonsample_output_select = '1'
                                and output_delayed.opcode /= ulong_timed_sample_sample_op_e)
                      else output_delayed.valid;
  output_out.opcode <= ulong_timed_sample_sample_op_e when nonsample_output_select = '1'
                       else output_delayed.opcode;

  output_out.eom         <= output_delayed.eom;
  output_out.byte_enable <= output_delayed.byte_enable;
  output_out.data        <= output_delayed.data;

end rtl;
