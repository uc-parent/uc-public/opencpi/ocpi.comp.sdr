.. triggered_sampler_b HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _triggered_sampler_b-HDL-worker:


``triggered_sampler_b`` HDL Worker
==================================
The Triggered Sampler primitive implements the functionality for all HDL workers. The workers provide only a very thin data type conversion layer. Because there is no sample data processing, this single primitive can be used for all protocol variants.

The HDL worker input and output ports have data widths matching the underlying protocol to provide throughput up to 1 sample per cycle.

Detail
------
.. ocpi_documentation_worker::

Data Flow
^^^^^^^^^
The input .take signal is driven from output .ready, regardless of whether the messages on input are being forwarded (in a capture) or discarded (outside a capture). So, even outside a capture, the input is drained no quicker than output "could" accept the data.

Captures are triggered on the detection of a SOM at the trigger port.

The trigger port is drained at the same rate as the input port (i.e. the trigger .take signal is asserted at the same time as the input .take signal).

Utilisation
-----------
.. ocpi_documentation_utilization::
