// Timegate RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>
#include <cmath>
#include <cstdint>

#include "../../math/common/time_utils.hh"
#include "../common/opcodes.hh"
#include "timegate-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace TimegateWorkerTypes;

class TimegateWorker : public TimegateWorkerBase {
  // Properties
  RCCBoolean bypass{false};
  RCCBoolean gate_all_opcodes{false};
  std::chrono::seconds leap_seconds{0};
  const uint32_t time_correction_seconds{0};
  uint64_t time_correction_fraction{0};
  uint64_t actual_time_to_requested_time_delta{0};
  RCCBoolean late_time_sticky{false};

  // Internal state
  uint32_t current_time_seconds{0};
  uint64_t current_time_fraction{0};
  RCCBoolean sample_time_received{false};
  uint32_t gate_time_seconds{0};
  uint64_t gate_time_fraction{0};

#if __cplusplus < 202002L
  // gps time differs from UTC (system time) by:
  // epoch 00:00:00 6 January 1980 (3657 days = (10x365 day years) + (2
  // leap days) + (1st->6th Jan)):
  const std::chrono::seconds gps_epoch =
      std::chrono::seconds(3657L * 24L * 60L * 60L);
#endif

  // The chrono library gives time in standard (base 10) subdivisions of
  // seconds,
  // the smallest being nanoseconds. This needs to be converted to Q32.64.
  // The nanosecond to 64bit binary conversion is 2^64 / 10^9 = 18446744073.
  const uint64_t nano_to_64bit = 18446744073u;

  RCCResult bypass_written() {
    // Set the bypass flag.
    this->bypass = properties().bypass;
    return RCC_OK;
  }

  RCCResult leap_seconds_written() {
    // Set the leap seconds value.
    this->leap_seconds = std::chrono::seconds(properties().leap_seconds);
    return RCC_OK;
  }

  RCCResult time_correction_written() {
    this->time_correction_fraction = properties().time_correction;
    // The time_correction is defined as a 32 bit property
    // therefore shift the value to the top 32 most significant bits,
    // in preparation for subtracting from gate_time_fraction.
    this->time_correction_fraction <<= 32;
    return RCC_OK;
  }

  RCCResult late_time_sticky_written() {
    // Clear the late_time flag.
    if (properties().late_time_sticky) {
      this->late_time_sticky = false;
    }
    return RCC_OK;
  }

  // notification that late_time_sticky property will be read
  RCCResult late_time_sticky_read() {
    properties().late_time_sticky = this->late_time_sticky;
    return RCC_OK;
  }

  // notification that actual_time_to_requested_time_delta property will be read
  RCCResult actual_time_to_requested_time_delta_read() {
    properties().actual_time_to_requested_time_delta =
        this->actual_time_to_requested_time_delta;
    return RCC_OK;
  }

  // Update current_time from system clock.
  void update_current_time() {
#if __cplusplus >= 202002L
    auto time_since_epoch = std::chrono::gps_clock::now().time_since_epoch();
#else
    auto time_since_epoch =
        std::chrono::high_resolution_clock::now().time_since_epoch() -
        this->leap_seconds - gps_epoch;
#endif
    this->current_time_seconds =
        std::chrono::duration_cast<std::chrono::seconds>(time_since_epoch)
            .count();
    time_since_epoch -= std::chrono::seconds(this->current_time_seconds);
    this->current_time_fraction =
        std::chrono::duration_cast<std::chrono::nanoseconds>(time_since_epoch)
            .count() *
        nano_to_64bit;
  }

  // Test if time1 is less than time2.
  RCCBoolean less_than(uint32_t seconds1, uint64_t fraction1, uint32_t seconds2,
                       uint64_t fraction2) {
    if (seconds1 == seconds2) {
      return fraction1 < fraction2;
    }
    return seconds1 < seconds2;
  }

  // Calculate time difference (actual - adjusted received).
  // If no received time then leave delta at zero.
  void update_actual_time_to_requested_time_delta() {
    if (this->sample_time_received) {
      uint32_t delta_seconds{this->current_time_seconds};
      uint64_t delta_fraction{this->current_time_fraction};
      time_utils::subtract(&delta_seconds, &delta_fraction,
                           this->gate_time_seconds, this->gate_time_fraction);
      this->actual_time_to_requested_time_delta =
          (uint64_t(delta_seconds) << 32) | (delta_fraction >> 32);
    }
  }

  RCCResult run(bool) {
    // Pass everything through the component if gating function is disabled.
    if (this->bypass) {
      return forward_opcode(input, output);
    }

    if (input.opCode() == Timed_sampleTime_OPERATION) {
      uint8_t* input_data = input.data();

      this->gate_time_fraction = *reinterpret_cast<uint64_t*>(input_data);
      input_data += sizeof(uint64_t);
      this->gate_time_seconds = *reinterpret_cast<uint32_t*>(input_data);

      time_utils::subtract(&this->gate_time_seconds, &this->gate_time_fraction,
                           this->time_correction_seconds,
                           this->time_correction_fraction);
      this->sample_time_received = true;
      // Set late flag if current time is after gate time.
      this->update_current_time();
      if (less_than(this->gate_time_seconds, this->gate_time_fraction,
                    this->current_time_seconds, this->current_time_fraction)) {
        this->late_time_sticky = true;
      }
      return forward_opcode(input, output);
    } else if (input.opCode() == Timed_sampleDiscontinuity_OPERATION) {
      this->sample_time_received = false;
      this->late_time_sticky = false;
      this->actual_time_to_requested_time_delta = 0;
      return forward_opcode(input, output);
    } else {
      // Forward samples until a timegate can be performed.
      if (!this->sample_time_received) {
        return forward_opcode(input, output);
      }

      if (this->gate_all_opcodes ||
          input.opCode() == Timed_sampleSample_OPERATION) {
        // Hold if current time is before gate time.
        this->update_current_time();
        if (less_than(this->current_time_seconds, this->current_time_fraction,
                      this->gate_time_seconds, this->gate_time_fraction)) {
          return RCC_OK;
        }
        this->update_actual_time_to_requested_time_delta();
        // No need to do time checks now target time is reached.
        this->sample_time_received = false;
      }
      return forward_opcode(input, output);
    }
  }
};

TIMEGATE_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
TIMEGATE_END_INFO
