-- HDL Implementation of timegate.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.wci.control_op_t;
use ocpi.util.max;

library sdr_util;
use sdr_util.sdr_util.slv_to_opcode;
use sdr_util.sdr_util.opcode_t;
use sdr_util.sdr_util.cdc_single_bit;
use sdr_util.sdr_util.cdc_fast_pulse_to_slow_sticky;
use sdr_util.sdr_util.cdc_bits_feedback;
use sdr_util.sdr_util.cdc_fifo;

architecture rtl of worker is
  constant time_width_c : natural := 96;
  constant nchunks_c    : natural := natural(ceil(real(max(input_in.data'length, time_width_c))/real(input_in.data'length)));
  -- The time gate state machine
  type state_t is (open_s,              -- gate is open, samples are flowing
                   time_coming_s,  -- time is coming in chunks, not yet complete
                   time_waiting_s,      -- time has arrived and we are waiting
                   error_s);  -- things are broken, protocol is erroneous, we're stuck
  signal current_state                   : state_t;
  -- The fifo from input port (and clock) to output port (and clock)
  constant fifo_index_eof_c              : natural := 6+input_in.data'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_eom_c              : natural := 5+input_in.data'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_som_c              : natural := 4+input_in.data'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_valid_c            : natural := 3+input_in.data'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_byte_enable_high_c : natural := 2+input_in.data'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_byte_enable_low_c  : natural := 2+input_in.data'high+input_in.opcode'high;
  constant fifo_index_opcode_high_c      : natural := 1+input_in.data'high+input_in.opcode'high;
  constant fifo_index_opcode_low_c       : natural := 1+input_in.data'high;
  constant fifo_index_data_high_c        : natural := 0+input_in.data'high;
  constant fifo_index_data_low_c         : natural := 0;
  signal fifo_in                         : std_logic_vector(fifo_index_eof_c downto fifo_index_data_low_c);
  signal fifo_out                        : std_logic_vector(fifo_index_eof_c downto fifo_index_data_low_c);
  signal fifo_enqueue                    : bool_t;
  signal fifo_dequeue                    : bool_t;
  signal fifo_empty_n                    : bool_t;
  signal fifo_full_n                     : bool_t;
  signal fifo_out_is_eof                 : bool_t;
  signal fifo_out_is_eom                 : bool_t;
  signal fifo_out_is_som                 : bool_t;
  signal fifo_out_is_valid               : bool_t;
  signal fifo_out_byte_enable            : std_logic_vector(input_in.byte_enable'range);
  signal fifo_out_opcode                 : std_logic_vector(input_in.opcode'range);
  signal fifo_out_data                   : std_logic_vector(input_in.data'range);
  signal fifo_out_is_time                : bool_t;
  signal fifo_out_is_discontinuity       : bool_t;
  signal fifo_out_is_sample              : bool_t;
  -- Timekeeping
  -- Mostly reading time in chunks if data port is narrower than time
  signal time_chunk_index                : natural;
  signal time_shift_register             : unsigned((nchunks_c*input_in.data'length)-1 downto 0);
  signal time_to_transmit                : unsigned(time_width_c-1 downto 0);  -- the corrected time when transmit should be enabled
  signal time_now                        : ulonglong_t;
  -- output port signals
  signal output_out_som                  : bool_t;
  signal output_out_eom                  : bool_t;
  signal output_out_valid                : bool_t;
  signal output_out_give                 : bool_t;
  -- status signals
  signal error                           : bool_t;
  signal time_late                       : bool_t;  -- timestamp arrived after its time
  signal clr_late_time_sticky            : bool_t;
  signal time_delta                      : ulonglong_t;
  signal time_delta_update               : std_logic;
  signal time_delta_update_ready         : std_logic;
  signal time_correction_ready           : std_logic;
  -- Resynchronised signals
  signal bypass_cdc_output               : std_logic;
  signal gate_all_opcodes_cdc_output     : std_logic;
  signal time_correction_cdc_output      : std_logic_vector(props_in.time_correction'range);
  signal time_delta_cdc_output           : std_logic_vector(time_delta'range);
  signal time_delta_transfer             : std_logic;
  signal time_correction_transfer        : std_logic;

begin
  -----------------------------------------------------------------------------
  -- Control Clock Domain (ctl_in.clk)
  -----------------------------------------------------------------------------

  -- The output processing determines whether the stored time has passed before
  -- the gate is opened and strobes a signal. If this occurs, the
  -- cdc_fast_pulse_to_slow_sticky primitive is used to store the
  -- occurrence and pass it to the control clock domain, which is then cleared
  -- when the clr_late_time_sticky_written property is written.
  resync_timegate_time_late_i : cdc_fast_pulse_to_slow_sticky
    port map(
      -- fast clock domain
      fast_clk    => output_in.clk,
      fast_reset  => output_in.reset,
      fast_pulse  => time_late,         -- pulse to be detected
      -- slow clock domain
      slow_clk    => ctl_in.clk,
      slow_reset  => ctl_in.reset,
      slow_sticky => props_out.late_time_sticky,  -- clears sticky bit
      slow_clear  => clr_late_time_sticky);  -- sticky bit set when fast_pulse is high

  -- The Delta between the actual time transmitted to the requested timestamp
  -- is passed to the control clock domain using the cdc_bits_feedback
  -- primitive.
  resync_timegate_time_delta_i : cdc_bits_feedback
    generic map(
      width_g => time_delta'length)
    port map(
      src_clk    => output_in.clk,
      src_reset  => output_in.reset,
      src_enable => time_delta_transfer,
      src_ready  => time_delta_update_ready,
      src_in     => std_logic_vector(time_delta),
      dst_clk    => ctl_in.clk,
      dst_reset  => ctl_in.reset,
      dst_out    => time_delta_cdc_output);

  time_delta_transfer                           <= (time_delta_update and time_delta_update_ready);
  props_out.actual_time_to_requested_time_delta <= ulonglong_t(time_delta_cdc_output);

  -- Handles making time_late sticky and clearing the sticky bit if
  -- props_in.late_time_sticky is written true
  clr_late_time_sticky <= props_in.late_time_sticky_written and
                          props_in.late_time_sticky and
                          ctl_in.is_operating;

  -----------------------------------------------------------------------------
  -- Input Clock Domain (input_in.clk)
  -----------------------------------------------------------------------------

  -- The input control signals are used to enable the input data, opcode,
  -- byte_enable, valid, SOM, EOM and EOF into one side of a clock domain
  -- crossing FIFO on the input clock domain.
  input_out.take <= fifo_full_n and input_in.ready;

  -- Concatenate input signals into the CDC FIFO
  fifo_in(fifo_index_eof_c)                                                  <= input_in.eof;
  fifo_in(fifo_index_eom_c)                                                  <= input_in.eom;
  fifo_in(fifo_index_som_c)                                                  <= input_in.som;
  fifo_in(fifo_index_valid_c)                                                <= input_in.valid;
  fifo_in(fifo_index_byte_enable_high_c downto fifo_index_byte_enable_low_c) <= input_in.byte_enable;
  fifo_in(fifo_index_opcode_high_c downto fifo_index_opcode_low_c)           <= input_in.opcode;
  fifo_in(fifo_index_data_high_c downto fifo_index_data_low_c)               <= input_in.data;

  -- Enqueue signals into the CDC FIFO
  fifo_enqueue <= fifo_full_n and (input_in.ready or input_in.eof);

  -----------------------------------------------------------------------------
  -- Output Clock Domain (output_in.clk)
  -----------------------------------------------------------------------------

  -- The bypass property is passed to the output clock domain using
  -- the cdc_single_bit primitive.
  resync_timegate_bypass_i : cdc_single_bit
    generic map(
      dst_registers_g        => 2,  -- Range 2 - 10 synchroniser registers for increasing MTBF
      src_register_g         => '1',  -- 0=no, 1=yes input register for improved MTBF
      register_reset_level_g => '1')    -- 0=low, 1=high
    port map(
      src_clk    => ctl_in.clk,  -- optional; required when src_register_g='1'
      src_reset  => ctl_in.reset,  -- optional; required when src_register_g='1'
      src_enable => '1',         -- optional; required when src_register_g='1'
      src_in     => props_in.bypass,
      dst_clk    => output_in.clk,
      dst_reset  => output_in.reset,    -- optional; if not required, tie '0'
      dst_out    => bypass_cdc_output);

  -- The gate_all_opcodes property is passed to the output clock domain using
  -- the cdc_single_bit primitive.
  resync_timegate_gate_all_opcodes_i : cdc_single_bit
    generic map(
      dst_registers_g        => 2,  -- Range 2 - 10 synchroniser registers for increasing MTBF
      src_register_g         => '1',  -- 0=no, 1=yes input register for improved MTBF
      register_reset_level_g => '0')    -- 0=low, 1=high
    port map(
      src_clk    => ctl_in.clk,  -- optional; required when src_register_g='1'
      src_reset  => ctl_in.reset,  -- optional; required when src_register_g='1'
      src_enable => '1',         -- optional; required when src_register_g='1'
      src_in     => props_in.gate_all_opcodes,
      dst_clk    => output_in.clk,
      dst_reset  => output_in.reset,    -- optional; if not required, tie '0'
      dst_out    => gate_all_opcodes_cdc_output);

  -- The time_correction property is passed to the output clock domain using
  -- the cdc_bits_feedback primitive.
  resync_timegate_time_correction_i : cdc_bits_feedback
    generic map(
      width_g => props_in.time_correction'length)  -- : positive := 1);
    port map(
      src_clk    => ctl_in.clk,
      src_reset  => ctl_in.reset,
      src_enable => time_correction_transfer,
      src_ready  => time_correction_ready,
      src_in     => std_logic_vector(props_in.time_correction),
      dst_clk    => output_in.clk,
      dst_reset  => output_in.reset,
      dst_out    => time_correction_cdc_output);

  time_correction_transfer <= props_in.time_correction_written and time_correction_ready;

  -- The stored information is extracted from the FIFO and processed on the
  -- output clock domain utilising the time interface, which is also on the
  -- output clock domain.
  resync_timegate_input_fifo_i : cdc_fifo
    generic map(
      width_g => fifo_in'length,
      depth_g => to_integer(cdc_fifo_depth))
    port map(
      src_clk     => input_in.clk,
      src_reset   => input_in.reset,
      src_enqueue => fifo_enqueue,
      src_in      => fifo_in,
      src_full_n  => fifo_full_n,
      dst_clk     => output_in.clk,
      dst_reset   => output_in.reset,
      dst_dequeue => fifo_dequeue,
      dst_out     => fifo_out,
      dst_empty_n => fifo_empty_n);

  -- Time is in the output clock domain, configured in timegate.xml
  time_now <= time_in.seconds & time_in.fraction;

  -- Slice signals out of the CDC FIFO
  fifo_out_is_eof      <= to_bool(fifo_out(fifo_index_eof_c));
  fifo_out_is_eom      <= to_bool(fifo_out(fifo_index_eom_c));
  fifo_out_is_som      <= to_bool(fifo_out(fifo_index_som_c));
  fifo_out_is_valid    <= to_bool(fifo_out(fifo_index_valid_c));
  fifo_out_byte_enable <= fifo_out(fifo_index_byte_enable_high_c downto fifo_index_byte_enable_low_c);
  fifo_out_opcode      <= fifo_out(fifo_index_opcode_high_c downto fifo_index_opcode_low_c);
  fifo_out_data        <= fifo_out(fifo_index_data_high_c downto fifo_index_data_low_c);

  -- Decode opcodes
  fifo_out_is_sample        <= to_bool(slv_to_opcode(fifo_out_opcode) = sample_op_e);
  fifo_out_is_time          <= to_bool(slv_to_opcode(fifo_out_opcode) = time_op_e);
  fifo_out_is_discontinuity <= to_bool(slv_to_opcode(fifo_out_opcode) = discontinuity_op_e);

  -- Dequeue signals out of the CDC FIFO
  -- Messages will be transferred without delay unless they're queued up
  -- behind delayed message(s). Messages will not be queued if:
  --  The gate is open or the gate is closed and:
  --  Only gating sample opcodes and a non-sample opcode has been received
  --  Gating all but time and discontinuity and one of them has been received
  fifo_dequeue <= to_bool(fifo_empty_n and not its(fifo_out_is_eof) and output_in.ready and
                          ((current_state = open_s) or
                           (not its(fifo_out_is_sample) and not its(gate_all_opcodes_cdc_output)) or
                           ((its(fifo_out_is_time) or its(fifo_out_is_discontinuity)) and its(gate_all_opcodes_cdc_output))));

  -- Internal signals prior to output
  output_out_som   <= fifo_out_is_som and fifo_dequeue;
  output_out_eom   <= fifo_out_is_eom and fifo_dequeue;
  output_out_valid <= fifo_out_is_valid and fifo_dequeue;
  output_out_give  <= (output_out_som or output_out_eom or output_out_valid) and output_in.ready and fifo_dequeue;

  -- Drive outputs
  output_out.data        <= fifo_out_data;
  output_out.som         <= output_out_som;
  output_out.eom         <= output_out_eom;
  output_out.eof         <= fifo_out_is_eof and fifo_empty_n;
  output_out.valid       <= output_out_valid;
  output_out.byte_enable <= fifo_out_byte_enable;
  output_out.give        <= output_out_give;
  output_out.opcode      <= fifo_out_opcode;

  -- Handles setting time_late
  time_late_p : process (output_in.clk)
  begin
    if rising_edge(output_in.clk) then
      if its(output_in.reset) then
        time_late <= bfalse;
      else
        if its(time_late) then
          time_late <= bfalse;
        end if;
        if current_state = time_waiting_s and time_now > time_to_transmit(95 downto 32) then
          time_late <= btrue;
        end if;
      end if;
    end if;
  end process;

  -- The state machine transitions between getting time, waiting for time, and
  -- passing samples
  timegate_fsm_p : process(output_in.clk)
  begin
    if rising_edge(output_in.clk) then
      if its(output_in.reset) then
        -- Until a Time message is received, the component remains in its
        -- default state where timed samples on the Input are transferred to
        -- the Output port without delay.
        current_state <= open_s;        -- gate is initially open
      else
        case current_state is
          -----------------------
          when open_s =>                -- gate is open, samples are flowing
            -----------------------
            if its(bypass_cdc_output) then
              -- If the bypass property is set True, Sample messages
              -- are always transferred from the timed samples on the Input to
              -- the Output port
              null;
            elsif its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if nchunks_c = 1 then     -- Time captured in a single chunk
                -- Once a Time message is received the Time will be stored.
                current_state <= time_waiting_s;
              else                      -- Time captured over multiple chunks
                current_state <= time_coming_s;
              end if;
            end if;
          -----------------------
          when time_coming_s =>   -- time is coming in chunks, not yet complete
            -----------------------
            if its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if time_chunk_index = nchunks_c - 1 then
                -- Once a Time message is received the Time will be stored.
                current_state <= time_waiting_s;
              end if;
            elsif its(fifo_dequeue) then  -- Extracting opcode following incomplete Time capture
              current_state <= error_s;
            end if;
          -----------------------
          when time_waiting_s =>        -- time has arrived and we are waiting
            -----------------------
            if its(bypass_cdc_output) then
              current_state <= open_s;
            elsif its(fifo_out_is_discontinuity) and its(fifo_dequeue) then
              -- A Discontinuity message will return the Timegate to its default
              -- state where timed samples on the Input are transferred to the
              -- Output port without delay.
              current_state <= open_s;
            elsif its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if nchunks_c = 1 then     -- Time captured in a single chunk
                null;
              else                      -- Time captured over multiple chunks
                current_state <= time_coming_s;
              end if;
            elsif time_now >= time_to_transmit(time_width_c-1 downto 32) then
              -- Sample messages will be delayed, preventing messages being
              -- consumed from the input port, until the current time matches or
              -- exceeds the stored (gated) time minus the time_correction
              -- property.
              current_state <= open_s;
            end if;
          -----------------------
          when error_s =>  -- things are broken, protocol is erroneous, we're stuck
            -----------------------
            null;
          -----------------------
          when others =>
            -----------------------
            current_state <= error_s;
        -----------------------
        end case;
      end if;
    end if;
  end process;

  -- The state machine signals between getting time, waiting for time, and
  -- passing samples
  timegate_fsm_signals_p : process(output_in.clk)
    variable time_stored_v : unsigned(time_shift_register'range) := (others => '0');
  begin
    if rising_edge(output_in.clk) then
      if its(output_in.reset) then
        error               <= bfalse;
        time_delta          <= (others => '0');
        time_delta_update   <= '0';
        time_shift_register <= (others => '0');
        time_chunk_index    <= 0;
        time_to_transmit    <= (others => '0');
      else
        time_delta_update <= '0';       -- Default
        case current_state is
          -----------------------
          when open_s =>                -- gate is open, samples are flowing
            -----------------------
            if its(bypass_cdc_output) then
              -- If the bypass property is set True, Sample messages
              -- are always transferred from the timed samples on the Input to
              -- the Output port
              null;
            elsif its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if nchunks_c = 1 then     -- Time captured in a single chunk
                -- Once a Time message is received the Time will be stored.
                time_to_transmit <= unsigned(fifo_out_data(time_to_transmit'range)) - shift_left(resize(ulong_t(time_correction_cdc_output), 64), 32);
              else                      -- Time captured over multiple chunks
                time_shift_register <= unsigned(fifo_out_data) & time_shift_register(time_shift_register'high downto fifo_out_data'length);
                time_chunk_index    <= 1;
              end if;
            end if;
          -----------------------
          when time_coming_s =>  -- time is coming in chunks, not yet complete
            -----------------------
            if its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if time_chunk_index = nchunks_c - 1 then
                time_stored_v       := unsigned(fifo_out_data) & time_shift_register(time_shift_register'high downto fifo_out_data'length);
                -- Once a Time message is received the Time will be stored.
                time_to_transmit    <= time_stored_v(time_to_transmit'range) - shift_left(resize(ulong_t(time_correction_cdc_output), 64), 32);
                time_chunk_index    <= 0;
                time_shift_register <= (others => '0');
              else
                time_shift_register <= unsigned(fifo_out_data) & time_shift_register(time_shift_register'high downto fifo_out_data'length);
                time_chunk_index    <= time_chunk_index + 1;
              end if;
            end if;
          -----------------------
          when time_waiting_s =>        -- time has arrived and we are waiting
            -----------------------
            if its(fifo_out_is_discontinuity) and its(fifo_dequeue) then
              -- A Discontinuity message will return the Timegate to its default
              -- state where timed samples on the Input are transferred to the
              -- Output port without delay.
              time_delta       <= (others => '0');
              time_to_transmit <= (others => '0');
            elsif its(fifo_out_is_time) and its(fifo_dequeue) then  -- time opcode
              if nchunks_c = 1 then     -- Time captured in a single chunk
                -- Once a Time message is received the Time will be stored.
                time_to_transmit <= unsigned(fifo_out_data(time_to_transmit'range)) - shift_left(resize(ulong_t(time_correction_cdc_output), 64), 32);
              else                      -- Time captured over multiple chunks
                time_shift_register <= unsigned(fifo_out_data) & time_shift_register(time_shift_register'high downto fifo_out_data'length);
                time_chunk_index    <= 1;
              end if;
            elsif time_now >= time_to_transmit(time_width_c-1 downto 32) then
              -- The current time - (stored time - time_correction) will be
              -- captured when the gate is opened and provided as the
              -- actual_time_to_requested_time_delta property, which will equal
              -- zero if the data has been delayed.
              time_delta        <= time_now - time_to_transmit(time_width_c-1 downto 32);
              -- Strobe to indicate the time_delta has been updated
              time_delta_update <= '1';
            end if;
          -----------------------
          when error_s =>  -- things are broken, protocol is erroneous, we're stuck
            -----------------------
            error <= btrue;
          -----------------------
          when others =>
            -----------------------
            null;
        -----------------------
        end case;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------

end rtl;
