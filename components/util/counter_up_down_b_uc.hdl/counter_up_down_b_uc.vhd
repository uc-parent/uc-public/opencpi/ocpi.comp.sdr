-- HDL implementation of counter_up_down_b_uc.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_util;
use sdr_util.sdr_util.counter_updown;

architecture rtl of worker is

  constant delay_c        : integer := 1;
  constant counter_size_c : integer := output_out.data'length;

  signal flush         : std_logic;
  signal discontinuity : std_logic;

  -- Counter signals
  signal counter_reset  : std_logic;
  signal counter_enable : std_logic;
  signal counter_value  : unsigned(counter_size_c - 1 downto 0);

begin

  input_out.take <= output_in.ready;

  -- Reset counter
  flush <= '1' when input_in.ready = '1' and output_in.ready = '1'
           and input_in.opcode = bool_timed_sample_flush_op_e else '0';
  discontinuity <= '1' when input_in.ready = '1' and output_in.ready = '1'
                   and input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';
  counter_reset <= ctl_in.reset or flush or discontinuity;

  -- Valid sample data
  counter_enable <= '1' when props_in.enable = '1' and output_in.ready = '1' and
                    input_in.valid = '1' and input_in.data(0) = '1' and
                    input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- Instantiate updown counter
  counter_updown_i : counter_updown
    generic map (
      counter_size_g => counter_size_c
      )
    port map (
      clk        => ctl_in.clk,
      reset      => counter_reset,
      enable     => counter_enable,
      direction  => props_in.direction,
      load_trig  => props_in.counter_value_written,
      load_value => props_in.counter_value,
      length     => props_in.size,
      count_out  => counter_value
      );

  props_out.counter_value <= counter_value;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.bool_to_unsigned_char_protocol_delay
    generic map (
      delay_g      => delay_c,
      data_width_g => input_in.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => std_logic_vector(counter_value),
      output_out          => output_out
      );

end rtl;
