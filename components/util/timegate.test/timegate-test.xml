<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <!-- Case 00: Typical - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case 01: Property: Bypass
       Note: Due to framework limitations bypass is set to True, forcing data to
             be passed from input to output immediately. Setting to False is
             currently not possible. For the remaining tests the bypass flag
             will be set to True. -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case 02: Property: gate_all_opcodes - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" values="true,false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case XX: Omitting Property: leap_seconds test
       Note: Property is only required to generate the "correct" GPS time at the
             time of this components release, therefore currently any other value is incorrect. -->
  <!-- Case 03: Property: time_correction - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" values="0,1,65535,4294901760,4294967295"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case XX: Omitting Property: late_time_sticky test,
       Note: Primary function is to indicate when time is in the past,
             framework limitation means this cannot be tested automatically. -->
  <!-- Case 04: Property: port_width - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" values="8,16,32,64"/>
  </case>
  <!-- Case 05: Sample - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="all_zero,all_maximum,large_positive,near_zero"/>
  </case>
  <!-- Case 06: Input Stressing - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case 07: Message Size - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 08: Time Opcode - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 09: Sample Interval Opcode - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 10: Flush Opcode - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 11: Discontinuity Opcode - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 12: Metadata Opcode - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="positive,consecutive"/>
  </case>
  <!-- Case 13: Hardware Testing Custom - with Bypass set to True -->
  <case>
    <input port="input" script="generate.py --case custom" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
  </case>
  <!-- Case 14: Soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="bypass" value="true"/>
    <property name="gate_all_opcodes" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="time_correction" value="0"/>
    <property name="late_time_sticky" value="false"/>
    <property name="port_width" value="8"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
