#!/usr/bin/env python3

# Python implementation of Timegate for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of Timegate for verification."""

import logging
import opencpi.ocpi_testing as ocpi_testing
from sdr_interface.sample_time import SampleTime


class TimeGate(ocpi_testing.Implementation):
    """Protocol-less version of the Timegate implementation."""

    def __init__(self, bypass=False, gate_all_opcodes=False, leap_seconds=0, time_correction=0,
                 late_time_sticky=False):
        """Timegate initialisation.

        Args:
            bypass (bool): Ignores/drops timestamps, disabling the time-gating functionality.
            gate_all_opcodes (bool): Enable/disable ability to gate more than just the sample messages.
            leap_seconds (int): Leap seconds to subtract from the GPS epoch.
            time_correction (uint): Unsigned fixed point fraction of second.
            late_time_sticky (bool): Indicates if the corrected timestamp arrives late.
        """
        super().__init__()
        self.bypass = bypass
        self.gate_all_opcodes = gate_all_opcodes
        self.late_time_sticky = late_time_sticky

        logging.info("Timegate settings are:"
                     f"bypass: {bypass}"
                     f"gate_all_opcodes: {gate_all_opcodes}"
                     f"leap_seconds: {leap_seconds}"
                     f"time_correction: {time_correction}"
                     f"late_time_sticky: {late_time_sticky}")

        self.actual_time_to_requested_time_delta = 0
        self.time_stored = False
        self._input_time = SampleTime(group_delay_seconds=0,
                                      group_delay_fractional=time_correction)

        self._input_time.leap_seconds = leap_seconds

        self.reset()

    def reset(self):
        """Put the implementation into the same state as at initialisation."""
        self.time_stored = False
        self._input_time.reset()

    def _get_current_time(self):
        """Gets the current time.

        Args:
            None.

        Returns:
            decimal : Current time.
        """
        _current_time = SampleTime()
        _current_time.time = _current_time.get_gps_timestamp()

        return (_current_time.opcode_time())

    def _handle_gating_function(self):
        """Common function to use when checking time and setting internal properties."""
        if self._input_time.time is not None:
            if self._get_current_time() > self._input_time.opcode_time():
                self.late_time_sticky = True
                self.actual_time_to_requested_time_delta = (
                    self._get_current_time() - self._input_time.opcode_time())

        if self.time_stored is True:
            # Loop here until the timegate opens
            while self._get_current_time() < self._input_time.opcode_time():
                # Do nothing
                pass

            self.actual_time_to_requested_time_delta = (
                self._get_current_time() - self._input_time.opcode_time())

            # rest back to initial state once gate is opened.
            self.reset()

    def sample(self, samples):
        """Sample opcode handling.

        Args:
            samples (dict): Opcode, value dictionary.

        Returns:
            dict: Formatted messages.
        """
        if self.bypass is False:
            self._handle_gating_function()

        messages = [{"opcode": "sample", "data": samples}]
        return self.output_formatter(messages)

    def flush(self, input):
        """Flush opcode handler.

        Args:
            input (dict): Input port data.

        Returns:
            Messages to output.
        """
        if self.gate_all_opcodes and not self.bypass:
            self._handle_gating_function()

        messages = [{"opcode": "flush", "data": None}]
        return self.output_formatter(messages)

    def discontinuity(self, input):
        """Discontinuity opcode handler.

        Args:
            input (dict): Input port data.

        Returns:
            Messages to output.
        """
        self.reset()
        messages = [{"opcode": "discontinuity", "data": None}]
        return self.output_formatter(messages)

    def sample_interval(self, input):
        """Sample Interval opcode handler.

        Args:
            input (dict): Input port data.

        Returns:
            List of messages to output.
        """
        if self.gate_all_opcodes and not self.bypass:
            self._handle_gating_function()

        messages = [{"opcode": "sample_interval", "data": input}]
        return self.output_formatter(messages)

    def time(self, input):
        """Time opcode handler.

        Args:
            input (dict): Input port data.

        Returns:
            List of messages to output.
        """
        self._input_time.time = input
        self.time_stored = True

        messages = [{"opcode": "time",
                     "data": self._input_time.opcode_time()}]
        return self.output_formatter(messages)

    def metadata(self, input):
        """Metadata opcode handler.

        Args:
            input  (dict): Input port data.

        Returns:
            Messages to output.
        """
        if self.gate_all_opcodes and not self.bypass:
            self._handle_gating_function()

        messages = [{"opcode": "metadata", "data": input}]
        return self.output_formatter(messages)
