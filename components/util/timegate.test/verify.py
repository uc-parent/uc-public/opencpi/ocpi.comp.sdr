#!/usr/bin/env python3

# Runs checks for timegate testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Timegate OCPI Verification script."""

import os
import sys
import decimal
import logging
from xml.dom import minidom
import opencpi.ocpi_testing as ocpi_testing
from timegate import TimeGate

input_file = str(sys.argv[-1])
output_file = str(sys.argv[-2])

case_text = str(os.environ.get("OCPI_TESTCASE"))
case = int(case_text[-2:])

subcase_text = str(os.environ.get("OCPI_TESTSUBCASE"))
subcase = int(subcase_text)

# Get the application xml, and use this to find the initial value for
# late_time_sticky and actual_time_to_requested_time_delta.
# Note: this is done as the OCPI_TEST_xxx is modified by the UUT, and the value
# present at the end of the application run is passed to this verify script.
app = minidom.parse(f"../../gen/applications/case{case:02}.{subcase:02}.xml")
for inst in app.getElementsByTagName("instance"):
    if inst.getAttribute("name") == "timegate":
        component = inst
        for prop in inst.getElementsByTagName("property"):
            if prop.getAttribute("name") == "late_time_sticky":
                initial_late_time_sticky = prop.getAttribute(
                    "value").upper() == "TRUE"

test_id = ocpi_testing.get_test_case()
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))

bypass = os.environ["OCPI_TEST_bypass"].upper() == "TRUE"
gate_all_opcodes = os.environ["OCPI_TEST_gate_all_opcodes"].upper() == "TRUE"
leap_seconds = int(os.environ["OCPI_TEST_leap_seconds"])
time_correction = int(os.environ["OCPI_TEST_time_correction"])
actual_time_to_requested_time_delta = int(
    os.environ["OCPI_TEST_actual_time_to_requested_time_delta"])
late_time_sticky = os.environ["OCPI_TEST_late_time_sticky"].upper() == "TRUE"

port_width = int(os.environ["OCPI_TEST_port_width"])

if port_width == 8:
    protocol = "uchar_timed_sample"
    generator = ocpi_testing.generator.UnsignedCharacterGenerator()
elif port_width == 16:
    protocol = "ushort_timed_sample"
    generator = ocpi_testing.generator.UnsignedShortGenerator()
elif port_width == 32:
    protocol = "ulong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongGenerator()
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongLongGenerator()
else:
    raise ValueError(f"Unsupported port length: {port_width}")

if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logfile = os.path.dirname(output_file)
logfile = os.path.join(logfile, f"{test_id}.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")

timegate_implementation = TimeGate(
    bypass=bypass,
    gate_all_opcodes=gate_all_opcodes,
    leap_seconds=leap_seconds,
    time_correction=time_correction,
    late_time_sticky=initial_late_time_sticky)

verifier = ocpi_testing.Verifier(timegate_implementation)

verifier.set_port_types([protocol], [protocol], ["equal"])

if not verifier.verify(test_id, [input_file], output_file):
    sys.exit(1)

if not bypass:
    UUT_late_time_sticky = late_time_sticky
    python_late_time_sticky = timegate_implementation.late_time_sticky

    if UUT_late_time_sticky != python_late_time_sticky:
        raise ValueError(f"late_time_sticky is invalid between Unit Under Test {UUT_late_time_sticky}" +
                         f" and Python Model {python_late_time_sticky}")

    UUT_delta_value = (decimal.Decimal(
        actual_time_to_requested_time_delta) / (2**32))
    python_delta_value = timegate_implementation.actual_time_to_requested_time_delta

    if UUT_delta_value != python_delta_value:
        # Ideally, the test framework would be able to mock the start time of the
        # UUT and python test runs to be the same, and then the time deltas could
        # be tested to ensure they are valid a tolerance of each other, and raise
        # a ValueError if not. But given UUT and python are both working with the
        # system time and aren't necessarily comparable, just log a warning so the
        # human running the tests can see the values.
        logging.warning(f"actual_time_to_requested_time_delta is invalid between Unit Under Test {UUT_delta_value}" +
                        f" and the Python Model {python_delta_value}")
