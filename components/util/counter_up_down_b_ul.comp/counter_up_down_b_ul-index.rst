.. counter_up_down_b_ul documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _counter_up_down_b_ul:


Counter Up Down (``counter_up_down_b_ul``)
==========================================
Counts the number of times a ``true`` data value occurs on the input.

Design
------
Counter that increments or decrements by one each time it receives a boolean ``true``. When a ``false`` is received the counter value does not change. The counter value is output whenever an input sample is received. Whether the counter increments or decrements is controlled by the ``direction`` property.

Interface
---------
.. literalinclude:: ../specs/counter_up_down_b_ul-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Only data values within a sample opcode message are counted.

Discontinuity and flush opcode messages reset the counter value to zero or ``size``, depending on the mode of operation, and are then output by the component. All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../counter_up_down_b_ul.hdl ../counter_up_down_b_ul.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Counter up-down primitive <counter_updown-primitive>`

 * :ref:`Protocol interface delay (narrow to wide) primitive v2 <narrow_to_wide_protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``counter_up_down_b_ul`` are:

 * Due to the size of an unsigned long the maximum output value is 4294967294, after this it will roll-over to 0.

 * Due to the output being an unsigned long the minimum output value is 0, after this it will reset to the value ``size`` - 1.

 * When in increment mode, if a value greater than ``size`` - 1 is loaded into the component this will roll-over to 0.

 * When in decrement mode, if a value greater than ``size`` - 1 is loaded into the component then the used value will be ``size`` - 1.

 * To use the maximum value of ``size`` it must be set to 0.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
