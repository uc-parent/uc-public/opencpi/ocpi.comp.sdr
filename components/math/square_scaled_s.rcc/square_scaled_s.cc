// Square_scaled RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "square_scaled_s-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Square_scaled_sWorkerTypes;

class Square_scaled_sWorker : public Square_scaled_sWorkerBase {
  const uint32_t scale_factor = SQUARE_SCALED_S_SCALE_OUTPUT;
  const uint32_t BITMASK_INT16 = 0xffff;
  RCCResult run(bool) {
    if (input.opCode() == Short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const int16_t* inData = input.sample().data().data();
      int16_t* outData = output.sample().data().data();
      output.setOpCode(Short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);

      for (size_t i = 0; i < length; i++) {
        int32_t result;
        result = *inData++;
        result *= result;
        // & 0xffff bit mask included in consideration of compiler
        // dependent behaviour when truncating int32_t to int16_t
        *outData++ = (int16_t)((result >> scale_factor) & BITMASK_INT16);
      }

      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

SQUARE_SCALED_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
SQUARE_SCALED_S_END_INFO
