#!/usr/bin/env python3

# Python implementation of exponentiation block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of exponentiation block for verification."""

import math

import opencpi.ocpi_testing as ocpi_testing


class Exponentiation(ocpi_testing.Implementation):
    """Testing implementation for Exponentiation."""

    def __init__(self, base, scale_input):
        """Initialise the Exponentiation class.

        Args:
            base (float): Base of the exponentiation.
            scale_input (int): Input scale factor (input is divided by 2^scale_input).
        """
        super().__init__(base=base, scale_input=scale_input)

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle input sample opcode.

        Args:
            values (list): Sample data on input port.

        Returns:
            Formatted messages.
        """
        output_values = []
        for value in values:
            scaled_value = value / 2**self.scale_input
            output_values.append(round(self.base**scaled_value))
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
