#!/usr/bin/env python3

# Python implementation of gain block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of gain block for verification."""

from numpy import int32


import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class Gain(ocpi_testing.Implementation):
    """Testing implementation for Gain."""

    def __init__(self, gain):
        """Initialise the Gain class.

        Args:
            gain (int): Gain multiplier value
        """
        super().__init__(gain=gain)

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle input sample opcode.

        Args:
            values (list): Sample data on input port.

        Returns:
            Formatted messages.
        """
        output_values = [0]*len(values)
        for index, value in enumerate(values):
            output_values[index] = int32(self.gain * value)

        max_message_length = ocpi_protocols.PROTOCOLS[
            "long_timed_sample"].max_sample_length
        messages = []
        for index in range(0, len(values), max_message_length):
            messages.append(
                {"opcode": "sample",
                 "data": output_values[index: index + max_message_length]})

        return self.output_formatter(messages)
