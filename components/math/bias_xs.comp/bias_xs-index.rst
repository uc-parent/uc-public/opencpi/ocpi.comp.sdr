.. Bias_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. bias_xs documentation

.. meta::
   :keywords: offset


.. _bias_xs:


Bias (``bias_xs``)
==================
Adds a fixed complex bias to all inputs.

Design
------
The mathematical representation of the implementation is given in :eq:`bias_xs-equation`.

.. math::
   :label: bias_xs-equation

   z[n] = x[n] + B


In :eq:`bias_xs-equation`:

 * :math:`x[n]` is the input values.

 * :math:`z[n]` is the output values.

 * :math:`B` is the complex bias.

A block diagram representation of the implementation is given in :numref:`bias_xs-diagram`.

.. _bias_xs-diagram:

.. figure:: bias_xs.svg
   :alt: Complex bias implementation.
   :align: center

   Complex bias implementation.

Interface
---------
.. literalinclude:: ../specs/bias_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
A bias is only added to values in sample opcode messages.

All other opcode messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../bias_xs.hdl ../bias_xs.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``bias_xs`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
