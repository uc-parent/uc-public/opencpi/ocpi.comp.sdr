// Complex combiner implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "complex_combiner_s_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Complex_combiner_s_xsWorkerTypes;

class Complex_combiner_s_xsWorker : public Complex_combiner_s_xsWorkerBase {
  RCCBoolean input_real_opcode_passthrough =
      COMPLEX_COMBINER_S_XS_INPUT_REAL_OPCODE_PASSTHROUGH;

  size_t input_real_processed_samples = 0;
  size_t input_imaginary_processed_samples = 0;
  size_t output_processed_samples = 0;

  RCCResult start() {
    input_real_processed_samples = 0;
    input_imaginary_processed_samples = 0;
    output_processed_samples = 0;
    return RCC_OK;
  }

  RCCResult run(bool) {
    // Discard non sample opcode on non-forwarding input
    if (input_real.opCode() != Short_timed_sampleSample_OPERATION &&
        !input_real_opcode_passthrough) {
      input_real.advance();
      return RCC_OK;
    } else if (input_imaginary.opCode() != Short_timed_sampleSample_OPERATION &&
               input_real_opcode_passthrough) {
      input_imaginary.advance();
      return RCC_OK;
    }

    if (input_real.opCode() == Short_timed_sampleSample_OPERATION &&
        input_imaginary.opCode() == Short_timed_sampleSample_OPERATION) {
      const int16_t *input_real_data = input_real.sample().data().data();
      const int16_t *input_imaginary_data =
          input_imaginary.sample().data().data();
      Complex_short_timed_sampleSampleData *output_data =
          output.sample().data().data();
      // Advance the inputs to the next sample point to be handled
      input_real_data = &input_real_data[input_real_processed_samples];
      input_imaginary_data =
          &input_imaginary_data[input_imaginary_processed_samples];

      size_t real_length = input_real.sample().data().size();
      size_t imaginary_length = input_imaginary.sample().data().size();
      size_t input_real_unprocessed_samples =
          real_length - input_real_processed_samples;
      size_t input_imaginary_unprocessed_samples =
          imaginary_length - input_imaginary_processed_samples;
      size_t max_output_length =
          COMPLEX_COMBINER_S_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(*output_data);

      // New output buffer is being used
      if (output_processed_samples == 0) {
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        if (input_real_opcode_passthrough) {
          output.sample().data().resize(
              std::min(input_real_unprocessed_samples, max_output_length));
        } else {
          output.sample().data().resize(
              std::min(input_imaginary_unprocessed_samples, max_output_length));
        }
      }

      size_t output_unprocessed_samples =
          output.sample().data().size() - output_processed_samples;
      output_data = &output_data[output_processed_samples];

      // Output buffer space is restricting total samples that can be processed
      if (output_unprocessed_samples <= input_real_unprocessed_samples &&
          output_unprocessed_samples <= input_imaginary_unprocessed_samples) {
        for (size_t i = 0; i < output_unprocessed_samples; i++) {
          output_data->real = *input_real_data;
          output_data->imaginary = *input_imaginary_data;
          output_data++;
          input_real_data++;
          input_imaginary_data++;
        }
        input_real_processed_samples += output_unprocessed_samples;
        input_imaginary_processed_samples += output_unprocessed_samples;
        output.advance();
        output_processed_samples = 0;

      } else {
        size_t available_input_samples =
            std::min(input_real_unprocessed_samples,
                     input_imaginary_unprocessed_samples);
        for (size_t i = 0; i < available_input_samples; i++) {
          output_data->real = *input_real_data;
          output_data->imaginary = *input_imaginary_data;
          output_data++;
          input_real_data++;
          input_imaginary_data++;
        }
        input_real_processed_samples += available_input_samples;
        input_imaginary_processed_samples += available_input_samples;
        // Do not advance output as input data limited
        output_processed_samples += available_input_samples;
      }
      if (input_real_processed_samples == real_length) {
        input_real.advance();
        input_real_processed_samples = 0;
      }
      if (input_imaginary_processed_samples == imaginary_length) {
        input_imaginary.advance();
        input_imaginary_processed_samples = 0;
      }
      return RCC_OK;
    } else if (input_imaginary.opCode() == Short_timed_sampleTime_OPERATION ||
               input_real.opCode() == Short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      if (input_imaginary.opCode() == Short_timed_sampleTime_OPERATION) {
        output.time().fraction() = input_imaginary.time().fraction();
        output.time().seconds() = input_imaginary.time().seconds();
        input_imaginary.advance();
      } else {
        output.time().fraction() = input_real.time().fraction();
        output.time().seconds() = input_real.time().seconds();
        input_real.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input_imaginary.opCode() ==
                   Short_timed_sampleSample_interval_OPERATION ||
               input_real.opCode() ==
                   Short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      if (input_imaginary.opCode() ==
          Short_timed_sampleSample_interval_OPERATION) {
        output.sample_interval().fraction() =
            input_imaginary.sample_interval().fraction();
        output.sample_interval().seconds() =
            input_imaginary.sample_interval().seconds();
        input_imaginary.advance();
      } else {
        output.sample_interval().fraction() =
            input_real.sample_interval().fraction();
        output.sample_interval().seconds() =
            input_real.sample_interval().seconds();
        input_real.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input_imaginary.opCode() == Short_timed_sampleFlush_OPERATION ||
               input_real.opCode() == Short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      if (input_imaginary.opCode() == Short_timed_sampleFlush_OPERATION) {
        input_imaginary.advance();
      } else {
        input_real.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input_imaginary.opCode() ==
                   Short_timed_sampleDiscontinuity_OPERATION ||
               input_real.opCode() ==
                   Short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      if (input_imaginary.opCode() ==
          Short_timed_sampleDiscontinuity_OPERATION) {
        input_imaginary.advance();
      } else {
        input_real.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input_imaginary.opCode() ==
                   Short_timed_sampleMetadata_OPERATION ||
               input_real.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      if (input_imaginary.opCode() == Short_timed_sampleMetadata_OPERATION) {
        output.metadata().id() = input_imaginary.metadata().id();
        output.metadata().value() = input_imaginary.metadata().value();
        input_imaginary.advance();
      } else {
        output.metadata().id() = input_real.metadata().id();
        output.metadata().value() = input_real.metadata().value();
        input_real.advance();
      }
      output.advance();
      return RCC_OK;
    } else {
      setError("Unknown opCode received");
      return RCC_FATAL;
    }
  }
};

COMPLEX_COMBINER_S_XS_START_INFO

COMPLEX_COMBINER_S_XS_END_INFO
