// 128 bit math routines
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_MATH_COMMON_MULTIWORD_INTEGER_DIVIDE_HH_
#define COMPONENTS_MATH_COMMON_MULTIWORD_INTEGER_DIVIDE_HH_
//
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <algorithm>

//
//
// unsigned_divide
// ---------------
//
// Unsigned division of a by b giving quotient q = [a/b] and remainder r, such
// that
// a = q * b + r,    where 0 <= r < b.
//
// Dividend a is assumed non-negative (a >= 0) and divisor b is positive
// definite (b > 0).
//
// This assumes a native (i.e. fast) divide of uint64 by uint64 values.
// The input array word size must be half of this native divide size.
//
// WARNING: no checks on the validity of a and b are made for performance
// reasons.
//
// WARNING: Element [0] of the parameters is the least significant word.
//
// Nomenclature:
// Traditional long division (decimal) iterates over digits. A computer
// binary implementation iterates over binary words. Therefore in generic
// descriptions the term word and digit can be considered synonymous.
//
// Input arguments:
// dividend - Array of uint32 words.
// dsize - Number of words in dividend (and quotient).
// divisor - Array of uint32 words.
// rsize - Number of words in divisor (and remainder).
// quotient - Array of uint32 words to hold result.
// remainder - Array of uint32 words to hold result (optional).
//
// Output Results:
// Quotient
// Remainder (optional)
//

void multiword_unsigned_divide(const uint32_t* dividend, int dsize,
                               const uint32_t* divisor, int rsize,
                               uint32_t* quotient,
                               uint32_t* remainder = nullptr) {
  const int basebits{32};                 // Number of bits in a word.
  const uint64_t base{1llu << basebits};  // Number base (2**bits)
  const int64_t basemask{base - 1};       // Mask for single word.

  std::fill(quotient, quotient + dsize, 0);
  if (remainder) {
    std::fill(remainder, remainder + rsize, 0);
  }
  // Determine the number of words in dividend and divisor.
  int n{rsize};
  for (; n > 0 && divisor[n - 1] == 0; --n) {
  }

  int m{dsize};
  for (; m > 0 && dividend[m - 1] == 0; --m) {
  }

  // Technically, m can equal 0 here, if the dividend = 0. This is no
  // problem as it will be caught and handled by CASE 1 below.

  // CASE 0: Divide by zero. Trigger platform action (probably SIGFPE)

  if (n == 0) {
    quotient[0] = dividend[0] / divisor[0];
    return;
  }

  // CASE 1: m < n => quotient = 0; remainder = dividend.

  if (m < n) {
    if (remainder) {
      std::copy(dividend, dividend + dsize, remainder);
    }
    return;
  }

  // CASE 2: Divisor has only one word (n = 1). Do a short
  //         division and return.
  if (n < 2) {
    std::uint64_t partial{0};

    for (int i = m - 1; i >= 0; --i) {
      partial = (partial << basebits) + static_cast<std::uint64_t>(dividend[i]);
      quotient[i] = static_cast<std::uint32_t>(partial / divisor[0]);
      partial %= divisor[0];
    }

    if (remainder) {
      for (int i = 1; i < m; i++) {
        remainder[i] = 0;
      }
      remainder[0] = partial;
    }

    return;
  }

  // CASE 3: Divisor has more than one word (n > 1) and the dividend
  //         is at least as long as the divisor (m >= n).
  // Use Knuth's Algorithm D (The Art of Computer Programming, Volume 2:
  // Seminumerical Algorithms, Chapter 4.3: Multiple-Precision Arithmetic)
  // This implementation based on public domain 'divmnu.c'

  // Normalize by shifting divisor left just enough so that its high-order
  // bit is on, and shift dividend left the same amount. We may have to append a
  // high-order word on the dividend; we do that unconditionally.

  uint32_t shift{0};
  for (uint32_t size{16}, mask{~0u << size}, word{divisor[n - 1]}; size;
       size >>= 1, mask <<= size) {
    if ((word & mask) == 0) {
      shift += size;
      word <<= size;
    }
  }
  uint32_t normalised_divisor[n];
  for (int i = n - 1; i > 0; i--) {
    normalised_divisor[i] = (divisor[i] << shift) |
                            ((uint64_t)divisor[i - 1] >> (basebits - shift));
  }
  normalised_divisor[0] = divisor[0] << shift;

  uint32_t normalised_dividend[m + 1];
  normalised_dividend[m] = (uint64_t)dividend[m - 1] >> (basebits - shift);
  for (int i = m - 1; i > 0; i--) {
    normalised_dividend[i] = (dividend[i] << shift) |
                             ((uint64_t)dividend[i - 1] >> (basebits - shift));
  }
  normalised_dividend[0] = dividend[0] << shift;

  for (int j = m - n; j >= 0; j--) {  // Main loop.
    // Compute estimate qhat and rhat of quotient[j].
    uint64_t qhat, rhat;
    if (normalised_dividend[j + n] >= normalised_divisor[n - 1]) {
      // Overflow, clip at word max.
      qhat = base - 1;
    } else {
      qhat =
          (normalised_dividend[j + n] * base + normalised_dividend[j + n - 1]) /
          normalised_divisor[n - 1];
    }
    rhat =
        (normalised_dividend[j + n] * base + normalised_dividend[j + n - 1]) -
        qhat * normalised_divisor[n - 1];
    while (rhat < base && qhat * normalised_divisor[n - 2] >
                              base * rhat + normalised_dividend[j + n - 2]) {
      qhat = qhat - 1;
      rhat = rhat + normalised_divisor[n - 1];
    }

    // Multiply and subtract.
    int64_t k = 0;
    for (int i = 0; i < n; i++) {
      uint64_t p = qhat * normalised_divisor[i];
      int64_t temp =
          int64_t(normalised_dividend[i + j]) - k - int64_t(p & basemask);
      normalised_dividend[i + j] = temp;
      k = int64_t(p >> basebits) - (temp >> basebits);
    }
    int64_t temp = normalised_dividend[j + n] - k;
    normalised_dividend[j + n] = temp;

    quotient[j] = qhat;  // Store quotient word.
    if (temp < 0) {      // If we subtracted too much, add back.
      quotient[j] = quotient[j] - 1;
      k = 0;
      for (int i = 0; i < n; i++) {
        temp = int64_t(normalised_dividend[i + j]) +
               int64_t(normalised_divisor[i]) + k;
        normalised_dividend[i + j] = temp;
        k = temp >> basebits;
      }
      normalised_dividend[j + n] = normalised_dividend[j + n] + k;
    }
  }

  if (remainder) {
    // The remainder is that left in the dividend. Unnormalise into the
    // remainder.
    for (int i = 0; i < n - 1; i++) {
      remainder[i] =
          (normalised_dividend[i] >> shift) |
          ((uint64_t)normalised_dividend[i + 1] << (basebits - shift));
    }
    remainder[n - 1] = normalised_dividend[n - 1] >> shift;
  }
}
#endif  // COMPONENTS_MATH_COMMON_MULTIWORD_INTEGER_DIVIDE_HH_
