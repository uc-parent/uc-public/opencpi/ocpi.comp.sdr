// Common mathematical operations on time values
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_MATH_COMMON_TIME_UTILS_HH_
#define COMPONENTS_MATH_COMMON_TIME_UTILS_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>

// LINT EXCEPTION: cpp_011: 2: New class definition allowed in common
// implementation
class time_utils {
 private:
  // To handle division of 96-bit or 128-bit time values, split them into 32bit
  // words, and use this in-place divide of 32bit numerator/divisor including
  // previous remainder and returning new remainder
  template <typename T>
  static void multi_word_divide(uint64_t* u32_numerator, T divisor,
                                uint64_t* u32_remainder) {
    uint64_t u64_dividend = (*u32_numerator) + ((*u32_remainder) << 32);

    *u32_numerator = u64_dividend / divisor;
    *u32_remainder = u64_dividend % divisor;
  }

 public:
  // Add two times together, handling overflow from fractional in whole part
  static void add(uint32_t* seconds, uint64_t* fraction,
                  uint32_t seconds_to_add, uint64_t fraction_to_add) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    // If no fractional overflow
    if (fraction_to_add <= (UINT64_MAX - *fraction)) {
      // do a straight addition
      *fraction += fraction_to_add;
      *seconds += seconds_to_add;
    } else {
      // Calculate the remaining fraction after the overflow
      *fraction = (UINT64_MAX - 1) - (UINT64_MAX - fraction_to_add) -
                  (UINT64_MAX - *fraction);
      // and carry the 1 into the seconds
      *seconds += seconds_to_add + 1;
    }
  }

  // Add two times together, handling overflow from fractional in whole part
  // and keep seconds as a 64 bit number
  static void add(uint64_t* seconds, uint64_t* fraction,
                  uint64_t seconds_to_add, uint64_t fraction_to_add) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    // If no fractional overflow
    if (fraction_to_add <= (UINT64_MAX - *fraction)) {
      // do a straight addition
      *fraction += fraction_to_add;
      *seconds += seconds_to_add;
    } else {
      // Calculate the remaining fraction after the overflow
      *fraction = (UINT64_MAX - 1) - (UINT64_MAX - fraction_to_add) -
                  (UINT64_MAX - *fraction);
      // and carry the 1 into the seconds
      *seconds += seconds_to_add + 1;
    }
  }

  // Subtract two times, handling overflow from fractional in whole part
  static void subtract(uint32_t* seconds, uint64_t* fraction,
                       uint32_t seconds_to_subtract,
                       uint64_t fraction_to_subtract) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    // If no fractional overflow
    if (*fraction >= fraction_to_subtract) {
      // do a straight subtraction
      *seconds -= seconds_to_subtract;
      *fraction -= fraction_to_subtract;
    } else {
      // Calculate the remaining fraction after the overflow
      *fraction = (UINT64_MAX - fraction_to_subtract) + 1 + *fraction;
      // and carry the 1 into the seconds
      *seconds -= (seconds_to_subtract + 1);
    }
  }

  // Multiply the passed time by the 8 or 16 bit multiplier
  template <typename T, typename = typename std::enable_if<
                            std::is_same<T, uint8_t>::value ||
                            std::is_same<T, uint16_t>::value>::type>
  static void multiply(uint32_t* seconds, uint64_t* fraction, T multiplier) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    if (multiplier == 0) {
      *seconds = 0;
      *fraction = 0;
    } else {
      // Long multiply using 32 bit digits.
      // [seconds][fraction.ms][fraction.ls] * multiplier
      uint64_t result0 = (*fraction & 0xffffffff) * multiplier;
      uint64_t result1 = (*fraction >> 32) * multiplier;
      uint64_t result2 = uint64_t(*seconds) * multiplier;
      *fraction = result0 + (result1 << 32);
      uint64_t carry = *fraction < result0;
      *seconds = uint32_t(result2 + (result1 >> 32) + carry);
    }
  }

  // Multiply the passed time by the 8 or 16 bit multiplier
  // and keep seconds as a 64 bit number
  template <typename T, typename = typename std::enable_if<
                            std::is_same<T, uint8_t>::value ||
                            std::is_same<T, uint16_t>::value>::type>
  static void multiply(uint64_t* seconds, uint64_t* fraction, T multiplier) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    if (multiplier == 0) {
      *seconds = 0;
      *fraction = 0;
    } else {
      // Long multiply using 32 bit digits.
      // [seconds.ms][seconds.ls][fraction.ms][fraction.ls] * multiplier
      uint64_t result0 = (*fraction & 0xffffffff) * multiplier;
      uint64_t result1 = (*fraction >> 32) * multiplier;
      uint64_t result2 = (*seconds & 0xffffffff) * multiplier;
      uint64_t result3 = (*seconds >> 32) * multiplier;
      *fraction = result0 + (result1 << 32);
      uint64_t carry = *fraction < result0;
      *seconds = (result3 << 32) + result2 + (result1 >> 32) + carry;
    }
  }

  // Divide the passed time by the divisor (8 or 16 bit).
  template <typename T, typename = typename std::enable_if<
                            std::is_same<T, uint8_t>::value ||
                            std::is_same<T, uint16_t>::value>::type>
  static void divide(uint32_t* seconds, uint64_t* fraction, T divisor) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }

    // split
    uint64_t carry = 0;
    uint64_t hi_lo = *seconds;
    uint64_t lo_hi = *fraction >> 32;
    uint64_t lo_lo = *fraction & UINT32_MAX;
    // perform divide
    multi_word_divide(&hi_lo, divisor, &carry);
    multi_word_divide(&lo_hi, divisor, &carry);
    multi_word_divide(&lo_lo, divisor, &carry);
    // recombine
    *seconds = static_cast<uint32_t>(hi_lo);
    *fraction = (lo_hi << 32) + lo_lo;
  }

  // Divide the passed time by the divisor (8 or 16 bit).
  // and keep seconds as a 64 bit number
  template <typename T, typename = typename std::enable_if<
                            std::is_same<T, uint8_t>::value ||
                            std::is_same<T, uint16_t>::value>::type>
  static void divide(uint64_t* seconds, uint64_t* fraction, T divisor) {
    if (seconds == nullptr || fraction == nullptr) {
      return;  // Can't return result in a null pointer, so do nothing.
    }
    // split
    uint64_t carry = 0;
    uint64_t hi_hi = *seconds >> 32;
    uint64_t hi_lo = *seconds & UINT32_MAX;
    uint64_t lo_hi = *fraction >> 32;
    uint64_t lo_lo = *fraction & UINT32_MAX;
    // perform divide
    multi_word_divide(&hi_hi, divisor, &carry);
    multi_word_divide(&hi_lo, divisor, &carry);
    multi_word_divide(&lo_hi, divisor, &carry);
    multi_word_divide(&lo_lo, divisor, &carry);
    // recombine
    *seconds = (hi_hi << 32) + hi_lo;
    *fraction = (lo_hi << 32) + lo_lo;
  }
};

#endif  // COMPONENTS_MATH_COMMON_TIME_UTILS_HH_
