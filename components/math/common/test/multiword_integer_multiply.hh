// 128 bit math routines
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_MATH_COMMON_TEST_MULTIWORD_INTEGER_MULTIPLY_HH_
#define COMPONENTS_MATH_COMMON_TEST_MULTIWORD_INTEGER_MULTIPLY_HH_
//
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <algorithm>

// Note: Currently located in test directory as only used by gtest:
//       Tested by multiply_gtest
//       Used by divide_gtest
// Should be moved to common directory if used by component/library.

//
// Multiword Multiplication using Traditional Long Multiply.
//
// Multiples 2 multiword unsigned integral values by multiplying each word and
// accumulating the results.
// Performance is O(n*m) where n and m are the number of words excluding zero
// words. (There are faster algorithms, but these only have a performance
// advantage with large word counts).
//
// Args:
//    x,y         : Operands as arrays with least significant word first.
//    xsize,ysize : Number of words in x and y.
//    product     : Array to store result. Must contain xsize+ysize words.
//
// Returns:
//    Array
//
void multiword_unsigned_multiply(const uint32_t* x, size_t xsize,
                                 const uint32_t* y, size_t ysize,
                                 uint32_t* product) {
  std::fill(product, product + xsize + ysize, 0);
  while (xsize && x[xsize - 1] == 0) {
    --xsize;
  }
  while (ysize && y[ysize - 1] == 0) {
    --ysize;
  }
  if (xsize == 0 || ysize == 0) {
    return;
  }
  const uint32_t* xw{x};
  for (size_t xn{0}; xn != xsize; ++xn, ++xw) {
    if (*xw) {
      const uint32_t* yw{y};
      for (size_t yn{0}; yn != ysize; ++yn, ++yw) {
        uint64_t part_product =
            static_cast<uint64_t>(*xw) * static_cast<uint64_t>(*yw);
        bool carry{false};
        for (size_t pn{xn + yn}; pn < xsize + ysize && (part_product || carry);
             ++pn) {
          uint32_t sum = product[pn] + static_cast<uint32_t>(part_product);
          if (carry) {
            carry = ++sum <= product[pn];
          } else {
            carry = sum < product[pn];
          }
          product[pn] = sum;
          part_product >>= 32;
        }
      }
    }
  }
  return;
}

#endif  // COMPONENTS_MATH_COMMON_TEST_MULTIWORD_INTEGER_MULTIPLY_HH_
