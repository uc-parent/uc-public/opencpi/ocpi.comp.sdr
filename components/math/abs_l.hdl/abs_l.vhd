-- HDL Implementation of abs_l.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is
  constant min_int_c : signed(input_in.data'length-1 downto 0) :=
    to_signed(-2147483648, input_in.data'length);
begin
  -- take input data whenever the output is ready
  input_out.take <= output_in.ready;

  -- This component runs within a single clock cycle, with the output
  -- being registered in the wrapper around the worker.

  data_p : process (input_in.data, input_in.opcode)
  begin
    if input_in.opcode = long_timed_sample_sample_op_e then
      if signed(input_in.data) = min_int_c then
        output_out.data <= (others => '0');
      else
        output_out.data <= std_logic_vector(abs(signed(input_in.data)));
      end if;
    else
      output_out.data <= input_in.data;
    end if;
  end process;

  output_out.valid       <= input_in.valid;
  output_out.give        <= input_in.ready;
  output_out.opcode      <= input_in.opcode;
  output_out.byte_enable <= input_in.byte_enable;
  output_out.eom         <= input_in.eom;
end rtl;
