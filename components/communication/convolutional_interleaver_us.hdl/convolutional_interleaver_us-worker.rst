.. convolutional_interleaver_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _convolutional_interleaver_us-HDL-worker:


``convolutional_interleaver_us`` HDL Worker
===========================================
Implementation of a convolutional interleaver.

Detail
------
The convolutional interleaver worker uses shift registers to implement the delay lines.
The shift registers were written to infer an SRL.

The worker allows for setting a ``port_data_width`` parameter property to define the widths of the input and output ports.

References:

 * https://en.wikipedia.org/wiki/Burst_error-correcting_code#Interleaved_codes

 * https://www.etsi.org/deliver/etsi_en/300400_300499/300421/01.01.02_60/en_300421v010102p.pdf

.. ocpi_documentation_worker::

Worker Ports
~~~~~~~~~~~~

Inputs:

* ``input``: Size defined by ``port_data_width``.

  * Type: ``StreamInterface``

  * Protocol: ``ushort_timed_sample-prot``

  * Clock: ``output``

  * Data width: ``port_data_width``

Outputs:

* ``output``: Size defined by ``port_data_width``

  * Type: ``StreamInterface``

  * Protocol: ``ushort_timed_sample-prot``

  * Worker EOF: ``false``

  * InsertEOM: ``true``

  * ClockDirection: ``in``

  * Data width: ``port_data_width``

Utilisation
-----------
.. ocpi_documentation_utilization::
