-- Shift Reg
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------
-- Shift Reg
-------------------------------------------------------------------------------
--
-- Description:
--
-- Implements a shift register that is writtern to infer an SRL.
-- The srl_fifo primitive in the core/hdl/primitives/util is not
-- used because it does not have the behavior that is desired for
-- convolutional interleaver/de-interleaver delay lines.

library ieee;
use ieee.std_logic_1164.all, ieee.numeric_std.all;

entity shift_reg is
  generic (
    WIDTH : natural := 16;
    DEPTH : natural := 16);
  port (
    clk  : in  std_logic;
    en   : in  std_logic;
    din  : in  std_logic_vector(WIDTH-1 downto 0);
    dout : out std_logic_vector(WIDTH-1 downto 0));
end entity shift_reg;

architecture rtl of shift_reg is

  type regs_t is array (0 to DEPTH-1) of std_logic_vector(WIDTH-1 downto 0);
  signal regs : regs_t;

begin

  process (clk)
  begin
    if rising_edge(clk) then
      if en = '1' then
        for i in 1 to DEPTH-1 loop
          regs(i) <= regs(i-1);
        end loop;
        regs(0) <= din;
      end if;
    end if;
  end process;

  dout <= regs(DEPTH-1);

end rtl;
