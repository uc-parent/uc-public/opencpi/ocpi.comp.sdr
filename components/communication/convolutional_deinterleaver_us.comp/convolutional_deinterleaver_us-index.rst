.. convolutional_deinterleaver_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _convolutional_deinterleaver_us:


Convolutional De-interleaver (``convolutional_deinterleaver_us``)
=================================================================
Takes in symbols that are of the data type, unsigned short, and undoes rearrangement done by a convolutional interleaver.

Tested platforms include ``xsim``, ``e31x``.

Design
------
The convolutional de-interleaver component is used in a receive path to undue the rearranging of symbols
done by a convolutional interleaver.

The convolutional de-interleaver is constructed by having N different delay lines with each line having a smaller delay than the previous line. Each new input symbol
is placed on a different delay line. The last delay line has 0 delay. The number of delay lines is defined as N and the depth of the delay line shift registers is defined as D.
N should be set to a value greater than 1.

The convolution de-interleaver should have the same parameter values as the convolutional interleaver used in the transmit path.

A block diagram representation of the implementation is given in :numref:`convolutional_deinterleaver_us-diagram`.

.. _convolutional_deinterleaver_us-diagram:

.. figure:: convolutional_deinterleaver_us.svg
   :alt: Block diagram outlining convolutional de-interleaver implementation.
   :align: center

   Block diagram outlining convolutional de-interleaver implementation.

Interface
---------
.. literalinclude:: ../specs/convolutional_deinterleaver_us-spec.xml
   :language: xml

Ports
~~~~~
.. ocpi_documentation_ports::

   input:  Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The convolutional de-interleaver de-interleaves data with sample opcodes only.

All other opcodes pass through this component without any effect..

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../convolutional_deinterleaver_us.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependencies for this worker are:

 * ``shift_reg.vhd``

 * ``convolutional_delay_lines.vhd``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ocpi.util.all;``

Limitations
-----------
Limitations of ``convolutional_deinterleaver_us`` are:

 * None.

Testing
-------
This component test suite uses the OpenCPI unit test framework. The unit test sends symbols that would have been interleaved by a convolutional interleaver. It is then verified to make sure that the symbols were de-interleaved correctly and that the data in the original order is recovered.

.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
