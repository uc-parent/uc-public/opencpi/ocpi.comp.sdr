#!/usr/bin/env python3

# Python implementation of symbol mapper block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of symbol mapper block for verification."""

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class SymbolMapper(ocpi_testing.Implementation):
    """The Symbol Mapper class."""

    def __init__(self, length, symbols):
        """Symbol Mapper.

        Args:
            length (int): Number of elements to expect in the `symbols` list.
            symbols (list): Values of symbols in output_stream.
        """
        super().__init__(length=length, symbols=symbols)

    def reset(self):
        """Re-initialises internal state."""
        pass

    def sample(self, values):
        """Processes the sample opcode, and generates output data.

        Args:
            values (list): Sample input values

        Returns:
            Formatted message for output containing one or more sample
            opcodes containing mark and space values.
        """
        output_values = [0] * len(values)
        for index, value in enumerate(values):
            if value >= self.length:
                output_values[index] = self.symbols[0]
            else:
                output_values[index] = self.symbols[value]

        max_message_length = ocpi_protocols.PROTOCOLS[
            "long_timed_sample"].max_sample_length
        messages = []
        for index in range(0, len(values), max_message_length):
            messages.append(
                {"opcode": "sample",
                 "data": output_values[index: index + max_message_length]})

        return self.output_formatter(messages)
