-- data_unpack_uc_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  function in_to_out_opcode(inop : in uchar_timed_sample_opcode_t) return bool_timed_sample_opcode_t is
  begin
    return bool_timed_sample_opcode_t'val(uchar_timed_sample_opcode_t'pos(inop));
  end function;

  -- Interface signals/registers
  signal taken              : std_logic;
  signal taken_r            : std_logic;
  signal input_data_valid   : std_logic;
  signal input_in_r         : worker_input_in_t;
  -- State machine signals
  type state_t is (idle_s, go_s);
  signal current_state      : state_t;
  -- Unpacker signals
  signal count              : unsigned(7 downto 0) := (others => '0');
  signal data_reg           : std_logic_vector(input_in.data'length - 1 downto 0);
  signal unpack_out         : std_logic_vector(output_out.data'length - 1 downto 0);
  -- Output interface signals
  signal data_eom           : std_logic;
  signal unpacker_out_valid : std_logic;

begin
  -----------------------------------------------------------------------------
  -- Interface signals
  -----------------------------------------------------------------------------
  -- Take input data whenever both the input and the output are ready.
  taken          <= '1' when output_in.ready = '1' and current_state = idle_s and input_in.ready = '1' else '0';
  input_out.take <= '1' when output_in.ready = '1' and current_state = idle_s                          else '0';

  -- Valid only when the input is valid and contains the "data" opcode.
  input_data_valid <= '1' when input_in.valid = '1' and input_in.opcode = uchar_timed_sample_sample_op_e else '0';

  -----------------------------------------------------------------------------
  -- Input register
  -----------------------------------------------------------------------------
  -- Stores registered version of input interface
  input_register_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        taken_r <= '0';
      else
        if output_in.ready = '1' then
          taken_r <= taken;
        end if;
        if taken = '1' then
          input_in_r <= input_in;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Unpack shifter state machine
  -----------------------------------------------------------------------------
  -- Input loaded on Idle, data shifted out on Go.
  state_machine_advance : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        current_state <= idle_s;
      else
        case current_state is
          when idle_s =>
            if output_in.ready = '1' and input_data_valid = '1' then
              current_state <= go_s;
              count         <= to_unsigned(input_in.data'length - 1, count'length);
              data_reg      <= input_in.data;
            end if;
          when go_s =>
            if output_in.ready = '1' then
              count <= count - 1;
              if count = 0 then
                current_state <= idle_s;
              end if;
              if props_in.msb_first = '1' then
                data_reg <= data_reg(input_in.data'length - 2 downto 0) & '0';
              else                      -- LSB first
                data_reg <= '0' & data_reg(input_in.data'length - 1 downto 1);
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

-- End of message flag when counter is at last data value.
  data_eom <= '1' when count = 0 and input_in_r.eom = '1' else '0';

  unpack_out(0) <= data_reg(input_in.data'length - 1) when props_in.msb_first = '1' else data_reg(0);

  unpack_out(output_out.data'length - 1 downto 1) <= (others => '0');

------------------------------------------------------------------------------
-- Output interface signals
------------------------------------------------------------------------------
-- Give whenever there is either data or an opcode to be passed through,
-- and the output is ready to accept data.
  output_out.give        <= taken_r;
  output_out.data        <= unpack_out      when current_state = go_s                                               else input_in_r.data;
  output_out.byte_enable <= (others => '1') when current_state = go_s                                               else input_in_r.byte_enable;
  output_out.eom         <= data_eom        when current_state = go_s                                               else input_in_r.eom;
  output_out.valid       <= '1'             when (taken_r = '1' or current_state = go_s) and input_in_r.valid = '1' else '0';
  output_out.opcode      <= in_to_out_opcode(input_in_r.opcode);

end rtl;
