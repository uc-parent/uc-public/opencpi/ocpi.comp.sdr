.. polyphase_clock_synchroniser_c documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords:


.. _polyphase_clock_synchroniser_c:


Polyphase Clock Synchroniser (``polyphase_clock_synchroniser_c``)
=================================================================

Introduction
------------

.. ocpi_documentation_include:: ../doc/polyphase_clock_synchroniser/introduction.inc

   |component_name|: polyphase_clock_synchroniser_c
   |include_dir|: ../doc/polyphase_clock_synchroniser/

Interface
---------
.. literalinclude:: ../specs/polyphase_clock_synchroniser_c-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
.. include:: ../doc/polyphase_clock_synchroniser/opcode_handling.inc

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   taps: The actual number of taps required is defined through the ``taps_per_branch * max_interpolation``. Tap values are in the range (-1, 1) and passed as fixed point values in S0.7 format (i.e. 1 sign bit and 7 fractional bits), and can be converted to float by dividing by :math:`2^7`.

   loop_coefficients: Structured as: [:math:`K_p`, :math:`K_i`, :math:`K_{nco}`]. These are actually fixed point values in S5.2 format (i.e. 1 sign bit, 5 integer bits and 2 fractional bits), and can be converted to float by dividing by :math:`2^2 = 4`


Rounding Methods
~~~~~~~~~~~~~~~~
.. include:: ../doc/polyphase_clock_synchroniser/rounding.inc


Implementations
~~~~~~~~~~~~~~~

.. ocpi_documentation_implementations:: ../polyphase_clock_synchroniser_c.rcc ../polyphase_clock_synchroniser_c.hdl

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
.. include:: ../doc/polyphase_clock_synchroniser/dependencies.inc

Limitations
-----------
.. include:: ../doc/polyphase_clock_synchroniser/limitations_real.inc

Testing
-------
.. ocpi_documentation_test_result_summary::
