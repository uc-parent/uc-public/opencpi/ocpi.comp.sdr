// RCC implementation of pattern_detector_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "pattern_detector_b-worker.hh"

using namespace OCPI::RCC;
using namespace Pattern_detector_bWorkerTypes;

class Pattern_detector_bWorker : public Pattern_detector_bWorkerBase {
  uint64_t pattern = 0;
  uint64_t bitmask = 0;
  uint64_t shift_register = 0;
  uint64_t result = 0;
  size_t offset = 0;
  bool insert_discontinuity = 0;

  RCCResult pattern_written() {
    pattern = static_cast<uint64_t>(properties().pattern);
    return RCC_OK;
  }

  RCCResult bitmask_written() {
    bitmask = static_cast<uint64_t>(properties().bitmask);
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (insert_discontinuity == 1) {
      // Insert a discontinuity
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      insert_discontinuity = 0;
      return RCC_OK;
    }
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      // Normal operating case, search input samples for pattern
      size_t length = input.sample().data().size();
      size_t samples_remaining = length - offset;
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
      // Add offset to pointer to continue through input message after a match
      inData = inData + offset;
      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      // Resize to compensate for possible offset
      output.sample().data().resize(samples_remaining);

      for (size_t index = 0; index < samples_remaining; index++) {
        // Add new bit to LSB of shift register
        shift_register = shift_register << 1;
        shift_register += *inData;
        // Pass through data
        *outData = *inData;
        inData++;
        outData++;
        offset++;
        // Check for match - (shift register AND mask) XNOR (pattern AND mask)
        // When the mask is all zeros this disables the check and so no match
        // can occur.
        result = ~((shift_register & bitmask) ^ (pattern & bitmask));
        if (result == 0xFFFFFFFFFFFFFFFF && bitmask != 0x0000000000000000) {
          // We have a match, insert discontinuity next time worker runs
          insert_discontinuity = 1;
          // Resize output buffer as we are only sending pre-match samples
          output.sample().data().resize(index + 1);
          output.advance();
          return RCC_OK;
        }
      }
      // Finished going through each sample, get new input message
      offset = 0;
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode and reset shift register
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      shift_register = 0;
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Remove discontinuity opcode from stream and reset shift register
      shift_register = 0;
      input.advance();
      return RCC_OK;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

PATTERN_DETECTOR_B_START_INFO

PATTERN_DETECTOR_B_END_INFO
