// RCC implementation of crc_generator_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "crc_generator_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Crc_generator_bWorkerTypes;

const uint8_t max_polynomial_order = 64;

class Crc_generator_bWorker : public Crc_generator_bWorkerBase {
  const uint8_t *polynomial_taps = CRC_GENERATOR_B_POLYNOMIAL_TAPS;
  const RCCBoolean output_reflect = CRC_GENERATOR_B_OUTPUT_REFLECT;

  uint8_t crc_size = 0;
  bool polynomial[max_polynomial_order] = {0};
  bool crc[max_polynomial_order] = {0};
  bool seed[max_polynomial_order] = {0};
  bool final_xor[max_polynomial_order] = {0};

  uint8_t crc_buffer_index = 0;  // A cyclic buffer will be used for the CRC
                                 // calculation, this will index the position
                                 // of the least significant bit in the buffer
  bool crc_sent = false;

  RCCResult start() {
    // The first value in polynomial_taps (by documentation definition) is the
    // CRC size
    crc_size = this->polynomial_taps[0];

    for (uint8_t i = 1; i < max_polynomial_order; i++) {
      this->polynomial[this->polynomial_taps[i]] = 1;
    }
    return RCC_OK;
  }

  RCCResult seed_written() {
    for (uint8_t i = 0; i < max_polynomial_order; i++) {
      this->seed[i] = (properties().seed >> i) & 1;
    }
    memcpy(crc, seed, max_polynomial_order);
    return RCC_OK;
  }

  RCCResult final_xor_written() {
    for (uint8_t i = 0; i < max_polynomial_order; i++) {
      this->final_xor[i] = (properties().final_xor >> i) & 1;
    }
    return RCC_OK;
  }

  void calculate_crc(const bool *input_data, const size_t length) {
    for (size_t sample = 0; sample < length; sample++) {
      // A cyclic buffer is used to store the shift register values of the CRC
      // calculation - this saved copying each value to a new position in the
      // buffer for each input value, since the buffer starter index can be
      // updated and this has the same effect to copying the values
      bool roll_round_bit = crc[(crc_buffer_index + crc_size - 1) % crc_size];
      for (uint8_t i = 0; i < (crc_size - 1); i++) {
        crc[(crc_buffer_index + i) % crc_size] =
            (crc[(crc_buffer_index + i) % crc_size] !=
             (polynomial[i + 1] && (roll_round_bit != *input_data)));
      }
      if (crc_buffer_index == 0) {
        crc_buffer_index = crc_size - 1;
      } else {
        crc_buffer_index--;
      }
      crc[crc_buffer_index] = roll_round_bit != *input_data;
      input_data++;
    }
  }

  void xor_buffer(bool *buffer) {
    for (uint8_t i = 0; i < crc_size; i++) {
      // As the internal CRC calculation, holds the CRC in reverse order to what
      // is required for the output, the XOR values need to be used in a reverse
      // mapping
      buffer[i] = buffer[i] != final_xor[crc_size - i - 1];
    }
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const bool *input_data =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *output_data =
          reinterpret_cast<bool *>(output.sample().data().data());

      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      // Input data passes thorought without modification - each value is a
      // single byte so length does not need a scale factor to get number of
      // bytes to copy.
      memcpy(output_data, input_data, length);

      calculate_crc(input_data, length);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      if (!this->crc_sent) {
        // Pass through CRC of data received since last flush or discontinuity
        // opcode
        bool *output_data =
            reinterpret_cast<bool *>(output.sample().data().data());
        output.sample().data().resize(crc_size);
        output.setOpCode(Bool_timed_sampleSample_OPERATION);

        // Internally the CRC is calculated in reverse order for what is needed
        // to be output, so reflect in the normal case to correct
        if (this->output_reflect) {
          for (uint8_t i = 0; i < crc_size; i++) {
            output_data[i] = crc[(crc_buffer_index + i) % crc_size];
          }
        } else {
          for (uint8_t i = 0; i < crc_size; i++) {
            output_data[crc_size - 1 - i] =
                crc[(crc_buffer_index + i) % crc_size];
          }
        }

        xor_buffer(output_data);

        output.advance();
        this->crc_sent = true;
        // Set CRC to initial value
        memcpy(crc, seed, crc_size);
        crc_buffer_index = 0;
        return RCC_OK;
      }
      // Pass through flush opcode
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      this->crc_sent = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      if (!this->crc_sent) {
        // Pass through crc for data received since last flush or discontinuity
        // opcode
        bool *output_data =
            reinterpret_cast<bool *>(output.sample().data().data());
        output.sample().data().resize(crc_size);
        output.setOpCode(Bool_timed_sampleSample_OPERATION);

        // Internally the CRC is calculated in reverse order for what is needed
        // to be output, so reflect in the normal case to correct
        if (this->output_reflect) {
          for (uint8_t i = 0; i < crc_size; i++) {
            output_data[i] = crc[(crc_buffer_index + i) % crc_size];
          }
        } else {
          for (uint8_t i = 0; i < crc_size; i++) {
            output_data[crc_size - 1 - i] =
                crc[(crc_buffer_index + i) % crc_size];
          }
        }

        xor_buffer(output_data);

        output.advance();
        this->crc_sent = true;
        // Set CRC to initial value
        memcpy(crc, seed, crc_size);
        crc_buffer_index = 0;
        return RCC_OK;
      }
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      this->crc_sent = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

CRC_GENERATOR_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CRC_GENERATOR_B_END_INFO
