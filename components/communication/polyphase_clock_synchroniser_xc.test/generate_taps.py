#!/usr/bin/env python3

# This file is used to generate taps for the unit tests.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generator for the Tap Property of the Polyphase Clock Synchroniser."""

import os
import sys
import scipy.signal as sig
import numpy as np

number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
interpolation_factor = int(os.environ["OCPI_TEST_interpolation_factor"])

# Parse arguments
taps_type = sys.argv[1]
taps_fractional_bits = int(sys.argv[2])
output_path = sys.argv[3]

taps = (interpolation_factor * sig.firwin(
    number_of_taps,
    1/(interpolation_factor),
    window=("kaiser", 5.0)))

# Scale to integer format
integer_taps = [int((x/max(taps))*(pow(2, taps_fractional_bits)-2))
                for x in taps]

assert (np.all(np.array(integer_taps) < 2**taps_fractional_bits)
        and np.all(np.array(integer_taps) > -2**taps_fractional_bits)), (
    f"Filter taps goes beyond the range allowed for {taps_type} "
    f"({-(2**taps_fractional_bits)-1}-{(2**taps_fractional_bits)-1})"
    f"\nTaps: {integer_taps}")

with open(output_path, "w") as f:
    for tap in integer_taps:
        f.write(f"{tap}\n")
