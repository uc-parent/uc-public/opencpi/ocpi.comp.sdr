#!/usr/bin/env python3

# Runs on correct output for large_pattern_detector_b implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verifies the output for large_pattern_detector_b implementation."""

import os
import sys
import pathlib

import opencpi.ocpi_testing as ocpi_testing

from large_pattern_detector import LargePatternDetector

pattern_length_property = int(os.environ.get("OCPI_TEST_pattern_length"))
metadata_value_property = int(os.environ.get("OCPI_TEST_metadata_value"))
metadata_id_property = int(os.environ.get("OCPI_TEST_metadata_id"))
pattern_property = [(value.lower() == "true") for value
                    in os.environ.get("OCPI_TEST_pattern").split(",")]
bitmask_property = [(value.lower() == "true") for value
                    in os.environ.get("OCPI_TEST_bitmask").split(",")]
subcase = os.environ["OCPI_TEST_subcase"]
pattern_expected = (os.environ["OCPI_TEST_pattern_expected"].lower() == "true")

# Ensure pattern_property's length is equal to pattern_length_property
# (OCPI will trim multiple trailing zeros - we need to put them back)
pattern_property += [False] * (pattern_length_property - len(pattern_property))
# Ensure bitmask_property's length is equal to pattern_length_property
# (OCPI will trim multiple trailing zeros - we need to put them back)
bitmask_property += [False] * (pattern_length_property - len(bitmask_property))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

large_pattern_detector_implementation = LargePatternDetector(
    pattern_length_property,
    pattern_property,
    bitmask_property,
    metadata_value_property,
    metadata_id_property)

test_id = ocpi_testing.get_test_case()
test_case, test_subcase = ocpi_testing.id_to_case(test_id)
# No runtime variables has the worker under test so use the output data file
# name
case_worker_port = pathlib.Path(output_file_path).stem.split(".")
worker = f"{case_worker_port[-3]}.{case_worker_port[-2]}"
port = case_worker_port[-1]

verifier = ocpi_testing.Verifier(large_pattern_detector_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["bool_timed_sample"], ["equal"])
if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)

# Most tests should have a pattern to be detected.
if subcase == "with_flush" or subcase == "with_discontinuity":
    # No match expected if flush or discontinuity occurred
    # in the middle of the pattern
    pattern_expected = False
elif subcase == "shortest":
    # No match expected with shortest sample size
    pattern_expected = False

# Fail the test if no pattern was detected and we are expecting one to be found
if pattern_expected and not large_pattern_detector_implementation.pattern_detected:
    fail_message = "Failed to detect pattern"
    verifier.test_failed(worker, port, test_case, test_subcase, fail_message)
    sys.exit(1)
