<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <property test="true" name="pattern_expected" type="bool" value="true"/>
  <!-- Case 00: Typical case - short pattern lengths -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
  </case>
  <!-- Case 01: Pattern Length Property - test all pattern lengths
                not covered in typical test -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" values="8,256,512,1024,2048,4096,8192,16384"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" value="pattern_length"/>
  </case>
  <!-- Case 02: Metadata Value Property  -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" values="0,267,2451877210,18446744073709551615"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" value="metadata_value"/>
  </case>
  <!-- Case 03: Metadata ID Property  -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" values="0,267,2451877210"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" value="metadata_id"/>
  </case>
  <!-- Case 04: Pattern Property: Pattern split across more than one
                sample opcode message -->
  <!-- LINT EXCEPTION: testxml_001: 2: Custom generator case -->
  <case>
    <input port="input" script="generate.py --case pattern_cross_messages" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" values="sample_only,with_time,with_sample_interval,with_flush,with_discontinuity,with_metadata"/>
  </case>
  <!-- Case 05: Bitmask Property: Pattern matches in masked bits -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask"
      generate="generate_pattern.py --pattern='0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,
        1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,
        1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1'"/>
    <property name="subcase" value="bitmask"/>
  </case>
  <!-- Case 06: Bitmask Property: Pattern not matching in masked bits -->
  <!-- LINT EXCEPTION: testxml_001: 2: Custom generator case -->
  <case>
    <input port="input" script="generate.py --case ignored_pattern_bits" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask"
      generate="generate_pattern.py --pattern='0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,
        1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,
        1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1'"/>
  </case>
  <!-- Case 07: sample -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" values="all_zero,all_maximum"/>
  </case>
  <!-- Case 08: input_stressing -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
  </case>
  <!-- Case 09: message_size -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 10: time -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 11: sample_interval -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 12: flush -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 13: discontinuity -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 14: metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" value="128"/>
    <property name="metadata_value" value="0"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=all-true"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 15: soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="pattern_length" values="16,32,1024"/>
    <property name="metadata_value" values="2,7510452893,18446744073709551615"/>
    <property name="metadata_id" value="0"/>
    <property name="pattern" generate="generate_pattern.py --pattern-type=random --seed 30"/>
    <property name="bitmask" generate="generate_pattern.py --pattern-type=random --seed 5"/>
    <property name="pattern_expected" value="false"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
