#!/usr/bin/env python3

# Python implementation of large_pattern detector component for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of large pattern detector for verification."""

import opencpi.ocpi_testing as ocpi_testing
import numpy as np


class LargePatternDetector(ocpi_testing.Implementation):
    """Python implementation of large pattern detector for verification."""

    def __init__(self, pattern_length, pattern, bitmask, metadata_value, metadata_id):
        """Initialise a Large Pattern Detector.

        Args:
            pattern_length (int): The length of the pattern.
            pattern (list of bool): The pattern to look for in the data stream.
            bitmask (list of bool): The bit-mask to use when looking for the pattern.
            metadata_value (int): The value to use in the Metadata opcode added when a pattern is detected.
            metadata_id (int): The id to use in the Metadata opcode added when a pattern is detected.
        """
        super().__init__(pattern_length=pattern_length,
                         pattern=pattern,
                         bitmask=bitmask,
                         metadata_value=metadata_value,
                         metadata_id=metadata_id)

        self.memory = np.zeros(pattern_length, dtype=np.bool_)
        self.output = []
        self.pattern_length = pattern_length
        self.pattern = np.asarray(pattern)
        self.bitmask = np.asarray(bitmask)
        self.metadata_value = metadata_value
        self.metadata_id = metadata_id
        self.pattern_detected = False
        self.bits_received = 0

    def reset(self):
        """Reset to initial state."""
        self.memory = np.zeros(self.pattern_length, dtype=np.bool_)
        self.bits_received = 0

    def sample(self, values):
        """Handle an incoming sample opcode message.

        Args:
            values (list): Incoming samples on the input port.

        Returns:
            Formatted messages.
        """
        stream_index = 0
        output_list = []
        for position, value in enumerate(values):
            # Shift and insert new value at 'lsb'
            self.memory = np.roll(self.memory, -1)
            self.memory[self.pattern_length - 1] = value
            if self.bits_received < self.pattern_length:
                self.bits_received += 1
            # Pattern matching is disabled if bitmask is all zeros or
            # if not received sufficient bits since reset
            if np.any(self.bitmask) and self.bits_received >= self.pattern_length:
                # Bitwise logic: (pattern AND mask) XOR (memory AND mask)
                pattern_match = np.logical_xor(
                    np.logical_and(self.memory, self.bitmask),
                    np.logical_and(self.pattern, self.bitmask))
                if not np.any(pattern_match):
                    self.pattern_detected = True
                    # Add previous samples to output and insert metadata
                    output_list.append({
                        "opcode": "sample",
                        "data": values[stream_index:position + 1]})
                    output_list.append({
                        "opcode": "metadata",
                        "data": {
                            "id": self.metadata_id,
                            "value": self.metadata_value}})
                    stream_index = position + 1

        if stream_index < len(values):
            # If match does not occur at the end of the input data, or no match
            # is found, then there is still input data to be appended before
            # the method ends
            output_list.append({"opcode": "sample",
                                "data": values[stream_index:]})

        return self.output_formatter(output_list)

    def discontinuity(self, _input):
        """Handle a discontinuity opcode message.

        Resets back to initial state and passes the opcode through.

        Args:
            _input: Ignored data from the input port.

        Returns:
            Formatted output opcode messages.
        """
        self.reset()
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def flush(self, _input):
        """Handle a flush opcode message.

        Resets back to initial state and passes the opcode through.

        Args:
            _input: Ignored data from the input port.

        Returns:
            Formatted output opcode messages.
        """
        self.reset()
        return self.output_formatter(
            [{"opcode": "flush", "data": None}])
