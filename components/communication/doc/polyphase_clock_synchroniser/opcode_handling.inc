.. polyphase_clock_synchroniser_opcode_handling

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

* Time

   * Incoming: Resynchronises the internal input sample time.

   * Outgoing: This is used in conjunction with the Interpolator tap selection to calculate the outputted symbol time. Note that a change in Sample_Interval might have a delay before the correlating change is made to the outgoing time. Note also that if a Sample_Interval has not been received, the time for the next outputted symbol cannot be calculated, so the time is just passed straight through.

* Sample_Interval

   * Incoming: Value is stored and used in the calculation of the input sample time counter (as detailed above).

   * Outgoing: Sample Interval is set to the incoming sample interval multiplied by 2 (i.e. the data rate is halved).

* Flush

   * Zeros are fed in for a length of `taps_per_branch` (any produced samples are outputted).

   * Then, the Flush is forwarded to output.

* Discontinuity

   * Forwarded to output.

   * The Sample RAM, Accumulator and Loop filter are reset.

* Metadata

   * Forwarded to output.

* Samples

   * Incoming: Samples are consumed, leading to the internal counter and buffers to step on each sample.

   * Outgoing: Symbols are produced at half the rate of the input data stream (i.e. the data is decimated by 2).
