.. polyphase_clock_synchroniser_design

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

A symbol clock synchroniser produces symbols from a stream of samples. The most basic clock synchroniser works by picking a single sample out of the stream at the correct symbol rate (this ratio is known as the samples per symbol, or SPS). The problem with this simple approach is knowing whether the chosen sample is at the optimal location within the sample stream. This problem is shown in :numref:`|component_name|-symbol-sampling-points-diagram` below. The orange line shows the best sampling location, however if the stream was just down-sampled by SPS then you might end up actually sampling at the grey location. If the signal to noise ratio (SNR) is good, then this might be acceptable. However as the SNR drops, the chances of getting a bit error when decoding in this bad location increases.

.. _|component_name|-symbol-sampling-points-diagram:

.. figure:: |include_dir|symbol_sampling_points.svg
   :alt: Symbol Sampling Points.
   :align: center

   Symbol Sampling Points

In order to allow the selection of the best sampling point there needs to be some method of determining where the peaks and the zeros are in the data. At the most basic this can be achieved through a peak detector running over the SPS number of points. However, this is only a useful method if there are a high number of samples per symbol. If, however, there are only a few samples per symbol, it is necessary to instead interpolate first to allow this selection of an optimum point.

.. _|component_name|-interpolated-symbol-sampling-points-diagram:

.. figure:: |include_dir|interpolated_symbol_sampling_points.svg
   :alt: Interpolated Symbol Sampling Points.
   :align: center

   Interpolated Symbol Sampling Points

The interpolation allows the selection of the optimum points, where the number of samples per symbol is too low to rely on a direct down-sampling. The interpolation also is preferable to down-sampling because the additional low pass filter used within the interpolation filter can be shaped to the samples per symbol which will provide a processing gain compared to a direct down-sample.

Design
------

The polyphase symbol clock synchroniser attempts to recover symbol timing for an incoming data stream. Symbols are output from the component.

The symbol synchronisation is undertaken through the stages shown in :numref:`|component_name|-synchronisation-stages-diagram` below. This block is intended to detect and synchronise to a symbol stream for simple zero crossing modulation schemes (i.e. FSK, PSK).

.. _|component_name|-synchronisation-stages-diagram:

.. figure:: |include_dir|synchronisation_stages.svg
   :alt: Synchronisation Stages.
   :align: center

   Synchronisation Stages

* :math:`x(n)` defines the incoming (unsynchronised) symbol stream (at 2x regenerated sampling period)

* :math:`y(m)` defines the outgoing symbol data (at regenerated sampling period)

* :math:`e(m)` defines the outgoing calculated symbol error (at 2x regenerated sampling period)

**Each of these stages are detailed below.**

The implementation handles bandwidth limited signals (i.e. that are known to have 2 incoming samples for a single outgoing symbol).

The timing error detection uses a Gardner zero crossing detection method; this is suited to lower orders of modulation.


Notes
~~~~~

* The incoming data and the polyphase interpolation filter are assumed to be run at 2x the desired output symbol rate. This is to prevent ambiguity if the previous decimating filter / ADC was badly matched to the filter.

* The down sample by 2 is a simple 1 in 2 sample selection (i.e. no low pass filter present) - this is acceptable as every other sample should be the zero crossing point (as needed for the Gardner loop).

* Interpolation filter selection and loop filter should be free-running to allow for n-FSK functionality (i.e. if no zero crossing is detected then the error is ignored as opposed to being considered maximal).

* The selection of sub symbol may undergo a step change due to clock synchronisation.  This is limited to only a single step per loop cycle.

* For FSK modulation, matched filtering does not really perform as desired (zero crossing RRC can lead to the need for quadrature modulation methods). Therefore, a Gaussian transmit pulse shaping will show better performance in a real only demodulator.

References
~~~~~~~~~~

1. Symbol Synchronization for SDR Using a Polyphase Filterbank Based on an FPGA P. Fiala and R. Linhart (2015)

2. Multirate Digital Filters for Symbol Timing Synchronization in Software Defined Radios F. Harris, M. Rice (2001)

3. Samples to Digital Symbols \[GRCON2017\] A Walls (2017)

.. spelling:word-list::
   Filterbank
   Fiala
   Linhart
   Multirate

Combined Matching and Polyphase Interpolation Filter (Real)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The interpolation structure used within this component consists of a single multiply-accumulate (MAC) unit. This provides a resource efficient method, but limits the maximum possible throughput. As up to 2 polyphase branches might be calculated (if sample stuffing is occurring), the input throughput should be low enough that :math:`2 \times \texttt{taps_per_branch}` multiply-accumulate calculations can occur for each input sample.

The tap values and the number of taps to use is run-time settable, however a hard limit is defined as well to limit the amount of memory required.

.. _|component_name|-polyphase-interpolation-filter-diagram:

.. figure:: |include_dir|polyphase_interpolation_filter.svg
   :alt: Polyphase Interpolation Filter.
   :align: center

   Polyphase Interpolation Filter

Design
~~~~~~

New samples are inserted within the sample RAM, which acts as a simple circular buffer of the last N samples (where N is equal to the FIR tap count divided by the number of polyphase branches).

When a new sample is inserted...

   1. The relevant polyphase branch b0 tap is multiplied by the new sample.

   2. The modulo counter then jumps to the next tap for that branch (a jump of the up sampling ratio), and then multiplies with the z-1 incoming sample.

   3. The modulo counter continues until all the taps for that branch have been through the MAC. The output is then registered and the output Sample Valid line set High.

   4. The accumulator is then internally reset, awaiting the next incoming sample.


Gardner loop detector
~~~~~~~~~~~~~~~~~~~~~

The Timing Error detector implemented is a Gardner zero crossing detector. This works by comparing the zero crossing location against the difference between two symbols. Therefore an incoming sample rate of at least twice that of the output symbol rate is required.

The Gardner loop has the form: Error = Prompt x (Late - Early)  This is shown within :numref:`|component_name|-early_late_samples-diagram` with the :math:`n_3` value being used for the Prompt variable. However, as the data stream should be centred around zero, if shifted by one position, i.e. using :math:`n_3`, :math:`n_4` and :math:`n_5`, the timing error detector would produce an error with the opposite sign.

As can be seen in :numref:`|component_name|-error-variation-with-varying-sample-phase-diagram`, under a signal varying in the range (-1, 1) the error limits are also bounded to the same region (with the greatest error occurring at 45 degrees lag/lead - as there are 4 samples per cycle).

.. _|component_name|-early_late_samples-diagram:

.. figure:: |include_dir|symbol_location_for_early_late_samples.svg
   :alt: Gardner Loop Detector.
   :align: center

   Symbol location for Early/Late samples

.. _|component_name|-error-variation-with-varying-sample-phase-diagram:

.. figure:: |include_dir|error_variation_with_varying_sample_phase.svg
   :alt: Gardner Loop Detector.
   :align: center

   Error Variation with varying sample phase

Notes
~~~~~

1. A Gardner loop is sensitive to any DC bias upon the incoming samples (caused by the non-differentiated term).

2. If there is no hard decision on the incoming samples (i.e. there is a magnitude as opposed to being exactly -1 or 1), then additional timing errors can be introduced by a run of the same symbol. To get around this a sign comparison of all three terms is used to ensure only zero crossings are used.

3. If the input energy is varying with time (likely with an FM phase differential discriminator), then there should be an AGC block, stabilising this data. Without that, the output from the loop will not have the same proportional gain as is expected by the loop filter/PLL.

4. The coarse estimation of the number of samples per symbol is not part of this element, only the fine correction undertaken naturally through the timing estimation.


Loop Filter
~~~~~~~~~~~

The realistic output of the timing error detector is not a smooth curve as defined above, but instead a choppy set of values (caused by sampling clock errors, and slight frequency misalignment). As such a Phase Locked Loop (PLL) loop filter is desired to help smooth the variation seen. This should be a second order PI (Proportional-Integral) loop filter, as there is a need for phase recovery (sub sample alignment using the interpolator), as well as frequency recovery (clock misalignment, leading to drift, using the interpolator, and incoming sample stuff/skip).

The loop filter has the following equations:

.. math::
    E_i = (\texttt{ted} \times K_i) + E_i

.. math::
    Err = E_i + (\texttt{ted} \times K_p)

* :math:`\texttt{ted}` is the timing error detector output, (multiplied by any scaling required)

* :math:`K_p` is the proportional filter constant

* :math:`K_i` is the integrator filter constant

* :math:`E_i` is the integral error value

* :math:`Err` is the resultant error value

This is shown below as the start of the tap selection logic.


Sample and Tap Selection
~~~~~~~~~~~~~~~~~~~~~~~~

The tap and sample selection is shown in :numref:`|component_name|-sample-and-tap-selection-diagram` below. The output of the Timing Error Detector is filtered, then accumulated. This accumulated value provides the phase error. A wrapping detector allows for small frequency adjustment.

.. _|component_name|-sample-and-tap-selection-diagram:

.. figure:: |include_dir|sample_and_tap_selection.svg
   :alt: Sample and Tap Selection.
   :align: center

   Sample and Tap Selection

The Gardner timing error detector produces an output which has a magnitude related to the distance of the sample away from the zero crossing. As this value is used to correct the sample timing error, this value is negated.

The output from this timing error could lead to the zero crossing and actual usable samples being the wrong way round. To help detect this, the output symbol forwarded is the symbol with the highest magnitude of the two.

This symbol selection method can handle phase offsets from any sampling phase and small frequency errors (i.e. less than 100 ppm). However, this will not correct for a coarse frequency error, without an additional output sample skip/stuff mechanism.

.. _|component_name|-phase-correction-with-small-initial-symbol-phase-offset-diagram:

.. figure:: |include_dir|phase_correction_with_small_initial_symbol_phase_offset.svg
   :alt: Phase correction with small initial symbol phase offset.
   :align: center

   Phase correction with small initial symbol phase offset

.. _|component_name|-phase-correction-with-large-initial-symbol-offset-diagram:

.. figure:: |include_dir|phase_correction_with_large_initial_symbol_offset.svg
   :alt: Phase correction with large initial symbol offset.
   :align: center

   Phase correction with large initial symbol offset

.. _|component_name|-frequency-correction-diagram:

.. figure:: |include_dir|frequency_correction.svg
   :alt: Frequency Correction.
   :align: center

   Frequency Correction (note ever changing branch)

.. _|component_name|-symbol-skipping-(145)-diagram:

.. figure:: |include_dir|symbol_skipping_145.svg
   :alt: Symbol Skipping (145).
   :align: center

   Symbol Skipping (145)

.. _|component_name|-symbol-stuffing-(162)-diagram:

.. figure:: |include_dir|symbol_stuffing_162.svg
   :alt: Symbol Stuffing (162).
   :align: center

   Symbol Stuffing (162)
