-- bit_stuffer_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.bit_stuffer;

architecture rtl of worker is

  constant consecutive_ones_c : integer := to_integer(consecutive_ones);

  signal input_interface : worker_input_in_t;
  signal take            : std_logic;
  signal taken           : std_logic;
  signal get_next_value  : std_logic;
  signal valid_data      : std_logic;
  signal discontinuity   : std_logic;
  signal eom_d           : std_logic;
  signal reset           : std_logic;
  signal all_ones        : std_logic;
  signal bit_stuff       : std_logic;
  signal data_eom        : std_logic;

begin

  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready
  flush_insert_i : entity work.boolean_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  take <= output_in.ready and get_next_value;

  -- Data taken when input and output are ready
  taken <= take and input_interface.ready;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_interface.valid = '1' and
                input_interface.opcode = bool_timed_sample_sample_op_e else '0';

  -- Signal for discontinuity opcode
  discontinuity <= '1' when taken = '1' and
                   input_interface.opcode = bool_timed_sample_discontinuity_op_e else '0';

  -- Register eom flag for sample data
  eom_flag_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        eom_d <= '0';
      elsif valid_data = '1' then
        eom_d <= input_interface.eom;
      end if;
    end if;
  end process;

  -- Bit stuffer reset signal
  reset <= ctl_in.reset or discontinuity;

  -- Bit stuffer implementation
  bit_stuffer_i : bit_stuffer
    generic map (
      consecutive_ones_g => consecutive_ones_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => reset,
      enable         => output_in.ready,
      ready          => get_next_value,
      all_ones       => all_ones,
      data_valid_in  => valid_data,
      data_in        => input_interface.data(0),
      data_valid_out => open,
      data_out       => open
      );

  -- When the last input was the EOM, hold the EOM flag until the last sample
  bit_stuff <= all_ones and valid_data and input_interface.data(0);
  data_eom  <= (not get_next_value and eom_d) or
               (get_next_value and input_interface.eom and not bit_stuff);

  -- Streaming interface output
  output_mux_p : process(taken, valid_data, input_interface, output_in.ready,
                         get_next_value, data_eom)
  begin
    if taken = '1' or valid_data = '1' then
      output_out.give        <= input_interface.ready;
      output_out.valid       <= input_interface.valid;
      output_out.byte_enable <= input_interface.byte_enable;
      output_out.opcode      <= input_interface.opcode;
      output_out.data        <= input_interface.data;
    else
      -- When inserting zeros
      output_out.give        <= '0';
      output_out.valid       <= output_in.ready and not get_next_value;
      output_out.byte_enable <= (others => '1');
      output_out.opcode      <= bool_timed_sample_sample_op_e;
      output_out.data        <= (others => '0');
    end if;
    if taken = '1' and valid_data = '0' then
      output_out.eom <= input_interface.eom;
    else
      output_out.eom <= data_eom;
    end if;
  end process;

end rtl;
