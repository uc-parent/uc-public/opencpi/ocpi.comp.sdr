-- HDL Implementation of PRBS Synchroniser
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.prbs_synchroniser;

architecture rtl of worker is

  constant delay_c           : integer := 1;
  constant lfsr_poly_order_c : integer := to_integer(polynomial_taps(0));

  signal lfsr_seed             : std_logic_vector(lfsr_poly_order_c - 1 downto 0);
  signal valid                 : std_logic;
  signal discontinuity         : std_logic;
  signal reset                 : std_logic;
  signal prbs_bit              : std_logic;
  signal output_ready          : std_logic;
  signal taken                 : std_logic;
  signal take_passthrough      : std_logic;
  signal data_out              : std_logic_vector(output_out.data'length - 1 downto 0);
  signal bits_checked_counter  : unsigned(props_out.bits_checked_counter'length - 1 downto 0);
  signal error_counter         : unsigned(props_out.error_counter'length - 1 downto 0);
  signal bits_received_counter : unsigned(props_out.bits_received_counter'length - 1 downto 0);
  signal sync_loss_counter     : unsigned(props_out.sync_loss_counter'length - 1 downto 0);

begin

  -- PRBS generator module held in reset when enable property is 0
  reset <= ctl_in.reset or (not props_in.enable) or discontinuity;

  -- Always take data when input port is ready, and output is ready or
  -- not connected.
  output_ready   <= output_in.ready or output_in.reset;
  taken          <= input_in.ready and output_ready;
  input_out.take <= output_ready;

  -- Only passthrough all input signals when enabled. When disabled, suppress
  -- sample opcode messages.
  take_passthrough <= '1' when props_in.enable = '1' or input_in.opcode /= bool_timed_sample_sample_op_e else '0';

  -- Only enable synchroniser when data is ready, valid, and of the correct
  -- opcode.
  valid <= '1' when output_ready = '1' and input_in.valid = '1' and input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- PRBS synchroniser reset on discontinuity
  discontinuity <= '1' when taken = '1' and input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';

  -- Data bit is LSB of input data interface
  prbs_bit <= input_in.data(0);

  ------------------------------------------------------------------------------
  -- Interface delay module
  ------------------------------------------------------------------------------
  -- Delays streaming interface signals to align with the delay introduced by
  -- the prbs_synchroniser module.
  interface_delay_i : entity work.boolean_to_character_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take_passthrough,
      input_in            => input_in,
      processed_stream_in => data_out,
      output_out          => output_out
      );

  ------------------------------------------------------------------------------
  -- PRBS Synchroniser
  ------------------------------------------------------------------------------
  prbs_sync_i : prbs_synchroniser
    generic map (
      lfsr_width_g => lfsr_poly_order_c,
      lfsr_tap_1_g => to_integer(polynomial_taps(1)),
      lfsr_tap_2_g => to_integer(polynomial_taps(2)),
      lfsr_tap_3_g => to_integer(polynomial_taps(3)),
      lfsr_tap_4_g => to_integer(polynomial_taps(4)),
      lfsr_tap_5_g => to_integer(polynomial_taps(5))
      )
    port map (
      clk                     => ctl_in.clk,
      reset                   => reset,
      clk_en                  => valid,
      invert_i                => props_in.invert_input,
      data_bit_i              => prbs_bit,
      check_period_i          => props_in.check_period,
      error_threshold_i       => props_in.error_threshold,
      bits_checked_counter_o  => bits_checked_counter,
      bit_error_counter_o     => error_counter,
      bits_received_counter_o => bits_received_counter,
      sync_loss_counter_o     => sync_loss_counter,
      data_o                  => data_out
      );

  ------------------------------------------------------------------------------
  -- Output counters
  ------------------------------------------------------------------------------
  -- Register the counter properties so that all counters can be captured at
  -- the same time.
  properties_reg_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        props_out.bits_checked_counter  <= (others => '0');
        props_out.error_counter         <= (others => '0');
        props_out.bits_received_counter <= (others => '0');
        props_out.sync_loss_counter     <= (others => '0');
      else
        if ctl_in.is_operating = '1' and props_in.counter_update = '1' then
          props_out.bits_checked_counter  <= bits_checked_counter;
          props_out.error_counter         <= error_counter;
          props_out.bits_received_counter <= bits_received_counter;
          props_out.sync_loss_counter     <= sync_loss_counter;
        end if;
      end if;
    end if;
  end process;

end rtl;
