// RCC implementation of data_unpack_uc_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "data_unpack_uc_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Data_unpack_uc_bWorkerTypes;

class Data_unpack_uc_bWorker : public Data_unpack_uc_bWorkerBase {
  bool msb_first = false;
  size_t input_position = 0;

  // Notification that msb_first property has been written
  RCCResult msb_first_written() {
    this->msb_first = properties().msb_first;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Uchar_timed_sampleSample_OPERATION) {
      const uint16_t output_size = DATA_UNPACK_UC_B_OCPI_MAX_BYTES_OUTPUT;
      size_t length = input.sample().data().size();
      const uint8_t *inData =
          reinterpret_cast<const uint8_t *>(input.sample().data().data()) +
          this->input_position;
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());

      output.setOpCode(Bool_timed_sampleSample_OPERATION);

      // Pass through a zero length message
      if (length == 0) {
        output.sample().data().resize(0);
        return RCC_ADVANCE;
      }

      size_t output_position = 0;
      while (this->input_position < length && output_position < output_size) {
        for (uint8_t i = 0; i < 8; i++) {
          if (this->msb_first == true) {
            *outData = (*inData >> (7 - i)) & 0x01;
          } else {
            *outData = (*inData >> i) & 0x01;
          }
          output_position++;
          outData++;
        }
        this->input_position++;
        inData++;
      }

      output.sample().data().resize(output_position);

      if (this->input_position == length) {
        this->input_position = 0;
        return RCC_ADVANCE;
      } else {
        output.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Uchar_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

DATA_UNPACK_UC_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DATA_UNPACK_UC_B_END_INFO
