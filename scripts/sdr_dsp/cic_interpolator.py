#!/usr/bin/env python3

# Python implementation of cic_interpolator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Cascaded Integrator-Comb Interpolator implementation."""

from sdr_dsp.cic_comb import Comb
from sdr_dsp.cic_integrator import Integrator
import numpy as np


class Interpolate:
    """CIC Interpolator stage."""

    def __init__(self, upsample_factor):
        """CIC Interpolator stage.

        Args:
            upsample_factor (int): Number of samples to output for each input sample.
        """
        self._upsample_factor = upsample_factor

    def call(self, value):
        """Perform up-sampling.

        Adds zero samples to input.

        Args:
            value (numeric): Input sample.

        Returns:
            List of samples.
        """
        return [value] + ([0] * (self._upsample_factor - 1))


class CicInterpolatorSingleChannel():
    """Cascaded Integrator-Comb Interpolator class."""

    def __init__(self, cic_order, delay_factor, up_sample_factor,
                 output_scale_factor, cic_register_size, output_type=np.int16):
        """Initialise single channel interpolator.

        Args:
            cic_order (int): CIC filter order parameter.
            delay_factor (int): CIC filter delay parameter.
            up_sample_factor (int): Output sample count for each input sample.
            output_scale_factor (int): Number of bits to scale output sample value.
            cic_register_size (int): Size of CIC internal registers.
            output_type (type): Type (size) of output samples.
        """
        self.cic_order = cic_order
        self.delay_factor = delay_factor
        self.up_sample_factor = up_sample_factor
        self.output_scale_factor = output_scale_factor
        self.cic_register_size = cic_register_size
        self.output_type = output_type

        self._max_internal_value = 2**(self.cic_register_size - 1) - 1
        self._min_internal_value = -1 * (2**(self.cic_register_size - 1))

        self._integrators = [Integrator() for n in range(self.cic_order)]
        self._combs = [Comb(self.delay_factor) for n in range(self.cic_order)]
        self._interpolator = Interpolate(self.up_sample_factor)

    def reset(self):
        """Reset CIC filter buffers."""
        for n in range(0, self.cic_order):
            self._combs[n].reset()
            self._integrators[n].reset()

    def sample(self, values):
        """Processes an incoming sample messages.

        Args:
            values (list of int): Input sample values.

        Returns:
            List of upsampled-filtered-rounded output sample values.
        """
        output_values = []
        for value in values:
            # Apply each comb stage
            for n in range(0, self.cic_order):
                value = self._combs[n].call(value)
                # Match implementation bit width
                while value < self._min_internal_value:
                    value = value + (2**self.cic_register_size)
                while value > self._max_internal_value:
                    value = value - (2**self.cic_register_size)

            value = self._interpolator.call(value)

            # Apply each integrator stage
            for n in range(0, len(value)):
                for m in range(0, self.cic_order):
                    value[n] = self._integrators[m].call(value[n])
                    # Match implementation bit width
                    while value[n] < self._min_internal_value:
                        value[n] = value[n] + (2**self.cic_register_size)
                    while value[n] > self._max_internal_value:
                        value[n] = value[n] - (2**self.cic_register_size)

            # Do the output scaling
            output_values = output_values + list(value)

        rounded_output_values = []
        for value in (output_values):
            if self.output_scale_factor == 0:
                # Don't half-up round
                value = self.output_type(value)
                rounded_output_values.append(value)

            else:
                # Round half-up
                value = np.right_shift(
                    int(value), self.output_scale_factor - 1) + 1
                value = self.output_type(np.right_shift(value, 1))
                rounded_output_values.append(value)

        return rounded_output_values
