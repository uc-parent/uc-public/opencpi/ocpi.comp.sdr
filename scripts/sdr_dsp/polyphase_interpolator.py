#!/usr/bin/env python3

# Python implementation of polyphase interpolator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Polyphase Interpolator sample processing module."""

import decimal
import numpy as np
import logging

from sdr_dsp.scale_and_round import ScaleAndRound


class PolyphaseInterpolator:
    """A floating point version of a polyphase interpolator.

    This makes use of floating point numbers and expects inputs/outputs to
    be floating point too.
    """

    def __init__(self,
                 interpolation_factor,
                 taps=[],
                 rounding_type="truncate",
                 max_interpolation=8,
                 max_taps_per_branch=3,
                 logger_name="Interpolator",
                 logger_level="WARNING"):
        """Initialises a Polyphase Interpolator class.

        Args:
            interpolation_factor (int): The current upsampling factor.
            taps (list of float): The coefficient values.
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate".
            max_interpolation (int): The maximum upsampling factor.
            max_taps_per_branch (int): The maximum number of taps per branch.
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        self._interpolation_factor = interpolation_factor
        self._rounding_type = rounding_type
        self._max_interpolation = max_interpolation
        self._max_taps_per_branch = max_taps_per_branch

        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logger_level)
        self.logger.debug(
            "Polyphase Interpolator factor:{}, rounding mode: {}"
            .format(self._interpolation_factor, self._rounding_type))

        if len(taps) == 0:
            self.logger.warning(
                "Taps array should not be empty, when given samples")
            self._taps = [0] * (max_interpolation * max_taps_per_branch)
            self._taps_per_branch = int(max_taps_per_branch)
        else:
            self._taps_per_branch = int(np.ceil(
                float(len(taps)) / float(interpolation_factor)))
            self._taps = np.zeros(interpolation_factor * self._taps_per_branch)
            for n, t in enumerate(taps):
                self._taps[n] = t
            self.logger.debug("taps set ({} per branch): {}, length={}".format(
                self._taps_per_branch, self._taps, len(self._taps)))

        if len(taps) % interpolation_factor != 0:
            self.logger.info("taps have been padded with zeros")

        self.reset()

    def __repr__(self):
        """String representation of class object.

        Returns:
            str: Format <type>(interpolation factor, taps per branch).
        """
        return ("{}(N:{}, TapsPerBranch:{})"
                .format(type(self), self._interpolation_factor,
                        self._taps_per_branch))

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._data_sent = False
        self._branch = 0
        self._in_data_history = [0] * self._taps_per_branch

    @property
    def upsample(self):
        """Current Interpolation factor.

        Returns:
            int: Interpolation factor.
        """
        return self._interpolation_factor

    @upsample.setter
    def upsample(self, val):
        if val > self._max_interpolation:
            raise ValueError("ERROR: Requested upsample {} greater than max upsample {}"
                             .format(val, self._max_interpolation))
        self._interpolation_factor = val

    @property
    def data_sent(self):
        """Has data been sent?

        Returns:
            bool: True if data has been sent.
        """
        return self._data_sent

    @property
    def taps_per_branch(self):
        """Current taps per interpolation branch.

        Returns:
            int: Taps per Branch.
        """
        return self._taps_per_branch

    @taps_per_branch.setter
    def taps_per_branch(self, val):
        if val > self._max_taps_per_branch:
            raise ValueError("ERROR: Requested taps_per_branch {} greater than "
                             "max_taps_per_branch {}"
                             .format(val, self._max_taps_per_branch))
        self._taps_per_branch = int(val)

    @property
    def taps(self):
        """Taps used for the Interpolation filter.

        Returns:
            list: List of the taps.
        """
        return self._taps

    @taps.setter
    def taps(self, val):
        if len(val) > (self._max_taps_per_branch * self._max_interpolation):
            raise ValueError("ERROR: Requested taps {} greater than max_taps_per_branch "
                             "({}) and max_interpolation_factor ({})"
                             .format(val, self._max_taps_per_branch,
                                     self._max_interpolation))
        self._taps_per_branch = int(np.ceil(
            float(len(val)) / float(self._interpolation_factor)))
        self._taps = np.zeros(self._interpolation_factor
                              * self._taps_per_branch)
        for n, t in enumerate(val):
            self._taps[n] = t
        self.logger.debug("taps set to: {}, {} per branch".format(
            val, self._taps_per_branch))

    @property
    def branch(self):
        """Current branch being processed.

        Returns:
            int: Current branch within the interpolation filter.
        """
        return self._branch

    @branch.setter
    def branch(self, val):
        self._branch = val % (self._interpolation_factor)

    def update_input_history(self, data):
        """Update the input history with the passed sample.

        Args:
            data: The new sample to add to the history.
        """
        # Insert new sample, and trim to length
        length = len(self._in_data_history)
        self._in_data_history.insert(0, data)
        del self._in_data_history[length:]

    def step(self, sample):
        """Process a single value into the interpolator.

        Add the passed sample value to the input history, run one step of the
        interpolation to return the next sample value.

        Args:
            sample (various): The incoming sample value.

        Returns:
            The next interpolated sample value (various).
        """
        self.update_input_history(sample)
        output = self.step_without_history_update()
        self.logger.debug(f"Step: {sample} = {output}")
        return output

    def step_without_history_update(self):
        """Run one step of the interpolation to return the next sample value.

        Returns:
            The next interpolated sample value (float).
        """
        tap_index = int(self.branch)
        accumulation = 0.0
        history_index = 0
        while history_index < self._taps_per_branch:
            accumulation += (self.taps[tap_index] *
                             self._in_data_history[history_index])
            self.logger.debug("acc:{}, tap[{}]:{}, sample[{}]:{}".format(
                accumulation, tap_index, self.taps[tap_index], history_index, self._in_data_history[history_index]))
            tap_index += self._interpolation_factor
            history_index += 1
        self.logger.debug(
            f"Step (no new sample): {accumulation} (branch: {self.branch})")
        return accumulation

    def interpolate(self, sample):
        """Interpolate the sample provided.

        The sample is added to the history and produce the interpolated samples.

        Args:
            sample (various): The incoming sample value.

        Returns:
            Interpolated samples values (list of various).
        """
        old_branch = self.branch
        ret = [0] * self._interpolation_factor
        self._data_sent = True
        self.update_input_history(sample)
        for n in range(self._interpolation_factor):
            self.branch = n
            ret[n] = self.step_without_history_update()
        self.branch = old_branch
        self.logger.debug(f"Interpolate: {sample} = {ret}")
        return ret


class PolyphaseInterpolator_Int(PolyphaseInterpolator):
    """Integer version of the polyphase interpolator.

    This makes use of fixed point integers (i.e. integer arithmetic and shifts)
    and expects inputs/outputs to be integers.
    """

    def __init__(self,
                 interpolation_factor,
                 taps=[],
                 bit_depth=16,
                 rounding_type="truncate",
                 overflow_type="saturate",
                 max_interpolation=8,
                 max_taps_per_branch=3,
                 logger_name="Interpolator",
                 logger_level="WARNING"):
        """Initialises an integer math Polyphase Interpolator class.

        Args:
            interpolation_factor (int): The current upsampling factor.
            taps (list of int): The coefficient values.
            bit_depth (int): The number of bits in input/output integers.
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate".
            overflow_type (str): overflow type to use when rounding "wrap" or
                                 "saturate".
            max_interpolation (int): The maximum upsampling factor.
            max_taps_per_branch (int): The maximum number of taps per branch.
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        super().__init__(interpolation_factor=interpolation_factor,
                         taps=taps,
                         rounding_type=rounding_type,
                         max_interpolation=max_interpolation,
                         max_taps_per_branch=max_taps_per_branch,
                         logger_name=logger_name,
                         logger_level=logger_level)

        self._input_depth = bit_depth
        self._input_max = (2**(bit_depth-1))-1
        self._input_min = -(2**(bit_depth-1))

        self._coefficient_depth = int(
            2*bit_depth + np.ceil(np.log2(self._taps_per_branch)))
        self._coefficient_max = (2**(self._coefficient_depth-1))-1
        self._coefficient_min = -(2**(self._coefficient_depth-1))

        scale_and_round = ScaleAndRound(rounding_type=rounding_type,
                                        scale_factor=bit_depth-1,
                                        output_depth=bit_depth,
                                        overflow_type=overflow_type)
        self._scale_and_round = scale_and_round.scale_and_round

    def update_input_history(self, data):
        """Update the input history with the passed sample.

        Args:
            data: The new sample to add to the history.
        """
        val = int(np.trunc(data))  # Truncate just to ensure it is an int
        if val > self._input_max or val < self._input_min:
            raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                             .format(val, self._input_min, self._input_max))

        # Insert new sample, and trim to length
        length = len(self._in_data_history)
        self._in_data_history.insert(0, val)
        del self._in_data_history[length:]

    def step_without_history_update(self):
        """Run one step of the interpolation to return the next sample value.

        Returns:
            The next interpolated sample value (int).
        """
        tap_index = int(self.branch)
        accumulation = 0
        history_index = 0
        while history_index < self.taps_per_branch:
            accumulation += (int(self.taps[tap_index])
                             * int(self._in_data_history[history_index]))
            self.logger.debug("acc:{}, tap[{}]:{}, sample[{}]:{}".format(
                accumulation, tap_index, self.taps[tap_index], history_index, self._in_data_history[history_index]))
            tap_index += self._interpolation_factor
            history_index += 1

        out = self._scale_and_round(accumulation)
        self.logger.debug(
            f"sample (before rounding): {accumulation}, after rounding {out}")
        self.logger.debug(f"output sample: {out}")
        self.logger.debug(
            f"Step (no new sample): {accumulation} (branch: {self.branch})")
        return out


class PolyphaseInterpolator_ComplexInt(PolyphaseInterpolator_Int):
    """Complex integer version of the polyphase interpolator.

    This makes use of fixed point integers (i.e. integer arithmetic and shifts)
    and expects inputs/outputs to be complex integers.
    """

    def __init__(self,
                 interpolation_factor,
                 taps=[],
                 bit_depth=16,
                 rounding_type="truncate",
                 overflow_type="saturate",
                 max_interpolation=8,
                 max_taps_per_branch=3,
                 logger_name="Interpolator",
                 logger_level="WARNING"):
        """Initialises an complex integer math version of the Polyphase Interpolator class.

        Args:
            interpolation_factor (int): The current upsampling factor.
            taps (list of int): The coefficient values.
            bit_depth (int): The number of bits in input/output integers.
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate".
            overflow_type (str): overflow type to use when rounding "wrap" or
                                 "saturate".
            max_interpolation (int): The maximum upsampling factor.
            max_taps_per_branch (int): The maximum number of taps per branch.
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        super().__init__(interpolation_factor=interpolation_factor,
                         taps=taps,
                         bit_depth=bit_depth,
                         rounding_type=rounding_type,
                         overflow_type=overflow_type,
                         max_interpolation=max_interpolation,
                         max_taps_per_branch=max_taps_per_branch,
                         logger_name=logger_name,
                         logger_level=logger_level)

        self._input_max = (2**(bit_depth-1))-1
        self._input_min = -(2**(bit_depth-1))

    def update_input_history(self, data):
        """Update the input history with the passed sample.

        Args:
            data: The new sample to add to the history.
        """
        # Truncate just to ensure it is an int
        val = complex(int(np.trunc(data.real)),
                      int(np.trunc(data.imag)))
        if (val.real > self._input_max or val.real < self._input_min
                or val.imag > self._input_max or val.imag < self._input_min):
            raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                             .format(val, self._input_min, self._input_max))

        # Insert new sample, and trim to length
        length = len(self._in_data_history)
        self._in_data_history.insert(0, data)
        del self._in_data_history[length:]

    def step_without_history_update(self):
        """Run one step of the interpolation to return the next sample value.

        Returns:
            The next interpolated sample value (complex).
        """
        tap_index = int(self.branch)
        accumulation = 0
        history_index = 0
        while history_index < self._taps_per_branch:
            accumulation += complex(int(self.taps[tap_index]) * int(self._in_data_history[history_index].real),
                                    int(self.taps[tap_index]) * int(self._in_data_history[history_index].imag))
            self.logger.debug("acc:{}, tap[{}]:{}, sample[{}]:{}".format(
                accumulation, tap_index, self.taps[tap_index], history_index, self._in_data_history[history_index]))
            tap_index += self._interpolation_factor
            history_index += 1

        out = complex(self._scale_and_round(accumulation.real),
                      self._scale_and_round(accumulation.imag))
        self.logger.debug(
            f"sample (before rounding): {accumulation}, after rounding {out}")
        self.logger.debug(f"output sample: {out}")
        self.logger.debug(
            f"Step (no new sample): {accumulation} (branch: {self.branch})")
        return out
