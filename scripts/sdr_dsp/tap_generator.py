#!/usr/bin/env python3

# Generate taps for use in the unit tests.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Taps generation and scaling utility functions."""

import sys
import scipy.signal as sig
import numpy as np


def generate_taps(number_of_taps, taps_shape="kaiser",
                  number_of_branches=1, cut_off=0.5, seed=None):
    """Generate floating point tap values.

    Args:
        number_of_taps (int): Number of taps within the filter the filter.
        taps_shape (str): 'Shape' of the taps to be calculated.
        number_of_branches (int): Number of polyphase filters in use.
        cut_off (float): Cut-off frequency (only used if number_of_branches=1)
                         or for half_band taps.
        seed (int): Seed for random tap value generation.

    Returns:
        An array of tap values (float64).
    """
    taps_per_branch = number_of_taps // number_of_branches

    if taps_shape == "kaiser":
        if number_of_branches <= 1:
            taps = (number_of_branches * sig.firwin(
                taps_per_branch,
                cut_off,
                window=("kaiser", 5.0)))
        else:
            taps = (number_of_branches * sig.firwin(
                number_of_taps,
                1/(number_of_branches),
                window=("kaiser", 5.0)))
    elif taps_shape == "all_ones":
        taps = np.ones(number_of_taps)
    elif taps_shape == "all_zeros":
        taps = np.zeros(number_of_taps)
    elif taps_shape == "half_band":
        taps = np.empty(number_of_taps)
        taps = sig.firwin(number_of_taps, cut_off)
        # The "half-band" taps provided will probably not have the odd indexed
        # taps at exactly zero, to ensure the HW and SW models are aligned all
        # values below a threshold are set to 0
        taps[abs(taps) < 1e-8] = 0
    elif taps_shape == "half_band_corrupted":
        taps = np.empty(number_of_taps)
        taps = sig.firwin(number_of_taps, cut_off)
        # This is for testing how non-symmetrical coefficients are processed
        # The first set of coefficients are set to 0
        taps[:len(taps)//2] = 0
        # The "half-band" taps provided will probably not have the odd indexed
        # taps at exactly zero, to ensure the HW and SW models are aligned all
        # values below a threshold are set to 0
        taps[abs(taps) < 1e-8] = 0
    elif taps_shape == "random":
        np.random.seed(seed)
        # Generate taps in the open range (-1,+1)
        # N.B. Random returns [min,max) so will need to ensure bounds
        max_val = +1
        min_val = -1
        # Random returns values up to, but not including, max_val, but
        # does return down to *and* including min_val.
        # Need to not have exactly -UINT_MAX
        min_val += sys.float_info.epsilon
        taps = np.random.uniform(min_val, max_val, size=number_of_taps)
    elif taps_shape == "ramp":
        # Generate taps in and ramp in the open range (-1,+1)
        max_val = +1.0 - sys.float_info.epsilon
        min_val = -1.0 + sys.float_info.epsilon
        taps = np.linspace(min_val, max_val, num=number_of_taps)
    else:
        raise ValueError(
            f"generate_taps(): Unrecognised taps_shape: {taps_shape}")
    return taps


def scale_taps_to_integer_format(taps, taps_fractional_bits, normalize=True):
    """Scale floating point taps into integer format.

    Args:
        taps (list of float): Array of tap values.
        taps_fractional_bits (int): Number of binary bits used to describe tap value.
        normalise (bool): Re-scaling of tap values to constrain them to the range -1 to +1.

    Returns:
        Integer tap value, constrained to the binary width described by taps_fractional_bits.
    """
    if normalize:
        taps = [x/max(abs(taps)) for x in taps]

    integer_taps = np.array([int(x*(pow(2, taps_fractional_bits)-1))
                             for x in taps])

    min_allowed = -(2**taps_fractional_bits)
    max_allowed = +(2**taps_fractional_bits)-1
    if (np.any(integer_taps > max_allowed) or np.any(integer_taps < min_allowed)):
        raise ValueError(
            "Filter taps goes beyond the range allowed "
            f"({min_allowed}-{max_allowed})\nTaps: {integer_taps}")
    return integer_taps
