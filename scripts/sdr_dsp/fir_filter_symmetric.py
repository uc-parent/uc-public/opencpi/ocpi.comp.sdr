#!/usr/bin/env python3

# Python implementation of symmetric fir filter for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of a Symmetric FIR filter class."""

import logging
import decimal
import copy
import numpy as np

from sdr_dsp.scale_and_round import ScaleAndRound


class FirFilterSymmetric:
    """A Symmetric FIR filter class."""

    def __init__(self,
                 number_of_taps,
                 taps=[],
                 rounding_type="half_even",
                 half_band_interpolator=False,
                 half_band_decimator=False,
                 logger_name="FirFilterSymmetric",
                 logger_level: str = "INFO"):
        """Initialise all members.

        Args:
            number_of_taps (int): Number of coefficients.
            taps (list of float): The coefficient values
            rounding_type (str): How to mathematically round samples
            half_band_interpolator (bool): Set only if interpolating
            half_band_decimator (bool): Set only if decimating
            logger_name (str): Name of the logger instance.
            logger_level (str): Minimum level of log messages to be generated.
        """
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logger_level)

        self.number_of_taps = number_of_taps
        self.rounding_type = rounding_type
        self.half_band_interpolator = half_band_interpolator
        self.half_band_decimator = half_band_decimator

        if self.half_band_interpolator and self.half_band_decimator:
            raise ValueError(
                "Can only specify one of half_band_interpolator or half_band_decimator")

        if len(taps) == 0:
            self.logger.error("Taps array must not be empty")
            raise ValueError("taps array must not be empty")
        else:
            self._taps = [0] * number_of_taps
            # Use the setter to force symmetry
            self.taps = taps

        self.reset()

    @property
    def number_of_taps(self):
        """Get number of taps property."""
        return self._number_of_taps

    @number_of_taps.setter
    def number_of_taps(self, val):
        """Set number of taps property."""
        if val > 255:
            self.logger.error(
                "ERROR: Requested number of taps {} greater than max allowed ({})"
                .format(val, 255))
            return
        self._number_of_taps = val

    @property
    def taps(self):
        """Get taps property."""
        return self._taps

    @taps.setter
    def taps(self, val):
        """Set taps property."""
        if len(val) > self._number_of_taps:
            self.logger.error(
                "@taps.setter:ERROR: Requested taps {} greater than number_of_taps ({})".format(
                    val, self._number_of_taps))
            return

        self._taps = val[:self._number_of_taps]

        # Force taps to be symmetric
        n = len(self._taps)
        index = n

        for i in range(index//2):
            self._taps[i] = self._taps[index-1-i]

        # Correct for Half-band zeros
        if self.half_band_interpolator or self.half_band_decimator:
            self.logger.info("Central Tap {}, as tap length {}".format(
                (self._number_of_taps)//2, self._number_of_taps))
            for i in range(self._number_of_taps):
                if ((i != (self._number_of_taps)//2) and
                    ((i % 2 == 0 and self._number_of_taps % 4 == 1) or
                     (i % 2 == 1 and self._number_of_taps % 4 == 3))):
                    self._taps[i] = 0
        self.logger.debug(f"Taps set to: {self._taps}")

    def reset(self):
        """Reset values to defaults on discontinuity."""
        self.data_received = False
        self._in_data_history = [0] * self._number_of_taps
        self._buffer = []  # Only used for decimating.
        self.logger.info("Reset state")

    def buffer_count(self):
        """Size of buffer contents (for decimating).

        Returns:
            Current length of internal buffer
        """
        return len(self._buffer)

    def _update_input_history(self, data):
        """Insert new sample, and trim to length.

        Args:
            data : new sample.

        Returns:
            None
        """
        length = len(self._in_data_history)
        self._in_data_history.insert(0, data)
        del self._in_data_history[length:]

    def process_samples(self, input_):
        """Steps though processing each sample.

        Preprocessing if interpolating and removing output if decimating

        Args:
            input_  : Input Data

        Returns:
            Output messages
        """
        if self.half_band_decimator:
            # Hold buffer plus new input.
            input_samples = self._buffer + list(input_)
            self._buffer = []
            if len(input_samples) % 2 == 1:
                # Must have even number of samples for decimation, so
                # move odd end sample into hold buffer.
                self._buffer = input_samples[-1:]
                del input_samples[-1]
        else:
            input_samples = input_

        self.data_received = True
        self.logger.debug(f"Processing samples: length={len(input_samples)}")

        if self.half_band_interpolator:
            # Insert zeros between samples
            input_samples = [0] * 2*len(input_)
            input_samples[::2] = input_

        data = [self._step(s) for s in input_samples]

        if self.half_band_decimator:
            # Discard every other sample, starting with first
            sample_keep = False

            fir_data = np.array(data)
            data = []
            for d in fir_data:
                if sample_keep:
                    data.append(d)
                sample_keep = not sample_keep

        return data

    def _step(self, sample):
        """Performs the FIR filter algorithm.

        Args:
            sample : Opcode data for the input port.

        Returns:
            FIR output
        """
        self._update_input_history(sample)

        y = 0.0
        for i in range(self._number_of_taps):
            y += self.taps[i] * self._in_data_history[i]
        return y


class FirFilterSymmetric_Int(FirFilterSymmetric):
    """Integer version of the Symmetric Fir Filter.

    This makes use of fixed point integers (ie. integer arithmetic and shifts)
    and expects inputs/outputs to be integers
    """

    def __init__(self,
                 number_of_taps,
                 taps=[],
                 rounding_type="half_even",
                 overflow_type="wrap",
                 half_band_interpolator=False,
                 half_band_decimator=False,
                 logger_name="FirFilterSymmetric",
                 logger_level: str = "INFO",
                 input_depth=16,
                 internal_depth=36,
                 output_scale=15,
                 output_depth=16):
        """Initialise all members.

        Args:
            number_of_taps (int)          : Number of coefficients.
            taps (list of int)            : The coefficient values
            rounding_type (str)           : How to mathematically round samples
            overflow_type (str)           : How to handle overflow.
            half_band_interpolator (bool) : Set only if interpolating
            half_band_decimator (bool)    : Set only if decimating
            logger_name (str)             : Name of the logger instance.
            logger_level (str)            : Minimum level of log messages to be generated.
            input_depth (int)             : Depth of input in bits
            internal_depth (int)          : Internal depth in bits
            output_scale (int)            : Output scale in bits
            output_depth (int)            : Output depth in bits
        """
        super().__init__(
            number_of_taps=number_of_taps,
            taps=taps,
            rounding_type=rounding_type,
            half_band_interpolator=half_band_interpolator,
            half_band_decimator=half_band_decimator,
            logger_name=logger_name,
            logger_level=logger_level)

        self._input_max = (2**(input_depth-1))-1
        self._input_min = -(2**(input_depth-1))

        self._internal_max = +(2**(internal_depth-1))-1
        self._internal_min = -(2**(internal_depth-1))

        # Set scale_and_round function
        scale_and_round = ScaleAndRound(
            rounding_type=rounding_type,
            overflow_type=overflow_type,
            scale_factor=output_scale,
            output_depth=output_depth,
            logger_level=logger_level)
        self._scale_and_round = scale_and_round.scale_and_round
        self.logger.debug("Set rounding type/overflow/scale/depth: " +
                          f"{rounding_type}/{overflow_type}/{output_scale}/{output_depth}")

    def _step(self, sample):
        """Performs the FIR filter algorithm.

        Args:
            sample : Opcode data for the input port.

        Returns:
            FIR output
        """
        val = int(np.trunc(sample))
        if val > self._input_max or val < self._input_min:
            raise ValueError("Error: Value {} is outside of integer input range [{}-{}]"
                             .format(val, self._input_min, self._input_max))
        self._update_input_history(val)

        y = 0
        for i in range(self._number_of_taps):
            y += int(self.taps[i]) * int(self._in_data_history[i])
            if y > self._internal_max or y < self._internal_min:
                raise ValueError("Error: Value[{}] {} is outside of internal integer range [{}-{}]"
                                 .format(i, y, self._internal_min, self._internal_max))

        # Right-shift filter output by scaling factor rounded.
        out = self._scale_and_round(y)

        return out


class FirFilterSymmetric_ComplexInt(FirFilterSymmetric_Int):
    """Integer version of the Symmetric Fir Filter.

    This makes use of fixed point integers (ie. integer arithmetic and shifts)
    and expects inputs/outputs to be integers
    """

    def __init__(self,
                 number_of_taps,
                 taps=[],
                 rounding_type="half_even",
                 overflow_type="wrap",
                 half_band_interpolator=False,
                 half_band_decimator=False,
                 logger_name="FirFilterSymmetric",
                 logger_level: str = "INFO",
                 input_depth=16,
                 internal_depth=36,
                 output_scale=15,
                 output_depth=16):
        """Initialise all members.

        Args:
            number_of_taps (int)          : Number of coefficients.
            taps (list of int)            : The coefficient values
            rounding_type (str)           : How to mathematically round samples
            overflow_type (str)           : How to handle overflow.
            half_band_interpolator (bool) : Set only if interpolating.
            half_band_decimator (bool)    : Set only if decimating.
            logger_name (str)             : Name of the logger instance.
            logger_level(str)             : Minimum level of log messages to be generated.
            input_depth (int)             : Depth of input in bits.
            internal_depth (int)          : Internal depth in bits.
            output_scale (int)            : Output scale in bits.
            output_depth (int)            : Output depth in bits.
        """
        super().__init__(
            number_of_taps=number_of_taps,
            taps=taps,
            rounding_type=rounding_type,
            overflow_type=overflow_type,
            half_band_interpolator=half_band_interpolator,
            half_band_decimator=half_band_decimator,
            logger_name=logger_name,
            logger_level=logger_level,
            input_depth=input_depth,
            internal_depth=internal_depth,
            output_scale=output_scale,
            output_depth=output_depth)

    def _step(self, sample):
        """Performs the FIR filter algorithm on complex numbers.

        Args:
            sample : Opcode data for the input port.

        Returns:
            FIR output
        """
        val = complex(int(np.trunc(sample.real)), int(np.trunc(sample.imag)))
        if (val.real > self._input_max or val.real < self._input_min or
                val.imag > self._input_max or val.imag < self._input_min):
            raise ValueError("Error: Value {} is outside of input integer range [{}-{}]"
                             .format(val, self._input_min, self._input_max))
        self._update_input_history(val)

        y = complex(0, 0)
        for i in range(self._number_of_taps):
            y += complex(int(self.taps[i]) * int(self._in_data_history[i].real),
                         int(self.taps[i]) * int(self._in_data_history[i].imag))

        out = complex(self._scale_and_round(y.real),
                      self._scale_and_round(y.imag))

        return out
