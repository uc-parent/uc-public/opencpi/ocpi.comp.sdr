#!/usr/bin/env python3

# Python implementation of CIC filter, with decimator.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""CIC filter decimator stage."""


import numpy as np
import decimal
from sdr_dsp.cic_comb import Comb
from sdr_dsp.cic_integrator import Integrator

# Decimate stage


class Decimate:
    """CIC filter decimator class."""

    def __init__(self, down_sample_factor):
        """Decimate class.

        The Decimate class performs a downsample (sample selection), without any
        intrinsic filtering.

        Args:
            down_sample_factor (integer): ratio to downsample by.

        Returns:
            Instance of the Decimate Class.
        """
        self._down_sample_factor = down_sample_factor
        self.reset()

    def reset(self):
        """Clears the downsample counter."""
        self._down_sample_count = self._down_sample_factor - 1

    def call(self, value):
        """Controls whether the input value is output, or discarded.

        Args:
            value (integer): the value to next test against.

        Returns:
            None if the sample is discarded, otherwise the input value.
        """
        if self._down_sample_count == 0:
            self._down_sample_count = self._down_sample_factor - 1
            return value
        else:
            self._down_sample_count -= 1
            return None


class CicDecimatorSingleChannel():
    """Cascaded Integrator-Comb Decimator."""

    def __init__(self, cic_order, delay_factor, down_sample_factor,
                 output_scale_factor, cic_register_size, output_type=np.int16):
        """Cascaded Integrator-Comb Decimator class.

        Args:
            cic_order (integer): Number of Comb and Integrator stages present.
            delay_factor (integer): Comb delay factor.
            down_sample_factor (integer): output downsample ratio.
            output_scale_factor (integer): output scaling factor.
            cic_register_size (integer): bit depth of the CIC integers used internally.
            output_type (type): output integral type.

        Returns:
            Instance of the CicDecimatorSingleChannel Class.
        """
        self._cic_order = cic_order
        self._delay_factor = delay_factor
        self._down_sample_factor = down_sample_factor
        self._output_scale_factor = output_scale_factor
        self._cic_register_size = cic_register_size
        self._output_type = output_type

        self._max_internal_value = 2**(cic_register_size - 1) - 1
        self._min_internal_value = -1 * (2**(cic_register_size - 1))

        self._integrators = [Integrator() for n in range(cic_order)]
        self._combs = [Comb(delay_factor) for n in range(cic_order)]
        self._decimate = Decimate(down_sample_factor)

    def reset(self):
        """Clears all internal buffers."""
        for n in range(0, self._cic_order):
            self._combs[n].reset()
            self._integrators[n].reset()
        self._decimate.reset()

    def sample(self, values):
        """Process an incoming message with the sample opcode.

        Args:
            value (list): sample message contents.

        Returns:
            Array of output samples.
        """
        output_values = []
        for value in values:
            # Apply each integrator stage
            for n in range(self._cic_order):
                value = self._integrators[n].call(value)
                # Match implementation bit width. The CIC implementation
                # relies on wrapping the range of register size.
                while value < self._min_internal_value:
                    value = value + (2**self._cic_register_size)
                while value > self._max_internal_value:
                    value = value - (2**self._cic_register_size)

            # Decimate the data
            value = self._decimate.call(value)

            # Apply each comb stage - value can be None after decimator
            if value is not None:
                for n in range(self._cic_order):
                    value = self._combs[n].call(value)
                    # Match implementation bit width
                    while value < self._min_internal_value:
                        value = value + (2**self._cic_register_size)
                    while value > self._max_internal_value:
                        value = value - (2**self._cic_register_size)

                # Do the output scaling
                # Don't half-up round
                if self._output_scale_factor == 0:
                    value = self._output_type(value)
                    output_values.append(value)
                else:
                    # Round half-up
                    value = np.right_shift(
                        int(value), self._output_scale_factor - 1) + 1
                    value = self._output_type(np.right_shift(value, 1))
                    output_values.append(value)

        return output_values
