#!/usr/bin/env python3

# Python implementation of Polyphase Decimator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of Polyphase Decimator block for verification."""

import numpy as np
import logging


class PolyphaseDecimator():
    """A generalised floating point version of a Polyphase Decimator.

    This makes use of floating point numbers and expects inputs/outputs to
    be floating point too.
    """

    def __init__(self, decimation_factor: int,
                 taps=[],
                 max_taps_per_branch=255,
                 max_decimation=255,
                 rounding_type="half_even",
                 logger_name="Decimator",
                 logger_level="WARNING"):
        """Initialises a Polyphase Decimator class.

        Args:
            decimation_factor (int): The current down-sampling factor.
            taps (list of float): The coefficient values.
            max_taps_per_branch (int): The maximum number of taps per branch.
            max_decimation (int): The maximum down-sampling factor.
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate".
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        self._decimation_factor = int(decimation_factor)
        self._max_decimation = int(max_decimation)
        self._max_taps_per_branch = int(max_taps_per_branch)
        self._rounding_type = rounding_type

        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logger_level)

        self.logger.debug(
            f"Polyphase Decimation factor:{self._decimation_factor}")
        self.logger.debug(f"rounding type: {self._rounding_type}")

        if len(taps) == 0:
            self.logger.error("Taps array must not be empty")
            raise ValueError("taps array must not be empty")
        else:
            self._taps_per_branch = int(np.ceil(
                float(len(taps)) / float(decimation_factor)))
            self._taps = np.zeros([decimation_factor,
                                   int(self._taps_per_branch)])
            for n, t in enumerate(taps):
                self._taps[n % decimation_factor][n//decimation_factor] = t
            self.logger.debug("taps ({} per branch) set to: {}".format(
                self._taps_per_branch, taps))
            self.logger.info("Passed {} taps, with decimation_factor={} => {} taps per branch".format(
                len(taps), decimation_factor, self._taps_per_branch))

        if len(taps) % decimation_factor != 0:
            self.logger.info("taps have been padded to with zeros")

        self.reset()

    def __repr__(self):
        """Official string representation of object."""
        return ("{}(N:{}, TapsPerBranch:{})"
                .format(type(self), self._decimation_factor,
                        self._taps_per_branch))

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._idx = 0
        self._input_branch_idx = 0
        self._output_sample = 0
        self._history = [[0 for _ in range(self._taps_per_branch)]
                         for _ in range(self._decimation_factor)]

    @property
    def taps(self):
        """Read the tap values.

        Returns:
            List of tap values.
        """
        return self._taps

    @taps.setter
    def taps(self, val):
        if len(val) > (self._taps_per_branch * self._max_decimation):
            print("ERROR: Requested taps {} greater than max_taps_per_branch "
                  "and max_interpolation_factor {}".format(val, self._taps_per_branch))
            return
        self._taps = val

    def update_input_history(self, sample) -> bool:
        """Update the input history with the passed sample.

        Args:
            sample: The new sample to add to the history

        Returns:
            True if a sample should now be output, False if not.
        """
        self._history[self._input_branch_idx][0:self._taps_per_branch-1] = (
            self._history[self._input_branch_idx][1:self._taps_per_branch])
        self._history[self._input_branch_idx][self._taps_per_branch-1] = sample

        self._output_branch_idx = self._input_branch_idx

        # If this value aligns to the decimation factor, then we want to output
        # this value. This is really just a modulo function.
        if self._input_branch_idx == (self._decimation_factor-1):
            self._input_branch_idx = 0
            result = True
        else:
            self._input_branch_idx += 1
            result = False

        return result

    def _round_sample(self, value, binary_point):
        """Rounds the sample value to the nearest integer.

        Args:
            value (int): Sample value to round
            binary_point (int): The number of binary places to round

        Returns:
            int: The results rounded value
        """
        if self._rounding_type == "half_up":
            value = int(value)
            value >>= binary_point-1
            value += 1
            value >>= 1
            return int(value)
        elif self._rounding_type == "half_even":
            return int(np.around(value / 2.0**binary_point))
        else:  # Default to truncate:
            value = value / 2.0**binary_point
            if value < 0 and (np.trunc(value) != value):
                return int(np.trunc(value)) - 1
            else:
                return int(np.trunc(value))

    def decimate_samples(self, input_samples):
        """Process the passed sample through the decimation process.

        Args:
            input_samples (list): The samples data to process.

        Returns:
            A (potentially empty) list of output samples.
        """
        output_samples = []
        for s in input_samples:
            append_value = False
            if self.update_input_history(s):
                # Run the decimation
                append_value = True

            self._do_multiply_accumulate(self._taps[self._output_branch_idx],
                                         self._history[self._output_branch_idx])

            self.logger.debug("accumulator={}\nhistory={}\ntaps={}".format(
                self._output_sample,
                self._history[self._output_branch_idx],
                self._taps[self._output_branch_idx]))

            if append_value:
                output_samples.append(
                    self._round_and_saturate(self._output_sample))
                self.logger.debug("outputting={}".format(output_samples[-1]))
                self._output_sample = 0

        if len(output_samples) > 0:
            if isinstance(output_samples[0], complex):
                self.logger.debug("samples to output, len: {}, min:{}|{}, max:{}|{}"
                                  .format(len(output_samples),
                                          min(v.real for v in output_samples),
                                          min(v.imag for v in output_samples),
                                          max(v.imag for v in output_samples),
                                          max(v.imag for v in output_samples)))
            else:
                self.logger.debug("samples to output, len: {}, min:{}, max:{}"
                                  .format(len(output_samples),
                                          min(output_samples),
                                          max(output_samples)))

        return output_samples

    def _do_multiply_accumulate(self, taps, history):
        """Perform the multiply and accumulate of the taps and history.

        Result is accumulated into the self._output_sample variable.

        Note: Defined as a function to allow int and/or complex variants to
        overload and do rounding as required.

        Args:
            taps (list): tap values to use
            history (list): sample values to use
        """
        for tap, sample in zip(taps, history):
            self._output_sample += tap*sample
            self.logger.debug(
                f"t:{tap}, s:{sample} = out:{self._output_sample}")

    def _round_and_saturate(self, value):
        """Round value ready for output.

        Note: Floating point does not have a saturation value

        Args:
            value (numeric): value to be rounded

        Returns:
            numeric: Value rounded and saturated to be within bounds
        """
        return value


class PolyphaseDecimator_Int(PolyphaseDecimator):
    """Integer version of the polyphase decimator.

    This makes use of fixed point integers (i.e. integer arithmetic
    and shifts) and expects input/outputs to be based on integers.
    """

    def __init__(self, decimation_factor: int,
                 taps=[],
                 bit_depth=16,
                 max_taps_per_branch=255,
                 max_decimation=255,
                 rounding_type="half_even",
                 logger_name="Decimator",
                 logger_level="WARNING"):
        """Initialises an integer math Polyphase Decimator class.

        Args:
            decimation_factor (int): The current down-sampling factor
            taps (list of int): The coefficient values
            bit_depth (int): The number of bits in input/output integers.
            max_taps_per_branch (int): The maximum number of taps per branch
            max_decimation (int): The maximum down-sampling factor
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate"
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        super().__init__(decimation_factor=decimation_factor,
                         taps=taps,
                         max_taps_per_branch=max_taps_per_branch,
                         max_decimation=max_decimation,
                         rounding_type=rounding_type,
                         logger_name=logger_name,
                         logger_level=logger_level)
        self._bit_depth = bit_depth
        self._min_val = -(2**(self._bit_depth-1))
        self._max_val = +(2**(self._bit_depth-1))-1

    def _round_and_saturate(self, value):
        """Round value ready for output.

        Args:
            value (int): value to be rounded

        Returns:
            int: Value rounded and saturated to be within bounds
        """
        value = int(self._round_sample(value, self._bit_depth-1))
        return max(min(value, self._max_val), self._min_val)


class PolyphaseDecimator_ComplexInt(PolyphaseDecimator):
    """Complex integer version of the Polyphase Decimator.

    This makes use of fixed point integers (i.e. integer arithmetic
    and shifts) and expects inputs/outputs to be complex integers.
    """

    def __init__(self, decimation_factor: int,
                 taps=[],
                 bit_depth=16,
                 max_taps_per_branch=255,
                 max_decimation=255,
                 rounding_type="half_even",
                 logger_name="Decimator",
                 logger_level="WARNING"):
        """Initialises an complex integer math Polyphase Decimator class.

        Args:
            decimation_factor (int): The current down-sampling factor
            taps (list of int): The coefficient values
            bit_depth (int): The number of bits in input/output integers.
            max_taps_per_branch (int): The maximum number of taps per branch
            max_decimation (int): The maximum down-sampling factor
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate"
            logger_name (str): Name to use for python logger.
            logger_level (str/int): Level to use for python logger.
        """
        super().__init__(decimation_factor=decimation_factor,
                         taps=taps,
                         max_taps_per_branch=max_taps_per_branch,
                         max_decimation=max_decimation,
                         rounding_type=rounding_type,
                         logger_name=logger_name,
                         logger_level=logger_level)
        self._bit_depth = bit_depth
        self._min_val = -(2**(self._bit_depth-1))
        self._max_val = +(2**(self._bit_depth-1))-1

    def _do_multiply_accumulate(self, taps, history):
        """Perform the multiply and accumulate of the taps and history.

        Result is accumulated into the self._output_sample variable.

        Note: Defined as a function to allow int and/or complex variants to
        overload and do rounding as required.

        Args:
            taps (list): tap values to use
            history (list): sample val;ues to use
        """
        for tap, sample in zip(taps, history):
            self._output_sample += complex(int(tap*sample.real),
                                           int(tap*sample.imag))

    def _round_and_saturate(self, value):
        """Round value ready for output.

        Args:
            value (complex): value to be rounded

        Returns:
            complex: Value rounded and saturated to be within bounds
        """
        real_val = int(self._round_sample(value.real, self._bit_depth-1))
        imag_val = int(self._round_sample(value.imag, self._bit_depth-1))

        # Saturate to bounds
        return complex(
            max(min(real_val, self._max_val), self._min_val),
            max(min(imag_val, self._max_val), self._min_val))
