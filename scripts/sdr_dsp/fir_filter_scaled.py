#!/usr/bin/env python3

# Python implementation of scaled FIR filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of a FIR filter (scaled) class."""

import numpy
import scipy.signal

from sdr_dsp.scale_and_round import ScaleAndRound


class FirFilterScaled:
    """A FIR filter (scaled) class."""

    def __init__(self, taps, scale_factor, rounding_type, overflow_type, output_depth):
        """Initialises a FIR filter (scaled) class.

        Args:
            taps (list of float): Tap coefficient values.
            scale_factor (int): Number of bits to be removed from calculated output.
                                  If input is 16 bit, tap values are 16 bit and there are 64 taps, total output size is 16 + 16 + 8 = 40 bits.
                                  If scale_factor = 10, output data will be calculated output(25:10).
            rounding_type (string): Rounding method to use when scaling.
            overflow_type (string): How to overflow result.
            output_depth (int): Number of bits in output (8,16,32,64).
        """
        super().__init__()
        self._taps = taps
        # Set scale_and_round function
        scale_and_round = ScaleAndRound(
            rounding_type=rounding_type,
            overflow_type=overflow_type,
            scale_factor=scale_factor,
            output_depth=output_depth)
        self._scale_and_round = scale_and_round.scale_and_round

        self.reset()

    def reset(self):
        """Reset filter buffers."""
        self._filter_buffer = [0] * len(self._taps)
        self.data_received = False

    def run_filter(self, values):
        """FIR filter functionality.

        Multiplies input value by respective tap value, and accumulates to give output result.
        The result is then right-shifted by the scaling factor to give the final output value.

        Args:
            values (list): Sample opcode data for the input port.
            filter_buffer (list of int): Current filter values.

        Returns:
            Tuple consisting of 1) a list of int containing the output from
            the filter for all input data, and 2) a list of int containing
            the current filter values.
        """
        output = [0] * len(values)

        # Compute filter output for every input value.
        for index, value in enumerate(values):
            # Push next input value into filter
            self._filter_buffer = scipy.append(
                int(value), self._filter_buffer[:-1])

            # Multiply respective tap and input data values
            filter_result = int(0)
            for a in range(len(self._taps)):
                filter_result = (filter_result +
                                 int(self._taps[a]*self._filter_buffer[a]))

            # Right-shift filter output by scaling factor rounded.
            output[index] = self._scale_and_round(filter_result)

        self.data_received = True
        return (output)
