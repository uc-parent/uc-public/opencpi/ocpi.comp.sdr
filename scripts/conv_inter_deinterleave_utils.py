#!/usr/bin/env python3

# Python implementation of a symbol generator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""This script is used to perform convolutional interleaving and de-interleaving.

References:
https://en.wikipedia.org/wiki/Burst_error-correcting_code#Interleaved_codes
https://www.etsi.org/deliver/etsi_en/300400_300499/300421/01.01.02_60/en_300421v010102p.pdf
"""
import struct
import numpy as np
import sys


dt = np.dtype("<u2")

# Generates the input data used by interleaver


def generate_input_data(num_symbols, num_zeros_pad):
    """Generate input data for interleaver.

    Args:
        num_symbols (integral): Number of symbols to generate.
        num_zeros_pad (integral): Zero pad size.

    Returns:
        Generated data.
    """
    idata = np.empty(num_symbols, dtype=dt)
    for i in range(0, num_symbols):
        idata[i] = i
    idata = np.append(idata, np.zeros(num_zeros_pad, dtype=dt))

    return idata


def initialize_delay_lines_regs(N, D, mode):
    """Create delay lines registers.

    Args:
        N (integral): Size of delay lines to create.
        D: Initial data.
        mode (string): interleave or deinterleave.

    Returns:
        Initialised shift registers.
    """
    shift_regs = []
    if (mode == "interleave"):
        for i in range(1, N):
            shift_regs.append(np.zeros(D*i, dtype=dt).tolist())
    elif (mode == "deinterleave"):
        for i in range(1, N):
            shift_regs.append(np.zeros(D*(N-i), dtype=dt).tolist())
    else:
        print("Invalid mode for initialize_delay_lines_regs. Valid arguments for mode are 'interleave' and deinterleave")
        sys.exit(1)

    return shift_regs


def shift_data(reg):
    """Shift register.

    Args:
        reg (list): Registers to shift.

    Return:
        Shifted data.
    """
    reg_length = len(reg)
    temp = reg[0]
    for i in range(0, reg_length):
        prev = reg[i]
        reg[i] = temp
        temp = prev
    return reg


def convolutional_interleave(N, idata, shift_regs):
    """Perform convolutional interleave.

    Args:
        N (integral): Interleave factor.
        idata (list): Input data.
        shift_regs (list): Shift register delay line.

    Returns:
        Interleaved data.
    """
    idata_length = len(idata)
    interleaved_data = np.array([], dtype=np.uint32)
    j = 0
    k = 0
    for i in range(0, idata_length):
        reg = shift_regs[j]
        # On the non 0 delay, delay lines
        if (i % N != 0):
            interleaved_data = np.append(interleaved_data, reg[-1])
            shift_regs[j] = shift_data(reg)
            reg[0] = idata[i]
        else:
            interleaved_data = np.append(interleaved_data, idata[k])
            k += N
        # Set to the first non 0 delay, delay line
        if j == N-2:
            j = 0
        # Increment only if not on the 0 delay line
        elif (i % N != 0):
            j += 1
    return interleaved_data


def convolutional_deinterleave(N, interleaved_data, shift_regs):
    """Perform convolutional de-inteleave.

    Args:
        N (integral): Interleave factor.
        interleaved_data (list): Data to de interleave.
        shift_regs (list): Shift register delay line.

    Returns:
        Deinterleaved Data.
    """
    interleaved_data_length = len(interleaved_data)
    deinterleaved_data = np.array([], dtype=np.uint32)
    j = 0
    k = N-1
    # On the non 0 delay, delay lines
    for i in range(0, interleaved_data_length):
        reg = shift_regs[j]
        if (i % N != N-1):
            deinterleaved_data = np.append(deinterleaved_data, reg[-1])
            shift_regs[j] = shift_data(reg)
            reg[0] = interleaved_data[i]
        else:
            deinterleaved_data = np.append(
                deinterleaved_data, interleaved_data[k])
            k += N
        # Set to the first non 0 delay, delay line
        if j == N-2:
            j = 0
        # Increment only if not on the 0 delay line
        elif (i % N != N-1):
            j += 1
    return deinterleaved_data
